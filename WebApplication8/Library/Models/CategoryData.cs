﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Models
{
    public class CategoryData
    {
        public CategoryData()
        {
            Id = Guid.Empty;
            Name = new Dictionary<string, string>();
            Desc = new Dictionary<string, string>();
            Code = string.Empty;
            ImageId = new ImageId();
            //Image = new CategoryImage();
        }
        /*
        public async Task Save(DataContext dataContext)
        {
            var lang = await dataContext.Language.Where(t => t.Active).Select(t => t).ToListAsync();
            var isNew = Id == Guid.Empty;

            var category = isNew ? 
                new Category {
                    Id = Guid.NewGuid(),
                    Active = true,
                    Code = Code
                } : await dataContext.Category
                    .Include("Image")
                    .Include("Name.Language")
                    .Include("Description.Language")
                    .Where(t => t.Id == Id)
                    .FirstAsync();

            var name = new List<LocalizableString>();
            var desc = new List<LocalizableString>();

            category.Code = Code;

            foreach (var l in lang) {
                if (isNew) {
                    name.Add(new LocalizableString {
                        Id = Guid.NewGuid(),
                        Text = Name[l.ShortName],
                        Language = l,
                        Code = Name[l.ShortName],
                        IsSerializable = false,
                        IsSystem = false
                    });
                    desc.Add(new LocalizableString {
                        Id = Guid.NewGuid(),
                        Text = Desc[l.ShortName],
                        Language = l,
                        Code = Desc[l.ShortName],
                        IsSerializable = false,
                        IsSystem = false
                    });
                } else {
                    var cname = category.Name.Where(t => t.Language.ShortName == l.ShortName).First();
                    cname.Text = Name[l.ShortName];
                    cname.Code = Name[l.ShortName];
                    var cdesc = category.Description.Where(t => t.Language.ShortName == l.ShortName).First();
                    cdesc.Text = Desc[l.ShortName];
                    cdesc.Code = Desc[l.ShortName];
                }
            }

            if (isNew) {
                Id = category.Id;
                category.Name = name;
                category.Description = desc;
                dataContext.Category.Add(category);
            }

            if (Image?.Data != null) {
                if (Image.Category == null) {
                    Image.Category = category;
                }
                if (category.Image == null) {
                    category.Image = Image;
                } else {
                    category.Image.Data = Image.Data;
                }
            }

            await dataContext.SaveChangesAsync();
        }

        public static CategoryData Parse(string value, dynamic lang)
        {
            var result = new CategoryData();
            var o = JsonConvert.DeserializeObject<JObject>(value);
            result.Id = (o["id"] == null || string.IsNullOrEmpty(o["id"].Value<string>()) ? Guid.Empty : Guid.Parse(o["id"].Value<string>()));
            var image = o["image"] as JObject;
            var name = o["name"] as JObject;
            var desc = o["desc"] as JObject;
            var code = o["code"].Value<string>();
            result.Code = code;
            if (image != null) {
                result.ImageId = new ImageId {
                    Id = Guid.Parse(image["Id"].Value<string>())
                };
            }
            foreach (var l in lang) {
                result.Name[l.ShortName] = name[l.ShortName].ToString();
                result.Desc[l.ShortName] = HttpUtility.HtmlDecode(desc[l.ShortName].ToString());
            }
            return result;
        }
        */
        public ImageId ImageId { get; set; }
        // public CategoryImage Image { get; set; }

        public Guid Id { get; set; }
        public Dictionary<string, string> Name { get; set; }
        public Dictionary<string, string> Desc { get; set; }
        public string CName { get; set; }
        public string CDesc { get; set; }

        public string Code { get; set; }
    }
}
