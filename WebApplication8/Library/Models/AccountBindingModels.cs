﻿using System.ComponentModel.DataAnnotations;
using FluentValidation;
using FluentValidation.AspNetCore;

namespace AngularJSWebApi.Models
{
    // [Validation(typeof(ExternalLoginValidator))]
    public class AddExternalLoginBindingModel
    {
        public string ExternalAccessToken { get; set; }

        public override string ToString()
        {
            var v = ExternalAccessToken == null ? "null" : ExternalAccessToken;
            return $"ExternalAccessToken = {v}";
        }
    }

    public class ChangePasswordBindingModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Current password")]
        public string OldPassword { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "New password")]
        public string NewPassword { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public override string ToString()
        {
            var v1 = OldPassword == null ? "null" : OldPassword;
            var v2 = NewPassword == null ? "null" : NewPassword;
            var v3 = ConfirmPassword == null ? "null" : ConfirmPassword;
            return $"OldPassword = {v1}, NewPassword = {v2}, ConfirmPassword = {v3}";
        }
    }

    //[Validator(typeof(GuidValidator))]
    public class GuidBindingModel
    {
        public string Guid { get; set; }

        public override string ToString()
        {
            var v1 = Guid == null ? "null" : Guid;
            return $"Guid = {v1}";
        }
    }

    //[Validator(typeof(LoginValidator))]
    public class LoginBindingModel
    {
        public string Email { get; set; }

        public string TempGuid { get; set; }

        public string Password { get; set; }

        public string Response { get; set; }

        public override string ToString()
        {
            var v1 = Email == null ? "null" : Email;
            var v2 = TempGuid == null ? "null" : TempGuid;
            var v3 = Password == null ? "null" : Password;
            var v4 = Response == null ? "null" : Response;
            return $"Email = {v1}, TempGuid = {v2}, Password = {v3}, Response = {v4}";
        }
    }

    //[Validator(typeof(RegisterValidator))]
    public class RegisterBindingModel
    {
        public string Email { get; set; }

        public string TempGuid { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public override string ToString()
        {
            var v1 = Email == null ? "null" : Email;
            var v2 = TempGuid == null ? "null" : TempGuid;
            var v3 = Password == null ? "null" : Password;
            var v4 = ConfirmPassword == null ? "null" : ConfirmPassword;
            return $"Email = {v1}, TempGuid = {v2}, Password = {v3}, ConfirmPassword = {v4}";
        }
    }

    //[Validator(typeof(RegisterExternalValidator))]
    public class RegisterExternalBindingModel
    {
        public string Email { get; set; }

        public override string ToString()
        {
            var v1 = Email == null ? "null" : Email;
            return $"Email = {v1}";
        }
    }

    public class RemoveLoginBindingModel
    {
        [Required]
        [Display(Name = "Login provider")]
        public string LoginProvider { get; set; }

        [Required]
        [Display(Name = "Provider key")]
        public string ProviderKey { get; set; }
    }

    // [Validator(typeof(SetPasswordValidator))]
    public class SetPasswordBindingModel
    {
        public string RecoveryUid { get; set; }

        public string Password { get; set; }

        public string ConfirmPassword { get; set; }

        public override string ToString()
        {
            var v1 = RecoveryUid == null ? "null" : RecoveryUid;
            var v2 = Password == null ? "null" : Password;
            var v3 = ConfirmPassword == null ? "null" : ConfirmPassword;
            return $"RecoveryUid = {v1}, Password = {v2}, ConfirmPassword = {v3}";
        }
    }
}
