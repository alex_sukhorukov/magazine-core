﻿using System.Collections.Generic;

namespace AngularJSWebApi.Models
{
    // Models returned by AccountController actions.

    public class ExternalLoginViewModel
    {
        public string Name { get; set; }

        public string Url { get; set; }

        public string State { get; set; }

        public override string ToString()
        {
            var v1 = Name == null ? "null" : Name;
            var v2 = Url == null ? "null" : Url;
            var v3 = State == null ? "null" : State;
            return $"Name = {v1}, Url = {v2}, State = {v3}";
        }
    }

    public class ManageInfoViewModel
    {
        public string LocalLoginProvider { get; set; }

        public string Email { get; set; }

        public IEnumerable<UserLoginInfoViewModel> Logins { get; set; }

        public IEnumerable<ExternalLoginViewModel> ExternalLoginProviders { get; set; }

        public override string ToString()
        {
            var v1 = LocalLoginProvider == null ? "null" : LocalLoginProvider;
            var v2 = Email == null ? "null" : Email;
            var v3 = string.Empty;
            var v4 = string.Empty;
            if (Logins != null) {
                foreach (var l in Logins) {
                    if (string.IsNullOrEmpty(v3)) {
                        v3 += "[";
                    }
                    if (v3 != "[") {
                        v3 += ", ";
                    }
                    var v31 = l.LoginProvider == null ? "null" : l.LoginProvider;
                    var v32 = l.ProviderKey == null ? "null" : l.ProviderKey;
                    v3 += "{LoginProvider: " + v31 + ", ProviderKey: " + v32 + "}";
                }
                if (!string.IsNullOrEmpty(v3)) {
                    v3 += "]";
                }
            } else {
                v3 = "null";
            }
            if (ExternalLoginProviders != null) {
                foreach (var el in ExternalLoginProviders) {
                    if (string.IsNullOrEmpty(v4)) {
                        v4 += "[";
                    }
                    if (v4 != "[") {
                        v4 += ", ";
                    }
                    var v41 = el.Name == null ? "null" : el.Name;
                    var v42 = el.State == null ? "null" : el.State;
                    var v43 = el.Url == null ? "null" : el.Url;
                    v4 += "{Name: " + v41 + ", State: " + v42 + ", Url: " + v42 + "}";
                }
                if (!string.IsNullOrEmpty(v4)) {
                    v4 += "]";
                }
            } else {
                v4 = "null";
            }
            return $"LocalLoginProvider = {v1}, Email = {v2}, Logins = {v3}, ExternalLoginProviders = {v4}";
        }
    }

    public class UserInfoViewModel
    {
        public UserInfoViewModel()
        {
            Logins = new List<UserInfoViewModel>();
        }

        public string Id { get; set; }

        public string ExternalId { get; set; }

        public string Name { get; set; }

        public string PictureUrl { get; set; }

        public string Email { get; set; }

        public bool HasRegistered { get; set; }

        public string Type { get; set; }

        public List<UserInfoViewModel> Logins { get; set; }

        public override string ToString()
        {
            var v1 = Id == null ? "null" : Id;
            var v2 = ExternalId == null ? "null" : ExternalId;
            var v3 = Name == null ? "null" : Name;
            var v4 = PictureUrl == null ? "null" : PictureUrl;
            var v5 = Email == null ? "null" : Email;
            var v6 = HasRegistered.ToString();
            var v7 = Type == null ? "null" : Type;
            var v8 = string.Empty;
            if (Logins != null) {
                foreach (var l in Logins) {
                    if (string.IsNullOrEmpty(v8)) {
                        v8 += "[";
                    }
                    if (v8 != "[") {
                        v8 += ", ";
                    }
                    var v81 = l.Id == null ? "null" : l.Id;
                    var v82 = l.ExternalId == null ? "null" : l.ExternalId;
                    var v83 = l.Name == null ? "null" : l.Name;
                    var v84 = l.PictureUrl == null ? "null" : l.PictureUrl;
                    var v85 = l.Email == null ? "null" : l.Email;
                    var v86 = l.HasRegistered.ToString();
                    var v87 = l.Type == null ? "null" : l.Type;
                    v8 += $"{{ Id = {v81}, ExternalId = {v82}, Name = {v83}, PictureUrl = {v84}, Email = {v85}, HasRegistered = {v86}, Type = {v87} }}";
                }
                if (!string.IsNullOrEmpty(v8)) {
                    v8 += "]";
                }
            } else {
                v8 = "null";
            }
            return $"Id = {v1}, ExternalId = {v2}, Name = {v3}, PictureUrl = {v4}, Email = {v5}, HasRegistered = {v6}, Type = {v7}, Logins = {v8}";
        }
    }

    public class UserLoginInfoViewModel
    {
        public string LoginProvider { get; set; }

        public string ProviderKey { get; set; }

        public override string ToString()
        {
            var v1 = LoginProvider == null ? "null" : LoginProvider;
            var v2 = ProviderKey == null ? "null" : ProviderKey;
            return $"LoginProvider = {v1}, ProviderKey = {v2}";
        }
    }
}
