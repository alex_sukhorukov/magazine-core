﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace AngularJSWebApiEmpty.Models
{
    public class FaqData
    {
        public FaqData()
        {
            Id = Guid.Empty;
            Name = new Dictionary<string, string>();
            Desc = new Dictionary<string, string>();
        }
        /*
        public async Task Save(DataContext dataContext)
        {
            var lang = await dataContext.Language.Where(t => t.Active).Select(t => t).ToListAsync();
            var isNew = Id == Guid.Empty;

            var faq = isNew ? 
                new Faq {
                    Id = Guid.NewGuid()
                } : await dataContext.Faq
                    .Include("Name.Language")
                    .Include("Description.Language")
                    .Where(t => t.Id == Id)
                    .FirstAsync();

            var name = new List<LocalizableString>();
            var desc = new List<LocalizableString>();

            foreach (var l in lang) {
                if (isNew) {
                    name.Add(new LocalizableString {
                        Id = Guid.NewGuid(),
                        Text = Name[l.ShortName],
                        Language = l,
                        Code = Name[l.ShortName],
                        IsSerializable = false,
                        IsSystem = false
                    });
                    desc.Add(new LocalizableString {
                        Id = Guid.NewGuid(),
                        Text = Desc[l.ShortName],
                        Language = l,
                        Code = Desc[l.ShortName],
                        IsSerializable = false,
                        IsSystem = false
                    });
                } else {
                    var cname = faq.Name.Where(t => t.Language.ShortName == l.ShortName).First();
                    cname.Text = Name[l.ShortName];
                    cname.Code = Name[l.ShortName];
                    var cdesc = faq.Description.Where(t => t.Language.ShortName == l.ShortName).First();
                    cdesc.Text = Desc[l.ShortName];
                    cdesc.Code = Desc[l.ShortName];
                }
            }

            if (isNew) {
                Id = faq.Id;
                faq.Name = name;
                faq.Description = desc;
                dataContext.Faq.Add(faq);
            }

            await dataContext.SaveChangesAsync();
        }

        public static FaqData Parse(string value, dynamic lang)
        {
            var result = new FaqData();
            var o = JsonConvert.DeserializeObject<JObject>(value);
            result.Id = (o["id"] == null || string.IsNullOrEmpty(o["id"].Value<string>()) ? Guid.Empty : Guid.Parse(o["id"].Value<string>()));
            var name = o["name"] as JObject;
            var desc = o["desc"] as JObject;
            foreach (var l in lang) {
                result.Name[l.ShortName] = name[l.ShortName].ToString();
                result.Desc[l.ShortName] = HttpUtility.HtmlDecode(desc[l.ShortName].ToString());
            }
            return result;
        }
        */
        public Guid Id { get; set; }
        public Dictionary<string, string> Name { get; set; }
        public Dictionary<string, string> Desc { get; set; }
        public string CName { get; set; }
        public string CDesc { get; set; }
    }
}
