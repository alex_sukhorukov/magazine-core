﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Models
{
    public class SeriesData
    {
        public SeriesData()
        {
            Id = Guid.Empty;
            Name = new Dictionary<string, string>();
            Desc = new Dictionary<string, string>();
            Code = string.Empty;
            ImageId = new ImageId();
            // Image = new SeriesImage();
        }
        /*
        public async Task Save(DataContext dataContext)
        {
            var lang = await dataContext.Language.Where(t => t.Active).Select(t => t).ToListAsync();
            var isNew = Id == Guid.Empty;

            var series = isNew ?
                new Series
                {
                    Id = Guid.NewGuid(),
                    Active = true,
                    Code = Code
                } : await dataContext.Series
                    .Include("Image")
                    .Include("Name.Language")
                    .Include("Description.Language")
                    .Where(t => t.Id == Id)
                    .FirstAsync();

            var brand = dataContext.Brand.FirstOrDefault(t => t.Id == BrandId);
            series.Brand = brand;
            var name = new List<LocalizableString>();
            var desc = new List<LocalizableString>();

            series.Code = Code;

            foreach (var l in lang) {
                if (isNew) {
                    name.Add(new LocalizableString {
                        Id = Guid.NewGuid(),
                        Text = Name[l.ShortName],
                        Language = l,
                        Code = Name[l.ShortName],
                        IsSerializable = false,
                        IsSystem = false
                    });
                    desc.Add(new LocalizableString {
                        Id = Guid.NewGuid(),
                        Text = Desc[l.ShortName],
                        Language = l,
                        Code = Desc[l.ShortName],
                        IsSerializable = false,
                        IsSystem = false
                    });
                } else {
                    var cname = series.Name.Where(t => t.Language.ShortName == l.ShortName).First();
                    cname.Text = Name[l.ShortName];
                    cname.Code = Name[l.ShortName];
                    var cdesc = series.Description.Where(t => t.Language.ShortName == l.ShortName).First();
                    cdesc.Text = Desc[l.ShortName];
                    cdesc.Code = Desc[l.ShortName];
                }
            }

            if (isNew) {
                Id = series.Id;
                series.Name = name;
                series.Description = desc;
                dataContext.Series.Add(series);
            }

            if (Image?.Data != null) {
                if (Image.Series == null) {
                    Image.Series = series;
                }
                if (series.Image == null) {
                    series.Image = Image;
                } else {
                    series.Image.Data = Image.Data;
                }
            }

            await dataContext.SaveChangesAsync();
        }

        public static SeriesData Parse(string value, dynamic lang)
        {
            var result = new SeriesData();
            var o = JsonConvert.DeserializeObject<JObject>(value);
            var brandId = (string.IsNullOrEmpty(o["brandId"]?.Value<string>()) ? Guid.Empty : Guid.Parse(o["brandId"].Value<string>()));
            result.Id = (o["id"] == null || string.IsNullOrEmpty(o["id"].Value<string>()) ? Guid.Empty : Guid.Parse(o["id"].Value<string>()));
            result.BrandId = brandId;
            var image = o["image"] as JObject;
            var name = o["name"] as JObject;
            var desc = o["desc"] as JObject;
            var code = o["code"].Value<string>();
            result.Code = code;
            if (image != null) {
                result.ImageId = new ImageId {
                    Id = Guid.Parse(image["Id"].Value<string>())
                };
            }
            foreach (var l in lang) {
                result.Name[l.ShortName] = name[l.ShortName].ToString();
                result.Desc[l.ShortName] = HttpUtility.HtmlDecode(desc[l.ShortName].ToString());
            }
            return result;
        }
        */
        public Guid BrandId { get; set; }
        public ImageId ImageId { get; set; }
        // public SeriesImage Image { get; set; }

        public Guid Id { get; set; }
        public Dictionary<string, string> Name { get; set; }
        public Dictionary<string, string> Desc { get; set; }
        public string CName { get; set; }
        public string CDesc { get; set; }

        public string Code { get; set; }
    }
}
