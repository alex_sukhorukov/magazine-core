﻿using System;
using System.Collections.Generic;

namespace AngularJSWebApiEmpty.Models
{
    public class StringData
    {
        public StringData()
        {
            Id = new Dictionary<string, Guid>();
            Text = new Dictionary<string, string>();
            Code = string.Empty;
            CText = string.Empty;
        }
        /*
        public async Task Save(DataContext dataContext)
        {
            var lang = await dataContext.Language.Where(t => t.Active).ToListAsync();

            foreach (var l in lang) {
                LocalizableString ls = null;
                if (!Id.ContainsKey(l.ShortName) || Id[l.ShortName] == Guid.Empty) {
                    Id[l.ShortName] = Guid.NewGuid();
                    ls = new LocalizableString {
                        Id = Id[l.ShortName],
                        Code = Code,
                        Text = Text[l.ShortName],
                        IsSerializable = false,
                        IsSystem = false,
                        Language = l
                    };
                    dataContext.LocalizableString.Add(ls);
                } else {
                    var id = Id[l.ShortName];
                    ls = await dataContext.LocalizableString.Include("Language").Where(t => t.Id == id && t.Language.Id == l.Id).FirstAsync();
                    ls.Code = Code;
                    ls.Text = Text[l.ShortName];
                }

                await dataContext.SaveChangesAsync();
            }
        }
        */
        public Dictionary<string, Guid> Id { get; set; }
        public Dictionary<string, string> Text { get; set; }
        public string CText { get; set; }
        public string Code { get; set; }

        public override string ToString()
        {
            var v1 = CText == null ? "null" : CText;
            var v2 = Code == null ? "null" : Code;
            var v3 = string.Empty;
            if (Id != null) {
                foreach (var l in Id) {
                    if (string.IsNullOrEmpty(v3)) {
                        v3 += "[";
                    }
                    if (v3 != "[") {
                        v3 += ", ";
                    }
                    var v31 = l.Key == null ? "null" : l.Key;
                    var v32 = l.Value == null ? "null" : l.Value.ToString();
                    v3 += "{Key: " + v31 + ", Value: " + v32 + "}";
                }
                if (!string.IsNullOrEmpty(v3)) {
                    v3 += "]";
                }
            } else {
                v3 = "null";
            }
            var v4 = string.Empty;
            if (Text != null) {
                foreach (var l in Text) {
                    if (string.IsNullOrEmpty(v4)) {
                        v4 += "[";
                    }
                    if (v4 != "[") {
                        v4 += ", ";
                    }
                    var v41 = l.Key == null ? "null" : l.Key;
                    var v42 = l.Value == null ? "null" : l.Value;
                    v4 += "{Key: " + v41 + ", Value: " + v42 + "}";
                }
                if (!string.IsNullOrEmpty(v4)) {
                    v4 += "]";
                }
            } else {
                v4 = "null";
            }
            return $"CText = {v1}, Code = {v2}, Id = {v3}, Text = {v3}";
        }
    }
}
