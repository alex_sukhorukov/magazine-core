﻿namespace ProductStore.Filters
{
    /*
    [AttributeUsage(AttributeTargets.All)]
    public class StoreExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private ILogger _logger;

        public StoreExceptionFilterAttribute()
        {
            _logger = new Logger();
        }

        public override void OnException(HttpActionExecutedContext context)
        {
            var controllerName = context.ActionContext.ControllerContext.ControllerDescriptor.ControllerName;
            var arguments = context.ActionContext.ActionArguments;
            var actionName = context.ActionContext.ActionDescriptor.ActionName;
            var message = context.Exception.Message
                + (context?.Exception?.InnerException?.Message) + ", "
                + (context?.Exception?.InnerException?.InnerException?.Message) + ", "
                + (context?.Exception?.InnerException?.InnerException?.InnerException?.Message);

            var stackTrace = context.Exception.StackTrace;
            var entityValidationException = context.Exception as DbEntityValidationException;
            if (entityValidationException != null) {
                _logger.Trace("Entity validation exception:");
                foreach (var error in entityValidationException.EntityValidationErrors)
                {
                    var jsonObject = JsonConvert.SerializeObject(error.Entry.Entity, Formatting.None, new JsonSerializerSettings {
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    });
                    _logger.Trace($"Entity object: {jsonObject}");
                    foreach (var msg in error.ValidationErrors)
                    {
                        _logger.Trace($"\tProperty name: {msg.PropertyName}, message: {msg.ErrorMessage}");
                    }
                }
            }
            var helpLink = context.Exception.HelpLink;
            _logger.Trace($"Controller: {controllerName}");
            _logger.Trace($"Action: {actionName}");
            _logger.Trace("Arguments:");
            foreach (var a in arguments) {
                _logger.Trace("Name: " + a.Key + ", value: " + a.Value);
            }
            _logger.Trace($"Exception: {message}");
            _logger.Trace($"StackTrace: {stackTrace}");
            if (!string.IsNullOrEmpty(helpLink)) {
                _logger.Trace($"Help link: {helpLink}");
            }
            _logger.Trace($"Request: {context.Request}");
            _logger.Trace("-------------------------------------------------------------------------------------------------------------");
            context.Response = context.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, context.Exception);
        }
    }
    */
}