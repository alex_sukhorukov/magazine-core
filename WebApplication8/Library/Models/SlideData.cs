﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Models
{
    public class SlideData
    {
        public SlideData()
        {
            Id = Guid.Empty;
            Url = string.Empty;
            Position = 0;
            ImageId = new ImageId();
            // Image = new SlideImage();
        }
        /*
        public async Task Save(DataContext dataContext)
        {
            var isNew = Id == Guid.Empty;

            var slide = isNew ?
                new Slide
                {
                    Id = Guid.NewGuid(),
                } : await dataContext.Slide
                    .Include("Image")
                    .Where(t => t.Id == Id)
                    .FirstAsync();

            var category = dataContext.Category.FirstOrDefault(t => t.Id == CategoryId);
            slide.Category = category;
            slide.Url = Url;
            slide.Position = Position;

            if (isNew) {
                Id = slide.Id;
                dataContext.Slide.Add(slide);
            }

            if (Image?.Data != null)
            {
                if (Image.Slide== null)
                {
                    Image.Slide = slide;
                }
                if (slide.Image == null)
                {
                    slide.Image = Image;
                }
                else
                {
                    slide.Image.Data = Image.Data;
                }
            }

            await dataContext.SaveChangesAsync();
        }

        public static SlideData Parse(string value)
        {
            var result = new SlideData();
            var o = JsonConvert.DeserializeObject<JObject>(value);
            result.Id = (string.IsNullOrEmpty(o["id"]?.Value<string>()) ? Guid.Empty : Guid.Parse(o["id"].Value<string>()));
            var image = o["image"] as JObject;
            var url = o["url"].Value<string>();
            var categoryId = (string.IsNullOrEmpty(o["categoryId"]?.Value<string>()) ? Guid.Empty : Guid.Parse(o["categoryId"].Value<string>()));
            var position = o["position"].Value<int>();
            result.Url = url;
            result.CategoryId = categoryId;
            result.Position = position;
            if (image != null)
            {
                result.ImageId = new ImageId
                {
                    Id = Guid.Parse(image["Id"].Value<string>())
                };
            }
            return result;
        }
       */
        public ImageId ImageId { get; set; }
        // public SlideImage Image { get; set; }
        public Guid CategoryId { get; set; }
        public Guid Id { get; set; }
        public string Url { get; set; }
        public int Position{ get; set; }
    }
}
