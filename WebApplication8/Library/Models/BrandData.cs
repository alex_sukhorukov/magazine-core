﻿using DbModel;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationMVCFacebook.Data;

namespace Models
{
    public class BrandData
    {
        public BrandData()
        {
            Id = Guid.Empty;
            Name = string.Empty;
            ImageId = new ImageId();
            Image = new BrandImage();
        }

        public async Task Save(ApplicationDbContext dataContext)
        {
            var isNew = Id == Guid.Empty;

            var brand = isNew ? 
                new Brand {
                    Id = Guid.NewGuid(),
                    Name = Name
                } : await dataContext.Brand
                    .Include("Image")
                    .Where(t => t.Id == Id)
                    .FirstAsync();

            brand.Name = Name;

            if (isNew) {
                Id = brand.Id;
                dataContext.Brand.Add(brand);
            }

            if (Image?.Data != null) {
                if (Image.Brand == null) {
                    Image.Brand = brand;
                }
                if (brand.Image == null) {
                    brand.Image = Image;
                } else {
                    brand.Image.Data = Image.Data;
                }
            }

            await dataContext.SaveChangesAsync();
        }

        public static BrandData Parse(string value, dynamic lang)
        {
            var result = new BrandData();
            var o = JsonConvert.DeserializeObject<JObject>(value);
            result.Id = (o["id"] == null || string.IsNullOrEmpty(o["id"].Value<string>()) ? Guid.Empty : Guid.Parse(o["id"].Value<string>()));
            var image = o["image"].Value<JObject>();
            var name = o["name"].Value<string>();
            result.Name = name;
            if (image != null) {
                result.ImageId = new ImageId {
                    Id = Guid.Parse(image["Id"].Value<string>())
                };
            }
            return result;
        }

        public ImageId ImageId { get; set; }
        public BrandImage Image { get; set; }
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
