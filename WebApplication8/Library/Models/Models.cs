﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Models
{
    public class RemoveImageModel
    {
        public Guid Id { get; set; }

        public Guid ImageId { get; set; }

        public override string ToString()
        {
            var v1 = Id == null ? "null" : Id.ToString();
            var v2 = ImageId == null ? "null" : ImageId.ToString();
            return $"Id = {v1}, ImageId = {v2}";
        }
    }

    public class CommentModel
    {
        public Guid CommentId { get; set; }

        public Guid ExternalId { get; set; }

        public Guid UserId { get; set; }

        public Guid ProductId { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public override string ToString()
        {
            var v1 = ProductId == null ? "null" : ProductId.ToString();
            var v2 = Title == null ? "null" : Title;
            var v3 = Text == null ? "null" : Text;
            return $"ProductId = {v1}, Title = {v2}, Text = {v3}";
        }
    }

    public class RemoveModel
    {
        public Guid Id { get; set; }

        public override string ToString()
        {
            var v1 = Id == null ? "null" : Id.ToString();
            return $"Id = {v1}";
        }
    }

    public class RemoveStringModel
    {
        public Dictionary<string, Guid> Id { get; set; }

        public override string ToString()
        {
            var v1 = string.Empty;
            if (Id == null) {
                v1 = "null";
            } else {
                v1 = string.Empty;
                foreach (var i in Id) {
                    if (string.IsNullOrEmpty(v1)) {
                        v1 += "[";
                    }
                    if (v1 != "[") {
                        v1 += ", ";
                    }
                    v1 += "{" + i.Key + " = " + i.Value +"}";
                }
                if (!string.IsNullOrEmpty(v1)) {
                    v1 += "]";
                }
            }
            return $"Id = {v1}";
        }
    }

    public class UserInfo
    {
        public Guid Id { get; set; }

        public string Address { get; set; }

        public string Phone { get; set; }

        public string Email { get; set; }

        public override string ToString()
        {
            var v1 = Id == null ? "null" : Id.ToString();
            var v2 = Address == null ? "null" : Address;
            var v3 = Phone == null ? "null" : Phone;
            var v4 = Email == null ? "null" : Email;
            return $"Id = {v1}, Address = {v2}, Phone = {v3}, Email = { v4}";
        }
    }

    public struct IndexGuid
    {
        public Guid Id { get; set; }
        public int Index { get; set; }
    }

    public class SearchModel
    {
        public SearchModel()
        {
            Page = 1;
            PageSize = 12;
            Age = 0;
            All = false;
            Pol = 0;
            OnlyWithComments = false;
            ProductCode = string.Empty;
            LangId = 1;
            Text = string.Empty;
            Exists = false;
            SeriesIds = new List<string>();
            CategoryIds = new List<string>();
            BrandIds = new List<string>();
            SortBy = "ByDateDesc";
        }

        public static SearchModel Parse(string s)
        {
            var r = new SearchModel();
            var o = JsonConvert.DeserializeObject(s) as JObject;
            r.Age = o["age"] != null ? o["age"].Value<int>() : 0;
            r.All = o["all"] != null ? o["all"].Value<bool>() : false;
            r.Page = o["page"] != null ? o["page"].Value<int>() : 1;
            r.PageSize = o["pageSize"] != null ? o["pageSize"].Value<int>() : 12;
            r.LangId = o["langId"] != null ? o["langId"].Value<int>() : 0;
            r.BrandIds = o["brandIds"] != null ? o["brandIds"].ToObject<List<string>>() : new List<string>();
            r.CategoryIds = o["categoryIds"] != null ? o["categoryIds"].ToObject<List<string>>() : new List<string>();
            r.Exists = o["exists"] != null ? o["exists"].Value<bool>() : false;
            r.OnlyWithComments = o["onlyWithComments"] != null ? o["onlyWithComments"].Value<bool>() : false;
            r.Pol = o["pol"] != null ? o["pol"].Value<int>() : 0;
            r.PriceRange = o["priceRange"] != null ? o["priceRange"].Value<string>() : string.Empty;
            r.ProductCode = o["productCode"] != null ? o["productCode"].Value<string>() : string.Empty;
            r.SeriesIds = o["seriesIds"] != null ? o["seriesIds"].ToObject<List<string>>() : new List<string>();
            r.SortBy = o["sortBy"] != null ? o["sortBy"].Value<string>() : "ByDateDesc";
            r.Text  = o["text"] != null ? o["text"].Value<string>() : string.Empty;
            return r;
        }

        public static void Normalize(ref SearchModel model)
        {
            if (model.BrandIds == null) {
                model.BrandIds = new List<string>();
            }
            if (model.SeriesIds == null) {
                model.SeriesIds = new List<string>();
            }
            if (model.CategoryIds == null) {
                model.CategoryIds = new List<string>();
            }
            if (string.IsNullOrEmpty(model.PriceRange)) {
                model.PriceRange = string.Empty;
            }
            if (string.IsNullOrEmpty(model.ProductCode)) {
                model.ProductCode = string.Empty;
            }
            if (string.IsNullOrEmpty(model.SortBy)) {
                model.SortBy = string.Empty;
            }
            if (string.IsNullOrEmpty(model.Text)) {
                model.Text = string.Empty;
            }
        }

        public string Text { get; set; }
        public string PriceRange { get; set; }
        public int Age { get; set; }
        public int PageSize { get; set; }
        public int Page { get; set; }
        public bool All { get; set; }
        public bool Exists { get; set; }
        public int Pol { get; set; }
        public string SortBy { get; set; }
        public string ProductCode { get; set; }
        public bool OnlyWithComments { get; set; }
        public List<string> SeriesIds { get; set; }
        public List<string> CategoryIds { get; set; }
        public List<string> BrandIds { get; set; }
        public int LangId { get; set; }
    }

    public class ProductInfo
    {
        public Guid Id { get; set; }

        public int Count { get; set; }

        public override string ToString()
        {
            var v1 = Id == null ? "null" : Id.ToString();
            var v2 = Count.ToString();
            return $"Id = {v1}, Count = {v2}";
        }
    }

    public class WayForPayServiceModel
    {
        public WayForPayServiceModel() {

        }

        public string merchantAccount { get; set; }
        public string orderReference { get; set; }
        public string merchantSignature { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
        public string authCode { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string createdDate { get; set; }
        public string processingDate { get; set; }
        public string cardPan { get; set; }
        public string cardType { get; set; }
        public string issuerBankCountry { get; set; }
        public string issuerBankName { get; set; }
        public string recToken { get; set; }
        public string transactionStatus { get; set; }
        public string reason { get; set; }
        public string reasonCode { get; set; }
        public string fee { get; set; }
        public string paymentSystem { get; set; }
        public string clientName { get; set; }
    }

    public class PlaceOrderModel
    {
        public PlaceOrderModel()
        {
            ProductSet = new List<ProductInfo>();
        }

        public Guid DeliveryMethodId { get; set; }

        public Guid NPCityRef { get; set; }

        public Guid NPWarehouseRef { get; set; }

        public string FullName { get; set; }

        public string Email { get; set; }

        public string PreOrderNumber { get; set; }

        public string Phone { get; set; }

        public string AdditionalInfo { get; set; }

        public List<ProductInfo> ProductSet { get; set; }

        public override string ToString()
        {
            var v1 = DeliveryMethodId == null ? "null" : DeliveryMethodId.ToString();
            var v2 = NPCityRef == null ? "null" : NPCityRef.ToString();
            var v3 = NPWarehouseRef == null ? "null" : NPWarehouseRef.ToString();
            var v4 = FullName == null ? "null" : FullName;
            var v5 = Phone == null ? "null" : Phone;
            var v6 = Email == null ? "null" : Email;
            var v7 = AdditionalInfo == null ? "null" : AdditionalInfo;
            var v8 = string.Empty;
            var v9 = PreOrderNumber == null ? "null" : PreOrderNumber;
            if (ProductSet == null) {
                v8 = "null";
            } else {
                v8 = string.Empty;
                foreach (var ps in ProductSet) {
                    if (string.IsNullOrEmpty(v8)) {
                        v8 += "[";
                    }
                    if (v8 != "[") {
                        v8 += ", ";
                    }
                    var v81 = ps.Id == null ? "null" : ps.Id.ToString();
                    var v82 = ps.Count.ToString();
                    v8 += $"{{ Id = {v81}, Count = {v82} }}";
                }
                if (!string.IsNullOrEmpty(v8)) {
                    v8 += "]";
                }
            }
            return $"DeliveryMethodId = {v1}, NPCityRef = {v2}, NPWarehouseRef = {v3}, FullName = {v4}, Phone = {v5}, Email = {v6}, AdditionalInfo = {v7}, ProductSet = {v8}, PreOrderNumber = {v9}";
        }
    }

    public class OrderActionModel
    {
        public Guid Id { get; set; }

        public string Action { get; set; }

        public override string ToString()
        {
            var v1 = Id == null ? "null" : Id.ToString();
            var v2 = Action == null ? "null" : Action;
            return $"Id = {v1}, Action = {v2}";
        }
    }

    public class SalesByCategoryData
    {
        public string Category { get; set; }
        public int Month { get; set; }
        public decimal Total { get; set; }
    }
}