﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Models
{
    public class ImageId
    {
        public Guid Id { get; set; }
        public bool IsPreview { get; set; }
        public override string ToString()
        {
            var v1 = Id.ToString();
            var v2 = IsPreview.ToString();
            return $"Id = {v1}, IsPreview = {v2}";
        }
    }

    public class ProductData
    {
        public ProductData()
        {
            IdBrand = string.Empty;
            IdSeries = string.Empty;
            Name = new Dictionary<string, string>();
            Desc = new Dictionary<string, string>();
            ImageIds = new List<ImageId>();
        }
        /*
        public async Task Save(DataContext dataContext)
        {
            Product product = null;
            var now = DateTime.UtcNow;
            var isNew = false;
            if (Id == Guid.Empty && HttpContext.Current.Cache[PhantomId] != null) {
                Id = (Guid)HttpContext.Current.Cache[PhantomId];
            }
            if (Id == Guid.Empty) {
                isNew = true;
                Id = Guid.NewGuid();
                HttpContext.Current.Cache.Add(PhantomId, Id, null, now.AddMinutes(480), Cache.NoSlidingExpiration, CacheItemPriority.Normal, null);
                product = new Product {
                    Id = Id
                };
                var lang = await dataContext.Language.Where(t => t.Active).ToListAsync();
                foreach (var l in lang) {
                    var nameId = Guid.NewGuid();
                    var descId = Guid.NewGuid();
                    product.Name.Add(new LocalizableString {
                        Id = nameId,
                        Code = "PRODUCT_NAME_" + nameId,
                        Text = string.Empty,
                        IsSerializable = false,
                        Language = l
                    });
                    product.Description.Add(new LocalizableString {
                        Id = descId,
                        Code = "PRODUCT_DESC_" + descId,
                        Text = string.Empty,
                        IsSerializable = false,
                        Language = l
                    });
                }
            } else {
                product = await dataContext.Product
                    .Include("Name.Language")
                    .Include("ProductPrice")
                    .Include("Images")
                    .Include("Description.Language")
                    .Where(t => t.Id == Id).FirstAsync();
            }

            foreach (var name in product.Name) {
                if (Name.ContainsKey(name.Language.ShortName)) {
                    name.Text = Name[name.Language.ShortName];
                }
            }

            foreach (var desc in product.Description) {
                if (Desc.ContainsKey(desc.Language.ShortName)) {
                    desc.Text = Desc[desc.Language.ShortName];
                }
            }

            if (!string.IsNullOrEmpty(IdBrand)) {
                var brand = await dataContext.Brand.Where(t => t.Name == IdBrand).FirstOrDefaultAsync();
                product.Brand = brand;
            }

            if (!string.IsNullOrEmpty(IdSeries)) {
                var series = await dataContext.Series.Where(t => t.Code == IdSeries).FirstOrDefaultAsync();
                product.Series = series;
            }

            product.Active = Active;
            product.IsExists = Exists;
            product.Age = Age;
            product.Pol = Pol;
            product.NumberOfDetails = NumberOfDetails;
            product.InternalProductCode = InternalProductCode;
            product.Size = Size;
            product.WarrantyNumberOfMonth = WarrantyNumberOfMonth;

            foreach (var image in ImageIds) {
                var productImage = product.Images.FirstOrDefault(t => t.Id == image.Id);
                if (productImage != null) {
                    productImage.IsPreview = image.IsPreview;
                }
            }

            foreach (var image in Images) {
                if (image.Data != null) {
                    image.Product = product;
                    product.Images.Add(image);
                } else {
                    var productImage = product.Images.FirstOrDefault(t => t.Id == image.Id);
                    if (productImage != null) {
                        productImage.IsPreview = image.IsPreview;
                    }
                }
            }

            var category = await dataContext.Category.Where(t => t.Id == CategoryId).FirstAsync();
            var effectiveDate = GetDate(Start);
            var productPrice = product.ProductPrice.FirstOrDefault(t => t.StartDate == effectiveDate);
            if (productPrice == null) {
                var productBeforePrice = product.ProductPrice.Where(t => t.StartDate < effectiveDate).OrderByDescending(t => t.StartDate).FirstOrDefault();
                if (productBeforePrice == null || productBeforePrice.Price != Price) {
                    product.ProductPrice.Add(new ProductPrice {
                        Id = Guid.NewGuid(),
                        PopulationDate = now,
                        Price = Price,
                        Product = product,
                        StartDate = effectiveDate
                    });
                }
            } else {
                productPrice.StartDate = effectiveDate;
                productPrice.PopulationDate = now;
                productPrice.Price = Price;
            }

            product.Category = category;
            product.ProductCode = ProductCode;
            product.UpdatedAt = now;

            if (isNew) {
                product.CreatedAt = now;
                dataContext.Product.Add(product);
            }

            await dataContext.SaveChangesAsync();

            await ProcessRecent(dataContext, product);
        }
        
        public async Task ProcessRecent(DataContext dataContext, Product product)
        {
            var now = DateTime.UtcNow;

            var recents = await dataContext.ProductRecent
                .Include("Product")
                .Include("Recent")
                .Where(t => t.Product.Id == product.Id)
                .Select(t => t).ToListAsync();

            var recentsProductIds = recents.Select(t => t.Product.Id).ToList();

            if (recents.Count < 8) {
                var products = dataContext.Product
                    .Where(t => t.Id != product.Id)
                    .Where(t => !recentsProductIds.Contains(t.Id))
                    .Take(8 - recents.Count);

                var i = recents.Count;

                foreach (var p in products) {
                    var recent = new ProductRecent {
                        Id = Guid.NewGuid(),
                        Product = product,
                        Recent = p,
                        Index = i++,
                        IsReal = false,
                        PopulationDate = now
                    };

                    dataContext.ProductRecent.Add(recent);
                }
                await dataContext.SaveChangesAsync();
            }
        }

        public static ProductData Parse(string value, dynamic lang)
        {
            var result = new ProductData();
            var o = JsonConvert.DeserializeObject<JObject>(value);
            result.Id = (o["id"] == null ? Guid.Empty : Guid.Parse(o["id"].Value<string>()));
            result.PhantomId = o["phantomId"].Value<string>();
            result.Active = o["active"].Value<bool>();
            result.Exists = o["exists"].Value<bool>();
            result.Start = o["start"].Value<string>();
            result.Price = o["price"].Value<decimal>();
            result.IdBrand = o["idBrand"].Value<string>();
            result.IdSeries = o["idSeries"].Value<string>();
            result.ProductCode = o["productCode"].Value<string>();
            result.Age = o["age"].Value<int>();
            result.Pol = o["pol"].Value<int>();
            result.NumberOfDetails = o["numberOfDetails"].Value<int>();
            result.InternalProductCode = o["internalProductCode"].Value<string>();
            result.Size = o["size"].Value<string>();
            result.WarrantyNumberOfMonth = o["warrantyNumberOfMonth"].Value<int>();

            result.CategoryId = Guid.Parse(o["categoryId"].Value<string>());
            var images = o["images"] as JArray;
            var name = o["name"] as JObject;
            var desc = o["desc"] as JObject;
            if (images != null) {
                foreach (var image in images) {
                    result.ImageIds.Add(new ImageId {
                        Id = Guid.Parse(image["Id"].Value<string>()),
                        IsPreview = image["IsPreview"].Value<bool>()
                    });
                }
            }
            foreach (var l in lang) {
                result.Name[l.ShortName] = name[l.ShortName].ToString();
                result.Desc[l.ShortName] = HttpUtility.HtmlDecode(desc[l.ShortName].ToString());
            }
            return result;
        }

        private static DateTime GetDate(string value)
        {
            var parts = value.Split(' ');
            var d = int.Parse(parts[0].Split('.')[0]);
            var m = int.Parse(parts[0].Split('.')[1]);
            var y = int.Parse(parts[0].Split('.')[2]);
            var hh = int.Parse(parts[1].Split(':')[0]);
            var mm = int.Parse(parts[1].Split(':')[1]);
            var localDate = new DateTime(y, m, d, hh, mm, 0);
            return localDate.ToUniversalTime();
        }
        */
        public Dictionary<string, string> Name { get; set; }
        public Dictionary<string, string> Desc { get; set; }
        public bool Active { get; set; }
        public bool Exists { get; set; }
        public DateTime EffectiveDate { get; set; }
        public string Start { get; set; }
        public string PhantomId { get; set; }
        public decimal Price { get; set; }
        public int Age { get; set; }
        public int Pol { get; set; }
        public int NumberOfDetails { get; set; }
        public string InternalProductCode { get; set; }
        public string Size { get; set; }
        public int WarrantyNumberOfMonth { get; set; }
        public string ProductCode { get; set; }
        public string IdBrand { get; set; }
        public string IdSeries { get; set; }
        public Guid Id { get; set; }
        public List<ImageId> ImageIds { get; set; }
        // public List<ProductImage> Images { get; set; }
        public Guid CategoryId { get; set; }
        /*
        public override string ToString()
        {
            var v1 = Id.ToString();
            var v2 = Active.ToString();
            var v3 = Exists.ToString();
            var v4 = (EffectiveDate.ToShortDateString() + " " + EffectiveDate.ToLongTimeString());
            var v5 = Start ?? "null";
            var v6 = PhantomId ?? "null";
            var v7 = Price.ToString(CultureInfo.InvariantCulture);
            var v8 = CategoryId.ToString();
            string v9;
            if (Name == null) {
                v9 = "null";
            } else {
                v9 = string.Empty;
                foreach (var n in Name) {
                    if (string.IsNullOrEmpty(v9)) {
                        v9 += "[";
                    }
                    if (v9 != "[") {
                        v9 += ", ";
                    }
                    v9 += "{" + n.Key + " = " + n.Value + "}";
                }
                if (!string.IsNullOrEmpty(v9)) {
                    v9 += "]";
                }
            }
            string v10;
            if (Desc == null) {
                v10 = "null";
            } else {
                v10 = string.Empty;
                foreach (var d in Desc) {
                    if (string.IsNullOrEmpty(v10)) {
                        v10 += "[";
                    }
                    if (v10 != "[") {
                        v10 += ", ";
                    }
                    v10 += "{" + d.Key + " = " + d.Value + "}";
                }
                if (!string.IsNullOrEmpty(v10)) {
                    v10 += "]";
                }
            }
            string v11;
            if (ImageIds == null) {
                v11 = "null";
            } else {
                v11 = string.Empty;
                foreach (var d in ImageIds) {
                    if (string.IsNullOrEmpty(v11)) {
                        v11 += "[";
                    }
                    if (v11 != "[") {
                        v11 += ", ";
                    }
                    v11 += "{Id = " + d.Id + ", IsPreview = " + d.IsPreview + "}";
                }
                if (!string.IsNullOrEmpty(v11)) {
                    v11 += "]";
                }
            }
            string v12;
            if (Images == null) {
                v12 = "null";
            } else {
                v12 = string.Empty;
                foreach (var d in Images) {
                    if (string.IsNullOrEmpty(v12)) {
                        v12 += "[";
                    }
                    if (v12 != "[") {
                        v12 += ", ";
                    }
                    v12 += "{Id = " + d.Id + ", IsPreview = " + d.IsPreview + ", Data = " + (d.Data == null ? "null" : d.Data.LongLength + " bytes") + ", ProductId = " + (d.Product == null ? "null" : d.Product.Id.ToString()) + "}";
                }
                if (!string.IsNullOrEmpty(v12)) {
                    v12 += "]";
                }
            }
            string v13 = ProductCode ?? "null";
            var v14 = IdBrand ?? "null";
            var v15 = IdSeries ?? "null";
            return $"Id = {v1}, Active = {v2}, Exists = {v3}, EffectiveDate = {v4}, Start = {v5}, " +
                   $"PhantomId = {v6}, Price = {v7}, CategoryId = {v8}, Name = {v9}, Desc = {v10}, " +
                   $"ImageIds = {v11}, Images = {v12}, ProductCode = {v13}, Brand = {v14}, Series = { v15}";
        }
        */
    }
}