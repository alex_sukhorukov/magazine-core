﻿using System.Linq;
using FluentValidation;

namespace AngularJSWebApi.Models
{
    public class ExternalLoginValidator : AbstractValidator<AddExternalLoginBindingModel>
    {
        public ExternalLoginValidator()
        {
            RuleFor(x => x.ExternalAccessToken)
                .NotEmpty()
                .WithMessage("FIELD_CANNOT_BE_EMPTY");
        }
    }

    public class GuidValidator : AbstractValidator<GuidBindingModel>
    {
        public GuidValidator()
        {
            RuleFor(x => x.Guid)
                .NotEmpty()
                .WithMessage("FIELD_CANNOT_BE_EMPTY");
        }
    }

    public class RegisterValidator : AbstractValidator<RegisterBindingModel>
    {
        public RegisterValidator()
        {
            RuleFor(x => x.Email)
                .EmailAddress()
                .WithMessage("INCORRECT_EMAIL")
                .NotEmpty()
                .WithMessage("FIELD_CANNOT_BE_EMPTY");

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("FIELD_CANNOT_BE_EMPTY")
                .Length(6, 512)
                .WithMessage(string.Format("FIELD_LENGTH_SHOULD_BE_BETWEEN", 6, 512))
                .Must(HasDigit)
                .WithMessage("FIELD_SHOULD_INCLUDE_DIGIT")
                .Must(HasLetter)
                .WithMessage("FIELD_SHOULD_INCLUDE_LETTER")
                .Must(HasLower)
                .WithMessage("FIELD_SHOULD_INCLUDE_LOWER")
                .Must(HasUpper)
                .WithMessage("FIELD_SHOULD_INCLUDE_UPPER")
                .Must(HasSymbol)
                .WithMessage("FIELD_SHOULD_INCLUDE_SYMBOL");

            RuleFor(x => x.ConfirmPassword)
                .NotEmpty()
                .WithMessage("FIELD_CANNOT_BE_EMPTY")
                .Equal(t => t.Password)
                .WithMessage("PASSWORDS_ARE_NOT_THE_SAME");
        }

        private bool HasDigit(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsDigit(t))) {
                return false;
            }
            return true;
        }

        private bool HasUpper(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsUpper(t))) {
                return false;
            }
            return true;
        }

        private bool HasLower(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsLower(t))) {
                return false;
            }
            return true;
        }

        private bool HasSymbol(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsSymbol(t))) {
                return false;
            }
            return true;
        }

        private bool HasLetter(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsLetter(t))) {
                return false;
            }
            return true;
        }
    }

    public class SetPasswordValidator : AbstractValidator<SetPasswordBindingModel>
    {
        public SetPasswordValidator()
        {
            RuleFor(x => x.RecoveryUid)
                .NotEmpty()
                .WithMessage("FIELD_CANNOT_BE_EMPTY");

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("FIELD_CANNOT_BE_EMPTY")
                .Length(6, 512)
                .WithMessage(string.Format("FIELD_LENGTH_SHOULD_BE_BETWEEN", 6, 512))
                .Must(HasDigit)
                .WithMessage("FIELD_SHOULD_INCLUDE_DIGIT")
                .Must(HasLetter)
                .WithMessage("FIELD_SHOULD_INCLUDE_LETTER")
                .Must(HasLower)
                .WithMessage("FIELD_SHOULD_INCLUDE_LOWER")
                .Must(HasUpper)
                .WithMessage("FIELD_SHOULD_INCLUDE_UPPER")
                .Must(HasSymbol)
                .WithMessage("FIELD_SHOULD_INCLUDE_SYMBOL");

            RuleFor(x => x.ConfirmPassword)
                .NotEmpty()
                .WithMessage("FIELD_CANNOT_BE_EMPTY")
                .Equal(t => t.Password)
                .WithMessage("PASSWORDS_ARE_NOT_THE_SAME");
        }

        private bool HasDigit(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsDigit(t))) {
                return false;
            }
            return true;
        }

        private bool HasUpper(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsUpper(t))) {
                return false;
            }
            return true;
        }

        private bool HasLower(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsLower(t))) {
                return false;
            }
            return true;
        }

        private bool HasSymbol(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsSymbol(t))) {
                return false;
            }
            return true;
        }

        private bool HasLetter(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsLetter(t))) {
                return false;
            }
            return true;
        }
    }

    public class RegisterExternalValidator : AbstractValidator<RegisterExternalBindingModel>
    {
        public RegisterExternalValidator()
        {
            RuleFor(x => x.Email)
                .EmailAddress()
                .WithMessage("INCORRECT_EMAIL")
                .NotEmpty()
                .WithMessage("FIELD_CANNOT_BE_EMPTY");
        }
    }

    public class LoginValidator : AbstractValidator<LoginBindingModel>
    {
        public LoginValidator()
        {
            Init();
        }

        private void Init()
        {
            RuleFor(x => x.Email)
                .EmailAddress()
                .WithMessage("INCORRECT_EMAIL")
                .NotEmpty()
                .WithMessage("FIELD_CANNOT_BE_EMPTY");

            RuleFor(x => x.Response)
                .NotEmpty()
                .WithMessage("RECAPTCHA_INVALID");

            RuleFor(x => x.Password)
                .NotEmpty()
                .WithMessage("FIELD_CANNOT_BE_EMPTY")
                .Length(6, 512)
                .WithMessage(string.Format("FIELD_LENGTH_SHOULD_BE_BETWEEN", 6, 512))
                .Must(HasDigit)
                .WithMessage("FIELD_SHOULD_INCLUDE_DIGIT")
                .Must(HasLetter)
                .WithMessage("FIELD_SHOULD_INCLUDE_LETTER")
                .Must(HasLower)
                .WithMessage("FIELD_SHOULD_INCLUDE_LOWER")
                .Must(HasUpper)
                .WithMessage("FIELD_SHOULD_INCLUDE_UPPER")
                .Must(HasSymbol)
                .WithMessage("FIELD_SHOULD_INCLUDE_SYMBOL");
        }

        private bool HasDigit(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsDigit(t))) {
                return false;
            }
            return true;
        }

        private bool HasUpper(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsUpper(t))) {
                return false;
            }
            return true;
        }

        private bool HasLower(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsLower(t))) {
                return false;
            }
            return true;
        }

        private bool HasSymbol(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsSymbol(t))) {
                return false;
            }
            return true;
        }

        private bool HasLetter(string pwd)
        {
            if (string.IsNullOrEmpty(pwd)) {
                return false;
            }
            if (!pwd.Any(t => char.IsLetter(t))) {
                return false;
            }
            return true;
        }
    }
}