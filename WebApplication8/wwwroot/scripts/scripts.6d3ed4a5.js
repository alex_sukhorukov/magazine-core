(function($) {
    if (!$.browser && 1.9 <= parseFloat($.fn.jquery)) {
        var a = {
            browser: void 0,
            version: void 0,
            mobile: !1
        };
        navigator && navigator.userAgent && (a.ua = navigator.userAgent, a.webkit = /WebKit/i.test(a.ua), 
        a.browserArray = "MSIE Chrome Opera Kindle Silk BlackBerry PlayBook Android Safari Mozilla Nokia".split(" "), 
        /Sony[^ ]*/i.test(a.ua) ? a.mobile = "Sony" : /RIM Tablet/i.test(a.ua) ? a.mobile = "RIM Tablet" : /BlackBerry/i.test(a.ua) ? a.mobile = "BlackBerry" : /iPhone/i.test(a.ua) ? a.mobile = "iPhone" : /iPad/i.test(a.ua) ? a.mobile = "iPad" : /iPod/i.test(a.ua) ? a.mobile = "iPod" : /Opera Mini/i.test(a.ua) ? a.mobile = "Opera Mini" : /IEMobile/i.test(a.ua) ? a.mobile = "IEMobile" : /BB[0-9]{1,}; Touch/i.test(a.ua) ? a.mobile = "BlackBerry" : /Nokia/i.test(a.ua) ? a.mobile = "Nokia" : /Android/i.test(a.ua) && (a.mobile = "Android"), 
        /MSIE|Trident/i.test(a.ua) ? (a.browser = "MSIE", a.version = /MSIE/i.test(navigator.userAgent) && 0 < parseFloat(a.ua.split("MSIE")[1].replace(/[^0-9\.]/g, "")) ? parseFloat(a.ua.split("MSIE")[1].replace(/[^0-9\.]/g, "")) : "Edge", 
        /Trident/i.test(a.ua) && /rv:([0-9]{1,}[\.0-9]{0,})/.test(a.ua) && (a.version = parseFloat(a.ua.match(/rv:([0-9]{1,}[\.0-9]{0,})/)[1].replace(/[^0-9\.]/g, "")))) : /Chrome/.test(a.ua) ? (a.browser = "Chrome", 
        a.version = parseFloat(a.ua.split("Chrome/")[1].split("Safari")[0].replace(/[^0-9\.]/g, ""))) : /Opera/.test(a.ua) ? (a.browser = "Opera", 
        a.version = parseFloat(a.ua.split("Version/")[1].replace(/[^0-9\.]/g, ""))) : /Kindle|Silk|KFTT|KFOT|KFJWA|KFJWI|KFSOWI|KFTHWA|KFTHWI|KFAPWA|KFAPWI/i.test(a.ua) ? (a.mobile = "Kindle", 
        /Silk/i.test(a.ua) ? (a.browser = "Silk", a.version = parseFloat(a.ua.split("Silk/")[1].split("Safari")[0].replace(/[^0-9\.]/g, ""))) : /Kindle/i.test(a.ua) && /Version/i.test(a.ua) && (a.browser = "Kindle", 
        a.version = parseFloat(a.ua.split("Version/")[1].split("Safari")[0].replace(/[^0-9\.]/g, "")))) : /BlackBerry/.test(a.ua) ? (a.browser = "BlackBerry", 
        a.version = parseFloat(a.ua.split("/")[1].replace(/[^0-9\.]/g, ""))) : /PlayBook/.test(a.ua) ? (a.browser = "PlayBook", 
        a.version = parseFloat(a.ua.split("Version/")[1].split("Safari")[0].replace(/[^0-9\.]/g, ""))) : /BB[0-9]{1,}; Touch/.test(a.ua) ? (a.browser = "Blackberry", 
        a.version = parseFloat(a.ua.split("Version/")[1].split("Safari")[0].replace(/[^0-9\.]/g, ""))) : /Android/.test(a.ua) ? (a.browser = "Android", 
        a.version = parseFloat(a.ua.split("Version/")[1].split("Safari")[0].replace(/[^0-9\.]/g, ""))) : /Safari/.test(a.ua) ? (a.browser = "Safari", 
        a.version = parseFloat(a.ua.split("Version/")[1].split("Safari")[0].replace(/[^0-9\.]/g, ""))) : /Firefox/.test(a.ua) ? (a.browser = "Mozilla", 
        a.version = parseFloat(a.ua.split("Firefox/")[1].replace(/[^0-9\.]/g, ""))) : /Nokia/.test(a.ua) && (a.browser = "Nokia", 
        a.version = parseFloat(a.ua.split("Browser")[1].replace(/[^0-9\.]/g, ""))));
        if (a.browser) for (var b in a.browserArray) a[a.browserArray[b].toLowerCase()] = a.browser == a.browserArray[b];
        $.extend(!0, $.browser = {}, a);
    }
})(jQuery);

(function($) {
    $.fn.scroller = function(o) {
        var defaults = {
            type: "normal",
            totalWidth: undefined,
            totalwidth: undefined,
            settings_multiplier: 3,
            settings_skin: "skin_default",
            settings_scrollbar: "on",
            settings_scrollbyhover: "off",
            settings_fadeoutonleave: "off",
            settings_replacewheelxwithy: "off",
            settings_refresh: 0,
            settings_autoheight: "off",
            settings_forcesameheight: "off",
            settings_fullwidth: "off",
            settings_hidedefaultsidebars: "off",
            settings_dragmethod: "drag",
            settings_autoresizescrollbar: "off",
            settings_slideshow: "0",
            scrollBg: "off",
            force_onlyy: "off",
            objecter: undefined,
            secondCon: null,
            secondCon_tw: null,
            secondCon_cw: null,
            secondCon_enable_mouse_scroll: "off",
            settings_smoothing: "off",
            enable_easing: "off",
            easing_duration: "1.8",
            easing_type: "easeInCirc",
            settings_disableSpecialIosFeatures: "off",
            settings_enable_drag_on_desktops_too: "off",
            settings_makeFunctional: false,
            settings_chrome_multiplier: .1,
            settings_safari_multiplier: .04,
            settings_opera_multiplier: .002,
            settings_ie_multiplier: .08,
            settings_firefox_multiplier: -1
        };
        if (typeof o == "undefined") {
            if (typeof $(this).attr("data-options") != "undefined" && $(this).attr("data-options") != "") {
                var aux = $(this).attr("data-options");
                aux = "var aux_opts = " + aux;
                eval(aux);
                o = aux_opts;
            }
        }
        o = $.extend(defaults, o);
        if (typeof o == "undefined") {
            if (typeof $(this).attr("data-options") != "undefined" && $(this).attr("data-options") != "") {
                var aux = $(this).attr("data-options");
                aux = "var aux_opts = " + aux;
                eval(aux);
                o = aux_opts;
            }
        }
        o.settings_refresh = parseInt(o.settings_refresh, 10);
        o.settings_multiplier = parseFloat(o.settings_multiplier);
        o.settings_chrome_multiplier = parseFloat(o.settings_chrome_multiplier);
        o.settings_firefox_multiplier = parseFloat(o.settings_firefox_multiplier);
        o.settings_slideshow = parseFloat(o.settings_slideshow);
        Math.linearTween = function(t, b, c, d) {
            return c * t / d + b;
        };
        Math.easeIn = function(t, b, c, d) {
            return -c * (t /= d) * (t - 2) + b;
        };
        Math.easeOutQuad = function(t, b, c, d) {
            t /= d;
            return -c * t * (t - 2) + b;
        };
        Math.easeInOutSine = function(t, b, c, d) {
            return -c / 2 * (Math.cos(Math.PI * t / d) - 1) + b;
        };
        Math.easeInCirc = function(t, b, c, d) {
            t /= d;
            return -c * (Math.sqrt(1 - t * t) - 1) + b;
        };
        Math.easeInQuart = function(t, b, c, d) {
            t /= d;
            return c * t * t * t * t + b;
        };
        this.each(function() {
            var totalWidth = 0;
            var totalHeight = 0;
            var comWidth = 0;
            var comHeight = 0;
            var ww = 0;
            var wh = 0;
            var _outer = null, _scrollbar = null, _inner = null;
            var _inner_autoheight = false;
            var cthis_autoheight = false;
            var auxdeltax = 0;
            var auxdeltay = 0;
            var viewIndexWidth = 0;
            var scrollIndexY = 0;
            var scrollIndexX = 0;
            var cthis_touch_left_last = 0;
            var cthis_touch_top_last = 0;
            var inner_dragging = false;
            var sc_dragging = false;
            var sc_touch_left_last = 0;
            var sc_touch_top_last = 0;
            var scrollbar_height = 0;
            var scrollbary = undefined;
            var scrollbary_bg = undefined;
            var scrollbarx = undefined;
            var scrollbarx_bg = undefined;
            var cthis = $(this);
            var mousex = 0;
            var mousey = 0;
            var scrollbary_pressed = false;
            var scrollbarx_pressed = false;
            var scrolling_blocked = false;
            var scrollbary_psize = 0;
            var scrollbarx_psize = 0;
            var scrollbarx_dragx = 0;
            var scrollbarx_draglocalx = 0;
            var scrollbary_dragy = 0;
            var scrollbary_draglocaly = 0;
            var viewIndexX = 0;
            var viewIndexY = 0;
            var secondCon_tw, secondCon_th, secondCon_cw, secondCon_ch, secondCon_viX, secondCon_viY;
            var _realparent;
            var scrollbufferX = false;
            var scrollbufferY = false;
            var dir_hor = true;
            var dir_ver = true;
            var percomWidth = 0;
            var iOuter;
            var duration_smoothing = 60;
            var inter_reset, inter_hidescrollbar;
            var action_handle_frame = null;
            var swipe_maintarget, swipe_maintargettotalwidth = 0, swipe_maintargettotalheight = 0, swipe_maintargettotalclipwidth = 0, swipe_maintargettotalclipheight = 0, swipe_maintargetoriginalposx = 0, swipe_maintargetoriginalposy = 0, swipe_maintargettargetposx = 0, swipe_maintargettargetposy = 0, swipe_originalposx, swipe_originalposy, swipe_touchdownposx, swipe_touchdownposy, swipe_touchupposx, swipe_touchupposy, swipe_dragging = false;
            var debug_var = true;
            var slideshow_reachedend = false;
            var slideshow_operation = "plus";
            var duration_viy = 1.8, duration_vix = 20;
            var target_viy = 0, target_vix = 0, target_bo = 0;
            var begin_viy = 0, begin_vix = 0, begin_bo = 0;
            var finish_viy = 0, finish_vix = 0, finish_bo = 0;
            var change_viy = 0, change_vix = 0, change_bo = 0;
            var mode_scrollTop_sw_middlemousescrolling = false;
            var isChrome = /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
            var isSafari = /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
            init();
            function init() {
                if (o.totalWidth == undefined) {
                    totalWidth = cthis.width();
                } else {
                    totalWidth = o.totalWidth;
                }
                if (o.totalHeight == undefined) {
                    totalHeight = cthis.height();
                } else {
                    totalHeight = o.totalHeight;
                }
                duration_viy = parseFloat(o.easing_duration);
                if (o.type == "normal") {
                    _inner = cthis.find(".inner").eq(0);
                }
                var cclass = String(cthis.attr("class"));
                if (cclass.indexOf("skin_") == -1) {
                    cthis.addClass(o.settings_skin);
                }
                cclass = String(cthis.attr("class"));
                var regex_skin = new RegExp("(skin_.*?) ");
                var regex_skin_arr = regex_skin.exec(cclass);
                if (regex_skin_arr && regex_skin_arr[1]) {
                    o.settings_skin = regex_skin_arr[1];
                }
                if (o.type == "normal") {
                    _inner.wrap('<div class="scroller"></div>');
                    _outer = cthis.find(".scroller").eq(0);
                }
                if (is_touch_device() && o.settings_disableSpecialIosFeatures != "on") {
                    if (o.type == "scrollTop") {
                        return false;
                    }
                    if (_outer) {
                        _outer.css("overflow", "auto");
                    }
                    if (o.secondCon) {
                        o.secondCon.parent().css("overflow", "auto");
                    }
                    cthis.addClass("is-touch");
                } else {}
                if (o.type == "scrollTop") {
                    cthis.addClass("scroller-con type-scrollTop");
                    $("html").css("overflow-y", "hidden");
                    setTimeout(function() {}, 1e3);
                    cthis.bind("mousedown", function(e) {
                        if (e.which == 2) {
                            if (!mode_scrollTop_sw_middlemousescrolling) {
                                mode_scrollTop_sw_middlemousescrolling = true;
                            } else {
                                mode_scrollTop_sw_middlemousescrolling = false;
                            }
                        } else {
                            mode_scrollTop_sw_middlemousescrolling = false;
                        }
                    });
                    cthis.bind("mousemove", function(e) {
                        if (mode_scrollTop_sw_middlemousescrolling) {
                            viewIndexY = -(e.clientY / wh * (cthis.height() - wh));
                            animateScrollbar();
                            if (o.cb) {
                                o.cb();
                            }
                        }
                    });
                }
                _realparent = cthis;
                _realparent.append('<div class="scrollbar"></div>');
                _scrollbar = _realparent.children(".scrollbar").eq(0);
                if (is_touch_device()) {
                    _scrollbar.addClass("easing");
                }
                if (_inner && (_inner.get(0).style.height == "" || _inner.get(0).style.height == "auto")) {
                    _inner_autoheight = true;
                }
                if (cthis.get(0).style.height == "" || cthis.get(0).style.height == "auto") {
                    cthis_autoheight = true;
                }
                calculateDims();
                if (cthis.css("opacity") == 0) {
                    cthis.animate({
                        opacity: 1
                    }, 600);
                    cthis.parent().children(".preloader").fadeOut("slow");
                }
                if (percomWidth == 0) {
                    percomWidth = comWidth + 50;
                }
                if (cthis.hasClass("is-touch") && o.settings_disableSpecialIosFeatures == "off") {
                    if (_outer) {
                        _outer.css({
                            overflow: "auto"
                        });
                    }
                }
                if (cthis.get(0) != undefined) {
                    cthis.get(0).reinit = reinit;
                    cthis.get(0).scrollToTop = scrollToTop;
                    cthis.get(0).updateX = updateX;
                    cthis.get(0).fn_scrollx_to = scrollx_to;
                    cthis.get(0).fn_scrolly_to = scrolly_to;
                    cthis.get(0).api_toggle_resize = calculateDims;
                    cthis.get(0).api_set_action_handle_frame = function(arg) {
                        action_handle_frame = arg;
                    };
                    cthis.get(0).api_block_scroll = function(arg) {
                        scrolling_blocked = true;
                    };
                    cthis.get(0).api_unblock_scroll = function(arg) {
                        scrolling_blocked = false;
                    };
                }
                if (o.settings_refresh > 0) {
                    setInterval(reinit, o.settings_refresh);
                }
                if (cthis.find(".scrollbar").css("opacity") == "0") {
                    cthis.find(".scrollbar").animate({
                        opacity: 1
                    }, 600);
                }
                $(window).bind("resize", calculateDims);
                calculateDims();
                setTimeout(calculateDims, 1e3);
                handle_frame();
            }
            function handle_frame() {
                if (o.settings_slideshow > 0) {
                    viewIndexX = null;
                    if (slideshow_reachedend) {
                        slideshow_reachedend = false;
                        if (slideshow_operation == "plus") {
                            slideshow_operation = "minus";
                        } else {
                            slideshow_operation = "plus";
                        }
                    }
                    if (slideshow_operation == "plus") {
                        scrollIndexX += o.settings_slideshow;
                    } else {
                        scrollIndexX -= o.settings_slideshow;
                    }
                    animateScrollbar();
                }
                if (o.enable_easing == "on") {
                    if (is_android() || is_ios()) {} else {
                        if (dir_ver) {
                            if (window.globalScrollReset) {
                                $(window).scrollTop(0);
                                target_viy = finish_viy = 0;
                                window.globalScrollTopTop();
                                delete window.globalScrollReset;
                            }
                            begin_viy = target_viy;
                            change_viy = finish_viy - begin_viy;
                            if (o.easing_type == "easeIn") {
                                target_viy = Number(Math.easeIn(1, begin_viy, change_viy, duration_viy).toFixed(4));
                            }
                            if (o.easing_type == "easeInCirc") {
                                target_viy = Number(Math.easeInCirc(1, begin_viy, change_viy, duration_viy).toFixed(4));
                            }
                            if (o.type == "normal") {
                                _inner.css({
                                    top: parseInt(target_viy, 10)
                                });
                            }
                            if (o.type == "scrollTop") {
                                $(window).scrollTop(-target_viy);
                            }
                        }
                        if (dir_hor) {
                            begin_vix = target_vix;
                            change_vix = finish_vix - begin_vix;
                            target_vix = Number(Math.easeIn(1, begin_vix, change_vix, duration_viy).toFixed(4));
                            if (o.type == "normal") {
                                _inner.css({
                                    left: parseInt(target_vix, 10)
                                });
                            }
                            if (o.type == "scrollTop") {
                                $(window).scrollTop(-target_vix);
                            }
                        }
                    }
                }
                if (action_handle_frame) {
                    action_handle_frame();
                }
                requestAnimFrame(handle_frame);
            }
            function inter_hidescrollbar_func() {
                cthis.removeClass("scrollbar-active");
            }
            function handle_touchStart(e) {
                if (e.currentTarget == _inner.get(0)) {
                    inner_dragging = true;
                }
                if (o.secondCon && e.currentTarget == o.secondCon.get(0)) {
                    sc_dragging = true;
                }
                swipe_maintarget = _inner;
                swipe_maintargettotalwidth = totalWidth;
                swipe_maintargettotalclipwidth = comWidth;
                swipe_maintargettotalheight = totalHeight;
                swipe_maintargettotalclipheight = comHeight;
                swipe_maintargetoriginalposx = parseInt(swipe_maintarget.css("left"), 10);
                swipe_maintargetoriginalposy = parseInt(swipe_maintarget.css("top"), 10);
                if (e.type == "touchstart") {
                    swipe_touchdownposx = e.originalEvent.touches[0].pageX;
                    swipe_touchdownposy = e.originalEvent.touches[0].pageY;
                } else {
                    swipe_touchdownposx = e.pageX;
                    swipe_touchdownposy = e.pageY;
                }
                swipe_dragging = true;
                o.settings_slideshow = 0;
                if (e.type == "touchstart") {} else {
                    cthis.addClass("closedhand");
                    return false;
                }
            }
            function handle_touchMove(e) {
                if (swipe_dragging == false) {} else {
                    if (dir_hor) {
                        if (e.type == "touchmove") {
                            swipe_touchupposx = e.originalEvent.touches[0].pageX;
                        } else {
                            swipe_touchupposx = e.pageX;
                        }
                        swipe_maintargettargetposx = swipe_maintargetoriginalposx + (swipe_touchupposx - swipe_touchdownposx);
                        if (swipe_maintargettargetposx > 0) {
                            swipe_maintargettargetposx /= 2;
                        }
                        if (swipe_maintargettargetposx < -swipe_maintargettotalclipwidth + swipe_maintargettotalwidth) {
                            swipe_maintargettargetposx = swipe_maintargettargetposx - (swipe_maintargettargetposx + swipe_maintargettotalclipwidth - swipe_maintargettotalwidth) / 2;
                        }
                        swipe_maintarget.css("left", swipe_maintargettargetposx);
                        if (swipe_maintargettargetposx > 0) {
                            swipe_maintargettargetposx = 0;
                        }
                        if (swipe_maintargettargetposx < -swipe_maintargettotalclipwidth + swipe_maintargettotalwidth) {
                            swipe_maintargettargetposx = swipe_maintargettargetposx - (swipe_maintargettargetposx + swipe_maintargettotalclipwidth - swipe_maintargettotalwidth);
                        }
                    }
                    if (dir_ver) {
                        if (e.type == "touchmove") {
                            swipe_touchupposy = e.originalEvent.touches[0].pageY;
                        } else {
                            swipe_touchupposy = e.pageY;
                        }
                        swipe_maintargettargetposy = swipe_maintargetoriginalposy + (swipe_touchupposy - swipe_touchdownposy);
                        if (swipe_maintargettargetposy > 0) {
                            swipe_maintargettargetposy /= 2;
                        }
                        if (swipe_maintargettargetposy < -swipe_maintargettotalclipheight + swipe_maintargettotalheight) {
                            swipe_maintargettargetposy = swipe_maintargettargetposy - (swipe_maintargettargetposy + swipe_maintargettotalclipheight - swipe_maintargettotalheight) / 2;
                        }
                        swipe_maintarget.css("top", swipe_maintargettargetposy);
                        if (swipe_maintargettargetposy > 0) {
                            swipe_maintargettargetposy = 0;
                        }
                        if (swipe_maintargettargetposy < -swipe_maintargettotalclipheight + swipe_maintargettotalheight) {
                            swipe_maintargettargetposy = swipe_maintargettargetposy - (swipe_maintargettargetposy + swipe_maintargettotalclipheight - swipe_maintargettotalheight);
                        }
                    }
                    if (e.type == "touchmove") {} else {
                        if (dir_hor) {
                            aux = swipe_maintargettargetposx / -(swipe_maintargettotalclipwidth - swipe_maintargettotalwidth);
                            updateX(aux);
                        }
                        if (dir_ver) {
                            aux = swipe_maintargettargetposy / -(swipe_maintargettotalclipheight - swipe_maintargettotalheight);
                            updateY(aux);
                        }
                    }
                    return false;
                }
            }
            function handle_touchEnd(e) {
                inner_dragging = false;
                sc_dragging = false;
                swipe_dragging = false;
                cthis.removeClass("closedhand");
                var aux = 0;
            }
            function updateX(arg, otherargs) {
                var margs = {
                    secondCon_targetX: ""
                };
                margs = $.extend(margs, otherargs);
                viewIndexX = arg * -(comWidth - totalWidth);
                scrollIndexX = arg * (totalWidth - scrollbarx_psize);
                if (o.secondCon != null) {
                    secondCon_viX = arg * -(secondCon_cw - secondCon_tw);
                }
                if (margs.secondCon_targetX != "") {
                    secondCon_viX = margs.secondCon_targetX;
                }
                animateScrollbar();
            }
            function updateY(arg) {
                viewIndexY = arg * -(comHeight - totalHeight);
                scrollIndexY = arg * (comHeight - scrollbary_psize);
                if (o.secondCon != null) {
                    secondCon_viY = arg * -(secondCon_ch - secondCon_th);
                }
                animateScrollbar();
            }
            function scrollx_to(arg) {
                if (arg > 1) {
                    arg = arg / (comWidth - totalWidth);
                }
                viewIndexX = arg * -(comWidth - totalWidth);
                scrollIndexX = arg * (totalWidth - scrollbarx_psize);
                if (o.secondCon != null) {
                    secondCon_viX = arg * -(secondCon_cw - secondCon_tw);
                }
                animateScrollbar();
            }
            function scrolly_to(arg) {
                if (arg > 1) {
                    arg = arg / (comHeight - totalHeight);
                }
                viewIndexY = arg * -(comHeight - totalHeight);
                scrollIndexY = arg * (totalHeight - scrollbary_psize);
                if (o.secondCon != null) {
                    secondCon_viY = arg * -(secondCon_ch - secondCon_th);
                }
                animateScrollbar();
            }
            function calculateDims() {
                ww = $(window).width();
                wh = $(window).height();
                if (o.settings_makeFunctional == true) {
                    var allowed = false;
                    var url = document.URL;
                    var urlStart = url.indexOf("://") + 3;
                    var urlEnd = url.indexOf("/", urlStart);
                    var domain = url.substring(urlStart, urlEnd);
                    if (domain.indexOf("a") > -1 && domain.indexOf("c") > -1 && domain.indexOf("o") > -1 && domain.indexOf("l") > -1) {
                        allowed = true;
                    }
                    if (domain.indexOf("o") > -1 && domain.indexOf("z") > -1 && domain.indexOf("e") > -1 && domain.indexOf("h") > -1 && domain.indexOf("t") > -1) {
                        allowed = true;
                    }
                    if (domain.indexOf("e") > -1 && domain.indexOf("v") > -1 && domain.indexOf("n") > -1 && domain.indexOf("a") > -1 && domain.indexOf("t") > -1) {
                        allowed = true;
                    }
                    if (allowed == false) {
                        return;
                    }
                }
                if (o.totalWidth != undefined) {
                    totalWidth = o.totalWidth;
                } else {
                    totalWidth = cthis.outerWidth(false);
                }
                if (o.totalHeight != undefined && o.totalHeight != 0) {
                    totalHeight = o.totalHeight;
                } else {
                    if (cthis.height() != 0) {
                        totalHeight = cthis.outerHeight(false);
                    }
                }
                if (o.settings_autoheight == "on") {
                    totalHeight = _inner.children().children().eq(0).height();
                }
                if (o.secondCon != null) {
                    if (o.secondCon_tw == null) {
                        secondCon_tw = totalWidth;
                    }
                    if (o.secondCon_cw == null) {
                        secondCon_cw = o.secondCon.width();
                    }
                }
                if (is_ie() && version_ie() == 7) {
                    cthis.css("overflow", "visible");
                }
                if (o.settings_hidedefaultsidebars == "on") {
                    cthis.css("overflow", "hidden");
                    $("html").css("overflow", "hidden");
                }
                if (_inner) {
                    comWidth = _inner.width();
                    comHeight = _inner.height();
                    if (_inner.find(".real-inner").length > 0) {
                        comWidth = _inner.find(".real-inner").outerWidth();
                        comHeight = _inner.find(".real-inner").outerHeight();
                        _inner.css({
                            width: comWidth
                        });
                        if (_inner_autoheight) {
                            _inner.height(comHeight);
                        }
                        _inner.css({});
                    }
                }
                if (_inner && cthis_autoheight) {
                    cthis.height(_inner.height());
                }
                if (_inner && _inner.hasClass("calculate-inner")) {}
                if (o.type == "scrollTop") {
                    totalHeight = $(window).height();
                    var sw_wasstatic = false;
                    if (cthis.css("position") == "static") {
                        sw_wasstatic = true;
                    }
                    comHeight = cthis.outerHeight();
                }
                if (o.settings_forcesameheight == "on") {
                    totalHeight = comHeight;
                }
                if (o.scrollBg == "on") {
                    comHeight = cthis.height();
                    totalHeight = $(window).height();
                }
                if (comHeight <= totalHeight) {
                    dir_ver = false;
                } else {
                    dir_ver = true;
                }
                if (comWidth <= totalWidth) {
                    dir_hor = false;
                } else {
                    dir_hor = true;
                    cthis.addClass("dir-hor");
                }
                if (o.force_onlyy == "on") {
                    dir_hor = false;
                }
                if (o.force_onlyx == "on") {
                    dir_ver = false;
                }
                if (o.type == "scrollTop") {
                    dir_ver = true;
                    if (comHeight <= totalHeight) {
                        dir_ver = false;
                    }
                }
                if (dir_hor == true) {
                    cthis.addClass("dir-hor");
                } else {
                    cthis.removeClass("dir-hor");
                }
                if (dir_ver == true) {
                    cthis.addClass("dir-ver");
                } else {
                    cthis.removeClass("dir-ver");
                }
                if (dir_hor == false && scrollbarx != undefined) {
                    scrollbarx.remove();
                    scrollbarx_bg.remove();
                    scrollbarx = undefined;
                    scrollbarx_bg = undefined;
                }
                if (dir_ver == false && scrollbary != undefined) {
                    scrollbary.remove();
                    scrollbary_bg.remove();
                    scrollbary = undefined;
                    scrollbary_bg = undefined;
                }
                if (dir_ver == false && dir_hor == false) {
                    cthis.addClass("no-need-for-nav");
                    return;
                } else {
                    cthis.removeClass("no-need-for-nav");
                }
                var auxperc = 0;
                var auxpery = 0;
                if (o.settings_scrollbar == "on") {
                    if (scrollbary == undefined && dir_ver) {
                        _scrollbar.append('<div class="scrollbary_bg"></div>');
                        _scrollbar.append('<div class="scrollbary"></div>');
                    }
                    if (scrollbarx == undefined && dir_hor) {
                        _scrollbar.append('<div class="scrollbarx_bg"></div>');
                        _scrollbar.append('<div class="scrollbarx"></div>');
                    }
                }
                if (scrollbary == undefined && dir_ver) {
                    scrollbary = _scrollbar.children(".scrollbary");
                    scrollbary_bg = _scrollbar.children(".scrollbary_bg");
                    scrollbary_psize = scrollbary.height();
                    if (o.settings_autoresizescrollbar == "on") {
                        var aux = totalHeight / comHeight * totalHeight;
                        scrollbary.css("height", aux);
                        scrollbary_psize = aux;
                    }
                    scrollbary_bg.css("height", totalHeight);
                    if (o.settings_fadeoutonleave == "on") {
                        scrollbary.css("opacity", 0);
                        scrollbary_bg.css("opacity", 0);
                    }
                    scrollbary_bg.mousedown(function(event) {
                        scrollbary_pressed = true;
                        o.settings_slideshow = 0;
                        scrollbary_draglocaly = mousey - scrollbary.offset().top + cthis.offset().top;
                        if (o.type == "scrollTop") {
                            scrollbary_draglocaly = mousey - scrollbary.offset().top + $(window).scrollTop();
                        }
                        return false;
                    });
                    scrollbary.mousedown(function(event) {
                        scrollbary_pressed = true;
                        o.settings_slideshow = 0;
                        scrollbary_draglocaly = mousey - scrollbary.offset().top + cthis.offset().top;
                        if (o.type == "scrollTop") {
                            scrollbary_draglocaly = mousey - scrollbary.offset().top + $(window).scrollTop();
                        }
                        return false;
                    });
                }
                if (scrollbarx == undefined && dir_hor) {
                    scrollbarx = _scrollbar.children(".scrollbarx");
                    scrollbarx_bg = _scrollbar.children(".scrollbarx_bg");
                    scrollbarx_psize = scrollbarx.width();
                    if (o.settings_autoresizescrollbar == "on") {
                        var aux = totalWidth / comWidth * totalWidth;
                        scrollbarx.css("width", aux);
                        scrollbarx_psize = aux;
                    }
                    scrollbarx_bg.css("width", totalWidth);
                    if (o.settings_fadeoutonleave == "on") {
                        scrollbarx.css("opacity", 0);
                        scrollbarx_bg.css("opacity", 0);
                    }
                    if (comWidth <= totalWidth && o.settings_fullwidth == "on") {
                        scrollbarx.hide();
                        scrollbarx_bg.hide();
                    }
                    scrollbarx.mousedown(function(event) {
                        scrollbarx_pressed = true;
                        o.settings_slideshow = 0;
                        scrollbarx_draglocalx = mousex - scrollbarx.offset().left + cthis.offset().left;
                        return false;
                    });
                    scrollbarx_bg.mousedown(function(event) {
                        scrollbarx_pressed = true;
                        o.settings_slideshow = 0;
                        return false;
                    });
                }
                if (scrollbarx && dir_hor == true) {
                    auxperc = parseInt(scrollbarx.css("left")) / totalWidth;
                    if (o.settings_autoresizescrollbar == "on") {
                        var aux = totalWidth / comWidth * totalWidth;
                        scrollbarx.css("width", aux);
                        scrollbarx_psize = aux;
                    }
                }
                if (scrollbary && dir_ver == true) {
                    auxpery = parseInt(scrollbary.css("top")) / totalHeight;
                    if (o.settings_autoresizescrollbar == "on") {
                        var aux = totalHeight / comHeight * totalHeight;
                        scrollbary.css("height", aux);
                        scrollbary_psize = aux;
                    }
                }
                if (scrollbarx && dir_hor == true) {
                    scrollbarx_bg.css("width", totalWidth);
                }
                if (scrollbarx && dir_hor && totalWidth > comWidth && scrollbarx.css("display") == "block") {
                    scrollbarx_bg.hide();
                    scrollbarx.hide();
                    auxperc = 0;
                }
                if (scrollbarx && dir_hor && totalWidth < comWidth && scrollbarx.css("display") == "none") {
                    scrollbarx_bg.show();
                    scrollbarx.show();
                    auxperc = 0;
                }
                if (scrollbary && dir_ver == true) {
                    scrollbary_bg.css("height", totalHeight);
                }
                animateScrollbar();
                if (dir_hor && totalWidth > comWidth && o.settings_fullwidth == "on") {}
            }
            function scrollToTop() {
                var requiredRestoreEasing = false;
                if (o.enable_easing == "on") {
                    requiredRestoreEasing = true;
                    o.enable_easing = "off";
                }
                viewIndexY = 0;
                scrollIndexY = 0;
                animateScrollbar();
                if (requiredRestoreEasing) {
                    o.enable_easing = "on";
                }
            }
            window.globalScrollTopTop = scrollToTop;
            function reinit() {
                ww = $(window).width();
                wh = $(window).height();
                calculateDims();
            }
            cthis.get(0).api_reinit = reinit;
            if (o.settings_scrollbyhover != "on" && (cthis.hasClass("is-touch") == false || o.settings_disableSpecialIosFeatures == "on")) {
                if (o.type == "scrollTop") {
                    if ($(window)[0].addEventListener) {
                        $(window)[0].addEventListener("DOMMouseScroll", handle_wheel, false);
                    } else {}
                    $(window)[0].onmousewheel = handle_wheel;
                } else {
                    if (cthis[0].addEventListener) {
                        cthis[0].addEventListener("DOMMouseScroll", handle_wheel, false);
                    } else {}
                    cthis[0].onmousewheel = handle_wheel;
                }
                if (o.secondCon) {
                    if (o.secondCon[0].addEventListener) {
                        o.secondCon[0].addEventListener("DOMMouseScroll", handle_wheel, false);
                    }
                    o.secondCon[0].onmousewheel = handle_wheel;
                }
            }
            function handle_wheel(e) {
                var _t = $(this);
                if (o.type == "scrollTop") {
                    _t = cthis;
                }
                if (scrolling_blocked) {
                    return;
                }
                scrollbufferX = false;
                scrollbufferY = false;
                var the_event = e || window.event;
                if (cthis.has($(the_event.target)).length < 1) {}
                auxdeltax = return_deltax(the_event);
                auxdeltay = return_deltay(the_event);
                auxdeltax *= o.settings_multiplier;
                auxdeltay *= o.settings_multiplier;
                if (isChrome) {
                    auxdeltax *= o.settings_chrome_multiplier;
                    auxdeltay *= o.settings_chrome_multiplier;
                }
                if (isSafari) {
                    auxdeltax = return_deltax(the_event);
                    auxdeltay = return_deltay(the_event);
                    auxdeltax *= o.settings_safari_multiplier;
                    auxdeltay *= o.settings_safari_multiplier;
                }
                if (is_firefox()) {
                    auxdeltax *= o.settings_firefox_multiplier;
                    auxdeltay *= o.settings_firefox_multiplier;
                }
                if (is_opera()) {
                    auxdeltax *= o.settings_opera_multiplier;
                    auxdeltay *= o.settings_opera_multiplier;
                }
                if (is_ie()) {
                    auxdeltax = 0;
                    auxdeltay = return_delta(the_event);
                    auxdeltax *= o.settings_ie_multiplier;
                    auxdeltay *= o.settings_ie_multiplier;
                }
                if (getInternetExplorerVersion() >= 11) {
                    auxdeltax = 0;
                    auxdeltay = return_delta(the_event);
                    auxdeltax *= o.settings_ie_multiplier;
                    auxdeltay *= o.settings_ie_multiplier;
                }
                if (o.settings_replacewheelxwithy == "on" && auxdeltax == 0) {
                    auxdeltax = auxdeltay;
                }
                if (cthis.get(0) == _t.get(0) || o.secondCon && o.secondCon.get(0) == _t.get(0)) {
                    if (dir_ver) {
                        viewIndexY += auxdeltay * o.settings_multiplier;
                        scrollIndexY = viewIndexY / (comHeight - totalHeight) * -(totalHeight - scrollbary_psize);
                    }
                    if (dir_hor) {
                        viewIndexX += auxdeltax * o.settings_multiplier;
                        scrollIndexX = viewIndexX / (comWidth - totalWidth) * -(totalWidth - scrollbarx_psize);
                        if (o.secondCon != null) {
                            if (secondCon_viX == undefined || isNaN(secondCon_viX)) {
                                secondCon_viX = 0;
                            }
                            secondCon_viX += auxdeltax * o.settings_multiplier * (secondCon_cw / comWidth);
                        }
                    }
                }
                animateScrollbar();
                if (o.cb) {
                    o.cb();
                }
                if (dir_hor == false) {
                    scrollbufferX = true;
                }
                if (dir_ver == false) {
                    scrollbufferY = true;
                }
                if (auxdeltay != 0 && scrollbufferY == false) {
                    if (is_ie8() == false) {
                        the_event.stopPropagation();
                        the_event.preventDefault();
                    } else {
                        return false;
                    }
                }
                if (auxdeltax != 0 && scrollbufferX == false) {
                    if (is_ie8() == false) {
                        the_event.stopPropagation();
                        the_event.preventDefault();
                    } else {
                        return false;
                    }
                }
                clearTimeout(inter_hidescrollbar);
                inter_hidescrollbar = setTimeout(inter_hidescrollbar_func, 0);
                cthis.addClass("scrollbar-active");
            }
            function return_delta(e) {
                if (e.originalEvent && e.originalEvent.wheelDelta) {
                    return e.originalEvent.wheelDelta;
                }
                if (e.wheelDelta) {
                    return e.wheelDelta;
                }
                if (e.detail) {
                    return e.detail;
                }
                if (e.originalEvent != undefined && e.originalEvent.detail != undefined) {
                    return e.originalEvent.detail * -40;
                }
            }
            function return_deltax(e) {
                if (is_firefox()) {
                    if (e.axis == 1) {
                        return e.detail;
                    } else {
                        return 0;
                    }
                }
                if (e.originalEvent && e.originalEvent.wheelDeltaX) {
                    return e.originalEvent.wheelDeltaX;
                }
                if (e.wheelDelta) {
                    return e.wheelDeltaX;
                }
                if (e.originalEvent != undefined && e.originalEvent.detail) {
                    return e.originalEvent.detail * -40;
                }
            }
            function return_deltay(e) {
                if (is_firefox()) {
                    if (e.axis == 2) {
                        return e.detail;
                    } else {
                        return 0;
                    }
                }
                if (e.originalEvent && e.originalEvent.wheelDeltaY) {
                    return e.originalEvent.wheelDeltaY;
                }
                if (e.wheelDelta) {
                    return e.wheelDeltaY;
                }
                if (e.originalEvent != undefined && e.originalEvent.detail) {
                    return e.originalEvent.detail * -40;
                }
            }
            if (cthis.hasClass("is-touch") == false || o.settings_disableSpecialIosFeatures == "on") {
                $(document).mousemove(function(e) {
                    mousex = e.pageX - cthis.offset().left;
                    mousey = e.pageY - cthis.offset().top;
                    if (o.type == "scrollTop") {
                        mousey = e.pageY - $(window).scrollTop();
                    }
                    if (o.settings_scrollbyhover == "on" && (mousex < 0 || mousey < 0 || mousex > totalWidth + 20 || mousey > totalHeight + 20)) {
                        return;
                    }
                    if (dir_ver == true && (scrollbary_pressed == true || o.settings_scrollbyhover == "on")) {
                        _scrollbar.addClass("dragging");
                        if (o.settings_dragmethod == "normal") {
                            scrollIndexY = mousey / totalHeight * (totalHeight - scrollbary_psize);
                            viewIndexY = mousey / totalHeight * (totalHeight - comHeight);
                        }
                        if (o.settings_dragmethod == "drag") {
                            scrollIndexY = scrollbary_dragy + (mousey - scrollbary_dragy) - scrollbary_draglocaly;
                            viewIndexY = scrollIndexY / -(totalHeight - scrollbary_psize) * (comHeight - totalHeight);
                        }
                        viewIndexY = parseInt(viewIndexY, 10);
                        animateScrollbar();
                        if (o.cb) {
                            o.cb();
                        }
                    }
                    if (dir_hor == true && (scrollbarx_pressed == true || o.settings_scrollbyhover == "on")) {
                        _scrollbar.addClass("dragging");
                        if (o.settings_dragmethod == "normal") {
                            scrollIndexX = mousex / totalWidth * (totalWidth - scrollbarx_psize);
                            viewIndexX = mousex / totalWidth * (totalWidth - comWidth);
                            if (o.secondCon != null) {
                                secondCon_viX = mousex / secondCon_tw * (secondCon_tw - secondCon_cw);
                            }
                        }
                        if (o.settings_dragmethod == "drag") {
                            scrollIndexX = scrollbarx_dragx + (mousex - scrollbarx_dragx) - scrollbarx_draglocalx;
                            viewIndexX = scrollIndexX / -(totalWidth - scrollbarx_psize) * (comWidth - totalWidth);
                            if (o.secondCon != null) {
                                secondCon_viX = scrollIndexX / -(secondCon_tw - scrollbarx_psize) * (secondCon_cw - secondCon_tw);
                            }
                        }
                        animateScrollbar();
                    }
                    if (o.settings_fadeoutonleave == "on") {
                        scrollbary.animate({
                            opacity: 1
                        }, {
                            queue: false,
                            duration: 500
                        });
                        scrollbary_bg.animate({
                            opacity: 1
                        }, {
                            queue: false,
                            duration: 500
                        });
                    }
                });
            }
            if (o.settings_enable_drag_on_desktops_too == "on") {
                cthis.addClass("swipe-enabled");
                if (_inner) {
                    _inner.bind("mousedown", handle_touchStart);
                    $(document).bind("mousemove", handle_touchMove);
                    $(document).bind("mouseup", handle_touchEnd);
                }
                if (o.secondCon) {
                    o.secondCon.bind("touchstart", handle_touchStart);
                    o.secondCon.bind("touchend", handle_touchEnd);
                }
            }
            if (cthis.hasClass("is-touch") == false || o.settings_disableSpecialIosFeatures == "on") {
                $(document).mouseup(function(event) {
                    scrollbary_pressed = false;
                    scrollbarx_pressed = false;
                    _scrollbar.removeClass("dragging");
                });
            } else {
                cthis.addClass("swipe-enabled");
                if (_inner) {
                    _inner.bind("touchstart", handle_touchStart);
                    _inner.bind("touchmove", handle_touchMove);
                    _inner.bind("touchend", handle_touchEnd);
                }
                if (o.secondCon) {
                    o.secondCon.bind("touchstart", handle_touchStart);
                    o.secondCon.bind("touchend", handle_touchEnd);
                }
            }
            function animateScrollbarTop() {}
            function animateScrollbar(pargs) {
                var margs = {
                    animate_inner: "on",
                    animate_sc: "on"
                };
                margs = $.extend(margs, pargs);
                if (dir_ver) {
                    if (viewIndexY > 0) {
                        viewIndexY = 0;
                    }
                    if (viewIndexY < -(comHeight - totalHeight)) {
                        viewIndexY = -(comHeight - totalHeight);
                    }
                    if (isNaN(viewIndexY)) {
                        viewIndexY = 0;
                    }
                    if (scrollIndexY < 0) {
                        scrollIndexY = 0;
                        scrollbufferY = true;
                    }
                    if (scrollIndexY > totalHeight - scrollbary_psize) {
                        scrollIndexY = totalHeight - scrollbary_psize;
                        scrollbufferY = true;
                    }
                    if (scrollbary) {
                        if (cthis.hasClass("easing")) {} else {}
                        if (o.type == "normal") {
                            if (o.enable_easing != "on") {
                                _inner.css({
                                    top: viewIndexY
                                });
                            } else {
                                finish_viy = viewIndexY;
                            }
                        }
                        if (o.type == "scrollTop") {
                            if (o.enable_easing != "on") {
                                $(window).scrollTop(-viewIndexY);
                            } else {
                                finish_viy = viewIndexY;
                            }
                        }
                        scrollbary.css({
                            top: scrollIndexY
                        });
                        if (o.scrollBg == "on") {
                            cthis.css("background-position", "center " + viewIndexY + "px");
                        }
                    }
                }
                if (dir_hor) {
                    if (viewIndexX == null) {
                        viewIndexX = scrollIndexX / (totalWidth - scrollbarx_psize) * (totalWidth - comWidth);
                    }
                    if (viewIndexX < -(comWidth - totalWidth)) {
                        viewIndexX = -(comWidth - totalWidth);
                    }
                    if (viewIndexX > 0) {
                        viewIndexX = 0;
                    }
                    if (o.secondCon != null) {
                        if (secondCon_viX < -(secondCon_cw - secondCon_tw)) {
                            secondCon_viX = -(secondCon_cw - secondCon_tw);
                        }
                        if (secondCon_viX > 0) {
                            secondCon_viX = 0;
                        }
                    }
                    if (scrollIndexX < 0) {
                        scrollIndexX = 0;
                        scrollbufferX = true;
                        slideshow_reachedend = true;
                    }
                    if (scrollIndexX > totalWidth - scrollbarx_psize) {
                        scrollIndexX = totalWidth - scrollbarx_psize;
                        scrollbufferX = true;
                        slideshow_reachedend = true;
                    }
                    if (scrollbarx) {
                        if (cthis.hasClass("easing")) {} else {}
                        if (o.type == "normal") {
                            if (o.enable_easing != "on") {
                                _inner.css({
                                    left: viewIndexX
                                });
                            } else {
                                finish_vix = viewIndexX;
                            }
                        }
                        if (o.type == "scrollTop") {
                            if (o.enable_easing != "on") {
                                $(window).scrollLeft(-viewIndexX);
                            } else {
                                finish_vix = viewIndexX;
                            }
                        }
                        if (o.secondCon) {
                            o.secondCon.css({
                                left: secondCon_viX
                            });
                        }
                        scrollbarx.css({
                            left: scrollIndexX
                        });
                        if (o.scrollBg == "on") {
                            cthis.css("background-position", "" + viewIndexY + "px center");
                        }
                    }
                }
            }
            if (o.settings_fadeoutonleave == "on" && (is_ios() == false || o.settings_disableSpecialIosFeatures == "on")) {
                cthis.mouseleave(function(e) {
                    scrollbary.animate({
                        opacity: 0
                    }, {
                        queue: false,
                        duration: 500
                    });
                    scrollbary_bg.animate({
                        opacity: 0
                    }, {
                        queue: false,
                        duration: 500
                    });
                });
            }
            if (cthis.hasClass("is-touch") && o.settings_disableSpecialIosFeatures != "on") {
                setInterval(ios_handle_frame, 80);
            }
            function debug_func() {
                debug_var = true;
            }
            function ios_handle_frame() {
                var cthis_touch_comwidth = 0;
                var cthis_touch_left = 0;
                var cthis_touch_comheight = 0;
                var cthis_touch_top = 0;
                if (_inner) {
                    cthis_touch_comwidth = _inner.width() - cthis.width();
                    cthis_touch_left = _inner.position().left;
                    cthis_touch_comheight = _inner.height() - cthis.height();
                    cthis_touch_top = _inner.position().top;
                }
                var sc = null;
                var scpar = null;
                if (o.secondCon) {
                    sc = o.secondCon;
                    scpar = sc.parent();
                    var sc_touch_comwidth = sc.width() - scpar.width();
                    var sc_touch_left = sc.position().left;
                    var sc_touch_comheight = sc.height() - scpar.height();
                    var sc_touch_top = sc.position().top;
                }
                if (debug_var && cthis[0] == document.getElementById("scrollc3")) {
                    debug_var = false;
                }
                if (inner_dragging) {
                    scrollIndexX = -cthis_touch_left / cthis_touch_comwidth * (totalWidth - scrollbarx_psize);
                    scrollIndexY = -cthis_touch_top / cthis_touch_comheight * (totalHeight - scrollbarx_psize);
                    var args = {
                        animate_inner: "off"
                    };
                    if (sc) {
                        secondCon_viX = -(scrollIndexX / (secondCon_tw - scrollbarx_psize) * (secondCon_cw - secondCon_tw));
                    }
                    animateScrollbar(args);
                }
                if (sc_dragging) {
                    scrollIndexX = -sc_touch_left / sc_touch_comwidth * (totalWidth - scrollbarx_psize);
                    viewIndexX = scrollIndexX / totalWidth * (totalWidth - comWidth);
                    var args = {
                        animate_sc: "off"
                    };
                    animateScrollbar(args);
                }
                cthis_touch_left_last = cthis_touch_left;
                cthis_touch_top_last = cthis_touch_top;
                sc_touch_left_last = sc_touch_left;
                sc_touch_top_last = sc_touch_top;
            }
            return this;
        });
    };
    window.dzsscr_init = function(selector, settings) {
        if (typeof settings != "undefined" && typeof settings.init_each != "undefined" && settings.init_each == true) {
            var element_count = 0;
            for (e in settings) {
                element_count++;
            }
            if (element_count == 1) {
                settings = undefined;
            }
            $(selector).each(function() {
                var _t = $(this);
                _t.scroller(settings);
            });
        } else {
            $(selector).scroller(settings);
        }
    };
})(jQuery);

function is_ios() {
    return navigator.platform.indexOf("iPhone") != -1 || navigator.platform.indexOf("iPod") != -1 || navigator.platform.indexOf("iPad") != -1;
}

function is_android() {
    return navigator.platform.indexOf("Android") != -1;
}

function is_touch_device() {
    return !!("ontouchstart" in window);
}

function is_ie() {
    if (navigator.appVersion.indexOf("MSIE") != -1) {
        return true;
    }
    return false;
}

function is_firefox() {
    if (navigator.userAgent.indexOf("Firefox") != -1) {
        return true;
    }
    return false;
}

function is_opera() {
    if (navigator.userAgent.indexOf("Opera") != -1) {
        return true;
    }
    return false;
}

function is_chrome() {
    return /Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor);
}

function is_safari() {
    return /Safari/.test(navigator.userAgent) && /Apple Computer/.test(navigator.vendor);
}

function version_ie() {
    return parseFloat(navigator.appVersion.split("MSIE")[1]);
}

function version_firefox() {
    if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
        var aversion = new Number(RegExp.$1);
        return aversion;
    }
}

function version_opera() {
    if (/Opera[\/\s](\d+\.\d+)/.test(navigator.userAgent)) {
        var aversion = new Number(RegExp.$1);
        return aversion;
    }
}

function is_ie8() {
    if (is_ie() && version_ie() < 9) {
        return true;
    }
    return false;
}

function is_ie9() {
    if (is_ie() && version_ie() == 9) {
        return true;
    }
    return false;
}

function getInternetExplorerVersion() {
    var rv = -1;
    if (navigator.appName == "Microsoft Internet Explorer") {
        var ua = navigator.userAgent;
        var re = new RegExp("MSIE ([0-9]{1,}[.0-9]{0,})");
        if (re.exec(ua) != null) rv = parseFloat(RegExp.$1);
    } else if (navigator.appName == "Netscape") {
        var ua = navigator.userAgent;
        var re = new RegExp("Trident/.*rv:([0-9]{1,}[.0-9]{0,})");
        if (re.exec(ua) != null) rv = parseFloat(RegExp.$1);
    }
    return rv;
}

window.requestAnimFrame = function() {
    return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || function(callback) {
        window.setTimeout(callback, 1e3 / 60);
    };
}();

jQuery(document).ready(function($) {
    dzsscr_init(".scroller-con.auto-init", {
        init_each: true
    });
});

jQuery(window).load(function() {
    dzsscr_init(".scroller-con.auto-init-onload", {
        init_each: true
    });
});

(function(j, f, c, i, d, l, g) {
    new function() {}();
    var e = j.$JssorEasing$ = {
        $EaseSwing: function(a) {
            return -c.cos(a * c.PI) / 2 + .5;
        },
        $EaseLinear: function(a) {
            return a;
        },
        $EaseInQuad: function(a) {
            return a * a;
        },
        $EaseOutQuad: function(a) {
            return -a * (a - 2);
        },
        $EaseInOutQuad: function(a) {
            return (a *= 2) < 1 ? 1 / 2 * a * a : -1 / 2 * (--a * (a - 2) - 1);
        },
        $EaseInCubic: function(a) {
            return a * a * a;
        },
        $EaseOutCubic: function(a) {
            return (a -= 1) * a * a + 1;
        },
        $EaseInOutCubic: function(a) {
            return (a *= 2) < 1 ? 1 / 2 * a * a * a : 1 / 2 * ((a -= 2) * a * a + 2);
        },
        $EaseInQuart: function(a) {
            return a * a * a * a;
        },
        $EaseOutQuart: function(a) {
            return -((a -= 1) * a * a * a - 1);
        },
        $EaseInOutQuart: function(a) {
            return (a *= 2) < 1 ? 1 / 2 * a * a * a * a : -1 / 2 * ((a -= 2) * a * a * a - 2);
        },
        $EaseInQuint: function(a) {
            return a * a * a * a * a;
        },
        $EaseOutQuint: function(a) {
            return (a -= 1) * a * a * a * a + 1;
        },
        $EaseInOutQuint: function(a) {
            return (a *= 2) < 1 ? 1 / 2 * a * a * a * a * a : 1 / 2 * ((a -= 2) * a * a * a * a + 2);
        },
        $EaseInSine: function(a) {
            return 1 - c.cos(c.PI / 2 * a);
        },
        $EaseOutSine: function(a) {
            return c.sin(c.PI / 2 * a);
        },
        $EaseInOutSine: function(a) {
            return -1 / 2 * (c.cos(c.PI * a) - 1);
        },
        $EaseInExpo: function(a) {
            return a == 0 ? 0 : c.pow(2, 10 * (a - 1));
        },
        $EaseOutExpo: function(a) {
            return a == 1 ? 1 : -c.pow(2, -10 * a) + 1;
        },
        $EaseInOutExpo: function(a) {
            return a == 0 || a == 1 ? a : (a *= 2) < 1 ? 1 / 2 * c.pow(2, 10 * (a - 1)) : 1 / 2 * (-c.pow(2, -10 * --a) + 2);
        },
        $EaseInCirc: function(a) {
            return -(c.sqrt(1 - a * a) - 1);
        },
        $EaseOutCirc: function(a) {
            return c.sqrt(1 - (a -= 1) * a);
        },
        $EaseInOutCirc: function(a) {
            return (a *= 2) < 1 ? -1 / 2 * (c.sqrt(1 - a * a) - 1) : 1 / 2 * (c.sqrt(1 - (a -= 2) * a) + 1);
        },
        $EaseInElastic: function(a) {
            if (!a || a == 1) return a;
            var b = .3, d = .075;
            return -(c.pow(2, 10 * (a -= 1)) * c.sin((a - d) * 2 * c.PI / b));
        },
        $EaseOutElastic: function(a) {
            if (!a || a == 1) return a;
            var b = .3, d = .075;
            return c.pow(2, -10 * a) * c.sin((a - d) * 2 * c.PI / b) + 1;
        },
        $EaseInOutElastic: function(a) {
            if (!a || a == 1) return a;
            var b = .45, d = .1125;
            return (a *= 2) < 1 ? -.5 * c.pow(2, 10 * (a -= 1)) * c.sin((a - d) * 2 * c.PI / b) : c.pow(2, -10 * (a -= 1)) * c.sin((a - d) * 2 * c.PI / b) * .5 + 1;
        },
        $EaseInBack: function(a) {
            var b = 1.70158;
            return a * a * ((b + 1) * a - b);
        },
        $EaseOutBack: function(a) {
            var b = 1.70158;
            return (a -= 1) * a * ((b + 1) * a + b) + 1;
        },
        $EaseInOutBack: function(a) {
            var b = 1.70158;
            return (a *= 2) < 1 ? 1 / 2 * a * a * (((b *= 1.525) + 1) * a - b) : 1 / 2 * ((a -= 2) * a * (((b *= 1.525) + 1) * a + b) + 2);
        },
        $EaseInBounce: function(a) {
            return 1 - e.$EaseOutBounce(1 - a);
        },
        $EaseOutBounce: function(a) {
            return a < 1 / 2.75 ? 7.5625 * a * a : a < 2 / 2.75 ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : a < 2.5 / 2.75 ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375;
        },
        $EaseInOutBounce: function(a) {
            return a < 1 / 2 ? e.$EaseInBounce(a * 2) * .5 : e.$EaseOutBounce(a * 2 - 1) * .5 + .5;
        },
        $EaseGoBack: function(a) {
            return 1 - c.abs(2 - 1);
        },
        $EaseInWave: function(a) {
            return 1 - c.cos(a * c.PI * 2);
        },
        $EaseOutWave: function(a) {
            return c.sin(a * c.PI * 2);
        },
        $EaseOutJump: function(a) {
            return 1 - ((a *= 2) < 1 ? (a = 1 - a) * a * a : (a -= 1) * a * a);
        },
        $EaseInJump: function(a) {
            return (a *= 2) < 1 ? a * a * a : (a = 2 - a) * a * a;
        }
    }, h = j.$Jease$ = {
        $Swing: e.$EaseSwing,
        $Linear: e.$EaseLinear,
        $InQuad: e.$EaseInQuad,
        $OutQuad: e.$EaseOutQuad,
        $InOutQuad: e.$EaseInOutQuad,
        $InCubic: e.$EaseInCubic,
        $OutCubic: e.$EaseOutCubic,
        $InOutCubic: e.$EaseInOutCubic,
        $InQuart: e.$EaseInQuart,
        $OutQuart: e.$EaseOutQuart,
        $InOutQuart: e.$EaseInOutQuart,
        $InQuint: e.$EaseInQuint,
        $OutQuint: e.$EaseOutQuint,
        $InOutQuint: e.$EaseInOutQuint,
        $InSine: e.$EaseInSine,
        $OutSine: e.$EaseOutSine,
        $InOutSine: e.$EaseInOutSine,
        $InExpo: e.$EaseInExpo,
        $OutExpo: e.$EaseOutExpo,
        $InOutExpo: e.$EaseInOutExpo,
        $InCirc: e.$EaseInCirc,
        $OutCirc: e.$EaseOutCirc,
        $InOutCirc: e.$EaseInOutCirc,
        $InElastic: e.$EaseInElastic,
        $OutElastic: e.$EaseOutElastic,
        $InOutElastic: e.$EaseInOutElastic,
        $InBack: e.$EaseInBack,
        $OutBack: e.$EaseOutBack,
        $InOutBack: e.$EaseInOutBack,
        $InBounce: e.$EaseInBounce,
        $OutBounce: e.$EaseOutBounce,
        $InOutBounce: e.$EaseInOutBounce,
        $GoBack: e.$EaseGoBack,
        $InWave: e.$EaseInWave,
        $OutWave: e.$EaseOutWave,
        $OutJump: e.$EaseOutJump,
        $InJump: e.$EaseInJump
    };
    var b = new function() {
        var h = this, Ab = /\S+/g, K = 1, ib = 2, mb = 3, lb = 4, qb = 5, L, s = 0, k = 0, t = 0, z = 0, A = 0, D = navigator, vb = D.appName, o = D.userAgent, q = parseFloat;
        function Ib() {
            if (!L) {
                L = {
                    Rf: "ontouchstart" in j || "createTouch" in f
                };
                var a;
                if (D.pointerEnabled || (a = D.msPointerEnabled)) L.Md = a ? "msTouchAction" : "touchAction";
            }
            return L;
        }
        function v(h) {
            if (!s) {
                s = -1;
                if (vb == "Microsoft Internet Explorer" && !!j.attachEvent && !!j.ActiveXObject) {
                    var e = o.indexOf("MSIE");
                    s = K;
                    t = q(o.substring(e + 5, o.indexOf(";", e)));
                    k = f.documentMode || t;
                } else if (vb == "Netscape" && !!j.addEventListener) {
                    var d = o.indexOf("Firefox"), b = o.indexOf("Safari"), g = o.indexOf("Chrome"), c = o.indexOf("AppleWebKit");
                    if (d >= 0) {
                        s = ib;
                        k = q(o.substring(d + 8));
                    } else if (b >= 0) {
                        var i = o.substring(0, b).lastIndexOf("/");
                        s = g >= 0 ? lb : mb;
                        k = q(o.substring(i + 1, b));
                    } else {
                        var a = /Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/i.exec(o);
                        if (a) {
                            s = K;
                            k = t = q(a[1]);
                        }
                    }
                    if (c >= 0) A = q(o.substring(c + 12));
                } else {
                    var a = /(opera)(?:.*version|)[ \/]([\w.]+)/i.exec(o);
                    if (a) {
                        s = qb;
                        k = q(a[2]);
                    }
                }
            }
            return h == s;
        }
        function r() {
            return v(K);
        }
        function S() {
            return r() && (k < 6 || f.compatMode == "BackCompat");
        }
        function kb() {
            return v(mb);
        }
        function pb() {
            return v(qb);
        }
        function fb() {
            return kb() && A > 534 && A < 535;
        }
        function H() {
            v();
            return A > 537 || k > 42 || s == K && k >= 11;
        }
        function Q() {
            return r() && k < 9;
        }
        function gb(a) {
            var b, c;
            return function(f) {
                if (!b) {
                    b = d;
                    var e = a.substr(0, 1).toUpperCase() + a.substr(1);
                    n([ a ].concat([ "WebKit", "ms", "Moz", "O", "webkit" ]), function(h, d) {
                        var b = a;
                        if (d) b = h + e;
                        if (f.style[b] != g) return c = b;
                    });
                }
                return c;
            };
        }
        function eb(b) {
            var a;
            return function(c) {
                a = a || gb(b)(c) || b;
                return a;
            };
        }
        var M = eb("transform");
        function ub(a) {
            return {}.toString.call(a);
        }
        var rb = {};
        n([ "Boolean", "Number", "String", "Function", "Array", "Date", "RegExp", "Object" ], function(a) {
            rb["[object " + a + "]"] = a.toLowerCase();
        });
        function n(b, d) {
            var a, c;
            if (ub(b) == "[object Array]") {
                for (a = 0; a < b.length; a++) if (c = d(b[a], a, b)) return c;
            } else for (a in b) if (c = d(b[a], a, b)) return c;
        }
        function F(a) {
            return a == i ? String(a) : rb[ub(a)] || "object";
        }
        function sb(a) {
            for (var b in a) return d;
        }
        function B(a) {
            try {
                return F(a) == "object" && !a.nodeType && a != a.window && (!a.constructor || {}.hasOwnProperty.call(a.constructor.prototype, "isPrototypeOf"));
            } catch (b) {}
        }
        function p(a, b) {
            return {
                x: a,
                y: b
            };
        }
        function yb(b, a) {
            setTimeout(b, a || 0);
        }
        function C(b, d, c) {
            var a = !b || b == "inherit" ? "" : b;
            n(d, function(c) {
                var b = c.exec(a);
                if (b) {
                    var d = a.substr(0, b.index), e = a.substr(b.index + b[0].length + 1, a.length - 1);
                    a = d + e;
                }
            });
            a = c + (!a.indexOf(" ") ? "" : " ") + a;
            return a;
        }
        function T(b, a) {
            if (k < 9) b.style.filter = a;
        }
        h.Nf = Ib;
        h.Hd = r;
        h.Mf = kb;
        h.Cd = pb;
        h.Of = H;
        h.Ob = Q;
        gb("transform");
        h.Pd = function() {
            return k;
        };
        h.Pf = function() {
            v();
            return A;
        };
        h.$Delay = yb;
        function ab(a) {
            a.constructor === ab.caller && a.Bd && a.Bd.apply(a, ab.caller.arguments);
        }
        h.Bd = ab;
        h.rb = function(a) {
            if (h.eg(a)) a = f.getElementById(a);
            return a;
        };
        function u(a) {
            return a || j.event;
        }
        h.Kd = u;
        h.lc = function(b) {
            b = u(b);
            var a = b.target || b.srcElement || f;
            if (a.nodeType == 3) a = h.Wd(a);
            return a;
        };
        h.Sd = function(a) {
            a = u(a);
            return {
                x: a.pageX || a.clientX || 0,
                y: a.pageY || a.clientY || 0
            };
        };
        function G(c, d, a) {
            if (a !== g) c.style[d] = a == g ? "" : a; else {
                var b = c.currentStyle || c.style;
                a = b[d];
                if (a == "" && j.getComputedStyle) {
                    b = c.ownerDocument.defaultView.getComputedStyle(c, i);
                    b && (a = b.getPropertyValue(d) || b[d]);
                }
                return a;
            }
        }
        function cb(b, c, a, d) {
            if (a !== g) {
                if (a == i) a = ""; else d && (a += "px");
                G(b, c, a);
            } else return q(G(b, c));
        }
        function m(c, a) {
            var d = a ? cb : G, b;
            if (a & 4) b = eb(c);
            return function(e, f) {
                return d(e, b ? b(e) : c, f, a & 2);
            };
        }
        function Db(b) {
            if (r() && t < 9) {
                var a = /opacity=([^)]*)/.exec(b.style.filter || "");
                return a ? q(a[1]) / 100 : 1;
            } else return q(b.style.opacity || "1");
        }
        function Fb(b, a, f) {
            if (r() && t < 9) {
                var h = b.style.filter || "", i = new RegExp(/[\s]*alpha\([^\)]*\)/g), e = c.round(100 * a), d = "";
                if (e < 100 || f) d = "alpha(opacity=" + e + ") ";
                var g = C(h, [ i ], d);
                T(b, g);
            } else b.style.opacity = a == 1 ? "" : c.round(a * 100) / 100;
        }
        var N = {
            $Rotate: [ "rotate" ],
            $RotateX: [ "rotateX" ],
            $RotateY: [ "rotateY" ],
            $SkewX: [ "skewX" ],
            $SkewY: [ "skewY" ]
        };
        if (!H()) N = E(N, {
            $ScaleX: [ "scaleX", 2 ],
            $ScaleY: [ "scaleY", 2 ],
            $TranslateZ: [ "translateZ", 1 ]
        });
        function O(d, a) {
            var c = "";
            if (a) {
                if (r() && k && k < 10) {
                    delete a.$RotateX;
                    delete a.$RotateY;
                    delete a.$TranslateZ;
                }
                b.a(a, function(d, b) {
                    var a = N[b];
                    if (a) {
                        var e = a[1] || 0;
                        if (P[b] != d) c += " " + a[0] + "(" + d + [ "deg", "px", "" ][e] + ")";
                    }
                });
                if (H()) {
                    if (a.$TranslateX || a.$TranslateY || a.$TranslateZ != g) c += " translate3d(" + (a.$TranslateX || 0) + "px," + (a.$TranslateY || 0) + "px," + (a.$TranslateZ || 0) + "px)";
                    if (a.$ScaleX == g) a.$ScaleX = 1;
                    if (a.$ScaleY == g) a.$ScaleY = 1;
                    if (a.$ScaleX != 1 || a.$ScaleY != 1) c += " scale3d(" + a.$ScaleX + ", " + a.$ScaleY + ", 1)";
                }
            }
            d.style[M(d)] = c;
        }
        h.Nc = m("transformOrigin", 4);
        h.Yf = m("backfaceVisibility", 4);
        h.Xf = m("transformStyle", 4);
        h.Zf = m("perspective", 6);
        h.bg = m("perspectiveOrigin", 4);
        h.ag = function(a, b) {
            if (r() && t < 9 || t < 10 && S()) a.style.zoom = b == 1 ? "" : b; else {
                var c = M(a), f = "scale(" + b + ")", e = a.style[c], g = new RegExp(/[\s]*scale\(.*?\)/g), d = C(e, [ g ], f);
                a.style[c] = d;
            }
        };
        h.cc = function(b, a) {
            return function(c) {
                c = u(c);
                var e = c.type, d = c.relatedTarget || (e == "mouseout" ? c.toElement : c.fromElement);
                (!d || d !== a && !h.wf(a, d)) && b(c);
            };
        };
        h.c = function(a, c, d, b) {
            a = h.rb(a);
            if (a.addEventListener) {
                c == "mousewheel" && a.addEventListener("DOMMouseScroll", d, b);
                a.addEventListener(c, d, b);
            } else if (a.attachEvent) {
                a.attachEvent("on" + c, d);
                b && a.setCapture && a.setCapture();
            }
        };
        h.U = function(a, c, d, b) {
            a = h.rb(a);
            if (a.removeEventListener) {
                c == "mousewheel" && a.removeEventListener("DOMMouseScroll", d, b);
                a.removeEventListener(c, d, b);
            } else if (a.detachEvent) {
                a.detachEvent("on" + c, d);
                b && a.releaseCapture && a.releaseCapture();
            }
        };
        h.gc = function(a) {
            a = u(a);
            a.preventDefault && a.preventDefault();
            a.cancel = d;
            a.returnValue = l;
        };
        h.vf = function(a) {
            a = u(a);
            a.stopPropagation && a.stopPropagation();
            a.cancelBubble = d;
        };
        h.H = function(d, c) {
            var a = [].slice.call(arguments, 2), b = function() {
                var b = a.concat([].slice.call(arguments, 0));
                return c.apply(d, b);
            };
            return b;
        };
        h.qf = function(a, b) {
            if (b == g) return a.textContent || a.innerText;
            var c = f.createTextNode(b);
            h.wc(a);
            a.appendChild(c);
        };
        h.yb = function(d, c) {
            for (var b = [], a = d.firstChild; a; a = a.nextSibling) (c || a.nodeType == 1) && b.push(a);
            return b;
        };
        function tb(a, c, e, b) {
            b = b || "u";
            for (a = a ? a.firstChild : i; a; a = a.nextSibling) if (a.nodeType == 1) {
                if (X(a, b) == c) return a;
                if (!e) {
                    var d = tb(a, c, e, b);
                    if (d) return d;
                }
            }
        }
        h.v = tb;
        function V(a, d, f, b) {
            b = b || "u";
            var c = [];
            for (a = a ? a.firstChild : i; a; a = a.nextSibling) if (a.nodeType == 1) {
                X(a, b) == d && c.push(a);
                if (!f) {
                    var e = V(a, d, f, b);
                    if (e.length) c = c.concat(e);
                }
            }
            return c;
        }
        function nb(a, c, d) {
            for (a = a ? a.firstChild : i; a; a = a.nextSibling) if (a.nodeType == 1) {
                if (a.tagName == c) return a;
                if (!d) {
                    var b = nb(a, c, d);
                    if (b) return b;
                }
            }
        }
        h.Jf = nb;
        function hb(a, c, e) {
            var b = [];
            for (a = a ? a.firstChild : i; a; a = a.nextSibling) if (a.nodeType == 1) {
                (!c || a.tagName == c) && b.push(a);
                if (!e) {
                    var d = hb(a, c, e);
                    if (d.length) b = b.concat(d);
                }
            }
            return b;
        }
        h.Lf = hb;
        h.Kf = function(b, a) {
            return b.getElementsByTagName(a);
        };
        function E() {
            var e = arguments, d, c, b, a, h = 1 & e[0], f = 1 + h;
            d = e[f - 1] || {};
            for (;f < e.length; f++) if (c = e[f]) for (b in c) {
                a = c[b];
                if (a !== g) {
                    a = c[b];
                    var i = d[b];
                    d[b] = h && (B(i) || B(a)) ? E(h, {}, i, a) : a;
                }
            }
            return d;
        }
        h.o = E;
        function bb(f, g) {
            var d = {}, c, a, b;
            for (c in f) {
                a = f[c];
                b = g[c];
                if (a !== b) {
                    var e;
                    if (B(a) && B(b)) {
                        a = bb(a, b);
                        e = !sb(a);
                    }
                    !e && (d[c] = a);
                }
            }
            return d;
        }
        h.nd = function(a) {
            return F(a) == "function";
        };
        h.eg = function(a) {
            return F(a) == "string";
        };
        h.Vb = function(a) {
            return !isNaN(q(a)) && isFinite(a);
        };
        h.a = n;
        h.Fd = B;
        function U(a) {
            return f.createElement(a);
        }
        h.mb = function() {
            return U("DIV");
        };
        h.Ff = function() {
            return U("SPAN");
        };
        h.kd = function() {};
        function Y(b, c, a) {
            if (a == g) return b.getAttribute(c);
            b.setAttribute(c, a);
        }
        function X(a, b) {
            return Y(a, b) || Y(a, "data-" + b);
        }
        h.A = Y;
        h.j = X;
        function x(b, a) {
            if (a == g) return b.className;
            b.className = a;
        }
        h.ld = x;
        function xb(b) {
            var a = {};
            n(b, function(b) {
                if (b != g) a[b] = b;
            });
            return a;
        }
        function zb(b, a) {
            return b.match(a || Ab);
        }
        function R(b, a) {
            return xb(zb(b || "", a));
        }
        h.Mg = zb;
        function db(b, c) {
            var a = "";
            n(c, function(c) {
                a && (a += b);
                a += c;
            });
            return a;
        }
        function J(a, c, b) {
            x(a, db(" ", E(bb(R(x(a)), R(c)), R(b))));
        }
        h.Wd = function(a) {
            return a.parentNode;
        };
        h.O = function(a) {
            h.W(a, "none");
        };
        h.u = function(a, b) {
            h.W(a, b ? "none" : "");
        };
        h.Jg = function(b, a) {
            b.removeAttribute(a);
        };
        h.Vg = function() {
            return r() && k < 10;
        };
        h.Wg = function(d, a) {
            if (a) d.style.clip = "rect(" + c.round(a.$Top || a.I || 0) + "px " + c.round(a.$Right) + "px " + c.round(a.$Bottom) + "px " + c.round(a.$Left || a.D || 0) + "px)"; else if (a !== g) {
                var h = d.style.cssText, f = [ new RegExp(/[\s]*clip: rect\(.*?\)[;]?/i), new RegExp(/[\s]*cliptop: .*?[;]?/i), new RegExp(/[\s]*clipright: .*?[;]?/i), new RegExp(/[\s]*clipbottom: .*?[;]?/i), new RegExp(/[\s]*clipleft: .*?[;]?/i) ], e = C(h, f, "");
                b.Yb(d, e);
            }
        };
        h.N = function() {
            return +new Date();
        };
        h.F = function(b, a) {
            b.appendChild(a);
        };
        h.Zb = function(b, a, c) {
            (c || a.parentNode).insertBefore(b, a);
        };
        h.Jb = function(b, a) {
            a = a || b.parentNode;
            a && a.removeChild(b);
        };
        h.Ag = function(a, b) {
            n(a, function(a) {
                h.Jb(a, b);
            });
        };
        h.wc = function(a) {
            h.Ag(h.yb(a, d), a);
        };
        h.zg = function(a, b) {
            var c = h.Wd(a);
            b & 1 && h.z(a, (h.l(c) - h.l(a)) / 2);
            b & 2 && h.B(a, (h.k(c) - h.k(a)) / 2);
        };
        h.ac = function(b, a) {
            return parseInt(b, a || 10);
        };
        h.mg = q;
        h.wf = function(b, a) {
            var c = f.body;
            while (a && b !== a && c !== a) try {
                a = a.parentNode;
            } catch (d) {
                return l;
            }
            return b === a;
        };
        function Z(d, c, b) {
            var a = d.cloneNode(!c);
            !b && h.Jg(a, "id");
            return a;
        }
        h.Z = Z;
        h.Hb = function(e, f) {
            var a = new Image();
            function b(e, d) {
                h.U(a, "load", b);
                h.U(a, "abort", c);
                h.U(a, "error", c);
                f && f(a, d);
            }
            function c(a) {
                b(a, d);
            }
            if (pb() && k < 11.6 || !e) b(!e); else {
                h.c(a, "load", b);
                h.c(a, "abort", c);
                h.c(a, "error", c);
                a.src = e;
            }
        };
        h.Pg = function(d, a, e) {
            var c = d.length + 1;
            function b(b) {
                c--;
                if (a && b && b.src == a.src) a = b;
                !c && e && e(a);
            }
            n(d, function(a) {
                h.Hb(a.src, b);
            });
            b();
        };
        h.hd = function(a, g, i, h) {
            if (h) a = Z(a);
            var c = V(a, g);
            if (!c.length) c = b.Kf(a, g);
            for (var f = c.length - 1; f > -1; f--) {
                var d = c[f], e = Z(i);
                x(e, x(d));
                b.Yb(e, d.style.cssText);
                b.Zb(e, d);
                b.Jb(d);
            }
            return a;
        };
        function Gb(a) {
            var l = this, p = "", r = [ "av", "pv", "ds", "dn" ], e = [], q, k = 0, i = 0, d = 0;
            function j() {
                J(a, q, e[d || k || i & 2 || i]);
                b.bb(a, "pointer-events", d ? "none" : "");
            }
            function c() {
                k = 0;
                j();
                h.U(f, "mouseup", c);
                h.U(f, "touchend", c);
                h.U(f, "touchcancel", c);
            }
            function o(a) {
                if (d) h.gc(a); else {
                    k = 4;
                    j();
                    h.c(f, "mouseup", c);
                    h.c(f, "touchend", c);
                    h.c(f, "touchcancel", c);
                }
            }
            l.id = function(a) {
                if (a === g) return i;
                i = a & 2 || a & 1;
                j();
            };
            l.$Enable = function(a) {
                if (a === g) return !d;
                d = a ? 0 : 3;
                j();
            };
            l.$Elmt = a = h.rb(a);
            var m = b.Mg(x(a));
            if (m) p = m.shift();
            n(r, function(a) {
                e.push(p + a);
            });
            q = db(" ", e);
            e.unshift("");
            h.c(a, "mousedown", o);
            h.c(a, "touchstart", o);
        }
        h.bc = function(a) {
            return new Gb(a);
        };
        h.bb = G;
        h.tb = m("overflow");
        h.B = m("top", 2);
        h.z = m("left", 2);
        h.l = m("width", 2);
        h.k = m("height", 2);
        h.xd = m("marginLeft", 2);
        h.yd = m("marginTop", 2);
        h.s = m("position");
        h.W = m("display");
        h.G = m("zIndex", 1);
        h.Bb = function(b, a, c) {
            if (a != g) Fb(b, a, c); else return Db(b);
        };
        h.Yb = function(a, b) {
            if (b != g) a.style.cssText = b; else return a.style.cssText;
        };
        var W = {
            $Opacity: h.Bb,
            $Top: h.B,
            $Left: h.z,
            R: h.l,
            T: h.k,
            Kb: h.s,
            Sh: h.W,
            $ZIndex: h.G
        };
        function w(f, l) {
            var e = Q(), b = H(), d = fb(), j = M(f);
            function k(b, d, a) {
                var e = b.sb(p(-d / 2, -a / 2)), f = b.sb(p(d / 2, -a / 2)), g = b.sb(p(d / 2, a / 2)), h = b.sb(p(-d / 2, a / 2));
                b.sb(p(300, 300));
                return p(c.min(e.x, f.x, g.x, h.x) + d / 2, c.min(e.y, f.y, g.y, h.y) + a / 2);
            }
            function a(d, a) {
                a = a || {};
                var n = a.$TranslateZ || 0, p = (a.$RotateX || 0) % 360, q = (a.$RotateY || 0) % 360, u = (a.$Rotate || 0) % 360, l = a.$ScaleX, m = a.$ScaleY, f = a.Rh;
                if (l == g) l = 1;
                if (m == g) m = 1;
                if (f == g) f = 1;
                if (e) {
                    n = 0;
                    p = 0;
                    q = 0;
                    f = 0;
                }
                var c = new Cb(a.$TranslateX, a.$TranslateY, n);
                c.$RotateX(p);
                c.$RotateY(q);
                c.ve(u);
                c.xe(a.$SkewX, a.$SkewY);
                c.$Scale(l, m, f);
                if (b) {
                    c.$Move(a.D, a.I);
                    d.style[j] = c.qe();
                } else if (!z || z < 9) {
                    var o = "", i = {
                        x: 0,
                        y: 0
                    };
                    if (a.$OriginalWidth) i = k(c, a.$OriginalWidth, a.$OriginalHeight);
                    h.yd(d, i.y);
                    h.xd(d, i.x);
                    o = c.se();
                    var s = d.style.filter, t = new RegExp(/[\s]*progid:DXImageTransform\.Microsoft\.Matrix\([^\)]*\)/g), r = C(s, [ t ], o);
                    T(d, r);
                }
            }
            w = function(e, c) {
                c = c || {};
                var j = c.D, k = c.I, f;
                n(W, function(a, b) {
                    f = c[b];
                    f !== g && a(e, f);
                });
                h.Wg(e, c.$Clip);
                if (!b) {
                    j != g && h.z(e, (c.bd || 0) + j);
                    k != g && h.B(e, (c.dd || 0) + k);
                }
                if (c.Xd) if (d) yb(h.H(i, O, e, c)); else a(e, c);
            };
            h.lb = O;
            if (d) h.lb = w;
            if (e) h.lb = a; else if (!b) a = O;
            h.M = w;
            w(f, l);
        }
        h.lb = w;
        h.M = w;
        function Cb(j, k, o) {
            var d = this, b = [ 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, j || 0, k || 0, o || 0, 1 ], h = c.sin, g = c.cos, l = c.tan;
            function f(a) {
                return a * c.PI / 180;
            }
            function n(a, b) {
                return {
                    x: a,
                    y: b
                };
            }
            function m(b, c, f, g, i, l, n, o, q, t, u, w, y, A, C, F, a, d, e, h, j, k, m, p, r, s, v, x, z, B, D, E) {
                return [ b * a + c * j + f * r + g * z, b * d + c * k + f * s + g * B, b * e + c * m + f * v + g * D, b * h + c * p + f * x + g * E, i * a + l * j + n * r + o * z, i * d + l * k + n * s + o * B, i * e + l * m + n * v + o * D, i * h + l * p + n * x + o * E, q * a + t * j + u * r + w * z, q * d + t * k + u * s + w * B, q * e + t * m + u * v + w * D, q * h + t * p + u * x + w * E, y * a + A * j + C * r + F * z, y * d + A * k + C * s + F * B, y * e + A * m + C * v + F * D, y * h + A * p + C * x + F * E ];
            }
            function e(c, a) {
                return m.apply(i, (a || b).concat(c));
            }
            d.$Scale = function(a, c, d) {
                if (a != 1 || c != 1 || d != 1) b = e([ a, 0, 0, 0, 0, c, 0, 0, 0, 0, d, 0, 0, 0, 0, 1 ]);
            };
            d.$Move = function(a, c, d) {
                b[12] += a || 0;
                b[13] += c || 0;
                b[14] += d || 0;
            };
            d.$RotateX = function(c) {
                if (c) {
                    a = f(c);
                    var d = g(a), i = h(a);
                    b = e([ 1, 0, 0, 0, 0, d, i, 0, 0, -i, d, 0, 0, 0, 0, 1 ]);
                }
            };
            d.$RotateY = function(c) {
                if (c) {
                    a = f(c);
                    var d = g(a), i = h(a);
                    b = e([ d, 0, -i, 0, 0, 1, 0, 0, i, 0, d, 0, 0, 0, 0, 1 ]);
                }
            };
            d.ve = function(c) {
                if (c) {
                    a = f(c);
                    var d = g(a), i = h(a);
                    b = e([ d, i, 0, 0, -i, d, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 ]);
                }
            };
            d.xe = function(a, c) {
                if (a || c) {
                    j = f(a);
                    k = f(c);
                    b = e([ 1, l(k), 0, 0, l(j), 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 ]);
                }
            };
            d.sb = function(c) {
                var a = e(b, [ 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, c.x, c.y, 0, 1 ]);
                return n(a[12], a[13]);
            };
            d.qe = function() {
                return "matrix3d(" + b.join(",") + ")";
            };
            d.se = function() {
                return "progid:DXImageTransform.Microsoft.Matrix(M11=" + b[0] + ", M12=" + b[4] + ", M21=" + b[1] + ", M22=" + b[5] + ", SizingMethod='auto expand')";
            };
        }
        new function() {
            var a = this;
            function b(d, g) {
                for (var j = d[0].length, i = d.length, h = g[0].length, f = [], c = 0; c < i; c++) for (var k = f[c] = [], b = 0; b < h; b++) {
                    for (var e = 0, a = 0; a < j; a++) e += d[c][a] * g[a][b];
                    k[b] = e;
                }
                return f;
            }
            a.$ScaleX = function(b, c) {
                return a.cd(b, c, 0);
            };
            a.$ScaleY = function(b, c) {
                return a.cd(b, 0, c);
            };
            a.cd = function(a, c, d) {
                return b(a, [ [ c, 0 ], [ 0, d ] ]);
            };
            a.sb = function(d, c) {
                var a = b(d, [ [ c.x ], [ c.y ] ]);
                return p(a[0][0], a[1][0]);
            };
        }();
        var P = {
            bd: 0,
            dd: 0,
            D: 0,
            I: 0,
            $Zoom: 1,
            $ScaleX: 1,
            $ScaleY: 1,
            $Rotate: 0,
            $RotateX: 0,
            $RotateY: 0,
            $TranslateX: 0,
            $TranslateY: 0,
            $TranslateZ: 0,
            $SkewX: 0,
            $SkewY: 0
        };
        h.Jc = function(a) {
            var c = a || {};
            if (a) if (b.nd(a)) c = {
                Lc: c
            }; else if (b.nd(a.$Clip)) c.$Clip = {
                Lc: a.$Clip
            };
            return c;
        };
        function wb(c, a) {
            var b = {};
            n(c, function(c, d) {
                var e = c;
                if (a[d] != g) if (h.Vb(c)) e = c + a[d]; else e = wb(c, a[d]);
                b[d] = e;
            });
            return b;
        }
        h.ce = wb;
        h.jd = function(l, m, x, q, z, A, n) {
            var a = m;
            if (l) {
                a = {};
                for (var h in m) {
                    var B = A[h] || 1, w = z[h] || [ 0, 1 ], f = (x - w[0]) / w[1];
                    f = c.min(c.max(f, 0), 1);
                    f = f * B;
                    var u = c.floor(f);
                    if (f != u) f -= u;
                    var j = q.Lc || e.$EaseSwing, k, C = l[h], p = m[h];
                    if (b.Vb(p)) {
                        j = q[h] || j;
                        var y = j(f);
                        k = C + p * y;
                    } else {
                        k = b.o({
                            hc: {}
                        }, l[h]);
                        var v = q[h] || {};
                        b.a(p.hc || p, function(d, a) {
                            j = v[a] || v.Lc || j;
                            var c = j(f), b = d * c;
                            k.hc[a] = b;
                            k[a] += b;
                        });
                    }
                    a[h] = k;
                }
                var t = b.a(m, function(b, a) {
                    return P[a] != g;
                });
                t && b.a(P, function(c, b) {
                    if (a[b] == g && l[b] !== g) a[b] = l[b];
                });
                if (t) {
                    if (a.$Zoom) a.$ScaleX = a.$ScaleY = a.$Zoom;
                    a.$OriginalWidth = n.$OriginalWidth;
                    a.$OriginalHeight = n.$OriginalHeight;
                    a.Xd = d;
                }
            }
            if (m.$Clip && n.$Move) {
                var o = a.$Clip.hc, s = (o.$Top || 0) + (o.$Bottom || 0), r = (o.$Left || 0) + (o.$Right || 0);
                a.$Left = (a.$Left || 0) + r;
                a.$Top = (a.$Top || 0) + s;
                a.$Clip.$Left -= r;
                a.$Clip.$Right -= r;
                a.$Clip.$Top -= s;
                a.$Clip.$Bottom -= s;
            }
            if (a.$Clip && b.Vg() && !a.$Clip.$Top && !a.$Clip.$Left && !a.$Clip.I && !a.$Clip.D && a.$Clip.$Right == n.$OriginalWidth && a.$Clip.$Bottom == n.$OriginalHeight) a.$Clip = i;
            return a;
        };
    }();
    function n() {
        var a = this, d = [];
        function h(a, b) {
            d.push({
                rc: a,
                nc: b
            });
        }
        function g(a, c) {
            b.a(d, function(b, e) {
                b.rc == a && b.nc === c && d.splice(e, 1);
            });
        }
        a.$On = a.addEventListener = h;
        a.$Off = a.removeEventListener = g;
        a.m = function(a) {
            var c = [].slice.call(arguments, 1);
            b.a(d, function(b) {
                b.rc == a && b.nc.apply(j, c);
            });
        };
    }
    var m = function(z, C, g, M, P, J) {
        z = z || 0;
        var a = this, q, o, p, v, A = 0, H, I, G, B, y = 0, h = 0, m = 0, D, k, e, f, n, L, w = [], x;
        function Q(a) {
            e += a;
            f += a;
            k += a;
            h += a;
            m += a;
            y += a;
        }
        function u(p) {
            var i = p;
            if (n) {
                if (i >= f || i <= e && !L) i = ((i - e) % n + n) % n + e;
                if (g.Ye && i <= e) i = e + n;
            }
            if (!D || v || h != i) {
                var j = c.min(i, f);
                j = c.max(j, e);
                if (!D || v || j != m) {
                    if (J) {
                        var l = (j - k) / (C || 1);
                        if (g.$Reverse) l = 1 - l;
                        var o = b.jd(P, J, l, H, G, I, g);
                        if (x) b.a(o, function(b, a) {
                            x[a] && x[a](M, b);
                        }); else b.M(M, o);
                    }
                    a.sc(m - k, j - k);
                    var r = m, q = m = j;
                    b.a(w, function(b, c) {
                        var a = i <= h ? w[w.length - c - 1] : b;
                        a.E(m - y);
                    });
                    h = i;
                    D = d;
                    a.dc(r, q);
                }
            }
        }
        function E(a, b, d) {
            b && a.$Shift(f);
            if (!d) {
                e = c.min(e, a.uc() + y);
                f = c.max(f, a.Eb() + y);
            }
            w.push(a);
        }
        var r = j.requestAnimationFrame || j.webkitRequestAnimationFrame || j.mozRequestAnimationFrame || j.msRequestAnimationFrame;
        if (b.Mf() && b.Pd() < 7) r = i;
        r = r || function(a) {
            b.$Delay(a, g.$Interval);
        };
        function K() {
            if (q) {
                var d = b.N(), e = c.min(d - A, g.Yc), a = h + e * p;
                A = d;
                if (a * p >= o * p) a = o;
                u(a);
                if (!v && a * p >= o * p) N(B); else r(K);
            }
        }
        function t(g, i, j) {
            if (!q) {
                q = d;
                v = j;
                B = i;
                g = c.max(g, e);
                g = c.min(g, f);
                o = g;
                p = o < h ? -1 : 1;
                a.Mc();
                A = b.N();
                r(K);
            }
        }
        function N(b) {
            if (q) {
                v = q = B = l;
                a.Wc();
                b && b();
            }
        }
        a.$Play = function(a, b, c) {
            t(a ? h + a : f, b, c);
        };
        a.Uc = t;
        a.qb = N;
        a.Be = function(a) {
            t(a);
        };
        a.cb = function() {
            return h;
        };
        a.Rc = function() {
            return o;
        };
        a.Db = function() {
            return m;
        };
        a.E = u;
        a.$Move = function(a) {
            u(h + a);
        };
        a.$IsPlaying = function() {
            return q;
        };
        a.Ue = function(a) {
            n = a;
        };
        a.$Shift = Q;
        a.gb = function(a, b) {
            E(a, 0, b);
        };
        a.Hc = function(a) {
            E(a, 1);
        };
        a.Me = function(a) {
            f += a;
        };
        a.uc = function() {
            return e;
        };
        a.Eb = function() {
            return f;
        };
        a.dc = a.Mc = a.Wc = a.sc = b.kd;
        a.Cc = b.N();
        g = b.o({
            $Interval: 16,
            Yc: 50
        }, g);
        n = g.Dc;
        L = g.Pe;
        x = g.Oe;
        e = k = z;
        f = z + C;
        I = g.$Round || {};
        G = g.$During || {};
        H = b.Jc(g.$Easing);
    };
    var p = j.$JssorSlideshowFormations$ = new function() {
        var h = this, b = 0, a = 1, f = 2, e = 3, s = 1, r = 2, t = 4, q = 8, w = 256, x = 512, v = 1024, u = 2048, j = u + s, i = u + r, o = x + s, m = x + r, n = w + t, k = w + q, l = v + t, p = v + q;
        function y(a) {
            return (a & r) == r;
        }
        function z(a) {
            return (a & t) == t;
        }
        function g(b, a, c) {
            c.push(a);
            b[a] = b[a] || [];
            b[a].push(c);
        }
        h.$FormationStraight = function(f) {
            for (var d = f.$Cols, e = f.$Rows, s = f.$Assembly, t = f.Nb, r = [], a = 0, b = 0, p = d - 1, q = e - 1, h = t - 1, c, b = 0; b < e; b++) for (a = 0; a < d; a++) {
                switch (s) {
                  case j:
                    c = h - (a * e + (q - b));
                    break;

                  case l:
                    c = h - (b * d + (p - a));
                    break;

                  case o:
                    c = h - (a * e + b);

                  case n:
                    c = h - (b * d + a);
                    break;

                  case i:
                    c = a * e + b;
                    break;

                  case k:
                    c = b * d + (p - a);
                    break;

                  case m:
                    c = a * e + (q - b);
                    break;

                  default:
                    c = b * d + a;
                }
                g(r, c, [ b, a ]);
            }
            return r;
        };
        h.$FormationSwirl = function(q) {
            var x = q.$Cols, y = q.$Rows, B = q.$Assembly, w = q.Nb, A = [], z = [], u = 0, c = 0, h = 0, r = x - 1, s = y - 1, t, p, v = 0;
            switch (B) {
              case j:
                c = r;
                h = 0;
                p = [ f, a, e, b ];
                break;

              case l:
                c = 0;
                h = s;
                p = [ b, e, a, f ];
                break;

              case o:
                c = r;
                h = s;
                p = [ e, a, f, b ];
                break;

              case n:
                c = r;
                h = s;
                p = [ a, e, b, f ];
                break;

              case i:
                c = 0;
                h = 0;
                p = [ f, b, e, a ];
                break;

              case k:
                c = r;
                h = 0;
                p = [ a, f, b, e ];
                break;

              case m:
                c = 0;
                h = s;
                p = [ e, b, f, a ];
                break;

              default:
                c = 0;
                h = 0;
                p = [ b, f, a, e ];
            }
            u = 0;
            while (u < w) {
                t = h + "," + c;
                if (c >= 0 && c < x && h >= 0 && h < y && !z[t]) {
                    z[t] = d;
                    g(A, u++, [ h, c ]);
                } else switch (p[v++ % p.length]) {
                  case b:
                    c--;
                    break;

                  case f:
                    h--;
                    break;

                  case a:
                    c++;
                    break;

                  case e:
                    h++;
                }
                switch (p[v % p.length]) {
                  case b:
                    c++;
                    break;

                  case f:
                    h++;
                    break;

                  case a:
                    c--;
                    break;

                  case e:
                    h--;
                }
            }
            return A;
        };
        h.$FormationZigZag = function(p) {
            var w = p.$Cols, x = p.$Rows, z = p.$Assembly, v = p.Nb, t = [], u = 0, c = 0, d = 0, q = w - 1, r = x - 1, y, h, s = 0;
            switch (z) {
              case j:
                c = q;
                d = 0;
                h = [ f, a, e, a ];
                break;

              case l:
                c = 0;
                d = r;
                h = [ b, e, a, e ];
                break;

              case o:
                c = q;
                d = r;
                h = [ e, a, f, a ];
                break;

              case n:
                c = q;
                d = r;
                h = [ a, e, b, e ];
                break;

              case i:
                c = 0;
                d = 0;
                h = [ f, b, e, b ];
                break;

              case k:
                c = q;
                d = 0;
                h = [ a, f, b, f ];
                break;

              case m:
                c = 0;
                d = r;
                h = [ e, b, f, b ];
                break;

              default:
                c = 0;
                d = 0;
                h = [ b, f, a, f ];
            }
            u = 0;
            while (u < v) {
                y = d + "," + c;
                if (c >= 0 && c < w && d >= 0 && d < x && typeof t[y] == "undefined") {
                    g(t, u++, [ d, c ]);
                    switch (h[s % h.length]) {
                      case b:
                        c++;
                        break;

                      case f:
                        d++;
                        break;

                      case a:
                        c--;
                        break;

                      case e:
                        d--;
                    }
                } else {
                    switch (h[s++ % h.length]) {
                      case b:
                        c--;
                        break;

                      case f:
                        d--;
                        break;

                      case a:
                        c++;
                        break;

                      case e:
                        d++;
                    }
                    switch (h[s++ % h.length]) {
                      case b:
                        c++;
                        break;

                      case f:
                        d++;
                        break;

                      case a:
                        c--;
                        break;

                      case e:
                        d--;
                    }
                }
            }
            return t;
        };
        h.$FormationStraightStairs = function(q) {
            var u = q.$Cols, v = q.$Rows, e = q.$Assembly, t = q.Nb, r = [], s = 0, c = 0, d = 0, f = u - 1, h = v - 1, x = t - 1;
            switch (e) {
              case j:
              case m:
              case o:
              case i:
                var a = 0, b = 0;
                break;

              case k:
              case l:
              case n:
              case p:
                var a = f, b = 0;
                break;

              default:
                e = p;
                var a = f, b = 0;
            }
            c = a;
            d = b;
            while (s < t) {
                if (z(e) || y(e)) g(r, x - s++, [ d, c ]); else g(r, s++, [ d, c ]);
                switch (e) {
                  case j:
                  case m:
                    c--;
                    d++;
                    break;

                  case o:
                  case i:
                    c++;
                    d--;
                    break;

                  case k:
                  case l:
                    c--;
                    d--;
                    break;

                  case p:
                  case n:
                  default:
                    c++;
                    d++;
                }
                if (c < 0 || d < 0 || c > f || d > h) {
                    switch (e) {
                      case j:
                      case m:
                        a++;
                        break;

                      case k:
                      case l:
                      case o:
                      case i:
                        b++;
                        break;

                      case p:
                      case n:
                      default:
                        a--;
                    }
                    if (a < 0 || b < 0 || a > f || b > h) {
                        switch (e) {
                          case j:
                          case m:
                            a = f;
                            b++;
                            break;

                          case o:
                          case i:
                            b = h;
                            a++;
                            break;

                          case k:
                          case l:
                            b = h;
                            a--;
                            break;

                          case p:
                          case n:
                          default:
                            a = 0;
                            b++;
                        }
                        if (b > h) b = h; else if (b < 0) b = 0; else if (a > f) a = f; else if (a < 0) a = 0;
                    }
                    d = b;
                    c = a;
                }
            }
            return r;
        };
        h.$FormationSquare = function(i) {
            var a = i.$Cols || 1, b = i.$Rows || 1, j = [], d, e, f, h, k;
            f = a < b ? (b - a) / 2 : 0;
            h = a > b ? (a - b) / 2 : 0;
            k = c.round(c.max(a / 2, b / 2)) + 1;
            for (d = 0; d < a; d++) for (e = 0; e < b; e++) g(j, k - c.min(d + 1 + f, e + 1 + h, a - d + f, b - e + h), [ e, d ]);
            return j;
        };
        h.$FormationRectangle = function(f) {
            var d = f.$Cols || 1, e = f.$Rows || 1, h = [], a, b, i;
            i = c.round(c.min(d / 2, e / 2)) + 1;
            for (a = 0; a < d; a++) for (b = 0; b < e; b++) g(h, i - c.min(a + 1, b + 1, d - a, e - b), [ b, a ]);
            return h;
        };
        h.$FormationRandom = function(d) {
            for (var e = [], a, b = 0; b < d.$Rows; b++) for (a = 0; a < d.$Cols; a++) g(e, c.ceil(1e5 * c.random()) % 13, [ b, a ]);
            return e;
        };
        h.$FormationCircle = function(d) {
            for (var e = d.$Cols || 1, f = d.$Rows || 1, h = [], a, i = e / 2 - .5, j = f / 2 - .5, b = 0; b < e; b++) for (a = 0; a < f; a++) g(h, c.round(c.sqrt(c.pow(b - i, 2) + c.pow(a - j, 2))), [ a, b ]);
            return h;
        };
        h.$FormationCross = function(d) {
            for (var e = d.$Cols || 1, f = d.$Rows || 1, h = [], a, i = e / 2 - .5, j = f / 2 - .5, b = 0; b < e; b++) for (a = 0; a < f; a++) g(h, c.round(c.min(c.abs(b - i), c.abs(a - j))), [ a, b ]);
            return h;
        };
        h.$FormationRectangleCross = function(f) {
            for (var h = f.$Cols || 1, i = f.$Rows || 1, j = [], a, d = h / 2 - .5, e = i / 2 - .5, k = c.max(d, e) + 1, b = 0; b < h; b++) for (a = 0; a < i; a++) g(j, c.round(k - c.max(d - c.abs(b - d), e - c.abs(a - e))) - 1, [ a, b ]);
            return j;
        };
    }();
    j.$JssorSlideshowRunner$ = function(k, s, q, u, z) {
        var f = this, v, g, a, y = 0, x = u.$TransitionsOrder, r, h = 8;
        function t(a) {
            if (a.$Top) a.I = a.$Top;
            if (a.$Left) a.D = a.$Left;
            b.a(a, function(a) {
                b.Fd(a) && t(a);
            });
        }
        function j(g, f) {
            var a = {
                $Interval: f,
                $Duration: 1,
                $Delay: 0,
                $Cols: 1,
                $Rows: 1,
                $Opacity: 0,
                $Zoom: 0,
                $Clip: 0,
                $Move: l,
                $SlideOut: l,
                $Reverse: l,
                $Formation: p.$FormationRandom,
                $Assembly: 1032,
                $ChessMode: {
                    $Column: 0,
                    $Row: 0
                },
                $Easing: e.$EaseSwing,
                $Round: {},
                Pb: [],
                $During: {}
            };
            b.o(a, g);
            t(a);
            a.Nb = a.$Cols * a.$Rows;
            a.$Easing = b.Jc(a.$Easing);
            a.Ae = c.ceil(a.$Duration / a.$Interval);
            a.ae = function(c, b) {
                c /= a.$Cols;
                b /= a.$Rows;
                var f = c + "x" + b;
                if (!a.Pb[f]) {
                    a.Pb[f] = {
                        R: c,
                        T: b
                    };
                    for (var d = 0; d < a.$Cols; d++) for (var e = 0; e < a.$Rows; e++) a.Pb[f][e + "," + d] = {
                        $Top: e * b,
                        $Right: d * c + c,
                        $Bottom: e * b + b,
                        $Left: d * c
                    };
                }
                return a.Pb[f];
            };
            if (a.$Brother) {
                a.$Brother = j(a.$Brother, f);
                a.$SlideOut = d;
            }
            return a;
        }
        function o(B, h, a, w, o, m) {
            var z = this, u, v = {}, j = {}, n = [], f, e, s, q = a.$ChessMode.$Column || 0, r = a.$ChessMode.$Row || 0, g = a.ae(o, m), p = C(a), D = p.length - 1, t = a.$Duration + a.$Delay * D, x = w + t, k = a.$SlideOut, y;
            x += 50;
            function C(a) {
                var b = a.$Formation(a);
                return a.$Reverse ? b.reverse() : b;
            }
            z.Ud = x;
            z.Tb = function(d) {
                d -= w;
                var e = d < t;
                if (e || y) {
                    y = e;
                    if (!k) d = t - d;
                    var f = c.ceil(d / a.$Interval);
                    b.a(j, function(a, e) {
                        var d = c.max(f, a.te);
                        d = c.min(d, a.length - 1);
                        if (a.wd != d) {
                            if (!a.wd && !k) b.u(n[e]); else d == a.he && k && b.O(n[e]);
                            a.wd = d;
                            b.M(n[e], a[d]);
                        }
                    });
                }
            };
            h = b.Z(h);
            b.lb(h, i);
            if (b.Ob()) {
                var E = !h["no-image"], A = b.Lf(h);
                b.a(A, function(a) {
                    (E || a["jssor-slider"]) && b.Bb(a, b.Bb(a), d);
                });
            }
            b.a(p, function(h, i) {
                b.a(h, function(G) {
                    var K = G[0], J = G[1], t = K + "," + J, n = l, p = l, x = l;
                    if (q && J % 2) {
                        if (q & 3) n = !n;
                        if (q & 12) p = !p;
                        if (q & 16) x = !x;
                    }
                    if (r && K % 2) {
                        if (r & 3) n = !n;
                        if (r & 12) p = !p;
                        if (r & 16) x = !x;
                    }
                    a.$Top = a.$Top || a.$Clip & 4;
                    a.$Bottom = a.$Bottom || a.$Clip & 8;
                    a.$Left = a.$Left || a.$Clip & 1;
                    a.$Right = a.$Right || a.$Clip & 2;
                    var C = p ? a.$Bottom : a.$Top, z = p ? a.$Top : a.$Bottom, B = n ? a.$Right : a.$Left, A = n ? a.$Left : a.$Right;
                    a.$Clip = C || z || B || A;
                    s = {};
                    e = {
                        I: 0,
                        D: 0,
                        $Opacity: 1,
                        R: o,
                        T: m
                    };
                    f = b.o({}, e);
                    u = b.o({}, g[t]);
                    if (a.$Opacity) e.$Opacity = 2 - a.$Opacity;
                    if (a.$ZIndex) {
                        e.$ZIndex = a.$ZIndex;
                        f.$ZIndex = 0;
                    }
                    var I = a.$Cols * a.$Rows > 1 || a.$Clip;
                    if (a.$Zoom || a.$Rotate) {
                        var H = d;
                        if (b.Ob()) if (a.$Cols * a.$Rows > 1) H = l; else I = l;
                        if (H) {
                            e.$Zoom = a.$Zoom ? a.$Zoom - 1 : 1;
                            f.$Zoom = 1;
                            if (b.Ob() || b.Cd()) e.$Zoom = c.min(e.$Zoom, 2);
                            var N = a.$Rotate || 0;
                            e.$Rotate = N * 360 * (x ? -1 : 1);
                            f.$Rotate = 0;
                        }
                    }
                    if (I) {
                        var h = u.hc = {};
                        if (a.$Clip) {
                            var w = a.$ScaleClip || 1;
                            if (C && z) {
                                h.$Top = g.T / 2 * w;
                                h.$Bottom = -h.$Top;
                            } else if (C) h.$Bottom = -g.T * w; else if (z) h.$Top = g.T * w;
                            if (B && A) {
                                h.$Left = g.R / 2 * w;
                                h.$Right = -h.$Left;
                            } else if (B) h.$Right = -g.R * w; else if (A) h.$Left = g.R * w;
                        }
                        s.$Clip = u;
                        f.$Clip = g[t];
                    }
                    var L = n ? 1 : -1, M = p ? 1 : -1;
                    if (a.x) e.D += o * a.x * L;
                    if (a.y) e.I += m * a.y * M;
                    b.a(e, function(a, c) {
                        if (b.Vb(a)) if (a != f[c]) s[c] = a - f[c];
                    });
                    v[t] = k ? f : e;
                    var D = a.Ae, y = c.round(i * a.$Delay / a.$Interval);
                    j[t] = new Array(y);
                    j[t].te = y;
                    j[t].he = y + D - 1;
                    for (var F = 0; F <= D; F++) {
                        var E = b.jd(f, s, F / D, a.$Easing, a.$During, a.$Round, {
                            $Move: a.$Move,
                            $OriginalWidth: o,
                            $OriginalHeight: m
                        });
                        E.$ZIndex = E.$ZIndex || 1;
                        j[t].push(E);
                    }
                });
            });
            p.reverse();
            b.a(p, function(a) {
                b.a(a, function(c) {
                    var f = c[0], e = c[1], d = f + "," + e, a = h;
                    if (e || f) a = b.Z(h);
                    b.M(a, v[d]);
                    b.tb(a, "hidden");
                    b.s(a, "absolute");
                    B.ge(a);
                    n[d] = a;
                    b.u(a, !k);
                });
            });
        }
        function w() {
            var b = this, c = 0;
            m.call(b, 0, v);
            b.dc = function(d, b) {
                if (b - c > h) {
                    c = b;
                    a && a.Tb(b);
                    g && g.Tb(b);
                }
            };
            b.Ac = r;
        }
        f.ke = function() {
            var a = 0, b = u.$Transitions, d = b.length;
            if (x) a = y++ % d; else a = c.floor(c.random() * d);
            b[a] && (b[a].jb = a);
            return b[a];
        };
        f.re = function(w, x, l, m, b) {
            r = b;
            b = j(b, h);
            var i = m.fd, e = l.fd;
            i["no-image"] = !m.Lb;
            e["no-image"] = !l.Lb;
            var n = i, p = e, u = b, d = b.$Brother || j({}, h);
            if (!b.$SlideOut) {
                n = e;
                p = i;
            }
            var t = d.$Shift || 0;
            g = new o(k, p, d, c.max(t - d.$Interval, 0), s, q);
            a = new o(k, n, u, c.max(d.$Interval - t, 0), s, q);
            g.Tb(0);
            a.Tb(0);
            v = c.max(g.Ud, a.Ud);
            f.jb = w;
        };
        f.Gb = function() {
            k.Gb();
            g = i;
            a = i;
        };
        f.lf = function() {
            var b = i;
            if (a) b = new w();
            return b;
        };
        if (b.Ob() || b.Cd() || z && b.Pf() < 537) h = 16;
        n.call(f);
        m.call(f, -1e7, 1e7);
    };
    var k = j.$JssorSlider$ = function(p, hc) {
        var h = this;
        function Fc() {
            var a = this;
            m.call(a, -1e8, 2e8);
            a.nf = function() {
                var b = a.Db(), d = c.floor(b), f = t(d), e = b - c.floor(b);
                return {
                    jb: f,
                    pf: d,
                    Kb: e
                };
            };
            a.dc = function(b, a) {
                var e = c.floor(a);
                if (e != a && a > b) e++;
                Wb(e, d);
                h.m(k.$EVT_POSITION_CHANGE, t(a), t(b), a, b);
            };
        }
        function Ec() {
            var a = this;
            m.call(a, 0, 0, {
                Dc: r
            });
            b.a(C, function(b) {
                D & 1 && b.Ue(r);
                a.Hc(b);
                b.$Shift(gb / dc);
            });
        }
        function Dc() {
            var a = this, b = Vb.$Elmt;
            m.call(a, -1, 2, {
                $Easing: e.$EaseLinear,
                Oe: {
                    Kb: bc
                },
                Dc: r
            }, b, {
                Kb: 1
            }, {
                Kb: -2
            });
            a.Qb = b;
        }
        function rc(o, n) {
            var b = this, e, f, g, j, c;
            m.call(b, -1e8, 2e8, {
                Yc: 100
            });
            b.Mc = function() {
                O = d;
                R = i;
                h.m(k.$EVT_SWIPE_START, t(w.cb()), w.cb());
            };
            b.Wc = function() {
                O = l;
                j = l;
                var a = w.nf();
                h.m(k.$EVT_SWIPE_END, t(w.cb()), w.cb());
                !a.Kb && Hc(a.pf, s);
            };
            b.dc = function(i, h) {
                var b;
                if (j) b = c; else {
                    b = f;
                    if (g) {
                        var d = h / g;
                        b = a.$SlideEasing(d) * (f - e) + e;
                    }
                }
                w.E(b);
            };
            b.Rb = function(a, d, c, h) {
                e = a;
                f = d;
                g = c;
                w.E(a);
                b.E(0);
                b.Uc(c, h);
            };
            b.Qe = function(a) {
                j = d;
                c = a;
                b.$Play(a, i, d);
            };
            b.Te = function(a) {
                c = a;
            };
            w = new Fc();
            w.gb(o);
            w.gb(n);
        }
        function sc() {
            var c = this, a = Zb();
            b.G(a, 0);
            b.bb(a, "pointerEvents", "none");
            c.$Elmt = a;
            c.ge = function(c) {
                b.F(a, c);
                b.u(a);
            };
            c.Gb = function() {
                b.O(a);
                b.wc(a);
            };
        }
        function Bc(j, f) {
            var e = this, q, L, x, o, y = [], w, B, W, H, S, F, g, v, p;
            m.call(e, -u, u + 1, {});
            function E(a) {
                q && q.Ld();
                T(j, a, 0);
                F = d;
                q = new I.$Class(j, I, b.mg(b.j(j, "idle")) || qc);
                q.E(0);
            }
            function Y() {
                q.Cc < I.Cc && E();
            }
            function O(p, r, n) {
                if (!H) {
                    H = d;
                    if (o && n) {
                        var g = n.width, c = n.height, m = g, j = c;
                        if (g && c && a.$FillMode) {
                            if (a.$FillMode & 3 && (!(a.$FillMode & 4) || g > K || c > J)) {
                                var i = l, q = K / J * c / g;
                                if (a.$FillMode & 1) i = q > 1; else if (a.$FillMode & 2) i = q < 1;
                                m = i ? g * J / c : K;
                                j = i ? J : c * K / g;
                            }
                            b.l(o, m);
                            b.k(o, j);
                            b.B(o, (J - j) / 2);
                            b.z(o, (K - m) / 2);
                        }
                        b.s(o, "absolute");
                        h.m(k.$EVT_LOAD_END, f);
                    }
                }
                b.O(r);
                p && p(e);
            }
            function X(b, c, d, g) {
                if (g == R && s == f && P) if (!Gc) {
                    var a = t(b);
                    A.re(a, f, c, e, d);
                    c.Re();
                    U.$Shift(a - U.uc() - 1);
                    U.E(a);
                    z.Rb(b, b, 0);
                }
            }
            function ab(b) {
                if (b == R && s == f) {
                    if (!g) {
                        var a = i;
                        if (A) if (A.jb == f) a = A.lf(); else A.Gb();
                        Y();
                        g = new zc(j, f, a, q);
                        g.Oc(p);
                    }
                    !g.$IsPlaying() && g.kc();
                }
            }
            function G(d, h, l) {
                if (d == f) {
                    if (d != h) C[h] && C[h].Tc(); else !l && g && g.Je();
                    p && p.$Enable();
                    var m = R = b.N();
                    e.Hb(b.H(i, ab, m));
                } else {
                    var k = c.min(f, d), j = c.max(f, d), o = c.min(j - k, k + r - j), n = u + a.$LazyLoading - 1;
                    (!S || o <= n) && e.Hb();
                }
            }
            function bb() {
                if (s == f && g) {
                    g.qb();
                    p && p.$Quit();
                    p && p.$Disable();
                    g.sd();
                }
            }
            function eb() {
                s == f && g && g.qb();
            }
            function Z(a) {
                !M && h.m(k.$EVT_CLICK, f, a);
            }
            function Q() {
                p = v.pInstance;
                g && g.Oc(p);
            }
            e.Hb = function(c, a) {
                a = a || x;
                if (y.length && !H) {
                    b.u(a);
                    if (!W) {
                        W = d;
                        h.m(k.$EVT_LOAD_START, f);
                        b.a(y, function(a) {
                            if (!b.A(a, "src")) {
                                a.src = b.j(a, "src2");
                                b.W(a, a["display-origin"]);
                            }
                        });
                    }
                    b.Pg(y, o, b.H(i, O, c, a));
                } else O(c, a);
            };
            e.af = function() {
                var j = f;
                if (a.$AutoPlaySteps < 0) j -= r;
                var d = j + a.$AutoPlaySteps * xc;
                if (D & 2) d = t(d);
                if (!(D & 1) && !db) d = c.max(0, c.min(d, r - u));
                if (d != f) {
                    if (A) {
                        var g = A.ke(r);
                        if (g) {
                            var k = R = b.N(), h = C[t(d)];
                            return h.Hb(b.H(i, X, d, h, g, k), x);
                        }
                    }
                    ob(d);
                } else if (a.$Loop) {
                    e.Tc();
                    G(f, f);
                }
            };
            e.Ic = function() {
                G(f, f, d);
            };
            e.Tc = function() {
                p && p.$Quit();
                p && p.$Disable();
                e.ad();
                g && g.pe();
                g = i;
                E();
            };
            e.Re = function() {
                b.O(j);
            };
            e.ad = function() {
                b.u(j);
            };
            e.le = function() {
                p && p.$Enable();
            };
            function T(a, c, e) {
                if (b.A(a, "jssor-slider")) return;
                if (!F) {
                    if (a.tagName == "IMG") {
                        y.push(a);
                        if (!b.A(a, "src")) {
                            S = d;
                            a["display-origin"] = b.W(a);
                            b.O(a);
                        }
                    }
                    if (e) {
                        b.G(a, (b.G(a) || 0) + 1);
                        b.yd(a, 0);
                        b.xd(a, 0);
                        b.lb(a, {
                            $TranslateZ: 0
                        });
                    }
                }
                var f = b.yb(a);
                b.a(f, function(f) {
                    var h = f.tagName, i = b.j(f, "u");
                    if (i == "player" && !v) {
                        v = f;
                        if (v.pInstance) Q(); else b.c(v, "dataavailable", Q);
                    }
                    if (i == "caption") {
                        if (c) {
                            b.Nc(f, b.j(f, "to"));
                            b.Yf(f, b.j(f, "bf"));
                            b.j(f, "3d") && b.Xf(f, "preserve-3d");
                        } else if (!b.Hd()) {
                            var g = b.Z(f, l, d);
                            b.Zb(g, f, a);
                            b.Jb(f, a);
                            f = g;
                            c = d;
                        }
                    } else if (!F && !e && !o) {
                        if (h == "A") {
                            if (b.j(f, "u") == "image") o = b.Jf(f, "IMG"); else o = b.v(f, "image", d);
                            if (o) {
                                w = f;
                                b.W(w, "block");
                                b.M(w, V);
                                B = b.Z(w, d);
                                b.s(w, "relative");
                                b.Bb(B, 0);
                                b.bb(B, "backgroundColor", "#000");
                            }
                        } else if (h == "IMG" && b.j(f, "u") == "image") o = f;
                        if (o) {
                            o.border = 0;
                            b.M(o, V);
                        }
                    }
                    T(f, c, e + 1);
                });
            }
            e.sc = function(c, b) {
                var a = u - b;
                bc(L, a);
            };
            e.jb = f;
            n.call(e);
            b.Zf(j, b.j(j, "p"));
            b.bg(j, b.j(j, "po"));
            var N = b.v(j, "thumb", d);
            if (N) {
                e.ug = b.Z(N);
                b.O(N);
            }
            b.u(j);
            x = b.Z(cb);
            b.G(x, 1e3);
            b.c(j, "click", Z);
            E(d);
            e.Lb = o;
            e.gd = B;
            e.fd = j;
            e.Qb = L = j;
            b.F(L, x);
            h.$On(203, G);
            h.$On(28, eb);
            h.$On(24, bb);
        }
        function zc(y, f, p, q) {
            var a = this, n = 0, u = 0, g, i, e, c, j, t, r, o = C[f];
            m.call(a, 0, 0);
            function v() {
                b.wc(N);
                fc && j && o.gd && b.F(N, o.gd);
                b.u(N, !j && o.Lb);
            }
            function w() {
                a.kc();
            }
            function x(b) {
                r = b;
                a.qb();
                a.kc();
            }
            a.kc = function() {
                var b = a.Db();
                if (!B && !O && !r && s == f) {
                    if (!b) {
                        if (g && !j) {
                            j = d;
                            a.sd(d);
                            h.m(k.$EVT_SLIDESHOW_START, f, n, u, g, c);
                        }
                        v();
                    }
                    var l, p = k.$EVT_STATE_CHANGE;
                    if (b != c) if (b == e) l = c; else if (b == i) l = e; else if (!b) l = i; else l = a.Rc();
                    h.m(p, f, b, n, i, e, c);
                    var m = P && (!E || F);
                    if (b == c) (e != c && !(E & 12) || m) && o.af(); else (m || b != e) && a.Uc(l, w);
                }
            };
            a.Je = function() {
                e == c && e == a.Db() && a.E(i);
            };
            a.pe = function() {
                A && A.jb == f && A.Gb();
                var b = a.Db();
                b < c && h.m(k.$EVT_STATE_CHANGE, f, -b - 1, n, i, e, c);
            };
            a.sd = function(a) {
                p && b.tb(ib, a && p.Ac.$Outside ? "" : "hidden");
            };
            a.sc = function(b, a) {
                if (j && a >= g) {
                    j = l;
                    v();
                    o.ad();
                    A.Gb();
                    h.m(k.$EVT_SLIDESHOW_END, f, n, u, g, c);
                }
                h.m(k.$EVT_PROGRESS_CHANGE, f, a, n, i, e, c);
            };
            a.Oc = function(a) {
                if (a && !t) {
                    t = a;
                    a.$On($JssorPlayer$.Ne, x);
                }
            };
            p && a.Hc(p);
            g = a.Eb();
            a.Hc(q);
            i = g + q.qd;
            e = g + q.rd;
            c = a.Eb();
        }
        function Mb(a, c, d) {
            b.z(a, c);
            b.B(a, d);
        }
        function bc(c, b) {
            var a = x > 0 ? x : hb, d = Bb * b * (a & 1), e = Cb * b * (a >> 1 & 1);
            Mb(c, d, e);
        }
        function Rb() {
            qb = O;
            Kb = z.Rc();
            G = w.cb();
        }
        function ic() {
            Rb();
            if (B || !F && E & 12) {
                z.qb();
                h.m(k.Gg);
            }
        }
        function gc(f) {
            if (!B && (F || !(E & 12)) && !z.$IsPlaying()) {
                var d = w.cb(), b = c.ceil(G);
                if (f && c.abs(H) >= a.$MinDragOffsetToSlide) {
                    b = c.ceil(d);
                    b += fb;
                }
                if (!(D & 1)) b = c.min(r - u, c.max(b, 0));
                var e = c.abs(b - d);
                e = 1 - c.pow(1 - e, 5);
                if (!M && qb) z.Be(Kb); else if (d == b) {
                    ub.le();
                    ub.Ic();
                } else z.Rb(d, b, e * Xb);
            }
        }
        function Ib(a) {
            !b.j(b.lc(a), "nodrag") && b.gc(a);
        }
        function vc(a) {
            ac(a, 1);
        }
        function ac(a, c) {
            a = b.Kd(a);
            var j = b.lc(a);
            if (!L && !b.j(j, "nodrag") && wc() && (!c || a.touches.length == 1)) {
                B = d;
                Ab = l;
                R = i;
                b.c(f, c ? "touchmove" : "mousemove", Db);
                b.N();
                M = 0;
                ic();
                if (!qb) x = 0;
                if (c) {
                    var g = a.touches[0];
                    vb = g.clientX;
                    wb = g.clientY;
                } else {
                    var e = b.Sd(a);
                    vb = e.x;
                    wb = e.y;
                }
                H = 0;
                bb = 0;
                fb = 0;
                h.m(k.$EVT_DRAG_START, t(G), G, a);
            }
        }
        function Db(e) {
            if (B) {
                e = b.Kd(e);
                var f;
                if (e.type != "mousemove") {
                    var l = e.touches[0];
                    f = {
                        x: l.clientX,
                        y: l.clientY
                    };
                } else f = b.Sd(e);
                if (f) {
                    var j = f.x - vb, k = f.y - wb;
                    if (c.floor(G) != G) x = x || hb & L;
                    if ((j || k) && !x) {
                        if (L == 3) if (c.abs(k) > c.abs(j)) x = 2; else x = 1; else x = L;
                        if (kb && x == 1 && c.abs(k) - c.abs(j) > 3) Ab = d;
                    }
                    if (x) {
                        var a = k, i = Cb;
                        if (x == 1) {
                            a = j;
                            i = Bb;
                        }
                        if (!(D & 1)) {
                            if (a > 0) {
                                var g = i * s, h = a - g;
                                if (h > 0) a = g + c.sqrt(h) * 5;
                            }
                            if (a < 0) {
                                var g = i * (r - u - s), h = -a - g;
                                if (h > 0) a = -g - c.sqrt(h) * 5;
                            }
                        }
                        if (H - bb < -2) fb = 0; else if (H - bb > 2) fb = -1;
                        bb = H;
                        H = a;
                        tb = G - H / i / (Z || 1);
                        if (H && x && !Ab) {
                            b.gc(e);
                            if (!O) z.Qe(tb); else z.Te(tb);
                        }
                    }
                }
            }
        }
        function nb() {
            tc();
            if (B) {
                B = l;
                b.N();
                b.U(f, "mousemove", Db);
                b.U(f, "touchmove", Db);
                M = H;
                z.qb();
                var a = w.cb();
                h.m(k.$EVT_DRAG_END, t(a), a, t(G), G);
                E & 12 && Rb();
                gc(d);
            }
        }
        function mc(c) {
            if (M) {
                b.vf(c);
                var a = b.lc(c);
                while (a && v !== a) {
                    a.tagName == "A" && b.gc(c);
                    try {
                        a = a.parentNode;
                    } catch (d) {
                        break;
                    }
                }
            }
        }
        function Lb(a) {
            C[s];
            s = t(a);
            ub = C[s];
            Wb(a);
            return s;
        }
        function Hc(a, b) {
            x = 0;
            Lb(a);
            h.m(k.$EVT_PARK, t(a), b);
        }
        function Wb(a, c) {
            yb = a;
            b.a(S, function(b) {
                b.zc(t(a), a, c);
            });
        }
        function wc() {
            var b = k.od || 0, a = Y;
            if (kb) a & 1 && (a &= 1);
            k.od |= a;
            return L = a & ~b;
        }
        function tc() {
            if (L) {
                k.od &= ~Y;
                L = 0;
            }
        }
        function Zb() {
            var a = b.mb();
            b.M(a, V);
            b.s(a, "absolute");
            return a;
        }
        function t(a) {
            return (a % r + r) % r;
        }
        function nc(b, d) {
            if (d) if (!D) {
                b = c.min(c.max(b + yb, 0), r - u);
                d = l;
            } else if (D & 2) {
                b = t(b + yb);
                d = l;
            }
            ob(b, a.$SlideDuration, d);
        }
        function zb() {
            b.a(S, function(a) {
                a.vc(a.Wb.$ChanceToShow <= F);
            });
        }
        function kc() {
            if (!F) {
                F = 1;
                zb();
                if (!B) {
                    E & 12 && gc();
                    E & 3 && C[s].Ic();
                }
            }
        }
        function jc() {
            if (F) {
                F = 0;
                zb();
                B || !(E & 12) || ic();
            }
        }
        function lc() {
            V = {
                R: K,
                T: J,
                $Top: 0,
                $Left: 0
            };
            b.a(T, function(a) {
                b.M(a, V);
                b.s(a, "absolute");
                b.tb(a, "hidden");
                b.O(a);
            });
            b.M(cb, V);
        }
        function mb(b, a) {
            ob(b, a, d);
        }
        function ob(h, f, k) {
            if (Tb && (!B && (F || !(E & 12)) || a.$NaviQuitDrag)) {
                O = d;
                B = l;
                z.qb();
                if (f == g) f = Xb;
                var e = Eb.Db(), b = h;
                if (k) {
                    b = e + h;
                    if (h > 0) b = c.ceil(b); else b = c.floor(b);
                }
                if (D & 2) b = t(b);
                if (!(D & 1)) b = c.max(0, c.min(b, r - u));
                var j = (b - e) % r;
                b = e + j;
                var i = e == b ? 0 : f * c.abs(j);
                i = c.min(i, f * u * 1.5);
                z.Rb(e, b, i || 1);
            }
        }
        h.$PlayTo = ob;
        h.$GoTo = function(a) {
            w.E(Lb(a));
        };
        h.$Next = function() {
            mb(1);
        };
        h.$Prev = function() {
            mb(-1);
        };
        h.$Pause = function() {
            P = l;
        };
        h.$Play = function() {
            if (!P) {
                P = d;
                C[s] && C[s].Ic();
            }
        };
        h.$SetSlideshowTransitions = function(b) {
            a.$SlideshowOptions.$Transitions = b;
        };
        h.$SetCaptionTransitions = function(a) {
            I.$Transitions = a;
            I.Cc = b.N();
        };
        h.$SlidesCount = function() {
            return T.length;
        };
        h.$CurrentIndex = function() {
            return s;
        };
        h.$IsAutoPlaying = function() {
            return P;
        };
        h.$IsDragging = function() {
            return B;
        };
        h.$IsSliding = function() {
            return O;
        };
        h.$IsMouseOver = function() {
            return !F;
        };
        h.$LastDragSucceded = function() {
            return M;
        };
        function X() {
            return b.l(y || p);
        }
        function jb() {
            return b.k(y || p);
        }
        h.$OriginalWidth = h.$GetOriginalWidth = X;
        h.$OriginalHeight = h.$GetOriginalHeight = jb;
        function Gb(c, d) {
            if (c == g) return b.l(p);
            if (!y) {
                var a = b.mb(f);
                b.ld(a, b.ld(p));
                b.Yb(a, b.Yb(p));
                b.W(a, "block");
                b.s(a, "relative");
                b.B(a, 0);
                b.z(a, 0);
                b.tb(a, "visible");
                y = b.mb(f);
                b.s(y, "absolute");
                b.B(y, 0);
                b.z(y, 0);
                b.l(y, b.l(p));
                b.k(y, b.k(p));
                b.Nc(y, "0 0");
                b.F(y, a);
                var i = b.yb(p);
                b.F(p, y);
                b.bb(p, "backgroundImage", "");
                b.a(i, function(c) {
                    b.F(b.j(c, "noscale") ? p : a, c);
                    b.j(c, "autocenter") && Nb.push(c);
                });
            }
            Z = c / (d ? b.k : b.l)(y);
            b.ag(y, Z);
            var h = d ? Z * X() : c, e = d ? c : Z * jb();
            b.l(p, h);
            b.k(p, e);
            b.a(Nb, function(a) {
                var c = b.ac(b.j(a, "autocenter"));
                b.zg(a, c);
            });
        }
        h.$ScaleHeight = h.$GetScaleHeight = function(a) {
            if (a == g) return b.k(p);
            Gb(a, d);
        };
        h.$ScaleWidth = h.$SetScaleWidth = h.$GetScaleWidth = Gb;
        h.Nd = function(a) {
            var d = c.ceil(t(gb / dc)), b = t(a - s + d);
            if (b > u) {
                if (a - s > r / 2) a -= r; else if (a - s <= -r / 2) a += r;
            } else a = s + b - d;
            return a;
        };
        n.call(h);
        h.$Elmt = p = b.rb(p);
        var a = b.o({
            $FillMode: 0,
            $LazyLoading: 1,
            $ArrowKeyNavigation: 1,
            $StartIndex: 0,
            $AutoPlay: l,
            $Loop: 1,
            $HWA: d,
            $NaviQuitDrag: d,
            $AutoPlaySteps: 1,
            $AutoPlayInterval: 3e3,
            $PauseOnHover: 1,
            $SlideDuration: 500,
            $SlideEasing: e.$EaseOutQuad,
            $MinDragOffsetToSlide: 20,
            $SlideSpacing: 0,
            $Cols: 1,
            $Align: 0,
            $UISearchMode: 1,
            $PlayOrientation: 1,
            $DragOrientation: 1
        }, hc);
        a.$HWA = a.$HWA && b.Of();
        if (a.$Idle != g) a.$AutoPlayInterval = a.$Idle;
        if (a.$ParkingPosition != g) a.$Align = a.$ParkingPosition;
        var hb = a.$PlayOrientation & 3, xc = (a.$PlayOrientation & 4) / -4 || 1, eb = a.$SlideshowOptions, I = b.o({
            $Class: q,
            $PlayInMode: 1,
            $PlayOutMode: 1,
            $HWA: a.$HWA
        }, a.$CaptionSliderOptions);
        I.$Transitions = I.$Transitions || I.$CaptionTransitions;
        var rb = a.$BulletNavigatorOptions, W = a.$ArrowNavigatorOptions, ab = a.$ThumbnailNavigatorOptions, Q = !a.$UISearchMode, y, v = b.v(p, "slides", Q), cb = b.v(p, "loading", Q) || b.mb(f), Jb = b.v(p, "navigator", Q), ec = b.v(p, "arrowleft", Q), cc = b.v(p, "arrowright", Q), Hb = b.v(p, "thumbnavigator", Q), pc = b.l(v), oc = b.k(v), V, T = [], yc = b.yb(v);
        b.a(yc, function(a) {
            a.tagName == "DIV" && !b.j(a, "u") && T.push(a);
            b.G(a, (b.G(a) || 0) + 1);
        });
        var s = -1, yb, ub, r = T.length, K = a.$SlideWidth || pc, J = a.$SlideHeight || oc, Yb = a.$SlideSpacing, Bb = K + Yb, Cb = J + Yb, dc = hb & 1 ? Bb : Cb, u = c.min(a.$Cols, r), ib, x, L, Ab, S = [], Sb, Ub, Qb, fc, Gc, P, E = a.$PauseOnHover, qc = a.$AutoPlayInterval, Xb = a.$SlideDuration, sb, db, gb, Tb = u < r, D = Tb ? a.$Loop : 0, Y, M, F = 1, O, B, R, vb = 0, wb = 0, H, bb, fb, Eb, w, U, z, Vb = new sc(), Z, Nb = [];
        if (r) {
            if (a.$HWA) Mb = function(a, c, d) {
                b.lb(a, {
                    $TranslateX: c,
                    $TranslateY: d
                });
            };
            P = a.$AutoPlay;
            h.Wb = hc;
            lc();
            b.A(p, "jssor-slider", d);
            b.G(v, b.G(v) || 0);
            b.s(v, "absolute");
            ib = b.Z(v, d);
            b.Zb(ib, v);
            if (eb) {
                fc = eb.$ShowLink;
                sb = eb.$Class;
                db = u == 1 && r > 1 && sb && (!b.Hd() || b.Pd() >= 8);
            }
            gb = db || u >= r || !(D & 1) ? 0 : a.$Align;
            Y = (u > 1 || gb ? hb : -1) & a.$DragOrientation;
            var xb = v, C = [], A, N, Fb = b.Nf(), kb = Fb.Rf, G, qb, Kb, tb;
            Fb.Md && b.bb(xb, Fb.Md, [ i, "pan-y", "pan-x", "none" ][Y] || "");
            U = new Dc();
            if (db) A = new sb(Vb, K, J, eb, kb);
            b.F(ib, U.Qb);
            b.tb(v, "hidden");
            N = Zb();
            b.bb(N, "backgroundColor", "#000");
            b.Bb(N, 0);
            b.Zb(N, xb.firstChild, xb);
            for (var pb = 0; pb < T.length; pb++) {
                var Ac = T[pb], Cc = new Bc(Ac, pb);
                C.push(Cc);
            }
            b.O(cb);
            Eb = new Ec();
            z = new rc(Eb, U);
            b.c(v, "click", mc, d);
            b.c(p, "mouseout", b.cc(kc, p));
            b.c(p, "mouseover", b.cc(jc, p));
            if (Y) {
                b.c(v, "mousedown", ac);
                b.c(v, "touchstart", vc);
                b.c(v, "dragstart", Ib);
                b.c(v, "selectstart", Ib);
                b.c(f, "mouseup", nb);
                b.c(f, "touchend", nb);
                b.c(f, "touchcancel", nb);
                b.c(j, "blur", nb);
            }
            E &= kb ? 10 : 5;
            if (Jb && rb) {
                Sb = new rb.$Class(Jb, rb, X(), jb());
                S.push(Sb);
            }
            if (W && ec && cc) {
                W.$Loop = D;
                W.$Cols = u;
                Ub = new W.$Class(ec, cc, W, X(), jb());
                S.push(Ub);
            }
            if (Hb && ab) {
                ab.$StartIndex = a.$StartIndex;
                Qb = new ab.$Class(Hb, ab);
                S.push(Qb);
            }
            b.a(S, function(a) {
                a.mc(r, C, cb);
                a.$On(o.jc, nc);
            });
            b.bb(p, "visibility", "visible");
            Gb(X());
            zb();
            a.$ArrowKeyNavigation && b.c(f, "keydown", function(b) {
                if (b.keyCode == 37) mb(-a.$ArrowKeyNavigation); else b.keyCode == 39 && mb(a.$ArrowKeyNavigation);
            });
            var lb = a.$StartIndex;
            if (!(D & 1)) lb = c.max(0, c.min(lb, r - u));
            z.Rb(lb, lb, 0);
        }
    };
    k.$EVT_CLICK = 21;
    k.$EVT_DRAG_START = 22;
    k.$EVT_DRAG_END = 23;
    k.$EVT_SWIPE_START = 24;
    k.$EVT_SWIPE_END = 25;
    k.$EVT_LOAD_START = 26;
    k.$EVT_LOAD_END = 27;
    k.Gg = 28;
    k.$EVT_POSITION_CHANGE = 202;
    k.$EVT_PARK = 203;
    k.$EVT_SLIDESHOW_START = 206;
    k.$EVT_SLIDESHOW_END = 207;
    k.$EVT_PROGRESS_CHANGE = 208;
    k.$EVT_STATE_CHANGE = 209;
    var o = {
        jc: 1
    };
    j.$JssorBulletNavigator$ = function(e, C) {
        var f = this;
        n.call(f);
        e = b.rb(e);
        var s, A, z, r, k = 0, a, m, j, w, x, h, g, q, p, B = [], y = [];
        function v(a) {
            a != -1 && y[a].id(a == k);
        }
        function t(a) {
            f.m(o.jc, a * m);
        }
        f.$Elmt = e;
        f.zc = function(a) {
            if (a != r) {
                var d = k, b = c.floor(a / m);
                k = b;
                r = a;
                v(d);
                v(b);
            }
        };
        f.vc = function(a) {
            b.u(e, a);
        };
        var u;
        f.mc = function(E) {
            if (!u) {
                s = c.ceil(E / m);
                k = 0;
                var o = q + w, r = p + x, n = c.ceil(s / j) - 1;
                A = q + o * (!h ? n : j - 1);
                z = p + r * (h ? n : j - 1);
                b.l(e, A);
                b.k(e, z);
                for (var f = 0; f < s; f++) {
                    var C = b.Ff();
                    b.qf(C, f + 1);
                    var l = b.hd(g, "numbertemplate", C, d);
                    b.s(l, "absolute");
                    var v = f % (n + 1);
                    b.z(l, !h ? o * v : f % j * o);
                    b.B(l, h ? r * v : c.floor(f / (n + 1)) * r);
                    b.F(e, l);
                    B[f] = l;
                    a.$ActionMode & 1 && b.c(l, "click", b.H(i, t, f));
                    a.$ActionMode & 2 && b.c(l, "mouseover", b.cc(b.H(i, t, f), l));
                    y[f] = b.bc(l);
                }
                u = d;
            }
        };
        f.Wb = a = b.o({
            $SpacingX: 10,
            $SpacingY: 10,
            $Orientation: 1,
            $ActionMode: 1
        }, C);
        g = b.v(e, "prototype");
        q = b.l(g);
        p = b.k(g);
        b.Jb(g, e);
        m = a.$Steps || 1;
        j = a.$Rows || 1;
        w = a.$SpacingX;
        x = a.$SpacingY;
        h = a.$Orientation - 1;
        a.$Scale == l && b.A(e, "noscale", d);
        a.$AutoCenter && b.A(e, "autocenter", a.$AutoCenter);
    };
    j.$JssorArrowNavigator$ = function(a, g, h) {
        var c = this;
        n.call(c);
        var r, e, f, j;
        b.l(a);
        b.k(a);
        var p, m;
        function k(a) {
            c.m(o.jc, a, d);
        }
        function t(c) {
            b.u(a, c);
            b.u(g, c);
        }
        function s() {
            p.$Enable(h.$Loop || e > 0);
            m.$Enable(h.$Loop || e < r - h.$Cols);
        }
        c.zc = function(b, a, c) {
            if (c) e = a; else {
                e = b;
                s();
            }
        };
        c.vc = t;
        var q;
        c.mc = function(c) {
            r = c;
            e = 0;
            if (!q) {
                b.c(a, "click", b.H(i, k, -j));
                b.c(g, "click", b.H(i, k, j));
                p = b.bc(a);
                m = b.bc(g);
                q = d;
            }
        };
        c.Wb = f = b.o({
            $Steps: 1
        }, h);
        j = f.$Steps;
        if (f.$Scale == l) {
            b.A(a, "noscale", d);
            b.A(g, "noscale", d);
        }
        if (f.$AutoCenter) {
            b.A(a, "autocenter", f.$AutoCenter);
            b.A(g, "autocenter", f.$AutoCenter);
        }
    };
    j.$JssorThumbnailNavigator$ = function(g, B) {
        var h = this, y, p, a, v = [], z, x, e, q, r, u, t, m, s, f, j;
        n.call(h);
        g = b.rb(g);
        function A(n, f) {
            var g = this, c, m, l;
            function q() {
                m.id(p == f);
            }
            function k(d) {
                if (d || !s.$LastDragSucceded()) {
                    var a = e - f % e, b = s.Nd((f + a) / e - 1), c = b * e + e - a;
                    h.m(o.jc, c);
                }
            }
            g.jb = f;
            g.zd = q;
            l = n.ug || n.Lb || b.mb();
            g.Qb = c = b.hd(j, "thumbnailtemplate", l, d);
            m = b.bc(c);
            a.$ActionMode & 1 && b.c(c, "click", b.H(i, k, 0));
            a.$ActionMode & 2 && b.c(c, "mouseover", b.cc(b.H(i, k, 1), c));
        }
        h.zc = function(b, d, f) {
            var a = p;
            p = b;
            a != -1 && v[a].zd();
            v[b].zd();
            !f && s.$PlayTo(s.Nd(c.floor(d / e)));
        };
        h.vc = function(a) {
            b.u(g, a);
        };
        var w;
        h.mc = function(F, D) {
            if (!w) {
                y = F;
                c.ceil(y / e);
                p = -1;
                m = c.min(m, D.length);
                var h = a.$Orientation & 1, n = u + (u + q) * (e - 1) * (1 - h), j = t + (t + r) * (e - 1) * h, B = n + (n + q) * (m - 1) * h, o = j + (j + r) * (m - 1) * (1 - h);
                b.s(f, "absolute");
                b.tb(f, "hidden");
                a.$AutoCenter & 1 && b.z(f, (z - B) / 2);
                a.$AutoCenter & 2 && b.B(f, (x - o) / 2);
                b.l(f, B);
                b.k(f, o);
                var i = [];
                b.a(D, function(l, g) {
                    var j = new A(l, g), d = j.Qb, a = c.floor(g / e), k = g % e;
                    b.z(d, (u + q) * k * (1 - h));
                    b.B(d, (t + r) * k * h);
                    if (!i[a]) {
                        i[a] = b.mb();
                        b.F(f, i[a]);
                    }
                    b.F(i[a], d);
                    v.push(j);
                });
                var E = b.o({
                    $AutoPlay: l,
                    $NaviQuitDrag: l,
                    $SlideWidth: n,
                    $SlideHeight: j,
                    $SlideSpacing: q * h + r * (1 - h),
                    $MinDragOffsetToSlide: 12,
                    $SlideDuration: 200,
                    $PauseOnHover: 1,
                    $PlayOrientation: a.$Orientation,
                    $DragOrientation: a.$NoDrag || a.$DisableDrag ? 0 : a.$Orientation
                }, a);
                s = new k(g, E);
                w = d;
            }
        };
        h.Wb = a = b.o({
            $SpacingX: 0,
            $SpacingY: 0,
            $Cols: 1,
            $Orientation: 1,
            $AutoCenter: 3,
            $ActionMode: 1
        }, B);
        z = b.l(g);
        x = b.k(g);
        f = b.v(g, "slides", d);
        j = b.v(f, "prototype");
        u = b.l(j);
        t = b.k(j);
        b.Jb(j, f);
        e = a.$Rows || 1;
        q = a.$SpacingX;
        r = a.$SpacingY;
        m = a.$Cols;
        a.$Scale == l && b.A(g, "noscale", d);
    };
    function q(e, d, c) {
        var a = this;
        m.call(a, 0, c);
        a.Ld = b.kd;
        a.qd = 0;
        a.rd = c;
    }
    j.$JssorCaptionSlideo$ = function(v, j, u) {
        var a = this, w, n = {}, p = j.$Transitions, r = j.$Controls, e = new m(0, 0), t = [], o = [], f = [];
        m.call(a, 0, 0);
        function q(d, c) {
            var a = {};
            b.a(d, function(d, f) {
                var e = n[f];
                if (e) {
                    if (b.Fd(d)) d = q(d, c || f == "e"); else if (c) if (b.Vb(d)) d = w[d];
                    a[e] = d;
                }
            });
            return a;
        }
        function s(e, c) {
            var a = [], d = b.yb(e);
            b.a(d, function(d) {
                var h = b.j(d, "u") == "caption";
                if (h) {
                    var e = b.j(d, "t"), g = p[b.ac(e)] || p[e], f = {
                        $Elmt: d,
                        Ac: g
                    };
                    a.push(f);
                }
                if (c < 5) a = a.concat(s(d, c + 1));
            });
            return a;
        }
        function l(c, d, e) {
            var a = 0;
            !b.a(c, function(b, c) {
                a = c;
                return b.Q > e;
            }) && a++;
            c.splice(a, 0, d);
        }
        function z(k, n, e) {
            var a;
            if (r) {
                var j = b.j(k, "c");
                if (j) {
                    var h = r[b.ac(j)];
                    if (h) {
                        a = {
                            Q: h.r,
                            kb: 0,
                            ic: [],
                            Gc: [],
                            Qd: 0
                        };
                        l(f, a, h.b);
                    }
                }
            }
            b.a(n, function(g) {
                var f = b.o(d, {}, q(g)), j = b.Jc(f.$Easing);
                delete f.$Easing;
                if (f.$Left) {
                    f.D = f.$Left;
                    j.D = j.$Left;
                    delete f.$Left;
                }
                if (f.$Top) {
                    f.I = f.$Top;
                    j.I = j.$Top;
                    delete f.$Top;
                }
                var p = {
                    $Easing: j,
                    $OriginalWidth: e.R,
                    $OriginalHeight: e.T
                }, n = new m(g.b, g.d, p, k, e, f), h = t[g.b];
                if (h == i) {
                    h = {
                        Q: g.b,
                        ic: []
                    };
                    t[g.b] = h;
                    l(o, h, g.b);
                }
                h.ic.push(n);
                if (a && g.b + g.d > a.Q) {
                    a.kb = c.max(a.kb, g.b + g.d);
                    a.Gc.push(n);
                }
                e = b.ce(e, f);
            });
            if (a && a.Gc.length) {
                var g = new m(a.Q, a.kb - a.Q, {
                    Dc: a.kb - a.Q,
                    Pe: d,
                    Ye: d
                });
                b.a(a.Gc, function(a) {
                    g.gb(a, d);
                });
                g.$Shift(a.kb - a.Q);
                a.ic = [ g ];
            }
            return e;
        }
        function y(a) {
            b.a(a, function(f) {
                var a = f.$Elmt, e = b.l(a), d = b.k(a), c = {
                    $Left: b.z(a),
                    $Top: b.B(a),
                    D: 0,
                    I: 0,
                    $Opacity: 1,
                    $ZIndex: b.G(a) || 0,
                    $Rotate: 0,
                    $RotateX: 0,
                    $RotateY: 0,
                    $ScaleX: 1,
                    $ScaleY: 1,
                    $TranslateX: 0,
                    $TranslateY: 0,
                    $TranslateZ: 0,
                    $SkewX: 0,
                    $SkewY: 0,
                    R: e,
                    T: d,
                    $Clip: {
                        $Top: 0,
                        $Right: e,
                        $Bottom: d,
                        $Left: 0
                    }
                };
                c.bd = c.$Left;
                c.dd = c.$Top;
                z(a, f.Ac, c);
            });
        }
        function B(g, f, h) {
            var c = g.b - f;
            if (c) {
                var b = new m(f, c);
                b.gb(e, d);
                b.$Shift(h);
                a.gb(b);
            }
            a.Me(g.d);
            return c;
        }
        function A(g) {
            var c = e.uc(), d = 0;
            b.a(g, function(e, g) {
                e = b.o({
                    d: u
                }, e);
                B(e, c, d);
                c = e.b;
                d += e.d;
                if (!g || e.t == 2) {
                    a.qd = c;
                    a.rd = c + e.d;
                }
                b.a(f, function(a) {
                    if (a.kb > e.b) a.Qd += e.d;
                });
            });
        }
        function g(j, e, a) {
            var f = e.length;
            if (f > 4) for (var k = c.ceil(f / 4), d = 0; d < k; d++) {
                var h = e.slice(d * 4, c.min(d * 4 + 4, f)), i = new m(h[0].Q, a || 0);
                g(i, h, a);
                j.gb(i, a);
            } else b.a(e, function(c) {
                b.a(c.ic, function(b) {
                    b.$Shift(c.Qd || 0);
                    j.gb(b, a);
                });
            });
        }
        a.Ld = function() {
            a.E(-1, d);
        };
        w = [ h.$Swing, h.$Linear, h.$InQuad, h.$OutQuad, h.$InOutQuad, h.$InCubic, h.$OutCubic, h.$InOutCubic, h.$InQuart, h.$OutQuart, h.$InOutQuart, h.$InQuint, h.$OutQuint, h.$InOutQuint, h.$InSine, h.$OutSine, h.$InOutSine, h.$InExpo, h.$OutExpo, h.$InOutExpo, h.$InCirc, h.$OutCirc, h.$InOutCirc, h.$InElastic, h.$OutElastic, h.$InOutElastic, h.$InBack, h.$OutBack, h.$InOutBack, h.$InBounce, h.$OutBounce, h.$InOutBounce, h.$GoBack, h.$InWave, h.$OutWave, h.$OutJump, h.$InJump ];
        var C = {
            $Top: "y",
            $Left: "x",
            $Bottom: "m",
            $Right: "t",
            $Rotate: "r",
            $RotateX: "rX",
            $RotateY: "rY",
            $ScaleX: "sX",
            $ScaleY: "sY",
            $TranslateX: "tX",
            $TranslateY: "tY",
            $TranslateZ: "tZ",
            $SkewX: "kX",
            $SkewY: "kY",
            $Opacity: "o",
            $Easing: "e",
            $ZIndex: "i",
            $Clip: "c"
        };
        b.a(C, function(b, a) {
            n[b] = a;
        });
        y(s(v, 1));
        g(e, o);
        var x = j.$Breaks || [], k = [].concat(x[b.ac(b.j(v, "b"))] || []);
        k.push({
            b: e.Eb(),
            d: k.length ? 0 : u
        });
        A(k);
        g(a, f, a.Eb());
        a.E(-1);
    };
})(window, document, Math, null, true, false);

function onscrollx(force) {
    var fn = function() {
        if ($("body").hasClass("page-small") && !$("body").hasClass("show-sidebar")) {
            var top = $(window).scrollTop();
            if (top <= 62) {
                top = 62;
            }
            var navigationH = $("#navigation").outerHeight(true);
            var wrapperH = $("#wrapper").outerHeight(true);
            if (top - 62 > wrapperH - navigationH) {
                top = wrapperH - navigationH + 62;
            }
        } else {
            top = 62;
        }
        $("#menu").css({
            top: top
        });
    };
    var prevTimer = $("body").data("menutimer");
    if (prevTimer) {
        clearTimeout(prevTimer);
        prevTimer = 0;
    }
    var timer = setTimeout(fn, 50);
    $("body").data("menutimer", timer);
}

$(document).ready(function() {
    var m = new Date().getMonth() + 1;
    $("body").css({
        background: "url('../images/calendar/" + m.toString() + ".jpg')"
    });
    $(window).on("resize", function() {
        fixWrapperHeight();
    });
    setTimeout(function() {
        $(window).trigger("resize");
    }, 10);
    if ($(window).width() < 769) {
        $("body").addClass("page-small");
    } else {
        $("body").removeClass("page-small");
    }
    $(".splash").css("display", "none");
    window.addEventListener("touchstart", onscrollx, false);
    window.addEventListener("touchend", onscrollx, false);
    window.addEventListener("touchcancel", onscrollx, false);
    window.addEventListener("touchmove", onscrollx, false);
    if (isMobile.any()) {
        $(window).on("scroll", onscrollx);
    }
    $(window).on("orientationchange", function() {
        $("body").data("timer_count", 0);
        clearInterval($("body").data("timer"));
        var timerId = setInterval(function() {
            setTimeout(function() {
                var tc = $("body").data("timer_count");
                $("body").data("timer_count", tc + 1);
                $(window).trigger("resize");
                onscrollx();
                if (tc > 5) {
                    $("body").data("timer_count", 0);
                    clearInterval($("body").data("timer"));
                }
            }, 100);
        }, 150);
        $("body").data("timer", timerId);
    });
});

function fixWrapperHeight() {
    var headerH = 62;
    var footerHeight = Math.floor($(".footer").height());
    var windowH = Math.floor($(window).height()) - headerH - footerHeight - 2;
    var navHeight = Math.floor($("#navigation").height());
    var h = Math.max(windowH, navHeight) + "px";
    if (h < 480) {
        h = 480;
    }
    if ($("#wrapper").css("min-height") !== h) {
        $("#wrapper").css("min-height", h);
    }
}

function getBodySmall(w) {
    return w < 769;
}

function getBodyMedium(w) {
    return w < 992;
}

function getBodyWidth() {
    return $(window).width();
}

function swipedetect(el, events, callback, handleclick) {
    var touchsurface = el, swipedir, startX, startY, distX, distY, threshold = 5, restraint = 150, allowedTime = 1500, elapsedTime, startTime, handleswipe = callback || function(swipedir) {};
    var fnDown = function(touchobj, e) {
        swipedir = "none";
        dist = 0;
        startX = touchobj.pageX;
        startY = touchobj.pageY;
        startTime = new Date().getTime();
        e.preventDefault();
    };
    var fnUp = function(touchobj, e) {
        distX = touchobj.pageX - startX;
        distY = touchobj.pageY - startY;
        elapsedTime = new Date().getTime() - startTime;
        if (elapsedTime <= allowedTime) {
            if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint) {
                swipedir = distX < 0 ? "left" : "right";
            } else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint) {
                swipedir = distY < 0 ? "up" : "down";
            }
        }
        if (events.indexOf(swipedir) !== -1) {
            handleswipe(swipedir, e);
            e.preventDefault();
        } else if (swipedir === "none") {
            handleclick(e);
        }
    };
    if (!isMobile.any()) {
        touchsurface.addEventListener("mousedown", function(e) {
            var touchobj = e;
            fnDown(touchobj, e);
        }, false);
    } else {
        touchsurface.addEventListener("touchstart", function(e) {
            var touchobj = e.changedTouches[0];
            fnDown(touchobj, e);
        }, false);
    }
    touchsurface.addEventListener("touchmove", function(e) {
        e.preventDefault();
    }, false);
    if (!isMobile.any()) {
        touchsurface.addEventListener("mouseup", function(e) {
            var touchobj = e;
            fnUp(touchobj, e);
        }, false);
        touchsurface.addEventListener("click", function(e) {
            if (Math.abs(distX) > threshold || Math.abs(distY) > threshold) {
                e.stopPropagation();
                e.preventDefault();
            }
        }, false);
    } else {
        touchsurface.addEventListener("touchend", function(e) {
            var touchobj = e.changedTouches[0];
            fnUp(touchobj, e);
        }, false);
    }
}

window["apiUrl"] = location.origin;

window["appTitle"] = "Igruhi";

window.debugMode = false;

function updateTitle($state, v) {
    if (typeof v === "string") {
        var title = v;
    } else {
        title = v.translate($state.current.data.code);
    }
    $state.current.data.pageTitle = title;
    document.title = window.appTitle + " | " + title;
}

(function() {
    angular.module("homer", [ "ui.router", "ngSanitize", "ui.bootstrap", "cgNotify", "ngAnimate", "ui.map", "xeditable", "ui.select", "LocalStorageModule", "vcRecaptcha", "angularFileUpload" ]);
})();

function translationService($http, $injector, $q, localStorageService) {
    var urlBase = "/api/Values/";
    return {
        get: get,
        changeLocale: changeLocale,
        getComments: getComments,
        addComment: addComment,
        addSubComment: addSubComment,
        productRate: productRate,
        removeComment: removeComment,
        translate: translate
    };
    function addSubComment(model) {
        var request = $http.post(urlBase + "AddSubComment", model);
        return request.then(handleSuccess, handleError);
    }
    function productRate(model) {
        var request = $http.post(urlBase + "ProductRate", model);
        return request.then(handleSuccess, handleError);
    }
    function addComment(model) {
        var request = $http.post(urlBase + "AddComment", {
            ProductId: model.productId,
            Text: model.text,
            Title: model.title
        });
        return request.then(handleSuccess, handleError);
    }
    function removeComment(id) {
        var request = $http.post(urlBase + "RemoveComment", {
            Id: id
        });
        return request.then(handleSuccess, handleError);
    }
    function getComments(productId, page) {
        var request = $http.get(urlBase + "GetComments", {
            params: {
                productId: productId,
                page: page
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function translate(code) {
        var langId = localStorageService.get("langId") || 0;
        var _v = window["_translate"] || {};
        return _v[code] !== undefined ? _v[code] : "#" + code;
    }
    function changeLocale(langId) {
        var request = $http.get(urlBase + "ChangeLocale", {
            params: {
                langId: langId
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function get(langId, callback, error) {
        jQuery(function() {
            jQuery.ajax({
                url: urlBase + "GetLocalizableStrings",
                data: {
                    langId: langId
                },
                success: function(result) {
                    result = JSON.parse(result);
                    callback(result);
                },
                failure: function(result) {
                    error(result);
                },
                async: false
            });
        });
    }
    function handleError(response) {
        if (angular.isObject(response.data) && response.data.message) {
            return $q.reject(response.data.message);
        }
        return $q.reject("An unknown error occurred.");
    }
    function handleSuccess(response) {
        return response.data;
    }
}

angular.module("homer").service("translationService", translationService);

function loginService($http, $injector, $q) {
    var urlBase = "/api/Account/";
    return {
        getKeys: getKeys
    };
    function getKeys() {
        var request = $http.get(urlBase + "GetLoginKeys");
        return request.then(handleSuccess, handleError);
    }
    function handleError(response) {
        if (angular.isObject(response.data) && response.data.message) {
            return $q.reject(response.data.message);
        }
        return $q.reject("An unknown error occurred.");
    }
    function handleSuccess(response) {
        return response.data;
    }
}

angular.module("homer").service("loginService", loginService);

function adminService($http, $injector, $q) {
    var urlBase = "/api/Admin/";
    return {
        product: {
            save: save,
            remove: remove,
            removeProductPrice: removeProductPrice
        },
        string: {
            get: getStrings,
            save: saveString,
            remove: removeString
        },
        category: {
            get: getCategories,
            save: saveCategory,
            remove: removeCategory
        },
        faq: {
            get: getFaqs,
            save: saveFaq,
            remove: removeFaq
        },
        brand: {
            get: getBrands,
            save: saveBrand,
            remove: removeBrand
        },
        slide: {
            get: getSlides,
            save: saveSlide,
            remove: removeSlide
        },
        series: {
            get: getSeries,
            save: saveSeries,
            remove: removeSeries
        },
        formatDate: formatDate,
        ping: ping,
        removeImage: removeImage,
        resetCache: resetCache,
        removeSlideImage: removeSlideImage,
        removeBrandImage: removeBrandImage,
        removeCategoryImage: removeCategoryImage,
        removeSeriesImage: removeSeriesImage,
        getNPAreas: getNPAreas,
        getNPCities: getNPCities,
        getNPWarehouses: getNPWarehouses,
        updateNovaPoshta: updateNovaPoshta
    };
    function convertUTCDateToLocalDate(date) {
        var newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1e3);
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();
        newDate.setHours(hours - offset);
        return newDate;
    }
    function formatDate(dt, utc) {
        if (!dt) {
            return "";
        }
        if (!dt["getDate"]) {
            dt = new Date(Date.parse(dt));
        }
        if (utc) {
            dt = convertUTCDateToLocalDate(dt);
        }
        var d = dt.getDate();
        var m = dt.getMonth() + 1;
        var y = dt.getFullYear();
        var hh = dt.getHours();
        var mm = dt.getMinutes();
        if (d < 10) {
            d = "0" + d;
        }
        if (m < 10) {
            m = "0" + m;
        }
        if (hh < 10) {
            hh = "0" + hh;
        }
        if (mm < 10) {
            mm = "0" + mm;
        }
        return d + "." + m + "." + y + " " + hh + ":" + mm;
    }
    function ping() {
        var request = $http.get(urlBase + "Ping");
        return request.then(handleSuccess, handleError);
    }
    function getNPCities(areaRef) {
        var request = $http.get(urlBase + "GetNPCities", {
            params: {
                area: areaRef
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getSalesAmount(year) {
        var request = $http.get(urlBase + "GetSalesAmount", {
            params: {
                year: year
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getSalesCount(year) {
        var request = $http.get(urlBase + "GetSalesCount", {
            params: {
                year: year
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getSalesByRegion(year) {
        var request = $http.get(urlBase + "GetSalesByRegion", {
            params: {
                year: year
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getSalesByCategory(year) {
        var request = $http.get(urlBase + "GetSalesByCategory", {
            params: {
                year: year
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getSalesYears() {
        var request = $http.get(urlBase + "GetSalesYears");
        return request.then(handleSuccess, handleError);
    }
    function getNPAreas() {
        var request = $http.get(urlBase + "GetNPAreas");
        return request.then(handleSuccess, handleError);
    }
    function getNPWarehouses(cityRef) {
        var request = $http.get(urlBase + "GetNPWarehouses", {
            params: {
                city: cityRef
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getStrings(search) {
        var request = $http.get(urlBase + "GetStrings", {
            params: {
                search: search
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getCategories(search) {
        var request = $http.get(urlBase + "GetCategories", {
            params: {
                search: search
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getFaqs(search) {
        var request = $http.get(urlBase + "GetFaqs", {
            params: {
                search: search
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getBrands(search) {
        var request = $http.get(urlBase + "GetBrands", {
            params: {
                search: search
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getSlides(search) {
        var request = $http.get(urlBase + "GetSlides", {
            params: {
                search: search
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getSeries(search) {
        var request = $http.get(urlBase + "GetSeries", {
            params: {
                search: search
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function save(model) {
        var request = $http.post(urlBase + "Save", model);
        return request.then(handleSuccess, handleError);
    }
    function updateNovaPoshta() {
        var request = $http.post(urlBase + "UpdateNovaPoshta");
        return request.then(handleSuccess, handleError);
    }
    function saveString(model) {
        var request = $http.post(urlBase + "SaveString", model);
        return request.then(handleSuccess, handleError);
    }
    function saveCategory(model) {
        var request = $http.post(urlBase + "SaveCategory", model);
        return request.then(handleSuccess, handleError);
    }
    function saveFaq(model) {
        var request = $http.post(urlBase + "SaveFaq", model);
        return request.then(handleSuccess, handleError);
    }
    function saveBrand(model) {
        var request = $http.post(urlBase + "SaveBrand", model);
        return request.then(handleSuccess, handleError);
    }
    function saveSlide(model) {
        var request = $http.post(urlBase + "SaveSlide", model);
        return request.then(handleSuccess, handleError);
    }
    function saveSeries(model) {
        var request = $http.post(urlBase + "SaveSeries", model);
        return request.then(handleSuccess, handleError);
    }
    function removeImage(id, imageId) {
        var request = $http.post(urlBase + "RemoveImage", {
            id: id,
            imageId: imageId
        });
        return request.then(handleSuccess, handleError);
    }
    function resetCache() {
        var request = $http.get(urlBase + "ResetCache");
        return request.then(handleSuccess, handleError);
    }
    function removeCategoryImage(id, imageId) {
        var request = $http.post(urlBase + "RemoveCategoryImage", {
            id: id,
            imageId: imageId
        });
        return request.then(handleSuccess, handleError);
    }
    function removeSeriesImage(id, imageId) {
        var request = $http.post(urlBase + "RemoveSeriesImage", {
            id: id,
            imageId: imageId
        });
        return request.then(handleSuccess, handleError);
    }
    function removeBrandImage(id, imageId) {
        var request = $http.post(urlBase + "RemoveBrandImage", {
            id: id,
            imageId: imageId
        });
        return request.then(handleSuccess, handleError);
    }
    function removeSlideImage(id, imageId) {
        var request = $http.post(urlBase + "RemoveSlideImage", {
            id: id,
            imageId: imageId
        });
        return request.then(handleSuccess, handleError);
    }
    function removeProductPrice(id) {
        var request = $http.post(urlBase + "RemoveProductPrice", {
            id: id
        });
        return request.then(handleSuccess, handleError);
    }
    function remove(id) {
        var request = $http.post(urlBase + "Remove", {
            id: id
        });
        return request.then(handleSuccess, handleError);
    }
    function removeString(id) {
        var request = $http.post(urlBase + "RemoveString", {
            id: id
        });
        return request.then(handleSuccess, handleError);
    }
    function removeCategory(id) {
        var request = $http.post(urlBase + "RemoveCategory", {
            id: id
        });
        return request.then(handleSuccess, handleError);
    }
    function removeFaq(id) {
        var request = $http.post(urlBase + "RemoveFaq", {
            id: id
        });
        return request.then(handleSuccess, handleError);
    }
    function removeBrand(id) {
        var request = $http.post(urlBase + "RemoveBrand", {
            id: id
        });
        return request.then(handleSuccess, handleError);
    }
    function removeSlide(id) {
        var request = $http.post(urlBase + "RemoveSlide", {
            id: id
        });
        return request.then(handleSuccess, handleError);
    }
    function removeSeries(id) {
        var request = $http.post(urlBase + "RemoveSeries", {
            id: id
        });
        return request.then(handleSuccess, handleError);
    }
    function handleError(response) {
        if (angular.isObject(response.data) && response.data.message) {
            return $q.reject(response.data.message);
        }
        return $q.reject("An unknown error occurred.");
    }
    function handleSuccess(response) {
        return response.data;
    }
}

angular.module("homer").service("adminService", adminService);

function catalogService($http, $injector, $q, localStorageService, $rootScope) {
    var urlBase = "/api/Catalog/";
    return {
        product: {
            search: search
        },
        getProduct: getProduct,
        receiveTotalPages: receiveTotalPages,
        getLanguages: getLanguages,
        getProductImage: getProductImage,
        getFaqs: getFaqs,
        getBrands: getBrands,
        getSlides: getSlides,
        getCategories: getCategories,
        getSeries: getSeries,
        getSearchCategories: getSearchCategories,
        getCategoriesBrands: getCategoriesBrands
    };
    function getSlides(search) {
        var request = $http.get(urlBase + "GetSlides", {
            params: {
                search: search
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getBrands(search) {
        var request = $http.get(urlBase + "GetBrands", {
            params: {
                search: search
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getSearchCategories(code) {
        var langId = localStorageService.get("langId") || 0;
        var request = $http.get(urlBase + "GetSearchCategories", {
            params: {
                code: code,
                langId: langId
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getCategoriesBrands(code) {
        var request = $http.get(urlBase + "GetCategoriesBrands", {
            params: {
                code: code
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getSeries(code) {
        var langId = localStorageService.get("langId") || 0;
        var request = $http.get(urlBase + "GetSeries", {
            params: {
                code: code,
                langId: langId
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getCategories() {
        var langId = localStorageService.get("langId") || 0;
        var request = $http.get(urlBase + "GetCategories", {
            params: {
                langId: langId
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getFaqs() {
        var langId = localStorageService.get("langId") || 0;
        var request = $http.get(urlBase + "GetFaqs", {
            params: {
                langId: langId
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getLanguages() {
        var request = $http.get(urlBase + "GetLanguages");
        return request.then(handleSuccess, handleError);
    }
    function receiveTotalPages(model) {
        var langId = localStorageService.get("langId") || 0;
        var sortBy = $rootScope.sortBy;
        model.langId = langId;
        model.sortBy = sortBy;
        var request = $http.post(urlBase + "ReceiveTotalPages", model);
        return request.then(handleSuccess, handleError);
    }
    function getProductImage(o) {
        var request = $http.get(urlBase + "GetProductImage", {
            params: {
                id: o.id,
                imageId: o.imageId
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getProduct(id) {
        var langId = localStorageService.get("langId") || 0;
        var request = $http.get(urlBase + "GetProduct", {
            params: {
                id: id,
                langId: langId
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function search(model) {
        var sortBy = $rootScope.sortBy;
        var langId = localStorageService.get("langId") || 0;
        model.langId = langId;
        model.sortBy = sortBy;
        model = JSON.stringify(model);
        var request = $http.get(urlBase + "Search", {
            params: {
                model: model
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function handleError(response) {
        if (angular.isObject(response.data) && response.data.message) {
            return $q.reject(response.data.message);
        }
        return $q.reject("An unknown error occurred.");
    }
    function handleSuccess(response) {
        return response.data;
    }
}

angular.module("homer").service("catalogService", catalogService);

function cartService($http, $injector, $q) {
    var urlBase = "/api/Cart/";
    return {
        preOrder: preOrder,
        placeOrder: placeOrder,
        getDeliveryMethods: getDeliveryMethods
    };
    function getDeliveryMethods() {
        var request = $http.get(urlBase + "GetDeliveryMethods");
        return request.then(handleSuccess, handleError);
    }
    function preOrder(model) {
        var request = $http.post(urlBase + "PreOrder", model);
        return request.then(handleSuccess, handleError);
    }
    function placeOrder(model) {
        var request = $http.post(urlBase + "PlaceOrder", model);
        return request.then(handleSuccess, handleError);
    }
    function handleError(response) {
        if (angular.isObject(response.data) && response.data.message) {
            return $q.reject(response.data.message);
        }
        return $q.reject("An unknown error occurred.");
    }
    function handleSuccess(response) {
        return response.data;
    }
}

angular.module("homer").service("cartService", cartService);

function cabinetService($http, $injector, $q) {
    var urlBase = "/api/Cabinet/";
    return {
        saveUserInfo: saveUserInfo,
        getOrders: getOrders,
        getUserInfo: getUserInfo,
        orderAction: orderAction,
        removeOrder: removeOrder,
        removeProfile: removeProfile
    };
    function orderAction(id, action) {
        var request = $http.post(urlBase + "OrderAction", {
            Id: id,
            Action: action
        });
        return request.then(handleSuccess, handleError);
    }
    function getOrders(page, n, c, u) {
        var request = $http.get(urlBase + "GetOrders", {
            params: {
                page: page,
                number: n,
                calendar: c,
                userName: u
            }
        });
        return request.then(handleSuccess, handleError);
    }
    function getUserInfo() {
        var request = $http.get(urlBase + "GetUserInfo");
        return request.then(handleSuccess, handleError);
    }
    function removeOrder(id) {
        var request = $http.post(urlBase + "RemoveOrder", {
            Id: id
        });
        return request.then(handleSuccess, handleError);
    }
    function removeProfile(id) {
        var request = $http.post(urlBase + "RemoveProfile", {
            Id: id
        });
        return request.then(handleSuccess, handleError);
    }
    function saveUserInfo(model) {
        var request = $http.post(urlBase + "SaveUserInfo", model);
        return request.then(handleSuccess, handleError);
    }
    function handleError(response) {
        if (angular.isObject(response.data) && response.data.message) {
            return $q.reject(response.data.message);
        }
        return $q.reject("An unknown error occurred.");
    }
    function handleSuccess(response) {
        return response.data;
    }
}

angular.module("homer").service("cabinetService", cabinetService);

angular.module("homer").config(configState).run(function($rootScope, $state, editableOptions, $templateCache, localStorageService, translationService, $http) {
    $rootScope.$state = $state;
    $rootScope.appTitle = appTitle;
    editableOptions.theme = "bs3";
    var langId = localStorageService.get("langId") || 0;
    if (typeof langId !== "number") {
        langId = 0;
        localStorageService.set("langId", langId);
    }
    $rootScope.langId = langId;
    $rootScope.translate = function(code) {
        return translationService.translate(code);
    };
    window["_translate"] = localStorageService.get("_translate") || {};
    translationService.get(langId, function(values) {
        for (var i = 0; i < values.length; i++) {
            _translate[values[i].Code] = values[i].Text;
        }
        localStorageService.set("_translate", _translate);
        var templates = [ "views/store/index.html", "views/store/catalog.html", "views/store/cabinet.html", "views/store/about.html", "views/store/delivery.html", "views/store/product.html", "views/store/contacts.html", "views/store/faq.html", "views/store/feedback.html", "views/store/login.html", "views/store/register.html", "views/store/error_one.html", "views/store/error_two.html", "views/store/password_recovery.html", "views/store/recovery.html", "views/store/cart.html", "views/store/order.html", "views/store/search.html" ];
        $http.defaults.headers.common = {
            "Cache-Control": "public, max-age=3600"
        };
        var url;
        for (i in templates) {
            url = templates[i];
            $http.get(url, {
                cache: $templateCache
            });
        }
    }, function(response) {
        debugger;
    });
    var w = getBodyWidth();
    $rootScope.small = getBodySmall(w);
    $rootScope.medium = getBodyMedium(w);
});

angular.module("homer").factory("timeoutHttpIntercept", function($rootScope, $q) {
    return {
        request: function(config) {
            config.timeout = 3e5;
            return config;
        }
    };
});

function configState($stateProvider, $urlRouterProvider, $compileProvider, $httpProvider, $locationProvider) {
    $compileProvider.debugInfoEnabled(false);
    $locationProvider.hashPrefix("");
    $locationProvider.html5Mode({
        enabled: false,
        requireBase: false
    });
    $httpProvider.interceptors.push("timeoutHttpIntercept");
    $stateProvider.state("index", {
        url: "/index",
        templateUrl: "views/store/index.html",
        controller: "indexCtrl",
        data: {
            code: "Index"
        }
    }).state("catalog", {
        url: "/catalog/:code",
        templateUrl: "views/store/catalog.html",
        controller: "catalogCtrl",
        data: {
            code: "Catalog"
        }
    }).state("catalog_series", {
        url: "/catalog/:code/series/:series",
        templateUrl: "views/store/catalog.html",
        controller: "catalogCtrl",
        data: {
            code: "Catalog"
        }
    }).state("series", {
        url: "/catalog/:code/series",
        templateUrl: "views/store/series.html",
        controller: "seriesCtrl",
        data: {
            code: "Seriess"
        }
    }).state("cabinet", {
        url: "/cabinet/:tab?",
        templateUrl: "views/store/cabinet.html",
        controller: "cabinetCtrl",
        data: {
            code: "Cabinet"
        }
    }).state("about", {
        url: "/about",
        templateUrl: "views/store/about.html",
        controller: "emptyCtrl",
        data: {
            code: "About"
        }
    }).state("delivery", {
        url: "/delivery",
        templateUrl: "views/store/delivery.html",
        controller: "emptyCtrl",
        data: {
            code: "DeliveryAndPayments"
        }
    }).state("terms", {
        url: "/terms",
        templateUrl: "views/store/terms.html",
        controller: "emptyCtrl",
        data: {
            code: "Terms"
        }
    }).state("product", {
        url: "/product/:id",
        templateUrl: "views/store/product.html",
        controller: "productCtrl",
        data: {}
    }).state("contacts", {
        url: "/contacts",
        templateUrl: "views/store/contacts.html",
        controller: "contactsCtrl",
        data: {
            code: "Contacts"
        }
    }).state("faq", {
        url: "/faq",
        templateUrl: "views/store/faq.html",
        controller: "faqCtrl",
        data: {
            code: "FAQ"
        }
    }).state("feedback", {
        url: "/feedback",
        templateUrl: "views/store/feedback.html",
        controller: "feedbackCtrl",
        data: {
            code: "Feedback"
        }
    }).state("confirmemail", {
        url: "/confirmemail/:id",
        controller: "loginCtrl",
        templateUrl: "views/store/login.html",
        data: {
            code: "Login",
            specialClass: "blank"
        }
    }).state("confirmchangeemail", {
        url: "/confirmchangeemail/:id",
        controller: "cabinetCtrl",
        templateUrl: "views/store/cabinet.html",
        data: {
            code: "Cabinet"
        }
    }).state("login", {
        url: "/login",
        controller: "loginCtrl",
        templateUrl: "views/store/login.html",
        data: {
            code: "Login",
            specialClass: "blank"
        }
    }).state("register", {
        url: "/register",
        controller: "loginCtrl",
        templateUrl: "views/store/register.html",
        data: {
            code: "Register",
            specialClass: "blank"
        }
    }).state("error404", {
        url: "/error404",
        templateUrl: "views/store/error_one.html",
        controller: "emptyCtrl",
        data: {
            code: "Error 404",
            specialClass: "blank"
        }
    }).state("error500", {
        url: "/error500",
        templateUrl: "views/store/error_two.html",
        controller: "emptyCtrl",
        data: {
            code: "Error 500",
            specialClass: "blank"
        }
    }).state("password_recovery", {
        url: "/password_recovery",
        controller: "loginCtrl",
        templateUrl: "views/store/password_recovery.html",
        data: {
            code: "Recovery",
            specialClass: "blank"
        }
    }).state("recovery", {
        url: "/recovery/:id",
        controller: "loginCtrl",
        templateUrl: "views/store/recovery.html",
        data: {
            code: "Recovery",
            specialClass: "blank"
        }
    }).state("order", {
        url: "/order",
        controller: "cartCtrl",
        templateUrl: "views/store/order.html",
        data: {
            code: "OrderPlacement"
        }
    }).state("cart", {
        url: "/cart",
        templateUrl: "views/store/cart.html",
        controller: "cartCtrl",
        data: {
            code: "Cart"
        }
    }).state("search", {
        url: "/search",
        templateUrl: "views/store/search.html",
        controller: "searchCtrl",
        data: {
            code: "Search"
        }
    });
    $urlRouterProvider.otherwise(function($injector, $location) {
        var state = $injector.get("$state");
        var $rootScope = $injector.get("$rootScope");
        if (location.hash.indexOf("access_token=") !== -1 || location.hash === "#/error=access_denied") {
            state.go("login", $location.search());
        } else if (!state.current.name) {
            debugger;
            state.go("index");
        }
        return $location.path();
    });
}

angular.module("homer").filter("propsFilter", propsFilter);

function propsFilter() {
    return function(items, props) {
        var out = [];
        if (angular.isArray(items)) {
            items.forEach(function(item) {
                var itemMatches = false;
                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }
                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            out = items;
        }
        return out;
    };
}

var rainbow = new Rainbow();

rainbow.setSpectrum("lightgreen", "turquoise", "skyblue", "paleturquoise", "khaki", "navajowhite", "gold", "lightsalmon", "lightpink");

rainbow.setNumberRange(1, 100);

angular.module("homer").directive("sideNavigation", sideNavigation).directive("minimalizaMenu", minimalizaMenu).directive("sparkline", sparkline).directive("icheck", icheck).directive("inputMask", inputMask).directive("autofocus", autofocus).directive("panelTools", panelTools).directive("panelCloseTools", panelCloseTools).directive("panelToolsFullscreen", panelToolsFullscreen).directive("smallHeader", smallHeader).directive("animatePanel", animatePanel).directive("landingScrollspy", landingScrollspy).directive("clockPicker", clockPicker).directive("onLoadImage", onLoadImage).directive("dateTimePicker", dateTimePicker).directive("datePicker", datePicker).directive("summernoteair", summernoteair).directive("laddabtn", laddabtn).directive("backgroundColorFun", backgroundColorFun).directive("onRenderAnimate", onRenderAnimate).directive("sliderRange", sliderRange).directive("wordsCloud", wordsCloud);

function wordsCloud() {
    return {
        restrict: "A",
        scope: {
            words: "="
        },
        link: function(scope, element) {
            var el = $(element[0]);
            var words = [ {
                text: "Lorem",
                weight: 13,
                link: "http://a.com"
            }, {
                text: "Ipsum",
                weight: 10.5,
                link: "http://a.com"
            }, {
                text: "Dolor",
                weight: 9.4,
                link: "http://a.com"
            }, {
                text: "Sit",
                weight: 8,
                link: "http://a.com"
            }, {
                text: "Amet",
                weight: 6.2,
                link: "http://a.com"
            }, {
                text: "Consectetur",
                weight: 5,
                link: "http://a.com"
            }, {
                text: "Adipiscing",
                weight: 5,
                link: "http://a.com"
            }, {
                text: "zdipiscing",
                weight: 3,
                link: "http://a.com"
            } ];
            el.jQCloud(words, {
                autoResize: true,
                fontSize: {
                    to: .025,
                    from: .22
                }
            });
        }
    };
}

function sliderRange() {
    return {
        restrict: "A",
        replace: true,
        transclude: true,
        scope: {
            tooltip: "@",
            val: "=",
            min: "=",
            max: "=",
            step: "=",
            changeFn: "="
        },
        link: function(scope, element) {
            var el = $(element[0]);
            var tooltip = scope.tooltip || "hide";
            var sliderEl = el.bootstrapSlider({
                min: scope.min,
                step: scope.step,
                max: scope.max,
                value: scope.val,
                tooltip: tooltip,
                scale: "logarithmic"
            });
            sliderEl.on("change", function(event) {
                if (scope.changeFn) {
                    scope.changeFn(event);
                }
            });
            el.data("sliderEl", sliderEl);
        }
    };
}

function onRenderAnimate() {
    return {
        restrict: "A",
        scope: {
            animateIndex: "=",
            animateEffect: "@"
        },
        link: function(scope, element) {
            var el = $(element[0]);
            el.hide();
            setTimeout(function() {
                el.addClass(scope.animateEffect);
                el.show();
            }, scope.animateIndex * 50);
        }
    };
}

function inputMask() {
    return {
        restrict: "A",
        scope: {
            eMask: "="
        },
        link: function(scope, element) {
            var el = $(element[0]);
            el.inputmask({
                mask: scope.eMask
            });
        }
    };
}

function backgroundColorFun() {
    return {
        restrict: "A",
        scope: {
            backgroundColorFunIndex: "="
        },
        link: function(scope, element) {
            var el = $(element[0]);
            var index = Math.floor(Math.random() * 100);
            var styles = {};
            el.css(styles);
            el.parent().css({
                border: "2px solid rgb(250, 225, 163)",
                borderRadius: "4px"
            });
        }
    };
}

function autofocus($timeout) {
    return {
        restrict: "A",
        scope: {},
        link: function(scope, element, attr) {
            var el = $(element[0]);
            $timeout(function() {
                el.focus();
            });
        }
    };
}

function onLoadImage() {
    return {
        restrict: "A",
        scope: {
            onLoadFn: "=",
            onLoadItem: "="
        },
        link: function(scope, element, attr) {
            var el = $(element[0]);
            el.on("load", function() {
                var args = arguments;
                var el = $(this);
                scope.onLoadFn(scope.onLoadItem, "p");
            });
        }
    };
}

function sideNavigation($timeout) {
    return {
        restrict: "A",
        link: function(scope, element) {
            element.metisMenu();
            var menuElement = $('#side-menu a:not([href$="\\#"])');
            menuElement.click(function() {
                if ($(window).width() < 769) {
                    $("body").toggleClass("show-sidebar");
                }
            });
        }
    };
}

function minimalizaMenu($rootScope, $timeout, $state) {
    return {
        restrict: "EA",
        template: '<div class="header-link hide-menu" ng-click="minimalize($event)"><i class="fa fa-bars"></i></div>',
        controller: function($scope, $element) {
            if ($(window).width() < 769) {
                $rootScope.bodysmall = true;
            } else {
                $rootScope.bodysmall = false;
            }
            var rz = function() {
                var w = getBodyWidth();
                $rootScope.small = getBodySmall(w);
                $rootScope.medium = getBodyMedium(w);
                if ($rootScope.small) {
                    if ($rootScope.showSidebar) {
                        $rootScope.bodysmall = false;
                    } else {
                        $rootScope.bodysmall = true;
                    }
                    $rootScope.hideSidebar = false;
                } else {
                    $rootScope.bodysmall = false;
                    $rootScope.showSidebar = false;
                }
            };
            $(window).on("resize", function() {
                $timeout(function() {
                    rz();
                }, 10);
            });
            $scope.minimalize = function(e) {
                if ($(window).width() < 769) {
                    $rootScope.showSidebar = !$rootScope.showSidebar;
                } else {
                    $rootScope.hideSidebar = !$rootScope.hideSidebar;
                }
                rz();
                $timeout(function() {
                    var ct = 5;
                    clearInterval($scope.wrapperInterval);
                    $scope.wrapperInterval = setInterval(function() {
                        if (--ct > 0) {
                            $(window).trigger("resize");
                        } else {
                            clearInterval($scope.wrapperInterval);
                            $scope.wrapperInterval = 0;
                        }
                    }, 10);
                }, 50);
                e.stopPropagation();
                e.preventDefault();
            };
            $("body").on("click", "#side-menu", function(e) {
                if ($rootScope.showSidebar) {
                    $timeout(function() {
                        $scope.minimalize(e);
                    });
                }
            });
        }
    };
}

function sparkline() {
    return {
        restrict: "A",
        scope: {
            sparkData: "=",
            sparkOptions: "="
        },
        link: function(scope, element, attrs) {
            scope.$watch(scope.sparkData, function() {
                render();
            });
            scope.$watch(scope.sparkOptions, function() {
                render();
            });
            var render = function() {
                $(element).sparkline(scope.sparkData, scope.sparkOptions);
            };
        }
    };
}

function icheck($timeout) {
    return {
        restrict: "A",
        require: "ngModel",
        link: function($scope, element, $attrs, ngModel) {
            return $timeout(function() {
                var value;
                value = $attrs["value"];
                $scope.$watch($attrs["ngModel"], function(newValue) {
                    $(element).iCheck("update");
                });
                return $(element).iCheck({
                    checkboxClass: "icheckbox_square-green",
                    radioClass: "iradio_square-green"
                }).on("ifChanged", function(event) {
                    if ($(element).attr("type") === "checkbox" && $attrs["ngModel"]) {
                        $scope.$apply(function() {
                            return ngModel.$setViewValue(event.target.checked);
                        });
                    }
                    if ($(element).attr("type") === "radio" && $attrs["ngModel"]) {
                        return $scope.$apply(function() {
                            return ngModel.$setViewValue(value);
                        });
                    }
                });
            });
        }
    };
}

function panelTools($timeout) {
    return {
        restrict: "A",
        scope: true,
        templateUrl: "views/common/panel_tools.html",
        controller: function($scope, $element) {
            var hpanel = $element.closest("div.hpanel");
            $scope.showhide = function(e) {
                e.stopPropagation();
                e.preventDefault();
                var icon = $element.find("i:first");
                var body = hpanel.find("div.panel-body");
                var footer = hpanel.find("div.panel-footer");
                body.slideToggle(300);
                footer.slideToggle(200);
                icon.toggleClass("fa-chevron-up").toggleClass("fa-chevron-down");
                hpanel.toggleClass("").toggleClass("panel-collapse");
                $timeout(function() {
                    hpanel.resize();
                    hpanel.find("[id^=map-]").resize();
                }, 50);
            };
            var heading = hpanel.find("div.panel-heading");
            heading.on("click", $scope.showhide);
        }
    };
}

function panelCloseTools($timeout) {
    return {
        restrict: "A",
        scope: true,
        templateUrl: "views/common/panel_tools.html",
        controller: function($scope, $element) {
            $scope.closebox = function() {
                var hpanel = $element.closest("div.hpanel");
                hpanel.remove();
            };
        }
    };
}

function panelToolsFullscreen($timeout) {
    return {
        restrict: "A",
        scope: true,
        templateUrl: "views/common/panel_tools_fullscreen.html",
        controller: function($scope, $element) {
            $scope.showhide = function() {
                var hpanel = $element.closest("div.hpanel");
                var icon = $element.find("i:first");
                var body = hpanel.find("div.panel-body");
                var footer = hpanel.find("div.panel-footer");
                body.slideToggle(300);
                footer.slideToggle(200);
                icon.toggleClass("fa-chevron-up").toggleClass("fa-chevron-down");
                hpanel.toggleClass("").toggleClass("panel-collapse");
                $timeout(function() {
                    hpanel.resize();
                    hpanel.find("[id^=map-]").resize();
                }, 50);
            };
            $scope.closebox = function() {
                var hpanel = $element.closest("div.hpanel");
                hpanel.remove();
                if ($("body").hasClass("fullscreen-panel-mode")) {
                    $("body").removeClass("fullscreen-panel-mode");
                }
            };
            $scope.fullscreen = function() {
                var hpanel = $element.closest("div.hpanel");
                var icon = $element.find("i:first");
                $("body").toggleClass("fullscreen-panel-mode");
                icon.toggleClass("fa-expand").toggleClass("fa-compress");
                hpanel.toggleClass("fullscreen");
                setTimeout(function() {
                    $(window).trigger("resize");
                }, 100);
            };
        }
    };
}

function smallHeader() {
    return {
        restrict: "A",
        scope: true,
        controller: function($scope, $element) {
            $scope.small = function() {
                var icon = $element.find("i:first");
                var breadcrumb = $element.find("#hbreadcrumb");
                $element.toggleClass("small-header");
                breadcrumb.toggleClass("m-t-lg");
                icon.toggleClass("fa-arrow-up").toggleClass("fa-arrow-down");
            };
        }
    };
}

function animatePanel($timeout, $state) {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            var startAnimation = 0;
            var delay = .06;
            var start = Math.abs(delay) + startAnimation;
            var currentState = $state.current.name;
            if (!attrs.effect) {
                attrs.effect = "zoomIn";
            }
            if (attrs.delay) {
                delay = attrs.delay / 10;
            } else {
                delay = .06;
            }
            if (!attrs.child) {
                attrs.child = ".row > div";
            } else {
                attrs.child = "." + attrs.child;
            }
            var panel = element.find(attrs.child);
            panel.addClass("opacity-0");
            var renderTime = panel.length * delay * 1e3 + 700;
            $timeout(function() {
                panel = element.find(attrs.child);
                panel.addClass("stagger").addClass("animated-panel").addClass(attrs.effect);
                var panelsCount = panel.length + 10;
                var animateTime = panelsCount * delay * 1e4 / 10;
                panel.each(function(i, elm) {
                    start += delay;
                    var rounded = Math.round(start * 10) / 10;
                    $(elm).css("animation-delay", rounded + "s");
                    $(elm).removeClass("opacity-0");
                });
                $timeout(function() {
                    $(".stagger").css("animation", "");
                    $(".stagger").removeClass(attrs.effect).removeClass("animated-panel").removeClass("stagger");
                    panel.resize();
                }, animateTime);
            });
        }
    };
}

function landingScrollspy() {
    return {
        restrict: "A",
        link: function(scope, element, attrs) {
            element.scrollspy({
                target: ".navbar-fixed-top",
                offset: 80
            });
        }
    };
}

function clockPicker() {
    return {
        restrict: "A",
        link: function(scope, element) {
            element.clockpicker();
        }
    };
}

function summernoteair() {
    return {
        restrict: "A",
        scope: {
            value: "="
        },
        link: function(scope, element, attr) {
            $(element).summernote({
                onChange: function() {
                    scope.$apply();
                },
                airMode: JSON.parse(attr.mode)
            });
            $(element).summernote().code(scope.value);
        }
    };
}

function laddabtn() {
    return {
        restrict: "C",
        link: function(scope, element, attr) {
            $(element[0]).ladda();
        }
    };
}

function dateTimePicker() {
    return {
        require: "?ngModel",
        restrict: "AE",
        scope: {
            pick12HourFormat: "@",
            language: "@",
            useCurrent: "@",
            location: "@"
        },
        link: function(scope, elem, attrs) {
            elem.datetimepicker({
                pick12HourFormat: scope.pick12HourFormat,
                locale: scope.language,
                useCurrent: scope.useCurrent
            });
        }
    };
}

function datePicker($rootScope) {
    return {
        require: "?ngModel",
        restrict: "AE",
        scope: {
            language: "@",
            location: "@"
        },
        link: function(scope, elem, attrs) {
            var el = $(elem[0]);
            el.datetimepicker({
                format: "DD.MM.YYYY",
                viewMode: "days",
                inline: attrs.inline ? true : false,
                locale: scope.language
            });
            el.on("dp.change", function(e) {
                var d = el.data("DateTimePicker").date();
                if (d !== null) {
                    scope.dateTime = new Date(d.format());
                } else {
                    scope.dateTime = null;
                }
                $rootScope.$broadcast("emit:dateTimePicker", {
                    action: "changed",
                    dateTime: scope.dateTime,
                    element: el.attr("id")
                });
                scope.$apply();
            });
        }
    };
}

(function() {
    (function($) {
        $.each([ "show", "hide" ], function(i, ev) {
            var el = $.fn[ev];
            $.fn[ev] = function() {
                this.trigger(ev);
                return el.apply(this, arguments);
            };
        });
    })(jQuery);
    (function($) {
        var methods = [ "addClass", "toggleClass", "removeClass" ];
        $.each(methods, function(index, method) {
            var originalMethod = $.fn[method];
            $.fn[method] = function() {
                var result = originalMethod.apply(this, arguments);
                if (!this || this.length < 1) {
                    return result;
                }
                var oldClass = this[0].className;
                var newClass = this[0].className;
                this.trigger(method, [ oldClass, newClass ]);
                return result;
            };
        });
    })(jQuery);
    "use strict";
    angular.module("homer").directive("autofocus", function() {
        return {
            restrict: "A",
            link: function(scope, element) {
                var el = element[0];
                var focusFn = function() {
                    setTimeout(function() {
                        el.focus();
                    }, 250);
                };
                $(el).ready(focusFn);
                $(el).on("show", focusFn);
            }
        };
    });
})();

(function() {
    "use strict";
    angular.module("homer").directive("calcLeftMargin", function() {
        return {
            restrict: "A",
            link: function(scope, element) {
                var el = $(element[0]);
                el.ready(function() {
                    var w = el.width();
                    if (w !== 0) {
                        el.css({
                            "margin-left": -Math.round(w / 2) + "px"
                        });
                    } else {
                        el.data("interval", setInterval(function() {
                            w = el.width();
                            if (w !== 0) {
                                clearInterval(el.data("interval"));
                                el.css({
                                    "margin-left": -Math.round(w / 2) + "px"
                                });
                            }
                            scope.$apply();
                        }), 250);
                    }
                });
            }
        };
    });
})();

(function() {
    "use strict";
    angular.module("homer").directive("paging", [ function() {
        return {
            restrict: "AE",
            templateUrl: "scripts/directives/paging/paging.html",
            replace: true,
            transclude: true,
            scope: {
                pages: "=",
                onpageclick: "="
            },
            link: function(scope, elements, attr) {
                var el = $(elements[0]);
                scope.pages = scope.pages || {};
                var trackFn = function(n, o) {
                    scope.pages.items = [];
                    var count = scope.pages.visibleCount;
                    var total = scope.pages.totalCount;
                    var current = scope.pages.currentPage;
                    var start = scope.pages.currentPage - Math.round(count / 2) + 1;
                    scope.pages.firstReached = false;
                    scope.pages.lastReached = false;
                    if (start < 1) {
                        start = 1;
                    }
                    var counted = 0;
                    for (var i = start; i < start + count; i++) {
                        if (i > total) {
                            i = start - 1;
                            while (counted < count) {
                                if (i == 0) {
                                    break;
                                }
                                if (i == 1) {
                                    scope.pages.firstReached = true;
                                }
                                if (i == total) {
                                    scope.pages.lastReached = true;
                                }
                                scope.pages.items.splice(0, 0, {
                                    index: i,
                                    active: i == current
                                });
                                counted++;
                                i--;
                            }
                            break;
                        }
                        if (i == 1) {
                            scope.pages.firstReached = true;
                        }
                        scope.pages.items.push({
                            index: i,
                            active: i == current
                        });
                        if (i == total) {
                            scope.pages.lastReached = true;
                        }
                        counted++;
                    }
                };
                scope.$watch("pages.visibleCount", trackFn);
                scope.$watch("pages.currentPage", trackFn);
                scope.$watch("pages.totalCount", trackFn);
                scope.$watch("pages", trackFn);
            }
        };
    } ]);
})();

(function() {
    "use strict";
    angular.module("homer").directive("searchPanel", [ "$timeout", "$rootScope", "translationService", "$compile", "catalogService", function($timeout, $rootScope, translationService, $compile, catalogService) {
        return {
            restrict: "AE",
            templateUrl: "scripts/directives/search/search.html",
            replace: true,
            transclude: true,
            scope: {
                isSmall: "=",
                isCollapsed: "=",
                searchCfg: "=",
                applySearch: "=",
                clearSearch: "=",
                onRefresh: "=",
                code: "="
            },
            link: function(scope, elements, attr) {
                var el = $(elements[0]);
                var h = 300;
                var filtersEl = el.find(".filters");
                var min = 0, max = 0, categoryIds = [], brandIds = [], seriesIds = [];
                scope.small = scope.isSmall === true;
                if (($rootScope.small || scope.bodysmall) && scope.isSmall !== true) {
                    scope.filterVisible = false;
                } else {
                    scope.filterVisible = scope.isCollapsed === undefined ? true : scope.isCollapsed ? false : true;
                }
                scope.categoryCheck = function(c) {
                    scope.filtersChanged();
                };
                scope.clearText = function(cb) {
                    scope.text = "";
                    scope.textChanged(function() {
                        if ($rootScope.currentState != "index") {
                            if (!scope.small) {
                                $timeout(function() {
                                    scope.applySearch();
                                });
                            }
                        }
                    });
                };
                scope.brandCheck = function(b) {
                    if (!b.checked) {
                        $.each(b.Series, function(i, t) {
                            t.checked = false;
                        });
                    }
                    scope.filtersChanged();
                };
                scope.brandSeriesCheck = function(bs) {
                    scope.filtersChanged();
                };
                scope.ifDifferent = function(a, b) {
                    if (!a || !b) {
                        return true;
                    }
                    if (a.length !== b.length) {
                        return true;
                    }
                    for (var i = 0; i < a.length; i++) {
                        if (a[i] !== b[i]) {
                            return true;
                        }
                    }
                    return false;
                };
                scope.onChangeCode = function(code) {
                    catalogService.getSearchCategories(code || "").then(function(categories) {
                        if (scope.ifDifferent(scope.searchCategories, categories)) {
                            scope.searchCategories = categories;
                            $.each(scope.searchCategories, function(i, r) {
                                $.each(categoryIds, function(j, t) {
                                    if (r.Id === t) {
                                        r.ichecked = true;
                                    }
                                });
                            });
                        }
                        catalogService.getCategoriesBrands(code || "").then(function(brands) {
                            if (scope.ifDifferent(scope.searchBrands, brands)) {
                                if (seriesIds && $rootScope.cseries && seriesIds.indexOf($rootScope.cseries) == -1) {
                                    seriesIds = [];
                                    seriesIds.push($rootScope.cseries);
                                }
                                scope.searchBrands = brands;
                                $.each(scope.searchBrands, function(i, q) {
                                    q.ichecked = false;
                                    $.each(brandIds, function(j, w) {
                                        if (q.Id == w) {
                                            q.ichecked = true;
                                            return false;
                                        }
                                    });
                                    $.each(q.Series, function(z, s) {
                                        s.ichecked = false;
                                        $.each(seriesIds, function(x, m) {
                                            if (s.Code == m) {
                                                s.ichecked = true;
                                                return false;
                                            }
                                        });
                                    });
                                });
                            }
                            $timeout(function() {
                                scope.filtersChanged();
                            });
                        }, function(err) {
                            debugger;
                        });
                    }, function(err) {
                        debugger;
                    });
                };
                scope.applyCustom = function() {
                    $rootScope.$broadcast("applyCustom");
                };
                scope.keyPress = function(e) {
                    if (e.keyCode == 13) {
                        scope.textChanged(function() {
                            if (!scope.small) {
                                $timeout(function() {
                                    scope.applySearch();
                                });
                            }
                        });
                    }
                };
                scope.$watch("code", function(n, o) {
                    scope.onChangeCode(n);
                });
                $rootScope.$watch("cseries", function(n, o) {
                    if (!n) {
                        var searchCfg = scope.searchCfg;
                        searchCfg = {
                            text: ""
                        };
                        $rootScope.latestSearch = $rootScope.latestSearch || undefined;
                        $rootScope._searchCfg = $rootScope._searchCfg || undefined;
                        if ($rootScope._searchCfg) {
                            if ($rootScope.latestSearch) {
                                $rootScope.latestSearch.seriesIds = [];
                            }
                            $rootScope._searchCfg.seriesIds = [];
                        }
                        seriesIds = [];
                        brandIds = [];
                        categoryIds = [];
                    }
                    scope.onChangeCode($rootScope.code);
                });
                scope.onChangePol = function(p) {
                    scope.pol = p;
                    scope.filtersChanged();
                };
                scope.onChangeAge = function(v) {
                    scope.age = v;
                    scope.filtersChanged();
                };
                scope.advancedSearch = function() {
                    scope.filterVisible = !scope.filterVisible;
                    $timeout(function() {
                        $(window).trigger("resize");
                    }, 50);
                };
                scope.getUrl = function(url) {
                    $rootScope._t = $rootScope._t || 1;
                    return url + "&_t=" + $rootScope._t;
                };
                scope.translate = function(code) {
                    return translationService.translate(code);
                };
                scope.id = function(id) {
                    return (scope.small ? "_" : "") + id.replace(/-/g, "_");
                };
                scope.textChanged = function(cb) {
                    clearTimeout(scope.textTimer);
                    scope.textTimer = setTimeout(function() {
                        scope.filtersChanged();
                        scope.$apply();
                        if (cb) {
                            cb();
                        }
                    }, 500);
                };
                scope.clearFilters = function() {
                    scope.text = "";
                    scope.age = 0;
                    scope.pol = 0;
                    scope.number = "";
                    scope.exists = false;
                    scope.onlyWithComments = false;
                    scope.min = 0;
                    scope.max = 1e5;
                    scope.priceRangeArray = [ scope.min, scope.max ];
                    scope.priceRange = scope.min + "," + scope.max;
                    var priceMinEl = el.find(".price-range-min");
                    var priceMaxEl = el.find(".price-range-max");
                    priceMinEl.val(scope.min);
                    priceMaxEl.val(scope.max);
                    var slider = el.find(".price-range");
                    var sliderEl = slider.data("sliderEl");
                    sliderEl.bootstrapSlider("setValue", [ scope.min, scope.max ]);
                    if (!scope.small) {
                        $.each(scope.searchCategories, function(i, c) {
                            c.checked = false;
                        });
                    }
                    $.each(scope.searchBrands, function(i, b) {
                        if (!scope.small) {
                            b.checked = false;
                        }
                        $.each(b.Series, function(j, s) {
                            s.checked = false;
                        });
                    });
                    scope.filtersChanged();
                    $timeout(function() {
                        if (scope.clearSearch) {
                            scope.clearSearch();
                        }
                    });
                };
                scope.setValues = function(cfg, skipApply) {
                    scope.text = cfg.text;
                    scope.age = cfg.age;
                    scope.pol = cfg.pol;
                    scope.number = cfg.productCode;
                    scope.exists = cfg.exists;
                    scope.onlyWithComments = cfg.onlyWithComments;
                    if (cfg.priceRange) {
                        min = parseInt(cfg.priceRange.split(",")[0]);
                        max = parseInt(cfg.priceRange.split(",")[1]);
                    }
                    scope.min = 0;
                    scope.max = 1e5;
                    scope.priceRangeArray = [ scope.min, scope.max ];
                    if (!scope.isSmall) {
                        categoryIds = cfg.categoryIds;
                        brandIds = cfg.brandIds;
                        seriesIds = cfg.seriesIds;
                    }
                    if (skipApply !== true) {
                        scope.filtersChanged();
                    }
                };
                scope.filtersChanged = function() {
                    var searchCfg = scope.searchCfg;
                    searchCfg.text = scope.text;
                    searchCfg.pol = scope.pol;
                    searchCfg._page = 1;
                    searchCfg.priceRange = scope.priceRange;
                    searchCfg.age = scope.age;
                    searchCfg.exists = scope.exists;
                    searchCfg.productCode = scope.number;
                    searchCfg.onlyWithComments = scope.onlyWithComments;
                    searchCfg.categoryIds = $.map($.grep(scope.searchCategories, function(c, i) {
                        return c.checked;
                    }), function(cg, j) {
                        return cg.Id;
                    });
                    var brands = $.grep(scope.searchBrands, function(b, i) {
                        return b.checked;
                    });
                    searchCfg.brandIds = $.map(brands, function(bg, j) {
                        return bg.Id;
                    });
                    searchCfg.seriesIds = [];
                    scope.allUnchecked = {};
                    $.each(brands, function(i, b) {
                        scope.allUnchecked[b.Id] = 0;
                        $.each(b.Series, function(j, c) {
                            if (c.checked) {
                                scope.allUnchecked[b.Id]++;
                                searchCfg.seriesIds.push(c.Code);
                            }
                        });
                        scope.allUnchecked[b.Id] = scope.allUnchecked[b.Id] === 0 || scope.allUnchecked[b.Id] === b.Series.length;
                    });
                    if (scope.isSmall) {
                        $rootScope.$broadcast("search", searchCfg);
                    } else {
                        if ($rootScope.searchGlobalShadow) {
                            searchCfg = $rootScope.searchGlobalShadow;
                            delete $rootScope.searchGlobalShadow;
                            scope.setValues(searchCfg);
                            return;
                        }
                        $rootScope.$broadcast("searchGlobal", searchCfg);
                    }
                };
                scope.rangeChanged = function() {
                    clearTimeout(scope.rangeTimer);
                    scope.rangeTimer = setTimeout(function() {
                        scope.filtersChanged();
                        scope.$apply();
                    }, 500);
                };
                var someFieldsFilled = false;
                for (var p in $rootScope.searchGlobalShadow) {
                    if (p == "text" && $rootScope.searchGlobalShadow[p] || p != "text") {
                        someFieldsFilled = true;
                        break;
                    }
                }
                if (!scope.isSmall && $rootScope.searchGlobalShadow && someFieldsFilled) {
                    var searchCfg = $rootScope.searchGlobalShadow;
                    delete $rootScope.searchGlobalShadow;
                    scope.setValues(searchCfg, true);
                } else {
                    scope.text = scope.text || "";
                    scope.age = scope.age || 0;
                    scope.pol = scope.pol || 0;
                    scope.number = scope.number || "";
                    scope.small = scope.isSmall === true;
                    scope.min = scope.min || 0;
                    scope.max = scope.max || 1e5;
                    scope.priceRangeArray = [ scope.min, scope.max ];
                    scope.priceRange = scope.min + "," + scope.max;
                }
                var priceEl = el.find(".range-wrap");
                var html = '<div style="margin: 0px -8px 5px -10px;">' + '<input type="text" style="width:90px;display:inline-block;" class="form-control price-range-min">' + '<span style="text-align:center;display:inline-block;width:28px;">-</span>' + '<input type="text" style="width:90px;display:inline-block;float:right;" class="form-control price-range-max">' + "</div>" + '<input class="price-range" ng-model="priceRange" slider-range val="priceRangeArray" min="min" max="max" step="1" type="text" change-fn="rangeChanged">';
                var x = angular.element(html);
                priceEl.append(x);
                $compile(x)(scope);
                var priceMinEl = el.find(".price-range-min");
                var priceMaxEl = el.find(".price-range-max");
                var slider = el.find(".price-range");
                var sliderEl = slider.data("sliderEl");
                if (max) {
                    sliderEl.bootstrapSlider("setValue", [ min, max ]);
                    scope.priceRange = min + "," + max;
                    var priceMinEl = el.find(".price-range-min");
                    var priceMaxEl = el.find(".price-range-max");
                    priceMinEl.val(min);
                    priceMaxEl.val(max);
                }
                var fn = function() {
                    var _max = 1e5;
                    var minvalue = parseInt(priceMinEl.val()) || 0;
                    var maxvalue = parseInt(priceMaxEl.val()) || _max;
                    priceMinEl.val(minvalue);
                    priceMaxEl.val(maxvalue);
                    if (minvalue < 0) {
                        priceMinEl.val(0);
                        minvalue = 0;
                    }
                    if (minvalue > _max) {
                        priceMinEl.val(0);
                        minvalue = 0;
                    }
                    if (maxvalue < 0) {
                        priceMaxEl.val(_max);
                        maxvalue = _max;
                    }
                    if (maxvalue > _max) {
                        priceMaxEl.val(_max);
                        maxvalue = _max;
                    }
                    scope.priceRange = minvalue + "," + maxvalue;
                    sliderEl.bootstrapSlider("setValue", [ minvalue, maxvalue ]);
                    scope.filtersChanged();
                    scope.$apply();
                };
                priceMinEl.on("change", fn);
                priceMaxEl.on("change", fn);
                scope.$watch("priceRange", function(n, o) {
                    if (n) {
                        var v = n.split(",");
                        priceMinEl.val(v[0]);
                        priceMaxEl.val(v[1]);
                    }
                });
                $timeout(function() {
                    $(window).trigger("resize");
                });
            }
        };
    } ]);
})();

(function() {
    "use strict";
    angular.module("homer").directive("age", [ "$timeout", function($timeout) {
        return {
            restrict: "AE",
            templateUrl: "scripts/directives/age/age.html",
            replace: true,
            transclude: true,
            scope: {
                v: "=",
                onChange: "="
            },
            link: function(scope, elements, attr) {
                var el = $(elements[0]);
                scope.c1 = el.find("input[value=1]");
                scope.c2 = el.find("input[value=2]");
                scope.c3 = el.find("input[value=4]");
                scope.c4 = el.find("input[value=8]");
                scope.c5 = el.find("input[value=16]");
                scope.c6 = el.find("input[value=32]");
                scope.click = function() {
                    $timeout(function() {
                        var r = 0;
                        var items = el.find("input[name=age]:checked");
                        $.each(items, function(i, it) {
                            if (i == 0) {
                                r = parseInt(it.value);
                            } else {
                                r = r | parseInt(it.value);
                            }
                        });
                        scope.onChange(r);
                    });
                };
                scope.setValue = function(v) {
                    if (v === undefined || v === null) {
                        v = "";
                    }
                    var c1v = scope.c1v = (parseInt(v) & 1) == 1;
                    scope.c1.prop("checked", c1v);
                    var c2v = scope.c2v = (parseInt(v) & 2) == 2;
                    scope.c2.prop("checked", c2v);
                    var c3v = scope.c3v = (parseInt(v) & 4) == 4;
                    scope.c3.prop("checked", c3v);
                    var c4v = scope.c4v = (parseInt(v) & 8) == 8;
                    scope.c4.prop("checked", c4v);
                    var c5v = scope.c5v = (parseInt(v) & 16) == 16;
                    scope.c5.prop("checked", c5v);
                    var c6v = scope.c6v = (parseInt(v) & 32) == 32;
                    scope.c6.prop("checked", c6v);
                };
                scope.$watch("v", function(v) {
                    scope.setValue(v);
                });
            }
        };
    } ]);
})();

(function() {
    "use strict";
    angular.module("homer").directive("pol", [ "$timeout", "translationService", function($timeout, translationService) {
        return {
            restrict: "AE",
            templateUrl: "scripts/directives/pol/pol.html",
            replace: true,
            transclude: true,
            scope: {
                v: "=",
                onChange: "="
            },
            link: function(scope, elements, attr) {
                var el = $(elements[0]);
                scope.c1 = el.find("input[value=1]");
                scope.c2 = el.find("input[value=2]");
                scope.click = function() {
                    $timeout(function() {
                        var r = 0;
                        var items = el.find("input[name=pol]:checked");
                        $.each(items, function(i, it) {
                            if (i == 0) {
                                r = parseInt(it.value);
                            } else {
                                r = r | parseInt(it.value);
                            }
                        });
                        scope.onChange(r);
                    });
                };
                scope.translate = function(code) {
                    return translationService.translate(code);
                };
                scope.setValue = function(v) {
                    if (v === undefined || v === null) {
                        v = "";
                    }
                    var c1v = scope.c1v = (parseInt(v) & 1) == 1;
                    scope.c1.prop("checked", c1v);
                    var c2v = scope.c2v = (parseInt(v) & 2) == 2;
                    scope.c2.prop("checked", c2v);
                };
                scope.$watch("v", function(v) {
                    scope.setValue(v);
                });
            }
        };
    } ]);
})();

(function() {
    "use strict";
    angular.module("homer").directive("size", [ "$timeout", "$rootScope", "translationService", "$compile", function($timeout, $rootScope, translationService, $compile) {
        return {
            restrict: "AE",
            templateUrl: "scripts/directives/size/size.html",
            replace: true,
            transclude: true,
            scope: {
                v: "=",
                onChange: "="
            },
            link: function(scope, elements, attr) {
                var el = $(elements[0]);
                scope.width = 0;
                scope.height = 0;
                scope.length = 0;
                var trackFn = function(n, o) {
                    if (!scope.height) {
                        scope.v = parseInt(scope.length) + " x " + parseInt(scope.width);
                    } else {
                        scope.v = parseInt(scope.length) + " x " + parseInt(scope.width) + " x " + parseInt(scope.height);
                    }
                    if (scope.onChange) {
                        scope.onChange(scope.v);
                    }
                };
                scope.setValue = function(v) {
                    if (v === undefined || v === null) {
                        v = "";
                    }
                    var parts = v.split("x");
                    if (parts.length > 1) {
                        scope.length = parts[0].trim();
                        scope.width = parts[1].trim();
                    } else {
                        scope.length = 0;
                        scope.width = 0;
                    }
                    if (parts.length > 2) {
                        scope.height = parts[2].trim();
                    } else {
                        scope.height = 0;
                    }
                };
                scope.spinOption = {
                    verticalbuttons: true,
                    min: 0,
                    max: 1e4,
                    step: .1,
                    decimals: 1
                };
                scope.translate = function(code) {
                    return translationService.translate(code);
                };
                scope.$watch("v", function(v) {
                    scope.setValue(v);
                });
                scope.$watch("width", trackFn);
                scope.$watch("height", trackFn);
                scope.$watch("length", trackFn);
            }
        };
    } ]);
})();

(function() {
    "use strict";
    angular.module("homer").directive("recent", [ "$timeout", "$rootScope", "translationService", "$compile", function($timeout, $rootScope, translationService, $compile) {
        return {
            restrict: "AE",
            templateUrl: "scripts/directives/recent/recent.html",
            replace: true,
            transclude: true,
            scope: {
                items: "="
            },
            link: function(scope, elements, attr) {
                var el = $(elements[0]);
                scope.hideLeft = true;
                scope.hideRight = false;
                scope.pos = 0;
                var wrapper = el.find(".recent-wrapper");
                scope.getUrl = function(url) {
                    return url + "&_t=" + scope._t;
                };
                scope.translate = function(code) {
                    return translationService.translate(code);
                };
                var resizeFn = function() {
                    var items = wrapper.find(".recent-item");
                    if (items.length == 0) {
                        return;
                    }
                    var el = $(elements[0]);
                    var w = el.width() - 15 * 2;
                    scope.pw = w;
                    var iw = (w - 90) / 4;
                    scope.n = 4;
                    if (iw < 180) {
                        iw = (w - 60) / 3;
                        scope.n = 3;
                        if (iw < 180) {
                            scope.n = 2;
                            iw = (w - 30) / 2;
                            if (iw < 180) {
                                scope.n = 1;
                                iw = w;
                            }
                        }
                    }
                    iw = parseInt(iw);
                    $.each(items, function(i, t) {
                        $(t).css({
                            width: iw + "px"
                        });
                    });
                    var itemWidth = $(items[0]).outerWidth();
                    if (scope.pos > 0) {
                        var shift = scope.pos * (30 + itemWidth);
                        el.animate({
                            scrollLeft: shift + "px"
                        }, 0, "linear", function() {});
                    }
                };
                scope.$watch("items", function() {
                    resizeFn();
                    scope.show = true;
                });
                $timeout(function() {
                    $(window).on("resize", resizeFn);
                });
                scope.go = function(dir, e) {
                    if (e && e.preventDefault) {
                        e.preventDefault();
                        e.stopPropagation();
                    }
                    if (scope.scrolling) {
                        return false;
                    }
                    var item = wrapper.find(".recent-item")[0];
                    var itemWidth = $(item).outerWidth() + 30;
                    var skip = false;
                    if (dir == "left") {
                        if (scope.pos - 1 >= 0) {
                            scope.pos--;
                            var l = el[0].scrollLeft - itemWidth;
                            if (scope.pos - 1 < 0) {
                                scope.hideLeft = true;
                            } else {
                                scope.hideLeft = false;
                            }
                            scope.hideRight = false;
                        } else {
                            skip = true;
                            scope.hideLeft = true;
                        }
                    } else {
                        if (scope.pos + 1 + scope.n <= scope.items.length) {
                            scope.pos++;
                            var l = el[0].scrollLeft + itemWidth;
                            if (scope.pos + 1 + scope.n > scope.items.length) {
                                scope.hideRight = true;
                            } else {
                                scope.hideRight = false;
                            }
                            scope.hideLeft = false;
                        } else {
                            skip = true;
                            scope.hideRight = true;
                        }
                    }
                    if (!skip) {
                        scope.scrolling = true;
                        el.animate({
                            scrollLeft: l + "px"
                        }, 200, "swing", function() {
                            resizeFn();
                            scope.scrolling = false;
                        });
                    }
                };
                swipedetect(wrapper[0], "left right", function(swipedir, e) {
                    $timeout(function() {
                        if (swipedir == "left") {
                            scope.go("right", e);
                        } else if (swipedir == "right") {
                            scope.go("left", e);
                        }
                    });
                }, function(e) {
                    if (e.button !== 2) {
                        $(e.target).parents("a").click();
                    }
                });
            }
        };
    } ]);
})();

angular.module("homer").directive("touchSpin", touchSpin);

function touchSpin() {
    return {
        restrict: "A",
        scope: {
            spinOptions: "="
        },
        link: function(scope, element, attrs) {
            scope.$watch(scope.spinOptions, function() {
                render();
            });
            var render = function() {
                $(element).TouchSpin(scope.spinOptions);
            };
        }
    };
}

"use strict";

angular.module("homer").directive("ngThumb", [ "$window", function($window) {
    var helper = {
        support: !!($window.FileReader && $window.CanvasRenderingContext2D),
        isFile: function(item) {
            return angular.isObject(item) && item instanceof $window.File;
        },
        isImage: function(file) {
            var type = "|" + file.type.slice(file.type.lastIndexOf("/") + 1) + "|";
            return "|jpg|png|jpeg|bmp|gif|".indexOf(type) !== -1;
        }
    };
    return {
        restrict: "A",
        template: "<canvas/>",
        link: function(scope, element, attributes) {
            if (!helper.support) return;
            var params = scope.$eval(attributes.ngThumb);
            if (!helper.isFile(params.file)) return;
            if (!helper.isImage(params.file)) return;
            var canvas = element.find("canvas");
            var reader = new FileReader();
            reader.onload = onLoadFile;
            reader.readAsDataURL(params.file);
            function onLoadFile(event) {
                var img = new Image();
                img.onload = onLoadImage;
                img.src = event.target.result;
            }
            function onLoadImage() {
                var width = params.width || this.width / this.height * params.height;
                var height = params.height || this.height / this.width * params.width;
                canvas.attr({
                    width: width,
                    height: height
                });
                canvas[0].getContext("2d").drawImage(this, 0, 0, width, height);
            }
        }
    };
} ]);

(function() {
    "use strict";
    angular.module("homer").directive("translate", function() {
        return {
            restrict: "A",
            link: function(scope, element, attr) {
                var el = $(element[0]);
                el.ready(function() {
                    el.text(scope.translate(attr.translate));
                });
            }
        };
    });
})();

(function() {
    "use strict";
    angular.module("homer").directive("xdatatable", function() {
        return {
            restrict: "A",
            link: function(scope, element) {
                var el = $(element[0]);
                el.ready(function() {
                    el.dataTable({
                        language: {
                            url: "../../data/datatable/" + scope.langId + ".json"
                        },
                        ordering: false,
                        info: false
                    });
                });
            }
        };
    });
})();

(function(global, factory) {
    typeof exports === "object" && typeof module !== "undefined" && typeof require === "function" ? factory(require("../moment")) : typeof define === "function" && define.amd ? define([ "../moment" ], factory) : factory(global.moment);
})(this, function(moment) {
    "use strict";
    var enGb = moment.defineLocale("en-gb", {
        months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
        monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
        weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
        weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
        weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
        longDateFormat: {
            LT: "HH:mm",
            LTS: "HH:mm:ss",
            L: "DD/MM/YYYY",
            LL: "D MMMM YYYY",
            LLL: "D MMMM YYYY HH:mm",
            LLLL: "dddd, D MMMM YYYY HH:mm"
        },
        calendar: {
            sameDay: "[Today at] LT",
            nextDay: "[Tomorrow at] LT",
            nextWeek: "dddd [at] LT",
            lastDay: "[Yesterday at] LT",
            lastWeek: "[Last] dddd [at] LT",
            sameElse: "L"
        },
        relativeTime: {
            future: "in %s",
            past: "%s ago",
            s: "a few seconds",
            m: "a minute",
            mm: "%d minutes",
            h: "an hour",
            hh: "%d hours",
            d: "a day",
            dd: "%d days",
            M: "a month",
            MM: "%d months",
            y: "a year",
            yy: "%d years"
        },
        ordinalParse: /\d{1,2}(st|nd|rd|th)/,
        ordinal: function(number) {
            var b = number % 10, output = ~~(number % 100 / 10) === 1 ? "th" : b === 1 ? "st" : b === 2 ? "nd" : b === 3 ? "rd" : "th";
            return number + output;
        },
        week: {
            dow: 1,
            doy: 4
        }
    });
    return enGb;
});

(function(global, factory) {
    typeof exports === "object" && typeof module !== "undefined" && typeof require === "function" ? factory(require("../moment")) : typeof define === "function" && define.amd ? define([ "../moment" ], factory) : factory(global.moment);
})(this, function(moment) {
    "use strict";
    var ru = moment.defineLocale("ru", {
        months: "Январь_Февраль_Март_Апрель_Май_Июнь_Июль_Август_Сентябрь_Октябрь_Ноябрь_Декабрь".split("_"),
        monthsShort: "Янв_Фев_Мар_Апр_Май_Июн_Июл_Авг_Сен_Окт_Ноя_Дек".split("_"),
        weekdays: "Вокресенье_Понедельник_Вторник_Среда_Четверг_Пятница_Суббота".split("_"),
        weekdaysShort: "Вск_Пнд_Втр_Срд_Чтв_Птн_Сбт".split("_"),
        weekdaysMin: "Вс_Пн_Вт_Ср_Чт_Пт_Сб".split("_"),
        longDateFormat: {
            LT: "HH:mm",
            LTS: "HH:mm:ss",
            L: "DD.MM.YYYY",
            LL: "D MMMM YYYY",
            LLL: "D MMMM YYYY HH:mm",
            LLLL: "dddd, D MMMM YYYY HH:mm"
        },
        calendar: {
            sameDay: "[Сегодня в] LT",
            nextDay: "[Завтра в] LT",
            nextWeek: "dddd [в] LT",
            lastDay: "[Вчера в] LT",
            lastWeek: "[Посл] dddd [at] LT",
            sameElse: "L"
        },
        relativeTime: {
            future: "через %s",
            past: "%s назад",
            s: "неск. секунд",
            m: "минута",
            mm: "%d минут",
            h: "час",
            hh: "%d часов",
            d: "день",
            dd: "%d дней",
            M: "месяц",
            MM: "%d месяцев",
            y: "год",
            yy: "%d лет"
        },
        ordinalParse: /\d{1,2}(st|nd|rd|th)/,
        ordinal: function(number) {
            var b = number % 10, output = ~~(number % 100 / 10) === 1 ? "й" : b === 1 ? "й" : b === 2 ? "й" : b === 3 ? "й" : "й";
            return number + output;
        },
        week: {
            dow: 1,
            doy: 4
        }
    });
    return ru;
});

angular.module("homer").controller("googleMapCtrl", googleMapCtrl);

function googleMapCtrl($scope, $timeout) {
    $scope.mapOptions = {
        zoom: 14,
        center: new google.maps.LatLng(50.5904776, 30.4831982),
        styles: [ {
            featureType: "landscape",
            stylers: [ {
                saturation: -100
            }, {
                lightness: 65
            }, {
                visibility: "on"
            } ]
        }, {
            featureType: "poi",
            stylers: [ {
                saturation: -100
            }, {
                lightness: 51
            }, {
                visibility: "simplified"
            } ]
        }, {
            featureType: "road.highway",
            stylers: [ {
                saturation: -100
            }, {
                visibility: "simplified"
            } ]
        }, {
            featureType: "road.arterial",
            stylers: [ {
                saturation: -100
            }, {
                lightness: 30
            }, {
                visibility: "on"
            } ]
        }, {
            featureType: "road.local",
            stylers: [ {
                saturation: -100
            }, {
                lightness: 40
            }, {
                visibility: "on"
            } ]
        }, {
            featureType: "transit",
            stylers: [ {
                saturation: -100
            }, {
                visibility: "simplified"
            } ]
        }, {
            featureType: "administrative.province",
            stylers: [ {
                visibility: "off"
            } ]
        }, {
            featureType: "water",
            elementType: "labels",
            stylers: [ {
                visibility: "on"
            }, {
                lightness: -25
            }, {
                saturation: -100
            } ]
        }, {
            featureType: "water",
            elementType: "geometry",
            stylers: [ {
                hue: "#ffff00"
            }, {
                lightness: -25
            }, {
                saturation: -97
            } ]
        } ],
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
}

angular.module("homer").controller("appCtrl", appCtrl);

window.w4p = new Wayforpay();

function appCtrl($http, $scope, $timeout, $rootScope, loginAppFactory, localStorageService, catalogService, translationService, adminService, cartService, $state, $sce, vcRecaptchaService) {
    $rootScope.sortBy = localStorageService.get("sortBy") || "ByDateDesc";
    $rootScope.viewType = localStorageService.get("viewType") || 0;
    $rootScope.header = true;
    $rootScope.code = "";
    $scope.search = {
        text: ""
    };
    $rootScope.searchGlobal = {
        text: ""
    };
    $rootScope._t = 0;
    $scope.category = {};
    $scope.brand = {};
    $scope.categories = [];
    $scope.deliveryMethod = {};
    $scope.deliveryMethods = [];
    $rootScope.admin = false;
    $scope.areas = [];
    $scope.cities = [];
    $scope.warehouses = [];
    $scope.area = {};
    $scope.city = {};
    $scope.warehouse = {};
    $scope.comments = [];
    $scope.showMoreComments = false;
    $scope.commentsPage = 2;
    $scope.addingComment = false;
    $scope.comment = {
        title: "",
        text: "",
        productId: ""
    };
    $scope.getCategoryUrl = function(c) {
        if (c.Series) {
            var hasSeries = false;
            for (var s in c.Series) {
                if (c.Series[s]) {
                    hasSeries = true;
                    break;
                }
            }
        }
        if (!hasSeries) {
            return "#/catalog/" + c.Code;
        } else {
            return "#/catalog/" + c.Code + "/series";
        }
    };
    $scope.customClearFilter = function() {
        $rootScope.$broadcast("applyCustom");
    };
    $scope.fillProductParams = function(m) {
        var fn = function(i, p) {
            p.params = [];
            if (p.Brand) {
                p.params.push({
                    name: $scope.translate("Brand"),
                    value: p.Brand.Name,
                    image: $scope.getUrl("/api/Catalog/GetPreviewBrandImage?id=" + p.Brand.Id)
                });
            }
            if (p.Series) {
                p.params.push({
                    name: $scope.translate("Series"),
                    value: p.Series.Name
                });
            }
            if (p.Pol) {
                var boyFlag = (p.Pol & 1) !== 0;
                var girlFlag = (p.Pol & 2) !== 0;
                var v = "";
                if (boyFlag && !girlFlag) {
                    v = $scope.translate("ForBoys");
                }
                if (!boyFlag && girlFlag) {
                    v = $scope.translate("ForGirls");
                }
                if (boyFlag && girlFlag) {
                    v = $scope.translate("ForBothBoysAndGirls");
                }
                p.params.push({
                    name: $scope.translate("Pol"),
                    value: v
                });
            }
            if (p.Age) {
                v = "";
                var ages = 0;
                if ((p.Age & 1) !== 0) {
                    v += "0-3";
                    ages++;
                }
                if ((p.Age & 2) !== 0) {
                    if (v) {
                        v += ", ";
                    }
                    v += "3-6";
                    ages++;
                }
                if ((p.Age & 4) !== 0) {
                    if (v) {
                        v += ", ";
                    }
                    v += "4-7";
                    ages++;
                }
                if ((p.Age & 8) !== 0) {
                    if (v) {
                        v += ", ";
                    }
                    v += "7-9";
                    ages++;
                }
                if ((p.Age & 16) !== 0) {
                    if (v) {
                        v += ", ";
                    }
                    v += "9-16";
                    ages++;
                }
                if ((p.Age & 32) !== 0) {
                    if (v) {
                        v += ", ";
                    }
                    v += "16+";
                    ages++;
                }
                if (ages === 6) {
                    v = $scope.translate("AllAges");
                }
                p.params.push({
                    name: $scope.translate("Age"),
                    value: v
                });
            }
            p.params.push({
                name: $scope.translate("Number"),
                value: p.ProductCode
            });
            if (p.InternalProductCode) {
                p.params.push({
                    name: $scope.translate("Article"),
                    value: p.InternalProductCode
                });
            }
            if (p.WarrantyNumberOfMonth) {
                p.params.push({
                    name: $scope.translate("WarrantyNumberOfMonth"),
                    value: p.WarrantyNumberOfMonth
                });
            }
            if (p.Size && p.Size.indexOf("0 x 0") === -1) {
                p.params.push({
                    name: $scope.translate("Size"),
                    value: p.Size
                });
            }
        };
        if (Array.isArray(m)) {
            $.each(m, fn);
        } else {
            fn(0, m);
        }
    };
    $scope.changeView = function(t) {
        $scope.viewType = t;
        localStorageService.set("viewType", t);
    };
    $scope.keyPress = function(e) {
        if (e.keyCode === 13) {
            $scope.applySearchFilter();
        }
    };
    $scope.applySearchFilter = function() {
        $rootScope.searchGlobalShadow = $.extend({}, $rootScope.searchGlobal);
        $rootScope.currentPage = 1;
        $state.go("search");
    };
    $scope.$on("searchGlobal", function(scope, searchCfg) {
        $rootScope.searchGlobal = searchCfg;
    });
    $scope.getProfileUrl = function(s) {
        if (!s) {
            return;
        }
        if (s.Type === "Twitter") {
            return "https://twitter.com/" + s.Name;
        } else if (s.Type === "Facebook") {
            return "https://facebook.com/" + s.ExternalId;
        } else if (s.Type === "Google") {
            return "https://plus.google.com/" + s.ExternalId;
        } else if (s.Type === "Instagram") {
            return "https://instagram.com/" + s.Name;
        }
    };
    $scope.getProfileImage = function(s) {
        if (!s) {
            return;
        }
        if (s.Type === "Twitter") {
            return "https://avatars.io/twitter/" + s.Name + "/original";
        } else if (s.Type === "Facebook") {
            return "https://graph.facebook.com/" + s.ExternalId + "/picture?type=square&width=200&height=200";
        } else {
            if (s.PictureUrl) {
                return s.PictureUrl.indexOf("?") !== -1 ? s.PictureUrl + "&sz=180" : s.PictureUrl + "?sz=200";
            } else {
                return "/images/user.png";
            }
        }
    };
    $scope.$on("requiredLoggedIn", function() {
        $scope.requireLoggedIn = true;
        if (!$scope.loggedIn) {
            $scope.checkLogon(function() {
                if (!$scope.loggedIn) {
                    swal({
                        title: $scope.translate("Information"),
                        text: $scope.translate("NeedLoggonToProceed"),
                        type: "success"
                    });
                    $state.go("login");
                }
            });
        }
    });
    $scope.$on("optionalLoggedIn", function() {
        $scope.requireLoggedIn = false;
    });
    $scope.loadMoreComments = function(productId) {
        $scope.getComments(productId, $scope.commentsPage);
    };
    $scope.addSubComment = function(comment, e) {
        if (e.keyCode === 27) {
            comment.subComment = "";
            comment.readyToComment = false;
            return;
        }
        if (e.keyCode !== 13) {
            return;
        }
        if (!comment.subComment) {
            return;
        }
        $scope.addingComment = true;
        var model = {
            CommentId: comment.Id,
            UserId: comment.User.Id,
            ExternalId: comment.User.ExternalId,
            ProductId: comment.Product ? comment.Product.Id : "",
            Text: comment.subComment
        };
        translationService.addSubComment(model).then(function(subComment) {
            comment.showSubComments = true;
            $scope.addingComment = false;
            comment.SubComments = comment.SubComments || [];
            comment.SubComments.push(subComment);
            comment.subComment = "";
            comment.readyToComment = false;
        }, function(err) {
            debugger;
            $scope.addingComment = false;
        });
    };
    $scope.addNewComment = function(model, cb) {
        $scope.addingComment = true;
        translationService.addComment(model).then(function(comment) {
            $scope.addingComment = false;
            $scope.comments.splice(0, 0, comment);
            if (cb) {
                cb();
            }
        }, function(err) {
            debugger;
            $scope.addingComment = false;
        });
    };
    $scope.addComment = function() {
        $scope.addNewComment($scope.comment, function() {
            $scope.comment.text = "";
            $scope.comment.title = "";
        });
    };
    $scope.repeatGetComments = function() {
        $scope.commentsPage = 1;
        $scope.getComments($scope.lastProductId, 1);
    };
    $scope.clearComment = function() {
        $scope.comment = {
            title: "",
            text: "",
            productId: ""
        };
    };
    $scope.getComments = function(productId, page, cb) {
        $scope.lastProductId = productId;
        if (page === 1) {
            $scope.showMoreComments = false;
            $scope.commentsPage = 1;
        }
        $scope.loadingMore = true;
        if (page === 1) {
            $scope.comments = [];
        }
        $timeout(function() {
            translationService.getComments(productId, page).then(function(comments) {
                if ($scope.commentsPage === 1) {
                    $scope.comments = comments;
                } else {
                    $scope.comments = $scope.comments.concat(comments);
                }
                $scope.showMoreComments = comments.length === 4;
                if ($scope.showMoreComments) {
                    $scope.commentsPage++;
                }
                $scope.loadingMore = false;
                $scope.tabSelected();
                if (cb) {
                    cb();
                }
            }, function(err) {
                debugger;
                $scope.loadingMore = false;
            });
        });
    };
    $scope.removeComment = function(c, sc, isSubComment) {
        var comment = isSubComment ? sc : c;
        swal({
            title: $scope.translate("RemoveCommentQuestion"),
            text: $scope.translate("Question"),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: $scope.translate("Yes"),
            cancelButtonText: $scope.translate("No"),
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                translationService.removeComment(comment.Id).then(function() {
                    if (isSubComment) {
                        var index = c.SubComments.indexOf(sc);
                        if (index >= 0) {
                            c.SubComments.splice(index, 1);
                        }
                    } else {
                        index = $scope.comments.indexOf(c);
                        if (index >= 0) {
                            $scope.comments.splice(index, 1);
                        }
                        $scope.repeatGetComments();
                    }
                }, function(err) {
                    debugger;
                });
            }
        });
    };
    $rootScope.$broadcast("optionalLoggedIn");
    $scope.trustAsHtml = function(text) {
        return $sce.trustAsHtml(text);
    };
    cartService.getDeliveryMethods().then(function(methods) {
        $scope.deliveryMethods = methods;
        if ($scope.deliveryMethods.length > 0) {
            $scope.deliveryMethod.selected = $scope.deliveryMethods[0];
            $rootScope.$broadcast("deliveryMethodsLoaded");
        }
    }, function(err) {
        debugger;
    });
    catalogService.getCategories().then(function(categories) {
        $scope.categories = categories;
        if ($scope.categories.length > 0) {
            $scope.category.selected = $scope.categories[0];
        }
    }, function(err) {
        debugger;
    });
    catalogService.getBrands("").then(function(brands) {
        $scope.brands = brands;
        if ($scope.brands.length > 0) {
            $scope.brand.selected = $scope.brands[0];
        }
    }, function(err) {
        debugger;
    });
    $scope.cart = localStorageService.get("cart") || {};
    $scope.products = $scope["products"] || [];
    $scope.preventDefault = function(e) {
        e.preventDefault();
        e.stopPropagation();
    };
    $scope.clickStar = function(p, s, e) {
        p.Rating = parseFloat(s).toFixed(1).replace(".0", "");
        var model = {
            Id: p.Id,
            Count: s
        };
        translationService.productRate(model).then(function(result) {
            p.Rate = result.avgRate.toFixed(1).replace(".0", "");
            p.RateCount = result.count;
        }, function(err) {
            debugger;
        });
        e.stopPropagation();
        e.preventDefault();
    };
    $scope.clickedStar = function(p) {
        if (p) {
            return parseFloat(p.Rating).toFixed(1).replace(".0", "") || 0;
        }
        return 0;
    };
    $scope.isCartEmpty = function() {
        return $scope.cartCount() === 0;
    };
    $scope.cartCount = function() {
        var r = 0;
        for (var i in $scope.cart) {
            r++;
        }
        return r;
    };
    $scope.inCart = function(p) {
        if (!p) {
            return false;
        }
        return $scope.cart[p.Id];
    };
    $scope.loadCities = function() {
        $scope.areas = [];
        $scope.cities = [];
        $scope.warehouses = [];
        $scope.area.selected = undefined;
        $scope.city.selected = undefined;
        $scope.warehouse.selected = undefined;
        adminService.getNPAreas().then(function(response) {
            $scope.areas = response;
        }, function(err) {
            debugger;
        });
    };
    $scope.isNP = function(dm) {
        var scope = $scope;
        if (!dm || !dm.selected || !dm.selected.Id) {
            return false;
        }
        return dm.selected.Id.toUpperCase() === "B683A766-4239-4E45-B392-AD7C23E91EB0";
    };
    $scope.onSelectNPCity = function(item) {
        $scope.warehouses = [];
        $scope.warehouse.selected = undefined;
        adminService.getNPWarehouses(item.Ref).then(function(response) {
            $scope.warehouses = response;
        }, function(err) {
            debugger;
        });
    };
    $scope.onSelectNPArea = function(item) {
        $scope.cities = [];
        $scope.city.selected = undefined;
        $scope.warehouses = [];
        $scope.warehouse.selected = undefined;
        adminService.getNPCities(item.Ref).then(function(response) {
            $scope.cities = response;
        }, function(err) {
            debugger;
        });
    };
    $scope.goToCart = function(p, e) {
        if ($scope.cart[p.Id]) {
            delete $scope.cart[p.Id];
        } else {
            $scope.cart[p.Id] = p;
            $scope.cart[p.Id].Count = 1;
        }
        localStorageService.set("cart", $scope.cart);
        e.preventDefault();
        e.stopPropagation();
    };
    $scope.convertUTCDateToLocalDate = function(date) {
        var newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1e3);
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();
        newDate.setHours(hours + offset);
        return newDate;
    };
    $scope.formatDate = function(dt, utc) {
        return adminService.formatDate(dt, utc);
    };
    $scope.formatShortDate = function(dt) {
        if (!dt) {
            return "";
        }
        if (!dt["getDate"]) {
            dt = new Date(Date.parse(dt));
        }
        var d = dt.getDate();
        var m = dt.getMonth() + 1;
        var y = dt.getFullYear();
        if (d < 10) {
            d = "0" + d;
        }
        if (m < 10) {
            m = "0" + m;
        }
        return d + "." + m + "." + y;
    };
    $scope.changeLanguage = function(langId) {
        $scope.langId = langId;
        localStorageService.set("langId", langId);
        translationService.changeLocale(langId).then(function(response) {
            var langId = localStorageService.get("langId") || 0;
            setTimeout(function() {
                window.location.reload();
            }, 100);
        }, function() {
            debugger;
        });
    };
    $scope.logout = function() {
        var request = {
            method: "post",
            url: "/api/Account/Logout",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + $scope.token
            }
        };
        return $http(request).then(function(response) {
            $rootScope.$broadcast("cleanToken");
            $rootScope.$broadcast("checkLogon");
            $timeout(function() {
                $state.go("index");
            }, 50);
        }, function(err) {
            $state.go("index");
        });
    };
    $scope.translate = function(code) {
        return translationService.translate(code);
    };
    $scope.$on("authItem", function(o, data) {
        $scope.auth = data;
        localStorageService.set("auth", $scope.auth);
    });
    $scope.$on("updateLoginMsg", function(o, msg) {
        $scope.loginMsg = msg;
    });
    $scope.$on("updateErrorMsg", function(o, msg) {
        $scope.errorMsg = msg;
    });
    $scope.$on("checkLogon", function() {
        $scope.checkLogon();
    });
    $scope.$on("checkToken", function() {
        var oldToken = $scope.token;
        $scope.token = localStorageService.get("token");
        $scope.auth = localStorageService.get("auth");
        if (!$scope.auth && $scope.loggedIn) {
            $scope.logout();
        }
    });
    $scope.$on("cleanToken", function() {
        $scope.auth = undefined;
        $scope.token = undefined;
        localStorageService.remove("auth");
        localStorageService.remove("token");
        $rootScope.admin = false;
    });
    $scope.registerLocalStorageVar = function(name, cb) {
        if (localStorageService.get(name)) {
            $scope[name] = localStorageService.get(name);
        } else {
            localStorageService.set(name, $scope[name]);
            localStorageService.bind($scope, name);
        }
        if (cb) {
            cb();
        }
    };
    $scope.registerLocalStorageVar("token");
    $scope.registerLocalStorageVar("auth");
    $scope.scrollTopFn = function(event, toState, toParams, fromState, fromParams) {
        if (isMobile.any()) {
            $(window).scrollTop(0);
        } else {
            $(function() {
                $(window).scrollTop(0);
                if (window.globalScrollTopTop) {
                    window.globalScrollReset = true;
                    window.globalScrollTopTop();
                }
                if (!$scope.scrollInitialized) {
                    $(".scrollbar").remove();
                    $scope.scrollInitialized = true;
                    window.dzsscr_init($("body"), {
                        settings_multiplier: $.browser.mozilla ? 4 : $.browser.ua.toLowerCase().indexOf("edge") !== -1 ? 1 : 2,
                        type: "scrollTop",
                        settings_refresh: 1e3,
                        settings_smoothing: "on",
                        settings_autoresizescrollbar: "on",
                        settings_skin: "skin_apple",
                        enable_easing: "on",
                        cb: function() {
                            if (!isMobile.any()) {
                                onscrollx();
                            }
                        }
                    });
                }
            });
        }
        onscrollx(true);
    };
    $scope.checkLogon = function(cb) {
        var request = {
            method: "post",
            url: "/api/Account/CheckLogon",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + $scope.token
            }
        };
        return $http(request).then(function(response) {
            $scope.loggedIn = response.data;
            if ($scope.loggedIn) {
                $scope.loginMsg = "";
                $scope.errorMsg = "";
                adminService.ping().then(function(response) {
                    $rootScope.admin = response;
                }, function() {
                    $rootScope.admin = false;
                });
                if (cb) {
                    cb();
                }
            } else {
                $rootScope.admin = false;
                if (cb) {
                    cb();
                }
            }
        }, function(err) {
            debugger;
            $scope.loggedIn = false;
            $rootScope.admin = false;
            if (cb) {
                cb();
            }
        });
    };
    $scope.handleError = function(err) {
        $scope.errorMsg = "";
        $scope.loginMsg = "";
        var msgs = err.data ? err.data.ModelState : [ err.statusText || "" ];
        $scope.validation = {};
        for (var msg in msgs) {
            $.each(msgs[msg], function(i, m) {
                if (msg === "") {
                    if ($scope.errorMsg) {
                        $scope.errorMsg += "<br />";
                    }
                    $scope.errorMsg += m;
                } else {
                    var nm = msg.replace("model.", "").toLowerCase();
                    $scope.validation[nm] = $scope.validation[nm] || "";
                    if ($scope.validation[nm]) {
                        $scope.validation[nm] += "<br />";
                    }
                    $scope.validation[nm] += $scope.translate(m);
                }
            });
        }
    };
    $scope.crossDomainJsonp = function(event, toState, toParams, fromState, fromParams) {};
    $scope.clearWindowResizeEvents = function() {
        fixWrapperHeight();
        if (!$scope.onceResize) {
            $scope.onceResize = 1;
            $(window).bind("resize", function() {
                var w = getBodyWidth();
                $rootScope.small = getBodySmall(w);
                $rootScope.medium = getBodyMedium(w);
                if (!$rootScope.$$phase) {
                    $rootScope.$apply();
                }
            });
        }
    };
    $scope.checkRegistration = function(token, redirect) {
        loginAppFactory.checkRegistration(token).then(function(response) {
            if (response.data.HasRegistered) {
                $rootScope.$broadcast("authItem", response.data);
                localStorageService.set("token", token);
                $rootScope.$broadcast("checkLogon");
                if (redirect) {
                    if ($scope.$$childTail && $scope.$$childTail.widgetId !== undefined) {
                        setTimeout(function() {
                            $("div.g-recaptcha").hide();
                        }, 0);
                        var rct = 0;
                        var r = setInterval(function() {
                            if (rct >= 60 || $scope.$$childTail.widgetId >= 0) {
                                clearInterval(r);
                                $state.go("index");
                            } else {
                                rct++;
                            }
                            $scope.$apply();
                        }, 500);
                    } else {
                        $state.go("index");
                    }
                }
            } else {
                loginAppFactory.signupExternal(token).then(function(response) {
                    $rootScope.$broadcast("authItem", response.data);
                    localStorageService.set("token", token);
                    $rootScope.$broadcast("checkLogon");
                    if (redirect) {
                        $state.go("index");
                    }
                }, function(err) {
                    $scope.errorMsg = err.data.Message;
                    $scope.loginMsg = "";
                });
            }
        }, function(err) {
            $scope.errorMsg = "Check Registration failed";
            $scope.loginMsg = "";
        });
    };
    $scope.checkLocationHash = function() {
        if (location.hash) {
            debugger;
            if (location.hash.indexOf("access_token=") !== -1 && location.hash.split("access_token=")) {
                $scope.token = location.hash.split("access_token=")[1].split("&")[0];
                if ($scope.token) {
                    $scope.checkRegistration($scope.token, true);
                }
            } else {
                $rootScope.$broadcast("checkToken");
            }
        }
    };
    $scope.$on("$stateChangeStart", function(event, toState, toParams, fromState, fromParams) {
        $rootScope.searchGlobalShadow = $.extend({}, $rootScope.searchGlobal);
        $("#ui-view").data("lh", null);
    });
    $scope.tabSelected = function() {
        $(window).trigger("resize");
        setTimeout(function() {
            $(window).trigger("resize");
            setTimeout(function() {
                $(window).trigger("resize");
                setTimeout(function() {
                    $(window).trigger("resize");
                }, 250);
            }, 150);
        }, 50);
    };
    $scope.stop = function(e) {
        e.stopPropagation();
        e.preventDefault();
    };
    $scope.initScroll = function() {
        $scope.scrollTopFn();
        if (isMobile.any()) {
            $("body").css({
                overflow: "auto"
            });
        } else {
            $("body").css({
                overflow: "hidden"
            });
            if (!$scope.initScrollOnce) {
                $scope.initScrollOnce = true;
                $(function() {
                    setTimeout(function() {
                        $("body").on("click", function() {
                            $scope.tabSelected();
                        });
                        $("body").on("keyup", function() {
                            $scope.tabSelected();
                        });
                        $("body").on("keydown", function() {
                            $scope.tabSelected();
                        });
                        $scope.tabSelected();
                    }, 0);
                });
            }
        }
    };
    $scope.$on("$viewContentLoaded", function() {
        $scope.initScroll();
    });
    $scope.$on("$stateChangeStart", function() {
        debugger;
        $scope.scrollTopFn();
        if ($("#mobile-collapse").hasClass("in")) {
            $(".mobile-menu button").click();
        }
    });
    $scope.$on("$stateChangeSuccess", function(event, toState, toParams, fromState, fromParams) {
        $rootScope.currentState = $scope.currentState = toState ? toState.name : "";
        $scope.fromState = fromState ? fromState.name : "";
        $scope.clearWindowResizeEvents();
        $scope.checkLocationHash();
    });
    $scope.getUrl = function(url) {
        return url + "&_t=" + $rootScope._t;
    };
    $scope.$on("forceRandom", function() {
        $rootScope._t++;
    });
    $rootScope.$broadcast("forceRandom");
    $scope.stanimation = "bounceIn";
    $scope.runIt = true;
    $scope.runAnimation = function() {
        $scope.runIt = false;
        $timeout(function() {
            $scope.runIt = true;
        }, 10);
    };
    $rootScope.$broadcast("checkLogon");
    $(window).trigger("resize");
}

angular.module("homer").factory("loginAppFactory", function(localStorageService, $q, $http) {
    var o = {};
    o.checkRegistration = function(token) {
        var request = {
            method: "get",
            url: "/api/Account/GetUserInfo",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + token
            }
        };
        return $http(request);
    };
    o.signupExternal = function(token) {
        var request = {
            method: "post",
            url: "/api/Account/RegisterExternal",
            headers: {
                "Content-Type": "application/json",
                Authorization: "Bearer " + token
            },
            data: {}
        };
        return $http(request);
    };
    return o;
});

var isMobile = {
    Android: function() {
        return navigator.userAgent.toLowerCase().match(/android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.toLowerCase().match(/blackberry/i);
    },
    iOS: function() {
        return navigator.userAgent.toLowerCase().match(/iphone|ipad|ipod/i);
    },
    Opera: function() {
        return navigator.userAgent.toLowerCase().match(/opera mini/i);
    },
    Windows: function() {
        return navigator.userAgent.toLowerCase().match(/iemobile/i);
    },
    any: function() {
        return isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows();
    }
};

angular.module("homer").controller("indexCtrl", indexCtrl);

function indexCtrl($http, $scope, $timeout, $state, $rootScope, translationService) {
    updateTitle($state, translationService);
    $rootScope.$broadcast("optionalLoggedIn");
    $rootScope.header = true;
    $rootScope.code = "";
    $scope.showItem = function(p, prefix) {
        var el = $("#" + prefix + p.Id);
        el.fadeIn(250, function() {
            el.css({
                opacity: 1,
                display: "inline-block"
            });
        });
        $scope.tabSelected();
    };
    $scope.$watch("categories", function(n, o) {
        if (n && n.length > 0) {
            $timeout(function() {
                window.prerenderReady = true;
            }, 500);
        }
    });
    $timeout(function() {
        window.prerenderReady = true;
    }, 3e3);
}

angular.module("homer").controller("emptyCtrl", emptyCtrl);

function emptyCtrl($http, $scope, $timeout, $state, $rootScope, translationService) {
    updateTitle($state, translationService);
    $rootScope.header = true;
    $rootScope.code = "";
    $rootScope.$broadcast("optionalLoggedIn");
    $timeout(function() {
        window.prerenderReady = true;
    }, 3e3);
}

angular.module("homer").controller("faqCtrl", faqCtrl);

function faqCtrl($http, $scope, $timeout, $state, $rootScope, translationService, catalogService) {
    updateTitle($state, translationService);
    $rootScope.header = true;
    $rootScope.code = "";
    $rootScope.$broadcast("optionalLoggedIn");
    $scope.model = {
        faqs: []
    };
    $scope.loadingMore = true;
    catalogService.getFaqs().then(function(faqs) {
        $scope.model.faqs = faqs;
        $scope.loadingMore = false;
        $timeout(function() {
            window.prerenderReady = true;
        }, 3e3);
    }, function(err) {
        debugger;
        $scope.loadingMore = false;
        $timeout(function() {
            window.prerenderReady = true;
        }, 3e3);
    });
}

angular.module("homer").controller("contactsCtrl", emptyCtrl);

function contactsCtrl($http, $scope, $timeout, $state, $rootScope, translationService) {
    updateTitle($state, translationService);
    $rootScope.header = true;
    $rootScope.code = "";
    $rootScope.$broadcast("optionalLoggedIn");
    $timeout(function() {
        window.prerenderReady = true;
    }, 3e3);
}

angular.module("homer").controller("catalogCtrl", catalogCtrl);

function catalogCtrl($http, $scope, $timeout, $state, $rootScope, catalogService, translationService, localStorageService) {
    var scope = $scope;
    $rootScope.header = true;
    updateTitle($state, translationService);
    $rootScope.$broadcast("optionalLoggedIn");
    $scope.slides = [];
    $scope.startLoading = true;
    if ($rootScope.prevCode && $rootScope.prevCode !== $state.params.code) {}
    $scope.code = $state.params.code;
    $rootScope.code = $state.params.code;
    $scope.cseries = $state.params.series;
    $rootScope.cseries = $state.params.series;
    if ($scope.cseries) {
        $rootScope.latestSearch = $rootScope.latestSearch || {
            text: ""
        };
        var o = $rootScope.latestSearch;
        o.seriesIds = [];
        $rootScope._searchCfg = $rootScope._searchCfg || {
            categoryIds: [],
            brandIds: []
        };
        $rootScope._searchCfg.seriesIds = [];
    } else {
        $rootScope.latestSearch = $rootScope.latestSearch || undefined;
        $rootScope._searchCfg = $rootScope._searchCfg || undefined;
    }
    if ($rootScope.latestSearch && $rootScope.latestSearch.seriesIds) {
        var seriesIds = $rootScope.latestSearch.seriesIds;
        var _seriesIds = $rootScope._searchCfg.seriesIds;
        var brandIds = $rootScope.latestSearch.brandIds;
        var _brandIds = $rootScope._searchCfg.brandIds;
        var categoryIds = $rootScope.latestSearch.categoryIds;
        var _categoryIds = $rootScope._searchCfg.categoryIds;
        var removeSeriesIds = [], removeCategoryIds = [], removeBrandIds = [];
        $.each(seriesIds, function(i, s) {
            if (!($rootScope.prevCode && $rootScope.prevCode !== $state.params.code)) {
                var index = _seriesIds.indexOf(s);
                if (index === -1) {
                    _seriesIds.push(s);
                }
            } else {
                removeSeriesIds.push(i);
            }
        });
        $.each(removeSeriesIds, function(i, r) {
            seriesIds.splice(r, 1);
        });
        $.each(categoryIds, function(i, s) {
            if (!($rootScope.prevCode && $rootScope.prevCode !== $state.params.code)) {
                var index = _categoryIds.indexOf(s);
                if (index === -1) {
                    _categoryIds.push(s);
                }
            } else {
                removeCategoryIds.push(i);
            }
        });
        $.each(removeCategoryIds, function(i, r) {
            categoryIds.splice(r, 1);
        });
        $.each(brandIds, function(i, s) {
            if (!($rootScope.prevCode && $rootScope.prevCode !== $state.params.code)) {
                var index = _brandIds.indexOf(s);
                if (index === -1) {
                    _brandIds.push(s);
                }
            } else {
                removeBrandIds.push(i);
            }
        });
        $.each(removeBrandIds, function(i, r) {
            brandIds.splice(r, 1);
        });
        if (seriesIds && $scope.cseries && seriesIds.indexOf($scope.cseries) === -1) {
            seriesIds.push($scope.cseries);
        }
    }
    $rootScope.prevCode = $state.params.code;
    if ($rootScope.latestSearch) {
        $rootScope.search = $rootScope.searchCfg = jQuery.extend(true, {}, $rootScope.latestSearch);
    } else {
        $rootScope.search = $rootScope.searchCfg = {
            text: ""
        };
    }
    $timeout(function() {
        $scope.load(1, true);
    }, 100);
    $scope.showfilters = false;
    if (!$scope.code) {
        $state.go("index");
    }
    $scope.pageSizes = [ {
        value: 12,
        active: true
    }, {
        value: 24
    }, {
        value: 48
    } ];
    $scope.pages = {
        currentPage: 1,
        totalCount: 1,
        visibleCount: 7
    };
    $scope.getPageSize = function() {
        var value = $scope.pageSizes[0].value;
        $.each($scope.pageSizes, function(i, s) {
            if (s.active) {
                value = s.value;
                return false;
            }
        });
        return value;
    };
    var categoryLoaded = $scope.$on("categoryLoaded", function() {
        categoryLoaded();
        if (scope.slides.length === 0) {
            return;
        }
        $timeout(function() {
            scope.initSlider();
        }, 10);
    });
    catalogService.getSlides($scope.code).then(function(response) {
        scope.slides = Array.isArray(response) ? response : JSON.parse(response.data);
        $scope.$broadcast("categoryLoaded");
    }, function(err) {});
    $scope.getOpt = function() {
        var o = $rootScope.latestSearch || {};
        o.pageSize = $scope.getPageSize();
        if (o._page) {
            $scope.pages.currentPage = o._page;
            o._page = 0;
        }
        o.page = $scope.pages.currentPage;
        var langId = localStorageService.get("langId") || 0;
        o.langId = langId;
        o.all = false;
        $.each($scope.categories, function(i, c) {
            if (c.Code === $scope.code) {
                o.categoryIds = [ c.Id ];
                return false;
            }
        });
        return o;
    };
    $scope.updateTotalPages = function(cb) {
        var opt = $scope.getOpt();
        catalogService.receiveTotalPages(opt).then(function(totalCount) {
            $scope.pages.totalCount = parseInt(totalCount);
            if (cb) {
                cb();
            }
        }, function(err) {
            debugger;
        });
    };
    $scope.load = function(page, forceScrollToTop) {
        var opt = $scope.getOpt();
        $scope.products = [];
        $scope.startLoading = true;
        if (forceScrollToTop) {
            $scope.scrollTopFn();
        }
        $timeout(function() {
            catalogService.product.search(opt).then(function(response) {
                $scope.pages.totalCount = parseInt(response.total);
                $scope.startLoading = false;
                var _products = response.data;
                $scope.fillProductParams(_products);
                $scope.products = _products;
                $timeout(function() {
                    scope.tabSelected();
                });
                $timeout(function() {
                    window.prerenderReady = true;
                }, 3e3);
            }, function(err) {
                debugger;
                $scope.startLoading = false;
                $timeout(function() {
                    window.prerenderReady = true;
                }, 3e3);
            });
        });
    };
    $scope.$on("applyCustom", function() {
        $rootScope.search = jQuery.extend(true, {}, $rootScope._searchCfg);
        $rootScope.latestSearch = jQuery.extend(true, {}, $rootScope.search);
        $scope.applyFilter();
    });
    $scope.applyFilter = function() {
        $scope.load(1, true);
    };
    $scope.sort = function(by) {
        $rootScope.sortBy = by;
        localStorageService.set("sortBy", by);
        $scope.applyFilter();
    };
    $scope.$on("search", function(scope, searchCfg) {
        $rootScope._searchCfg = searchCfg;
        var opt = $scope.getOpt();
        var ss = JSON.stringify(opt);
        if ($scope.pss !== ss) {
            $scope.pss = ss;
            $scope.popt = opt;
        }
    });
    $scope.clearFilter = function() {
        $scope.load(1, true);
    };
    $scope.changePageSize = function(size) {
        $.each($scope.pageSizes, function(i, s) {
            s.active = false;
        });
        size.active = true;
        $scope.pages.currentPage = 1;
        if ($scope.pages.totalCount < $scope.pages.currentPage) {
            $scope.pages.currentPage = $scope.pages.totalCount;
        }
        $scope.load($scope.pages.currentPage, true);
    };
    $scope.showItem = function(p, prefix, cb) {
        var el = $("#" + prefix + p.Id);
        el.css({
            display: "inline-block"
        });
        if (cb) {
            cb();
        }
        $scope.tabSelected();
    };
    $scope.goToPage = function(page) {
        if (page === "first") {
            $scope.pages.currentPage = 1;
        } else if (page === "last") {
            $scope.pages.currentPage = $scope.pages.totalCount;
        } else {
            if (page.active) {
                return;
            }
            $scope.pages.currentPage = page.index;
            $.each($scope.pages.items, function(i, t) {
                if (t.index === page.index) {
                    t.active = true;
                } else if (t.active) {
                    t.active = false;
                }
            });
        }
        var pageSize = $scope.getPageSize();
        $scope.load($scope.pages.currentPage, true);
    };
    $scope.initSlider = function() {
        jQuery(document).ready(function($) {
            var jssor_1_SlideshowTransitions = [ {
                $Duration: 1200,
                x: -.3,
                $During: {
                    $Left: [ .3, .7 ]
                },
                $Easing: {
                    $Left: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            }, {
                $Duration: 1200,
                x: .3,
                $SlideOut: true,
                $Easing: {
                    $Left: $Jease$.$InCubic,
                    $Opacity: $Jease$.$Linear
                },
                $Opacity: 2
            } ];
            var jssor_1_options = {
                $AutoPlay: true,
                $SlideshowOptions: {
                    $Class: $JssorSlideshowRunner$,
                    $Transitions: jssor_1_SlideshowTransitions,
                    $TransitionsOrder: 1
                },
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                },
                $ThumbnailNavigatorOptions: {
                    $Class: $JssorThumbnailNavigator$,
                    $Cols: 1,
                    $Align: 0,
                    $NoDrag: true
                }
            };
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            function ScaleSlider() {
                var refSize = $(".content").outerWidth();
                var w = $("#wrapper .normalheader .panel-body").outerWidth();
                if (w !== jssor_1_slider.prevWidth) {
                    jssor_1_slider.$ScaleWidth(w);
                    jssor_1_slider.prevWidth = w;
                }
            }
            $(function() {
                ScaleSlider();
            });
            $(window).on("resize", ScaleSlider);
            $(window).on("load", ScaleSlider);
            $(window).on("orientationchange", ScaleSlider);
        });
    };
}

angular.module("homer").controller("seriesCtrl", seriesCtrl);

function seriesCtrl($http, $scope, $timeout, $state, $rootScope, catalogService, translationService, localStorageService, adminService) {
    var scope = $scope;
    $rootScope.header = true;
    updateTitle($state, translationService);
    $rootScope.$broadcast("optionalLoggedIn");
    $scope.cseries = undefined;
    $rootScope.cseries = undefined;
    $scope.startLoading = true;
    $scope.code = $state.params.code;
    $rootScope.code = $state.params.code;
    updateTitle($state, $scope.translate($state.current.data.code) + " " + $scope.code);
    if (!$scope.code) {
        $state.go("index");
    }
    $scope.getSeriesUrl = function(code, s) {
        if (s.isCategory) {
            return "#/catalog/" + code;
        } else {
            return "#/catalog/" + code + "/series/" + s.Code;
        }
    };
    $scope.getSeriesImageUrl = function(s) {
        if (s.isCategory) {
            return "/api/Catalog/GetPreviewCategoryImage?id=";
        } else {
            return "/api/Catalog/GetPreviewSeriesImage?id=";
        }
    };
    catalogService.getSeries($scope.code).then(function(series) {
        for (var c in $scope.categories) {
            if ($scope.code === $scope.categories[c].Code) {
                $scope.categories[c].isCategory = true;
                series.splice(0, 0, $scope.categories[c]);
            }
        }
        $scope.series = series;
        $scope.startLoading = false;
        $timeout(function() {
            window.prerenderReady = true;
        }, 3e3);
    }, function(err) {
        debugger;
        $scope.startLoading = false;
        $timeout(function() {
            window.prerenderReady = true;
        }, 3e3);
    });
}

angular.module("homer").controller("cartCtrl", cartCtrl);

function cartCtrl($http, $scope, $timeout, $state, $rootScope, translationService, localStorageService, cartService, cabinetService) {
    updateTitle($state, translationService);
    $rootScope.header = true;
    $rootScope.code = "";
    $scope.model = $scope.model || {};
    $scope.mask = "+38 (999) 999-99-99";
    $scope.orderNumber = $scope.orderNumber || {};
    $scope.$watch("loggedIn", function(n, o) {
        if (n) {
            cabinetService.getUserInfo().then(function(userInfo) {
                $scope.model.phone = userInfo.Phone;
                $scope.model.id = userInfo.Id;
                $scope.model.email = userInfo.Email;
                $scope.model.address = userInfo.Address;
            }, function(err) {
                debugger;
            });
        }
    });
    $scope.plusCountCart = function(p) {
        p.Count++;
        localStorageService.set("cart", $scope.cart);
    };
    $scope.loadCities();
    $scope.minusCountCart = function(p) {
        if (p.Count > 0) {
            p.Count--;
        }
        localStorageService.set("cart", $scope.cart);
    };
    $scope.removeFromCart = function(id) {
        swal({
            title: $scope.translate("RemoveFromCartQuestion"),
            text: $scope.translate("Question"),
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2af",
            confirmButtonText: $scope.translate("Yes"),
            cancelButtonText: $scope.translate("No"),
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                if ($scope.cart[id]) {
                    delete $scope.cart[id];
                    localStorageService.set("cart", $scope.cart);
                    $scope.$apply();
                }
            }
        });
    };
    $scope.proceedOrder = function() {
        if ($scope.totalCart() > 0 && !$scope.isCartEmpty()) {
            $state.go("order");
        }
    };
    $scope.totalCart = function() {
        var t = 0;
        for (var c in $scope.cart) {
            t += $scope.cart[c].ProductPrice.Price * $scope.cart[c].Count;
        }
        return t;
    };
    $scope.beautyAmount = function(value) {
        var v = value.toString();
        var s = "", k = 0;
        for (var i = v.length - 1; i >= 0; i--) {
            if (k === 3) {
                s = " " + s;
            }
            s = v[i] + s;
            k++;
        }
        return s;
    };
    $scope.phoneValid = function() {
        return $scope.model.phone && $scope.model.phone.length >= 10 && $scope.model.phone.indexOf("_") === -1;
    };
    $scope.emailValid = function() {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test($scope.model.email);
    };
    $scope.canOrder = function() {
        if (!$scope.deliveryMethod.selected) {
            return false;
        }
        if ($scope.isNP($scope.deliveryMethod)) {
            return $scope.phoneValid() && $scope.city && $scope.city.selected && $scope.warehouse && $scope.warehouse.selected && ($scope.loggedIn || $scope.emailValid() && $scope.model.fullName);
        } else {
            return $scope.phoneValid() && ($scope.loggedIn || $scope.emailValid() && $scope.model.fullName);
        }
    };
    $scope.order = function() {
        var model = {
            DeliveryMethodId: $scope.deliveryMethod.selected.Id,
            NPCityRef: $scope.city.selected ? $scope.city.selected.Ref : null,
            NPWarehouseRef: $scope.warehouse.selected ? $scope.warehouse.selected.Ref : null,
            Phone: $scope.model.phone,
            Email: $scope.model.email,
            FullName: $scope.model.fullName,
            AdditionalInfo: $scope.model.addinfo,
            ProductSet: []
        };
        for (var k in $scope.cart) {
            var t = $scope.cart[k];
            model.ProductSet.push({
                Id: t.Id,
                Count: t.Count
            });
        }
        var wfpMesssage = function(event) {
            if (event.data && event.data.substr(0, 1) === "{") {
                var jdata = JSON.parse(event.data);
                if ($scope.orderNumber[jdata.orderReference] === "received" || jdata.transactionStatus !== "Approved" && jdata.transactionStatus !== "Pending") {
                    return;
                }
                $scope.orderNumber[jdata.orderReference] = "received";
                $timeout(function() {
                    swal({
                        title: $scope.translate("OrderDone"),
                        text: $scope.loggedIn ? $scope.trustAsHtml($scope.translate("YouMaySeeOrdersInCabinetPage")) : $scope.trustAsHtml($scope.translate("YouMaySeeOrdersInCabinetPageAnonymous")),
                        html: true,
                        type: "success"
                    });
                    for (var k in $scope.cart) {
                        delete $scope.cart[k];
                    }
                    localStorageService.set("cart", $scope.cart);
                    $timeout(function() {
                        if ($scope.loggedIn) {
                            $state.go("cabinet", {
                                tab: "orders"
                            });
                        }
                    }, 250);
                });
            }
        };
        if (!$scope.wfpSubscribed) {
            $scope.wfpSubscribed = true;
            window.addEventListener("message", wfpMesssage, false);
        }
        var pay = function(m, model) {
            var langId = localStorageService.get("langId") || 0;
            cartService.placeOrder(model).then(function(orderId) {
                cabinetService.orderAction(orderId, "New").then(function(response) {}, function(err) {
                    debugger;
                });
            }, function(err) {
                debugger;
            });
            w4p.run({
                language: langId === 0 ? "RU" : "UA",
                merchantAccount: m.merchantAccount,
                merchantDomainName: m.merchantDomainName,
                authorizationType: "SimpleSignature",
                merchantSignature: m.hash,
                orderReference: m.orderNumber,
                orderDate: m.orderDate,
                amount: m.amount,
                serviceUrl: "https://www.igruhi.com/api/cart/serviceurl",
                currency: m.currency,
                productName: m.productName,
                productPrice: m.productPrice,
                productCount: m.productCount,
                clientEmail: model.Email,
                clientPhone: model.Phone,
                straightWidget: true
            }, function(response) {}, function(response) {
                debugger;
            }, function(response) {});
        };
        cartService.preOrder(model).then(function(data) {
            model.preOrderNumber = data.orderNumber;
            pay(data, model);
        }, function(err) {
            debugger;
        });
    };
    $timeout(function() {
        window.prerenderReady = true;
    }, 3e3);
}

angular.module("homer").controller("cabinetCtrl", cabinetCtrl);

function cabinetCtrl($http, $scope, $timeout, $state, $rootScope, translationService, cabinetService, localStorageService) {
    debugger;
    updateTitle($state, translationService);
    $rootScope.code = "";
    $rootScope.header = true;
    $scope.oneAtATime = true;
    $scope.showMoreOrders = true;
    $scope.pageOrders = $scope.pageOrders || 1;
    $scope.loading = false;
    var isConfirmChangeEmail = location.hash.indexOf("#/confirmchangeemail/") !== -1;
    if (isConfirmChangeEmail) {
        var guid = location.hash.replace("#/confirmchangeemail/", "");
        var request = {
            method: "post",
            url: "/api/Account/ConfirmChangeEmail",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                guid: guid
            }
        };
        $http(request).then(function(response) {
            if (response.data) {
                $scope.loginMsg = "Смена Email подтверждена.";
                $scope.errorMsg = "";
            } else {
                $scope.errorMsg = "Смена Email не подтверждена или ссылка устарела.";
                $scope.loginMsg = "";
            }
            $scope.getUserInfo();
        }, function(err) {
            $scope.handleError(err);
        });
    }
    $scope.orderStateColor = function(orderStateId) {
        if (orderStateId.toLowerCase() === "bc84afe9-c679-4298-bcb4-66c3485ba645") {
            return "#2c2";
        } else if (orderStateId.toLowerCase() === "c13f74d7-be87-41c7-9de9-589944bd8621") {
            return "#e7e";
        } else if (orderStateId.toLowerCase() === "9dd4c231-ffbc-483a-a1b6-4adbaf612c5f") {
            return "#f77";
        } else {
            return "#000";
        }
    };
    $scope.rainbow = new Rainbow();
    $scope.rainbow.setSpectrum("#34495e", "#9b59b6", "#3498db", "#62cb31", "#ffb606", "#e67e22", "#e74c3c", "#c0392b", "LightSteelBlue", "Maroon", "MediumOrchid", "MediumSeaGreen", "OliveDrab", "Orange", "Orchid", "PaleGoldenRod", "Pink", "RosyBrown", "Turquoise", "Tomato", "YellowGreen", "DarkSeaGreen", "Crimson", "Coral", "MediumPurple", "PeachPuff");
    $scope.rainbow.setNumberRange(1, 26);
    $scope.model = {
        orders: [],
        email: "",
        phone: "",
        address: "",
        calendar: "",
        number: "",
        userName: ""
    };
    $scope.removeProfile = function(p, e) {
        e.preventDefault();
        e.stopPropagation();
        swal({
            title: $scope.translate("RemoveProfileQuestion"),
            text: $scope.translate("Question"),
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: $scope.translate("Yes"),
            cancelButtonText: $scope.translate("No"),
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                $scope.loading = true;
                cabinetService.removeProfile(p.Id).then(function() {
                    var index = $scope.auth.Logins.indexOf(p);
                    if (index !== -1) {
                        $scope.auth.Logins.splice(index, 1);
                    }
                    $scope.loading = false;
                    localStorageService.set("auth", $scope.auth);
                }, function(err) {
                    debugger;
                    $scope.loading = false;
                });
            }
        });
    };
    $scope.removeOrder = function(o) {
        swal({
            title: $scope.translate("RemoveOrderQuestion"),
            text: $scope.translate("Question"),
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: $scope.translate("Yes"),
            cancelButtonText: $scope.translate("No"),
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                $scope.loading = true;
                cabinetService.removeOrder(o.Id).then(function() {
                    var index = $scope.model.orders.indexOf(o);
                    if (index !== -1) {
                        $scope.model.orders.splice(index, 1);
                    }
                    $scope.loading = false;
                }, function(err) {
                    debugger;
                    $scope.loading = false;
                });
            }
        });
    };
    $scope.getUserInfo = function(cb) {
        $scope.loading = true;
        $scope.loginMsg = "";
        $scope.errorMsg = "";
        cabinetService.getUserInfo().then(function(userInfo) {
            $scope.model.id = userInfo.Id;
            $scope.model.email = userInfo.Email;
            $scope.model.phone = userInfo.Phone;
            $scope.model.address = userInfo.Address;
            $scope.auth.Email = $scope.model.email;
            $scope.model.emailConfirmedFlag = userInfo.EmailConfirmedFlag;
            localStorageService.set("auth", $scope.auth);
            $scope.loading = false;
            if (cb) {
                cb();
            }
        }, function(err) {
            debugger;
            $scope.loading = false;
            if (cb) {
                cb();
            }
        });
    };
    $scope.getUserInfo(function() {
        $scope.$watch("model.email", $scope.saveInfo);
        $scope.$watch("model.phone", $scope.saveInfo);
        $scope.$watch("model.address", $scope.saveInfo);
        $rootScope.$broadcast("requiredLoggedIn");
        $timeout(function() {
            window.prerenderReady = true;
        }, 3e3);
    });
    $scope.orderTotal = function(o) {
        var s = o.TotalPrice;
        return s;
    };
    $rootScope.$on("emit:dateTimePicker", function(scope, opt) {
        var dt = opt.dateTime;
        if (opt.element.indexOf("calendar") === 0) {
            $scope.model.calendar = $scope.formatShortDate(dt);
        }
    });
    $scope.orderAction = function(order, action) {
        swal({
            title: $scope.translate("ChangeOrderStateTo") + ' "' + $scope.translate("OrderState_" + action) + '"?',
            text: $scope.translate("Question"),
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: $scope.translate("Yes"),
            cancelButtonText: $scope.translate("No"),
            closeOnConfirm: true,
            closeOnCancel: true
        }, function(isConfirm) {
            if (isConfirm) {
                $scope.loading = true;
                cabinetService.orderAction(order.Id, action).then(function(orderState) {
                    order.OrderState = orderState;
                    $scope.loading = false;
                }, function(err) {
                    debugger;
                    $scope.loading = false;
                });
            }
        });
    };
    $rootScope.$broadcast("requiredLoggedIn");
    if ($scope.active === undefined) {
        $scope.active = 0;
    }
    if ($state.params && $state.params.tab === "orders") {
        $scope.active = 1;
    }
    $scope.applyFilter = function() {
        $scope.pageOrders = 1;
        $scope.model.orders = [];
        $scope.model.calendar = $("#calendar").val();
        $scope.loading = true;
        cabinetService.getOrders($scope.pageOrders, $scope.model.number, $scope.model.calendar, $scope.model.userName).then(function(response) {
            $scope.model.orders = $scope.model.orders.concat(response);
            $scope.showMoreOrders = response.length === 10;
            if ($scope.showMoreOrders) {
                $scope.pageOrders = $scope.pageOrders + 1;
            }
            $scope.loading = false;
        }, function(err) {
            debugger;
            $scope.loading = false;
        });
    };
    $scope.clearFilter = function() {
        $scope.model.number = "";
        $scope.model.calendar = "";
        $("#calendar").val("");
        $scope.model.userName = "";
        $scope.applyFilter();
    };
    $scope.loadMore = function() {
        $scope.loading = true;
        cabinetService.getOrders($scope.pageOrders, $scope.model.number, $scope.model.calendar, $scope.model.userName).then(function(response) {
            $scope.model.orders = $scope.model.orders.concat(response);
            $scope.showMoreOrders = response.length === 10;
            if ($scope.showMoreOrders) {
                $scope.pageOrders = $scope.pageOrders + 1;
            }
            $scope.loading = false;
        }, function(err) {
            $scope.loading = false;
            debugger;
        });
    };
    $scope.loading = true;
    cabinetService.getOrders(1, "", "", "").then(function(response) {
        $scope.model.orders = response;
        $scope.showMoreOrders = response.length === 10;
        if ($scope.showMoreOrders) {
            $scope.pageOrders = $scope.pageOrders + 1;
        }
        $scope.loading = false;
    }, function(err) {
        $scope.loading = false;
        debugger;
    });
    $scope.saveInfo = function(n, o) {
        if (n === o) {
            return;
        }
        if ($scope.saveTimer) {
            $timeout.cancel($scope.saveTimer);
        }
        $scope.saveTimer = $timeout(function() {
            var email = $scope.model.email;
            cabinetService.saveUserInfo($scope.model).then(function(response) {
                $scope.loginMsg = "";
                if (parseInt(response) === 0) {
                    $scope.loginMsg = "На Email " + email + " отправлено письмо для подтверждения.";
                }
            }, function(err) {
                debugger;
            });
            $scope.saveTimer = undefined;
        }, 500);
    };
    if (isConfirmChangeEmail) {
        setTimeout(function() {
            location.replace("#/cabinet/");
        });
    }
}

angular.module("homer").controller("loginCtrl", loginCtrl);

function loginCtrl($http, $scope, $timeout, $state, $rootScope, localStorageService, loginAppFactory, vcRecaptchaService, translationService, loginService) {
    $rootScope.header = false;
    $scope.response = null;
    $scope.widgetId = null;
    updateTitle($state, translationService);
    $rootScope.$broadcast("optionalLoggedIn");
    $scope.getKeys = function() {
        loginService.getKeys().then(function(response) {
            $scope.model.key = response.RecaptchaKey;
            $scope.model.redirectUrl = response.RedirectUrl;
            $timeout(function() {
                window.prerenderReady = true;
            }, 3e3);
        }, function(err) {
            debugger;
            $timeout(function() {
                window.prerenderReady = true;
            }, 3e3);
        });
    };
    $scope.model = $scope.model || {};
    if (!$scope.model.key) {
        $scope.getKeys();
    }
    $scope.setResponse = function(response) {
        $scope.response = response;
    };
    $scope.setWidgetId = function(widgetId) {
        $scope.widgetId = widgetId;
    };
    $scope.cbExpiration = function() {
        vcRecaptchaService.reload($scope.widgetId);
        $scope.response = null;
    };
    $scope.authenticateExternalProvider = function(provider) {
        window.location.href = "/api/Account/ExternalLogin?provider=" + provider + "&response_type=token&client_id=self&redirect_uri=" + $scope.model.redirectUrl;
    };
    var isConfirmEmail = location.hash.indexOf("#/confirmemail/") !== -1;
    var isRecoveryEmail = location.hash.indexOf("#/recovery/") !== -1;
    if (isConfirmEmail) {
        var guid = location.hash.replace("#/confirmemail/", "");
        var request = {
            method: "post",
            url: "/api/Account/ConfirmEmail",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                guid: guid
            }
        };
        $http(request).then(function(response) {
            if (response.data) {
                $scope.loginMsg = "Email подтвержден. Теперь Вы можете войти на сайт.";
                $scope.errorMsg = "";
            } else {
                $scope.errorMsg = "Email не подтвержден.";
                $scope.loginMsg = "";
            }
            $state.transitionTo("login");
        }, function(err) {
            $scope.handleError(err);
        });
    }
    $scope.checkRecovery = function() {
        console.log($scope.recoveryUid);
        console.log($scope.recoveryUid.length);
        return !$scope.recoveryUid || $scope.recoveryUid.length !== 32;
    };
    $scope.resetPassword = function() {
        var recoveryUid = $scope.recoveryUid;
        var pwd = $scope.pwd;
        var pwd2 = $scope.pwd2;
        var request = {
            method: "post",
            url: "/api/Account/ResetPassword",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                RecoveryUid: recoveryUid,
                Password: pwd,
                ConfirmPassword: pwd2
            }
        };
        return $http(request).then(function(response) {
            $state.transitionTo("login");
            $rootScope.$broadcast("updateErrorMsg", "");
            $rootScope.$broadcast("updateLoginMsg", "Пароль успешено сменен.");
        }, function(err) {
            $scope.handleError(err);
        });
    };
    if (isRecoveryEmail) {
        var recoveryUid = location.hash.replace("#/recovery/", "");
        $scope.recoveryUid = recoveryUid;
    }
    $scope.login = function() {
        var email = $scope.email;
        var pwd = $scope.pwd;
        var response = $scope.response;
        var request = {
            method: "post",
            url: "/api/Account/Login",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                Email: email,
                Password: pwd,
                Response: response
            }
        };
        return $http(request).then(function(response) {
            $rootScope.$broadcast("authItem", response.data);
            $rootScope.$broadcast("checkLogon");
            $state.transitionTo("catalog");
        }, function(err) {
            vcRecaptchaService.reload($scope.widgetId);
            $scope.handleError(err);
        });
    };
    $scope.register = function() {
        var email = $scope.email;
        var pwd = $scope.pwd;
        var pwd2 = $scope.pwd2;
        var request = {
            method: "post",
            url: "/api/Account/Register",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                Email: email,
                Password: pwd,
                ConfirmPassword: pwd2
            }
        };
        return $http(request).then(function(response) {
            $state.transitionTo("login");
            $rootScope.$broadcast("updateErrorMsg", "");
            $rootScope.$broadcast("updateLoginMsg", "На почтовый ящик " + $scope.email + " было отправлено письмо для подтверждения регистрации.");
        }, function(err) {
            $scope.handleError(err);
        });
    };
    $scope.recovery = function() {
        var email = $scope.email;
        var request = {
            method: "post",
            url: "/api/Account/Recovery",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                Email: email
            }
        };
        return $http(request).then(function(response) {
            $rootScope.$broadcast("updateErrorMsg", "");
            $rootScope.$broadcast("updateLoginMsg", "На почтовый ящик " + $scope.email + " было отправлено письмо для подтверждения сброса пароля.");
        }, function(err) {
            $scope.handleError(err);
        });
    };
    if (location.hash === "#/error=access_denied") {
        debugger;
        $scope.errorMsg = location.hash;
    }
}

angular.module("homer").controller("productCtrl", productCtrl);

function productCtrl($http, $scope, $timeout, $state, $rootScope, translationService, catalogService) {
    $rootScope.$broadcast("optionalLoggedIn");
    $rootScope.header = true;
    $state.current.data.pageDesc = "-";
    $scope.onWindowResize = function() {
        var w = $(".tab-content").width();
        w = $("#preview").width();
        var h = w / 1.5;
        $("#gallery").height(h);
        if (w > 800) {
            $("#mirror").height(h);
            $("#mirror").prev().show();
            $("#mirror").show();
            $("#buffer").hide();
        } else {
            $("#mirror").prev().hide();
            $("#mirror").hide();
            $("#buffer").show();
        }
        $(".swipebox-gallery").arrangeImages({
            width: w,
            height: h
        });
    };
    $scope.startGallery = function() {
        $(".swipebox-gallery a:first-child img").click();
        $timeout(function() {
            $(window).trigger("resize");
        }, 150);
    };
    $(function() {
        $(window).off("resize", $scope.onWindowResize);
        $(window).on("resize", $scope.onWindowResize);
        $(".gallery-start").click(function(e) {
            e.preventDefault();
            $(".swipebox-gallery a:first-child img").click();
        });
        $(".ct-img").click(function(e) {
            e.preventDefault();
            $(".swipebox-gallery a:nth-child(6) img").click();
        });
        $(".fancybox-button").fancybox({
            prevEffect: "none",
            nextEffect: "none",
            closeBtn: true,
            padding: 0,
            helpers: {
                media: true,
                overlay: {
                    locked: false
                }
            }
        });
    });
    $scope.id = $state.params.id;
    $scope.productId = $scope.id;
    $scope.comment.productId = $scope.id;
    $scope.imageTitle = function(index, product) {
        return index + " " + $scope.translate("Of") + " " + (product.Images.length - 1);
    };
    catalogService.getProduct($scope.id).then(function(product) {
        $scope.product = product;
        updateTitle($state, $scope.product.Name);
        $rootScope.code = $scope.product.Code;
        $scope.productCode = $scope.product.ProductCode;
        $state.current.data.pageDesc = $scope.product.Description;
        $scope.getComments($scope.id, 1);
        $scope.fillProductParams($scope.product);
        $timeout(function() {
            $(window).trigger("resize");
        }, 10);
        $timeout(function() {
            window.prerenderReady = true;
        }, 3e3);
    }, function(err) {
        debugger;
        $timeout(function() {
            window.prerenderReady = true;
        }, 3e3);
    });
}

(function($) {
    $.fn.arrangeImages = function(config) {
        var cfg = config || {};
        cfg.width = cfg.width || 100;
        cfg.height = cfg.height || 100;
        if (cfg.small) {
            cfg.width = cfg.small;
            cfg.height = cfg.small;
        }
        cfg.width2by3 = Math.floor(cfg.width * 2 / 3);
        cfg.width1by2 = Math.floor(cfg.width * 1 / 2);
        cfg.width1by3 = cfg.width - cfg.width2by3;
        cfg.height1by2 = Math.floor(cfg.height * 1 / 2);
        cfg.height2by3 = Math.floor(cfg.height * 2 / 3);
        cfg.height1by3 = cfg.height - cfg.height2by3;
        return this.each(function() {
            var me = $(this);
            if (me.data("pw") === cfg.width && me.data("ph") === cfg.height) {
                return true;
            }
            $(".ct-img").css({
                width: 0,
                height: 0
            });
            var images = me.find("img").not("img.thumbnail");
            images.css({
                opacity: 0
            });
            var count = images.length;
            if (count === 0) {
                return;
            }
            var rank = count > 6 ? 6 : count;
            var cssProp = {
                width: cfg.width,
                height: cfg.height
            };
            me.css(cssProp);
            me.data("pw", cssProp.width);
            me.data("ph", cssProp.height);
            var smartSizeFn = function(image, iw, ih, forceHeightAlign) {
                var doResize = function(predefinedWidth, predefinedHeight) {
                    var w = parseInt(image.attr("data-w")) || image.width() || predefinedWidth;
                    var h = parseInt(image.attr("data-h")) || image.height() || predefinedHeight;
                    var k1 = w / h;
                    var k2 = iw / ih;
                    var el = image.parent();
                    if (el.hasClass("swipebox-video") && el.find(".play-image").length === 0) {
                        el.append('<div class="play-image"></div>');
                    }
                    var playImage = el.find(".play-image");
                    playImage.css({
                        left: (iw - 62) / 2,
                        top: (ih - 62) / 2
                    });
                    if (false) {
                        image.css({
                            height: "auto",
                            width: iw
                        });
                        var mt = Math.floor((ih - (Math.floor(image.height()) || Math.floor(predefinedHeight * iw / predefinedWidth))) / 2);
                        image.css({
                            "margin-top": mt,
                            "margin-left": "0.5px"
                        });
                    } else {
                        image.css({
                            width: "auto",
                            height: ih
                        });
                        var ml = Math.floor((iw - (Math.floor(image.width()) || Math.floor(predefinedWidth * ih / predefinedHeight))) / 2);
                        ml = ml - 2;
                        image.css({
                            "margin-left": ml,
                            "margin-top": "0.5px"
                        });
                    }
                    if (image.width() > 0) {
                        image.attr("data-w", image.width());
                        image.attr("data-h", image.height());
                    }
                };
                predefinedWidth = image.width() || parseInt(image.attr("data-w")) || parseInt(image.attr("width"));
                predefinedHeight = image.height() || parseInt(image.attr("data-h")) || parseInt(image.attr("height"));
                if (image.width() > 40 || predefinedWidth > 40) {
                    doResize(predefinedWidth, predefinedHeight);
                    image.css({
                        opacity: 1
                    });
                    $(window).trigger("resize");
                } else {
                    image.on("load", function() {
                        image.loaded = true;
                        image.css({
                            opacity: 1
                        });
                        $(window).trigger("resize");
                    });
                    if (!image.loaded) {
                        image.load(doResize);
                    }
                }
            };
            me.ready(function() {
                var ct = images.length;
                $.each(images, function(i, img) {
                    var image = $(img);
                    var el = image.parent();
                    switch (rank) {
                      case 1:
                        el.css({
                            width: cfg.width,
                            height: cfg.height,
                            left: 0,
                            top: 0
                        });
                        smartSizeFn(image, cfg.width, cfg.height);
                        break;

                      case 2:
                        if (i === 0) {
                            el.css({
                                width: cfg.width1by2,
                                height: cfg.height,
                                left: 0,
                                top: 0
                            });
                        } else {
                            el.css({
                                width: cfg.width1by2,
                                height: cfg.height,
                                left: cfg.width1by2,
                                top: 0
                            });
                        }
                        smartSizeFn(image, cfg.width1by2, cfg.height);
                        break;

                      case 3:
                        if (i === 0) {
                            el.css({
                                width: cfg.width2by3,
                                height: cfg.height,
                                left: 0,
                                top: 0
                            });
                            smartSizeFn(image, cfg.width2by3, cfg.height);
                        } else if (i === 1) {
                            el.css({
                                width: cfg.width1by3,
                                height: cfg.height1by2,
                                left: cfg.width2by3,
                                top: 0
                            });
                            smartSizeFn(image, cfg.width1by3, cfg.height1by2);
                        } else {
                            el.css({
                                width: cfg.width1by3,
                                height: cfg.height1by2,
                                left: cfg.width2by3,
                                top: cfg.height1by2
                            });
                            smartSizeFn(image, cfg.width1by3, cfg.height1by2);
                        }
                        break;

                      case 4:
                        if (i === 0) {
                            el.css({
                                width: cfg.width2by3,
                                height: cfg.height,
                                left: 0,
                                top: 0
                            });
                            smartSizeFn(image, cfg.width2by3, cfg.height);
                        } else if (i === 1) {
                            el.css({
                                width: cfg.width1by3,
                                height: cfg.height1by3,
                                left: cfg.width2by3,
                                top: 0
                            });
                            smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                        } else if (i === 2) {
                            el.css({
                                width: cfg.width1by3,
                                height: cfg.height1by3,
                                left: cfg.width2by3,
                                top: cfg.height1by3
                            });
                            smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                        } else if (i === 3) {
                            el.css({
                                width: cfg.width1by3,
                                height: cfg.height1by3,
                                left: cfg.width2by3,
                                top: cfg.height2by3
                            });
                            smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                        }
                        break;

                      case 5:
                        if (i === 0) {
                            el.css({
                                width: cfg.width2by3,
                                height: cfg.height2by3,
                                left: 0,
                                top: 0
                            });
                            smartSizeFn(image, cfg.width2by3, cfg.height2by3);
                        } else if (i === 1) {
                            el.css({
                                width: cfg.width1by3,
                                height: cfg.height1by3,
                                left: cfg.width2by3,
                                top: 0
                            });
                            smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                        } else if (i === 2) {
                            el.css({
                                width: cfg.width1by3,
                                height: cfg.height1by3,
                                left: cfg.width2by3,
                                top: cfg.height1by3
                            });
                            smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                        } else if (i === 3) {
                            el.css({
                                width: cfg.width1by2,
                                height: cfg.height1by3,
                                left: 0,
                                top: cfg.height2by3
                            });
                            smartSizeFn(image, cfg.width1by2, cfg.height1by3);
                        } else if (i === 4) {
                            el.css({
                                width: cfg.width1by2,
                                height: cfg.height1by3,
                                left: cfg.width1by2,
                                top: cfg.height2by3
                            });
                            smartSizeFn(image, cfg.width1by2, cfg.height1by3);
                        }
                        break;

                      case 6:
                        if (i === 0) {
                            el.css({
                                width: cfg.width2by3,
                                height: cfg.height2by3,
                                left: 0,
                                top: 0
                            });
                            smartSizeFn(image, cfg.width2by3, cfg.height2by3);
                        } else if (i === 1) {
                            el.css({
                                width: cfg.width1by3,
                                height: cfg.height1by3,
                                left: cfg.width2by3,
                                top: 0
                            });
                            smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                        } else if (i === 2) {
                            el.css({
                                width: cfg.width1by3,
                                height: cfg.height1by3,
                                left: cfg.width2by3,
                                top: cfg.height1by3
                            });
                            smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                        } else if (i === 3) {
                            el.css({
                                width: cfg.width1by3,
                                height: cfg.height1by3,
                                left: 0,
                                top: cfg.height2by3
                            });
                            smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                        } else if (i === 4) {
                            el.css({
                                width: cfg.width1by3,
                                height: cfg.height1by3,
                                left: cfg.width1by3,
                                top: cfg.height2by3
                            });
                            smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                        } else if (i >= 5) {
                            el.css({
                                width: cfg.width1by3,
                                height: cfg.height1by3,
                                left: cfg.width2by3,
                                top: cfg.height2by3
                            });
                            if (i >= 6) {
                                el.css({
                                    "z-index": -2,
                                    opacity: 0
                                });
                            }
                            smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                            if (ct >= 6) {
                                if (config.opaque !== 0) {
                                    var p = el.position();
                                    var s = {
                                        width: el.width(),
                                        height: el.height()
                                    };
                                    $(".ct-img").css({
                                        left: p.left,
                                        top: p.top,
                                        width: s.width,
                                        height: s.height
                                    });
                                    $(".ct-img-val").text("+" + (ct - 5));
                                }
                            }
                        }
                        break;
                    }
                    var cls = "place-" + rank + "-" + i;
                    if (!el.hasClass(cls)) {
                        el.addClass(cls);
                    }
                });
            });
        });
    };
})(jQuery);

angular.module("homer").controller("searchCtrl", searchCtrl);

function searchCtrl($http, $scope, $timeout, $state, $stateParams, $rootScope, translationService, localStorageService, catalogService) {
    updateTitle($state, translationService);
    $rootScope.header = true;
    $rootScope.$broadcast("optionalLoggedIn");
    $rootScope.code = "";
    $scope.startLoading = true;
    $scope.pageSizes = [ {
        value: 12,
        active: true
    }, {
        value: 24
    }, {
        value: 48
    } ];
    $scope.pages = {
        currentPage: 1,
        totalCount: 1,
        visibleCount: 7
    };
    $scope.getPageSize = function() {
        var value = $scope.pageSizes[0].value;
        $.each($scope.pageSizes, function(i, s) {
            if (s.active) {
                value = s.value;
                return false;
            }
        });
        return value;
    };
    $scope.getOpt = function() {
        var o = $rootScope.searchGlobal || {};
        o.pageSize = $scope.getPageSize();
        if (o._page) {
            $scope.pages.currentPage = $rootScope.currentPage || o._page;
            o._page = 0;
        }
        o.page = $scope.pages.currentPage;
        var langId = localStorageService.get("langId") || 0;
        o.langId = langId;
        o.all = false;
        $.each($scope.categories, function(i, c) {
            if (c.Code === $scope.code) {
                o.categoryIds = [ c.Id ];
                return false;
            }
        });
        return o;
    };
    $scope.updateTotalPages = function(cb) {
        var opt = $scope.getOpt();
        catalogService.receiveTotalPages(opt).then(function(totalCount) {
            $scope.pages.totalCount = parseInt(totalCount);
            if (cb) {
                cb();
            }
        }, function(err) {
            debugger;
        });
    };
    $scope.changePageSize = function(size) {
        $.each($scope.pageSizes, function(i, s) {
            s.active = false;
        });
        size.active = true;
        $scope.pages.currentPage = 1;
        if ($scope.pages.totalCount < $scope.pages.currentPage) {
            $scope.pages.currentPage = $scope.pages.totalCount;
        }
        $scope.load($scope.pages.currentPage, true);
    };
    $scope.goToPage = function(page) {
        if (page === "first") {
            $scope.pages.currentPage = 1;
        } else if (page === "last") {
            $scope.pages.currentPage = $scope.pages.totalCount;
        } else {
            if (page.active) {
                return;
            }
            $scope.pages.currentPage = page.index;
            $.each($scope.pages.items, function(i, t) {
                if (t.index === page.index) {
                    t.active = true;
                } else if (t.active) {
                    t.active = false;
                }
            });
        }
        var pageSize = $scope.getPageSize();
        $rootScope.currentPage = $scope.pages.currentPage;
        $scope.load($scope.pages.currentPage, true);
    };
    $scope.load = function(page, forceScrollToTop) {
        var opt = $scope.getOpt();
        opt.page = page;
        $scope.products = [];
        $scope.startLoading = true;
        if (forceScrollToTop) {
            $scope.scrollTopFn();
        }
        $timeout(function() {
            catalogService.product.search(opt).then(function(response) {
                $scope.startLoading = false;
                $scope.pages.totalCount = parseInt(response.total);
                var _products = response.data;
                $scope.fillProductParams(_products);
                $scope.products = _products;
                $timeout(function() {
                    window.prerenderReady = true;
                }, 3e3);
            }, function(err) {
                $scope.startLoading = false;
                debugger;
                $timeout(function() {
                    window.prerenderReady = true;
                }, 3e3);
            });
        });
    };
    $scope.clearFilter = function() {
        $rootScope.currentPage = $scope.pages.currentPage = 1;
        $scope.load(1, true);
    };
    $scope.applySearchFilter = function() {
        $rootScope.currentPage = $scope.pages.currentPage = 1;
        $scope.load(1, true);
    };
    $scope.sort = function(by) {
        $rootScope.sortBy = by;
        localStorageService.set("sortBy", by);
        $scope.applySearchFilter();
    };
    $rootScope.currentPage = $rootScope.currentPage || $scope.pages.currentPage;
    $scope.pages.currentPage = $rootScope.currentPage;
    $timeout(function() {
        $scope.load($rootScope.currentPage, true);
    });
}

angular.module("homer").controller("feedbackCtrl", feedbackCtrl);

function feedbackCtrl($http, $scope, $timeout, $state, $rootScope, translationService, cabinetService) {
    updateTitle($state, translationService);
    $rootScope.header = true;
    $rootScope.code = "";
    $scope.getComments("", 1, function() {
        $timeout(function() {
            window.prerenderReady = true;
        }, 3e3);
    });
    $scope.clearComment();
}