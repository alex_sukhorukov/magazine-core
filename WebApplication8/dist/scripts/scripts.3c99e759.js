window["apiUrl"] = location.origin;

window["appTitle"] = "Igruhi";

window.debugMode = false;

function updateTitle($state, v) {
    if (typeof v === "string") {
        var title = v;
    } else {
        title = v.translate($state.current.data.code);
    }
    $state.current.data.pageTitle = title;
    document.title = window.appTitle + " | " + title;
}

(function() {
    angular.module("homer", [ "ui.router", "ngSanitize", "ui.bootstrap", "cgNotify", "ngAnimate", "ui.map", "xeditable", "ui.select", "LocalStorageModule", "vcRecaptcha", "angularFileUpload" ]);
})();

angular.module("homer").config(configState).run(function($rootScope, $state, $templateCache, $http) {
    $rootScope.$state = $state;
    $rootScope.appTitle = appTitle;
});

angular.module("homer").factory("timeoutHttpIntercept", function($rootScope, $q) {
    return {
        request: function(config) {
            config.timeout = 3e5;
            return config;
        }
    };
});

function configState($stateProvider, $urlRouterProvider, $compileProvider, $httpProvider, $locationProvider) {
    $compileProvider.debugInfoEnabled(false);
    $locationProvider.html5Mode({
        enabled: false,
        requireBase: false
    });
    $httpProvider.interceptors.push("timeoutHttpIntercept");
    $stateProvider.state("index", {
        url: "/index",
        templateUrl: "views/store/index.html",
        controller: "indexCtrl",
        data: {
            code: "Index"
        }
    });
    $urlRouterProvider.otherwise(function($injector, $location) {
        var state = $injector.get("$state");
        var $rootScope = $injector.get("$rootScope");
        state.go("index");
        return $location.path();
    });
}

angular.module("homer").controller("indexCtrl", indexCtrl);

function indexCtrl($http, $scope, $timeout, $state, $rootScope) {
    $rootScope.header = true;
    $rootScope.code = "";
    debugger;
}