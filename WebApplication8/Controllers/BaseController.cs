﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using WebApplicationMVCFacebook.Data;
using WebApplicationMVCFacebook.Models;

namespace WebApp.Controllers
{
    // [StoreExceptionFilter]
    public class BaseController : ControllerBase
    {
        // private ILogger _logger;
        protected IMemoryCache _cache;
        protected ApplicationDbContext _dataContext;
        protected int _langId { get; set; } = 0;
        protected IConfiguration _configuration { get; }
        protected readonly IHttpContextAccessor _httpContextAccessor;

        public BaseController(ApplicationDbContext dataContext, IMemoryCache cache, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            // _logger = logger;
            _cache = cache;
            _dataContext = dataContext;
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
            if (_httpContextAccessor.HttpContext.Request.Cookies.ContainsKey("langId")) {
                var langCookie = _httpContextAccessor.HttpContext.Request.Cookies["langId"];
                if (langCookie != null) {
                    _langId = int.Parse(langCookie);
                }
            }
        }

        public bool IsAdmin {
            get {
                return HttpContext.User.IsInRole("Admin");
            }
        }

        protected void ClearCache()
        {
           (_cache as MemoryCache).Compact(100);
        }

        //public Cache Cache {
        //    get {
        //        return HttpContext.Current.Cache;
        //    }
        //}

        //protected void ClearCache()
        //{
        //    var enumerator = Cache.GetEnumerator();
        //    while (enumerator.MoveNext()) {
        //        Cache.Remove(enumerator.Key.ToString());
        //    }
        //}

        //protected void ClearCache(string literal)
        //{
        //    literal = literal.ToLower();
        //    var enumerator = Cache.GetEnumerator();
        //    while (enumerator.MoveNext()) {
        //        if (enumerator.Key.ToString().ToLower().Contains(literal)) {
        //            Cache.Remove(enumerator.Key.ToString());
        //        }
        //    }
        //}
    }
}