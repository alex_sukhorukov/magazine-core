﻿using WebApp.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;
using WebApplicationMVCFacebook.Data;
using WebApplicationMVCFacebook.Models;

namespace WebApp.Controllers
{
    [Authorize]
    // [StoreExceptionFilter]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CartController : BaseController
    {
        // private ApplicationUserManager _userManager;
        public CartController(ApplicationDbContext dataContext, IMemoryCache cache, IConfiguration configuration, IHttpContextAccessor httpContextAccessor) 
            : base(dataContext, cache, configuration, httpContextAccessor)
        {
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
        }

        [HttpGet]
        [AllowAnonymous]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<string> GetDeliveryMethods()
        {
            var cacheKey = "DeliveryMethods";
            var cached = _cache.Get<string>(cacheKey);
            string serializedMethods = string.Empty;
            if (cached != null) {
                serializedMethods = cached;
            } else {
                var methods = await _dataContext.DeliveryMethod
                    .Where(t => t.Active)
                    .Select(t => new {
                        t.Id,
                        Name = _langId == 0 ? t.Name.Text1 : (_langId == 1 ? t.Name.Text2 : t.Name.Text3)
                    }).OrderBy(t => t.Name).ToListAsync();
                serializedMethods = JsonConvert.SerializeObject(methods, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedMethods, TimeSpan.FromHours(120));
            }
            return serializedMethods;
        }

        //private async Task<string> GenerateOrderNumber()
        //{
        //    var seq = new OrderSequence();
        //    _dataContext.OrderSequence.Add(seq);
        //    await _dataContext.SaveChangesAsync();
        //    var sn = seq.Id.ToString().PadLeft(6, '0');
        //    return sn;
        //}

        //public ApplicationUserManager UserManager {
        //    get => _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
        //    private set => _userManager = value;
        //}

        //[HttpPost]
        //[AllowAnonymous]
        //[Route("ServiceUrl")]
        //public async Task<IHttpActionResult> ServiceUrl()
        //{
        //    var request = Request.GetOwinContext().Request;
        //    var orderReference = string.Empty;
        //    var transactionStatus = string.Empty;
        //    JObject model = null;
        //    if (request.Body.Length == 0) {
        //        return Content(HttpStatusCode.BadRequest, new { });
        //    }
        //    var bytes = new byte[request.Body.Length];
        //    var readed = request.Body.Read(bytes, 0, (int)request.Body.Length);
        //    if (readed > 0) {
        //        var value = Encoding.UTF8.GetString(bytes);
        //        _logger.Trace($"ServiceUrl={value}");
        //        model = JsonConvert.DeserializeObject(value) as JObject;
        //        orderReference = model["orderReference"].Value<string>();
        //        transactionStatus = model["transactionStatus"].Value<string>();
        //        if (transactionStatus == "Approved") {
        //            var orderPlacedStateId = Guid.Parse("2f232b72-82c8-40f7-bca6-64217a5cb916");
        //            var orderNewStateId = Guid.Parse("9003b4b6-089f-4bb6-a659-85677a9e98cd");
        //            var order = await _dataContext.Order.Include("OrderState").Where(t => t.Number == orderReference).FirstOrDefaultAsync();
        //            if (order != null && order.OrderState.Id == orderNewStateId) {
        //                var orderState = await _dataContext.OrderState.Include("Name.Language").Where(t => t.Id == orderPlacedStateId).FirstAsync();
        //                order.OrderState = orderState;
        //                await _dataContext.SaveChangesAsync();
        //                _logger.Trace($"Order placed={order.Number}");
        //            }
        //        }
        //    }
        //    var now = DateTime.UtcNow;
        //    var time = (long)now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
        //    var secret = ConfigurationManager.AppSettings["WayForPaySecret"];
        //    var status = "accept";
        //    var message = $"{orderReference};{status};{time}";
        //    string signature = HashHmac2(message, secret);
        //    _logger.Trace($"Reposnse for W4P: messsage = {message}, signature = {signature}");
        //    return Content(HttpStatusCode.OK, new {
        //        orderReference,
        //        status,
        //        time,
        //        signature
        //    });
        //}

        //private async Task<string> PlaceOrderApproved(Guid orderId)
        //{
        //    var result = string.Empty;
        //    var url = new Uri("https://www.igruhi.com/api/cabinet/OrderAction");
        //    var request = WebRequest.Create(url) as HttpWebRequest;
        //    request.Method = "POST";
        //    request.ContentType = "application/json; charset=utf-8";
        //    Stream requestStream = await request.GetRequestStreamAsync();
        //    var requestText = string.Empty;
        //    using (var streamWriter = new StreamWriter(requestStream)) {
        //        requestText = "{\"Id\":\"" + orderId.ToString() + "\",\"Action\":\"Placed\"}";
        //        streamWriter.Write(requestText);
        //        streamWriter.Close();
        //    }
        //    HttpWebResponse respose = (HttpWebResponse) await request.GetResponseAsync();
        //    var responseStream = respose.GetResponseStream();
        //    using (var reader = new StreamReader(responseStream)) {
        //        result = reader.ReadToEnd();
        //    }
        //    return result;
        //}

        //[HttpPost]
        //[AllowAnonymous]
        //[Route("PreOrder")]
        //public async Task<IHttpActionResult> PreOrder(PlaceOrderModel model)
        //{
        //    var now = DateTime.UtcNow;
        //    var orderDate = (long)now.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalMilliseconds;
        //    var secret = ConfigurationManager.AppSettings["WayForPaySecret"];
        //    var merchantAccount = ConfigurationManager.AppSettings["MerchantAccount"];
        //    var merchantDomainName = ConfigurationManager.AppSettings["MerchantDomainName"];
        //    var currency = ConfigurationManager.AppSettings["MerchantCurrency"];
        //    var orderNumber = await GenerateOrderNumber();
        //    var productString = string.Empty;
        //    var productCount = string.Empty;
        //    var productPrice = string.Empty;
        //    decimal totalPrice = 0m, fixPrice = 0m, discount = 0;

        //    foreach (var ps in model.ProductSet) {

        //        var product = await _dataContext.Product
        //            .Include("ProductPrice")
        //            .Include("Name.Language")
        //            .Where(t => t.Id == ps.Id)
        //            .FirstAsync();

        //        if (!string.IsNullOrEmpty(productString)) {
        //            productString += ";";
        //        }
        //        productString += product.Name.Where(t => t.Language.Id == langId).First().Text;

        //        if (!string.IsNullOrEmpty(productCount)) {
        //            productCount += ";";
        //        }
        //        productCount += ps.Count;

        //        var price = product.ProductPrice
        //            .Where(z => z.StartDate < now)
        //            .OrderByDescending(m => m.StartDate)
        //            .FirstOrDefault();

        //        // Discount = discount // TODO: implement
        //        fixPrice = price.Price;

        //        if (!string.IsNullOrEmpty(productPrice)) {
        //            productPrice += ";";
        //        }
        //        productPrice += fixPrice;

        //        totalPrice += (fixPrice - discount) * ps.Count;
        //    }

        //    var message = $"{merchantAccount};{merchantDomainName};{orderNumber};{orderDate};{totalPrice};{currency};{productString};{productCount};{productPrice}";
        //    string hash = HashHmac2(message, secret);

        //    return Content(HttpStatusCode.OK, new {
        //        merchantAccount,
        //        merchantDomainName,
        //        orderDate,
        //        amount = totalPrice.ToString(),
        //        productName = productString.Split(';'),
        //        productCount = productCount.Split(';'),
        //        productPrice = productPrice.Split(';'),
        //        orderNumber,
        //        currency,
        //        hash
        //    });
        //}

        //private static string HashHmac2(string message, string secret)
        //{
        //    Encoding encoding = Encoding.UTF8;
        //    using (var hmac = new HMACMD5(encoding.GetBytes(secret))) {
        //        var msg = encoding.GetBytes(message);
        //        var hash = hmac.ComputeHash(msg);
        //        return BitConverter.ToString(hash).ToLower().Replace("-", string.Empty);
        //    }
        //}

        //// CHECK_STATUS
        //// {"merchantAccount":"www_igruhi_com","reason":"Ok","reasonCode":1100,"orderReference":"010066","amount":1,"currency":"UAH","authCode":"424990","createdDate":1515344925,"processingDate":1515344977,"cardPan":"51****2507","cardType":"MasterCard","issuerBankCountry":"Ukraine","issuerBankName":"JSC STATE SAVINGS BANK OF UKRAINE (JSC OSCHADBANK)","transactionStatus":"Approved","refundAmount":0,"settlementDate":"","settlementAmount":0,"fee":0.03,"merchantSignature":"835c57cd7228f59265376816c9067b9e"}

        //[HttpPost]
        //[AllowAnonymous]
        //[Route("PlaceOrder")]
        //public async Task<IHttpActionResult> PlaceOrder(PlaceOrderModel model)
        //{
        //    var now = DateTime.UtcNow;
        //    User user = null;
        //    UserShadow shadow = null;
        //    var orderNewState = Guid.Parse("9003b4b6-089f-4bb6-a659-85677a9e98cd");
        //    var logonUser = HttpContext.Current.Request.Params["LOGON_USER"];

        //    if (!string.IsNullOrEmpty(logonUser)) {

        //        var userInfo = await UserManager.Users
        //            .Where(t => t.UserName == logonUser || t.Email == logonUser)
        //            .OrderBy(t => t.UserName)
        //            .FirstAsync();

        //        user = await _dataContext.User.Where(t => t.Email == userInfo.Email).FirstAsync();
        //    } else {
        //        shadow = new UserShadow {
        //            CreatedAt = now,
        //            Email = model.Email,
        //            FullName = model.FullName,
        //            PhoneNumber = model.Phone,
        //            UserHostAddress = HttpContext.Current.Request.UserHostAddress
        //        };
        //    }

        //    var npWarehouse = Guid.Empty == model.NPWarehouseRef ? null :
        //        await _dataContext.NPWarehouse
        //            .Where(t => t.Ref == model.NPWarehouseRef)
        //            .FirstOrDefaultAsync();

        //    var orderState = await _dataContext.OrderState
        //            .Where(t => t.Id == orderNewState)
        //            .FirstAsync();

        //    var npCity = Guid.Empty == model.NPCityRef ? null :
        //        await _dataContext.NPCity
        //            .Where(t => t.Ref == model.NPCityRef)
        //            .FirstOrDefaultAsync();

        //    var deliveryMethod = await _dataContext.DeliveryMethod
        //        .Where(t => t.Id == model.DeliveryMethodId)
        //        .FirstAsync();

        //    var order = new Order {
        //        User = user,
        //        UserShadow = shadow,
        //        PopulationDate = now,
        //        Phone = model.Phone,
        //        OrderState = orderState,
        //        OrderAddress = null,
        //        Number = model.PreOrderNumber,
        //        AdditionalInfo = model.AdditionalInfo,
        //        NPWarehouse = npWarehouse,
        //        NPCity = npCity,
        //        DeliveryMethod = deliveryMethod
        //    };

        //    decimal totalPrice = 0m, fixPrice = 0m, discount = 0;

        //    foreach (var ps in model.ProductSet) {

        //        var product = await _dataContext.Product
        //            .Include("ProductPrice")
        //            .Where(t => t.Id == ps.Id)
        //            .FirstAsync();

        //        var recents = await _dataContext.ProductRecent
        //                .Include("Product")
        //                .Include("Recent")
        //                .Where(t => t.Product.Id == product.Id)
        //                .OrderBy(t => t.IsReal)
        //                .ThenBy(t => t.Index)
        //                .ToListAsync();

        //        var recentProductIds = recents
        //            .Where(t => t.Recent != null)
        //            .Select(t => t.Recent.Id)
        //            .ToList();

        //        var index = 0;

        //        foreach (var pps in model.ProductSet) {
        //            var productRecent = await _dataContext.Product
        //                .Include("ProductPrice")
        //                .Where(t => t.Id == pps.Id)
        //                .FirstAsync();

        //            if (product.Id == productRecent.Id) {
        //                continue;
        //            }

        //            if (recentProductIds.Contains(pps.Id)) {
        //                var r = recents.Where(t => t.Recent.Id == pps.Id).FirstOrDefault();
        //                if (r != null) {
        //                    r.IsReal = true;
        //                }
        //                continue;
        //            }

        //            if (index < 8) {
        //                if (recents.Count == 8) {
        //                    recentProductIds[index] = productRecent.Id;
        //                    recents[index].Recent = productRecent;
        //                    recents[index].IsReal = true;
        //                    recents[index].PopulationDate = now;
        //                } else {
        //                    _dataContext.ProductRecent.Add(new ProductRecent {
        //                        Id = Guid.NewGuid(),
        //                        IsReal = true,
        //                        Index = index,
        //                        PopulationDate = now,
        //                        Product = product,
        //                        Recent = productRecent
        //                    });
        //                }

        //                await _dataContext.SaveChangesAsync();

        //                recents = await _dataContext.ProductRecent
        //                   .Include("Product")
        //                   .Include("Recent")
        //                   .Where(t => t.Product.Id == product.Id)
        //                   .OrderBy(t => t.IsReal)
        //                   .ThenBy(t => t.Index)
        //                   .ToListAsync();

        //                recentProductIds = recents
        //                    .Where(t => t.Recent != null)
        //                    .Select(t => t.Recent.Id)
        //                    .ToList();

        //                index++;
        //            } else {
        //                break;
        //            }
        //        }

        //        var productPrice = product.ProductPrice
        //            .Where(z => z.StartDate < now)
        //            .OrderByDescending(m => m.StartDate)
        //            .FirstOrDefault();

        //        fixPrice = productPrice.Price;

        //        order.ProductSets.Add(new ProductSet {
        //            Order = order,
        //            Product = product,
        //            Quantity = ps.Count,
        //            FixPrice = fixPrice,
        //            Discount = discount // TODO: implement
        //        });

        //        totalPrice += (fixPrice - discount) * ps.Count;
        //    }

        //    order.TotalPrice = totalPrice;

        //    _dataContext.Order.Add(order);

        //    await _dataContext.SaveChangesAsync();

        //    return Content(HttpStatusCode.OK, order.Id);
        //}

    }
}