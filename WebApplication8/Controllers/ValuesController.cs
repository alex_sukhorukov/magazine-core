﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using WebApp.Controllers;
using DbModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using WebApplicationMVCFacebook.Data;
using WebApplicationMVCFacebook.Models;

namespace WebApp.Controllers
{
    [AllowAnonymous]
    // [StoreExceptionFilter]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ValuesController : BaseController
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        public ValuesController(ApplicationDbContext dataContext, IMemoryCache cache, IConfiguration configuration, IHttpContextAccessor httpContextAccessor, UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager)
            : base(dataContext, cache, configuration, httpContextAccessor)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<string> GetLocalizableStrings(int langId)
        {
            var cacheKey = "LocalizableStrings_" + langId;
            var cachedImage = _cache.Get<string>(cacheKey);
            string serializedStrings;
            if (cachedImage != null) {
                serializedStrings = cachedImage;
            } else {
                var values = await _dataContext.LocalizableString.Where(t => t.IsSystem && t.IsSerializable).Select(t => new {
                    t.Code,
                    Text = langId == 0 ? t.Text1 : (langId == 1 ? t.Text2 : t.Text3)
                }).OrderBy(t => t.Code).ToListAsync();
                serializedStrings = JsonConvert.SerializeObject(values, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedStrings, TimeSpan.FromHours(120));
            }
            var option = new CookieOptions { Expires = DateTime.Now.AddDays(365) };
            if (_httpContextAccessor.HttpContext.Request.Cookies.ContainsKey("langId")) {
                Response.Cookies.Delete("langId");
            }
            Response.Cookies.Append("langId", langId.ToString(), option);
            return serializedStrings;
        }

        public string GetExternal(User user, string key)
        {
            if (user == null) {
                return null;
            }
            var userInfo = _userManager.Users.Where(t => t.Email == user.Email && t.UserName == user.FullName).OrderBy(t => t.UserName).FirstOrDefault();
            if (userInfo == null) {
                userInfo = _userManager.Users.Where(t => t.Email == user.Email).OrderBy(t => t.UserName).First();
            }
            string externalId;
            var info = _signInManager.GetExternalLoginInfoAsync().GetAwaiter().GetResult();
            if (info != null) {
                switch (key) {
                    case "ExternalId":
                        externalId = info.ProviderKey;
                        return externalId;
                    case "Type": {
                        var externalType = info.LoginProvider;
                        return externalType;
                    }
                    case "Name": {
                        var externalName = userInfo.UserName;
                        return externalName;
                    }
                    case "PictureUrl": {
                        externalId = info.ProviderKey;
                        var externalPictureUrl = _dataContext.UserExtraData.Where(t => t.ProviderKey == externalId).Select(z => z.PictureUrl).FirstOrDefault();
                        return externalPictureUrl;
                    }
                }
            }
            return null;
        }

        [HttpGet]
        public async Task<string> GetComments(string productId, int page)
        {
            var uid = string.IsNullOrEmpty(productId) ? Guid.Empty : Guid.Parse(productId);
            var pageSize = 4;

            Expression<Func<Comment, bool>> filterByProduct = x => x.Product.Id == uid;
            Expression<Func<Comment, bool>> filterAllProducts = x => x.Id != null;
            Expression<Func<Comment, bool>> filter = string.IsNullOrEmpty(productId) ? filterAllProducts : filterByProduct;

            var cacheKey = "Comments_" + (string.IsNullOrEmpty(productId) ? "EMPTY" : productId) + "_" + page.ToString();
            var cached = _cache.Get<string>(cacheKey);
            string serializedComments = string.Empty;
            if (cached != null) {
                serializedComments = cached;
            } else {
                var comments = await _dataContext.Comment
                    .Include("User")
                    .Include("Subcomments.User")
                    .Include("Product.Name")
                    .Where(filter)
                    .Where(t => !t.IsSubComment)
                    .OrderByDescending(t => t.PopulationDate)
                    .ThenBy(t => t.Id)
                    .Skip(pageSize * (page - 1))
                    .Take(pageSize)
                .Select(t => new {
                    t.Id,
                    t.SubComments,
                    t.PopulationDate,
                    Product = t.Product != null ? new {
                        t.Product.Id,
                        Name = _langId == 0 ? t.Product.Name.Text1 : (_langId == 1 ? t.Product.Name.Text2 : t.Product.Name.Text3)
                    } : null,
                    t.Rate,
                    t.Text,
                    t.Title,
                    t.Likes,
                    t.Dislikes,
                    t.User
                })
                .OrderByDescending(t => t.PopulationDate)
                .ToListAsync();

                var eo = new List<dynamic>();

                foreach (var c in comments) {
                    var subcomments = c.SubComments.Select(x => new {
                        x.Dislikes,
                        x.Id,
                        x.Likes,
                        x.PopulationDate,
                        x.Product,
                        x.Rate,
                        x.Text,
                        x.Title,
                        User = new {
                            ExternalId = GetExternal(x.User, "ExternalId"),
                            Type = GetExternal(x.User, "Type"),
                            Name = GetExternal(x.User, "Name"),
                            Fullname = x.User?.FullName,
                            PictureUrl = GetExternal(x.User, "PictureUrl")
                        }
                    }).OrderBy(q => q.PopulationDate).ToList();
                    var e = new {
                        c.Dislikes,
                        c.Id,
                        c.Likes,
                        SubComments = subcomments,
                        c.PopulationDate,
                        c.Product,
                        c.Rate,
                        c.Text,
                        c.Title,
                        User = new {
                            ExternalId = GetExternal(c.User, "ExternalId"),
                            Type = GetExternal(c.User, "Type"),
                            Name = GetExternal(c.User, "Name"),
                            Fullname = c.User.FullName,
                            PictureUrl = GetExternal(c.User, "PictureUrl")
                        }
                    };

                    eo.Add(e);
                }
                serializedComments = JsonConvert.SerializeObject(eo, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedComments, TimeSpan.FromHours(8));
            }
            return serializedComments;
        }
    }
}
