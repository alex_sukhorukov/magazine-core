﻿using WebApp.Controllers;
using DbModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Remotion.Linq.Parsing.Structure;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;
using WebApplicationMVCFacebook.Data;
using WebApplicationMVCFacebook.Models;

namespace WebApp.Controllers
{
    [AllowAnonymous]
    // [StoreExceptionFilter]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AdminController : BaseController
    {
        public AdminController(ApplicationDbContext dataContext, IMemoryCache cache, IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
            : base(dataContext, cache, configuration, httpContextAccessor)
        {
        }

        [HttpGet]
        [AllowAnonymous]
        public bool Ping()
        {
            return IsAdmin;
        }

        [HttpGet]
        [AllowAnonymous]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<string> GetNPCities(string area)
        {
            var areaId = Guid.Parse(area);
            var cacheKey = area;
            var cached = _cache.Get<string>(cacheKey);
            string serializedCities = string.Empty;
            if (cached != null) {
                serializedCities = cached;
            } else {
                var cities = await _dataContext.NPCity.Where(t => t.Area == areaId).OrderBy(t => t.Description).ToListAsync();
                serializedCities = JsonConvert.SerializeObject(cities, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedCities, TimeSpan.FromHours(120));
            }
            return serializedCities;
        }

        [HttpGet]
        [AllowAnonymous]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<string> GetNPAreas()
        {
            var cacheKey = "Areas";
            var cached = _cache.Get<string>(cacheKey);
            string serializedAreas = string.Empty;
            if (cached != null) {
                serializedAreas = cached;
            } else {
                var areas = await _dataContext.NPArea.OrderBy(t => t.Description).ToListAsync();
                serializedAreas = JsonConvert.SerializeObject(areas, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedAreas, TimeSpan.FromHours(120));
            }
            return serializedAreas;
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetCache()
        {
            ClearCache();
            return Ok();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<string> GetNPWarehouses(string city)
        {
            var cityId = Guid.Parse(city);
            var cacheKey = city;
            var cached = _cache.Get<string>(cacheKey);
            string serializedWarehouses = string.Empty;
            if (cached != null) {
                serializedWarehouses = cached;
            } else {
                var warehouses = await _dataContext.NPWarehouse.Where(t => t.CityRef == cityId).OrderBy(t => t.Number).ToListAsync();
                serializedWarehouses = JsonConvert.SerializeObject(warehouses, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedWarehouses, TimeSpan.FromHours(120));
            }
            return serializedWarehouses;
        }
    }
}