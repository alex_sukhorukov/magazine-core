﻿using WebApp.Controllers;
using DbModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Identity;
using WebApp.Models;
using WebApplicationMVCFacebook.Data;
using WebApplicationMVCFacebook.Models;

namespace WebApp.Controllers
{
    [AllowAnonymous]
    // [StoreExceptionFilter]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CatalogController : BaseController
    {
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public CatalogController(ApplicationDbContext dataContext, 
            IMemoryCache cache, 
            IConfiguration configuration, 
            IHttpContextAccessor httpContextAccessor, 
            IHostingEnvironment env, 
            UserManager<ApplicationUser> userManager, 
            SignInManager<ApplicationUser> signInManager)
            : base(dataContext, cache, configuration, httpContextAccessor) {
            _userManager = userManager;
            _signInManager = signInManager;
            _env = env;
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<FileContentResult> GetPreviewImage(string id)
        {
            var guid = Guid.Parse(id);
            var photoExists = false;
            byte[] bytes = null;
            var cacheKey = "Preview_" + guid;
            var cachedImage = _cache.Get<byte[]>(cacheKey);
            if (cachedImage != null) {
                bytes = cachedImage;
                photoExists = true;
            } else {
                var product = await _dataContext.Product.Where(t => t.Id == guid).FirstOrDefaultAsync();
                var image = product?.Images?.FirstOrDefault(t => t.IsPreview);
                if (image != null) {
                    bytes = image.Data;
                    photoExists = true;
                }
            }
            if (cachedImage == null && bytes != null) {
                _cache.Set(cacheKey, bytes, TimeSpan.FromHours(24));
            }
            if (!photoExists) {
                bytes = await GetNoPhotoBytesAsync();
            }
            return File(bytes, "image/png", false);
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<FileContentResult> GetPreviewCategoryImage(string id)
        {
            var guid = Guid.Parse(id);
            var photoExists = false;
            byte[] bytes = null;
            var cacheKey = "Category_" + guid;
            var cachedImage = _cache.Get<byte[]>(cacheKey);
            if (cachedImage != null) {
                bytes = cachedImage;
                photoExists = true;
            } else {
                var data = await _dataContext.Category.Where(t => t.Id == guid).Select(t => t.Image.Data).FirstOrDefaultAsync();
                if (data != null) {
                    bytes = data;
                    photoExists = true;
                }
            }
            if (cachedImage == null && bytes != null) {
                _cache.Set(cacheKey, bytes, TimeSpan.FromHours(24));
            }
            if (!photoExists) {
                bytes = await GetNoPhotoBytesAsync();
            }
            return File(bytes, "image/png", false);
        }

        [HttpGet]
        public async Task<FileContentResult> GetPreviewSeriesImage(string id)
        {
            var guid = Guid.Parse(id);
            var photoExists = false;
            byte[] bytes = null;
            var cacheKey = "Series_" + guid.ToString();
            var cachedImage = _cache.Get<byte[]>(cacheKey);
            if (cachedImage != null) {
                bytes = cachedImage;
                photoExists = true;
            } else {
                var data = await _dataContext.Series.Where(t => t.Id == guid).Select(t => t.Image.Data).FirstOrDefaultAsync();
                if (data != null) {
                    bytes = data;
                    photoExists = true;
                }
            }
            if (cachedImage == null && bytes != null) {
                _cache.Set(cacheKey, bytes, TimeSpan.FromHours(24));
            }
            if (!photoExists) {
                bytes = await GetNoPhotoBytesAsync();
            }
            return File(bytes, "image/png", false);
        }

        [HttpGet]
        public async Task<FileContentResult> GetPreviewBrandImage(string id)
        {
            var guid = Guid.Parse(id);
            var photoExists = false;
            byte[] bytes = null;
            var cacheKey = "Brand_" + guid;
            var cachedImage = _cache.Get<byte[]>(cacheKey);
            if (cachedImage != null) {
                bytes = cachedImage;
                photoExists = true;
            } else {
                var data = await _dataContext.Brand.Where(t => t.Id == guid).Select(t => t.Image.Data).FirstOrDefaultAsync();
                if (data != null) {
                    bytes = data;
                    photoExists = true;
                }
            }
            if (cachedImage == null && bytes != null) {
                _cache.Set(cacheKey, bytes, TimeSpan.FromHours(24));
            }
            if (!photoExists) {
                bytes = await GetNoPhotoBytesAsync();
            }
            return File(bytes, "image/png", false);
        }

        /*
        [HttpGet]
        [Route("GetPreviewSlideImage")]
        public async Task<HttpResponseMessage> GetPreviewSlideImage(string id)
        {
            var guid = Guid.Parse(id);
            var photoExists = false;
            byte[] bytes = null;
            var cacheKey = "Slide_" + guid.ToString();
            var cachedImage = Cache[cacheKey];
            if (cachedImage != null) {
                bytes = cachedImage as byte[];
                photoExists = true;
            } else {
                var slide = await _dataContext.Slide.Include("Image").Where(t => t.Id == guid).FirstOrDefaultAsync();
                if (slide != null) {
                    var image = slide.Image;
                    if (image != null) {
                        bytes = image.Data;
                        photoExists = true;
                    }
                }
            }
            if (cachedImage == null && bytes != null) {
                Cache.Add(cacheKey, bytes, null, System.Web.Caching.Cache.NoAbsoluteExpiration, Utils.General.H24, System.Web.Caching.CacheItemPriority.Low, null);
            }
            if (!photoExists) {
                bytes = GetNoPhotoBytes();
            }
            var result = new HttpResponseMessage(HttpStatusCode.OK) {
                Content = new ByteArrayContent(bytes)
            };
            result.Content.Headers.ContentLength = bytes.LongLength;
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");
            result.Headers.CacheControl = new CacheControlHeaderValue();
            result.Headers.CacheControl.MaxAge = Utils.General.H24;
            result.Headers.CacheControl.Public = true;
            result.Headers.CacheControl.Private = false;
            return result;
        }

        [HttpGet]
        [Route("GetProductImage")]
        public async Task<HttpResponseMessage> GetProductImage(string id, string imageId)
        {
            var guid = Guid.Parse(id);
            var guidImage = Guid.Parse(imageId);
            var photoExists = false;
            byte[] bytes = null;
            var cacheKey = "ProductImage_" + id + imageId;
            var cachedImage = Cache[cacheKey];
            if (cachedImage != null) {
                bytes = cachedImage as byte[];
                photoExists = true;
            } else {
                var product = await _dataContext.Product.Include("Images").Where(t => t.Id == guid).FirstOrDefaultAsync();
                if (product != null) {
                    var image = product.Images.Where(t => t.Id == guidImage).FirstOrDefault();
                    if (image != null) {
                        bytes = image.Data;
                        photoExists = true;
                    }
                }
            }
            if (cachedImage == null && bytes != null) {
                Cache.Add(cacheKey, bytes, null, System.Web.Caching.Cache.NoAbsoluteExpiration, Utils.General.H24, System.Web.Caching.CacheItemPriority.Low, null);
            }
            if (!photoExists) {
                bytes = GetNoPhotoBytes();
            }
            var result = new HttpResponseMessage(HttpStatusCode.OK) {
                Content = new ByteArrayContent(bytes)
            };
            result.Content.Headers.ContentLength = bytes.LongLength;
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/jpg");
            result.Headers.CacheControl = new CacheControlHeaderValue();
            result.Headers.CacheControl.MaxAge = Utils.General.H24;
            result.Headers.CacheControl.Public = true;
            result.Headers.CacheControl.Private = false;
            return result;
        }
        */

        private async Task<byte[]> GetNoPhotoBytesAsync() {
            var filename = Path.Combine(_env.ContentRootPath, "images\\no-photo.png");
            return await System.IO.File.ReadAllBytesAsync(filename);
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<string> GetSlides(string search)
        {
            search = string.IsNullOrEmpty(search) ? string.Empty : search.ToLower();
            var cacheKey = string.IsNullOrEmpty(search) ? "Slides" : "Slides_" + search;
            var cached = _cache.Get<string>(cacheKey);
            string serializedSlides;
            if (cached != null && !IsAdmin) {
                serializedSlides = cached;
            } else {
                var isGuid = Guid.TryParse(search, out var categoryId);
                Expression<Func<Slide, bool>> expr;
                if (!isGuid) {
                    expr = t => t.Category.Code == search;
                } else {
                    expr = t => t.Category.Id == categoryId;
                }
                var slides = await _dataContext.Slide
                    .Where(expr)
                    .OrderBy(t => t.Position).ToListAsync();
                var list = new List<SlideData>();
                foreach (var s in slides) {
                    var sd = new SlideData {
                        Id = s.Id,
                        Url = s.Url,
                        Position = s.Position,
                        CategoryId = s.Category.Id,
                        ImageId = new ImageId {
                            Id = s.Image?.Id ?? Guid.Empty
                        }
                    };
                    list.Add(sd);
                }
                serializedSlides = JsonConvert.SerializeObject(list, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedSlides, TimeSpan.FromHours(24));
            }
            return serializedSlides;
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<string> GetLanguages()
        {
            var cacheKey = "Languages";
            var cached = _cache.Get<string>(cacheKey);
            string serializedLanguages;
            if (cached != null) {
                serializedLanguages = cached;
            } else {
                var languages = await _dataContext.Language.Where(t => t.Active).Select(t => new {
                    t.Id,
                    t.Name,
                    t.ShortName
                }).ToListAsync();
                serializedLanguages = JsonConvert.SerializeObject(languages, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedLanguages, TimeSpan.FromHours(120));
            }
            return serializedLanguages;
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<string> GetCategories(int langId)
        {
            var cacheKey = "CatalogCategories_" + langId;
            var cached = _cache.Get<string>(cacheKey);
            string serializedCategories;
            if (cached != null) {
                serializedCategories = cached;
            } else {
                var categories = await _dataContext.Category
                    .Where(t => t.Active)
                    .Select(t => new {
                        t.Id,
                        CName = langId == 0 ? t.Name.Text1 : (langId == 1 ? t.Name.Text2 : t.Name.Text3),
                        CDesc = langId == 0 ? t.Description.Text1 : (langId == 1 ? t.Description.Text2 : t.Description.Text3),
                        t.Code,
                        Image = new {
                            Id = t.Image == null ? Guid.Empty : t.Image.Id
                        }
                    }).OrderBy(t => t.Code).ToListAsync();
                var categoryIds = categories.Select(x => x.Id).ToList();
                var series = await _dataContext.Product
                    .Where(t => categoryIds.Contains(t.Category.Id))
                    .Select(z => new {
                        CategoryId = z.Category.Id,
                        SeriesCode = z.Series.Code
                    })
                    .Distinct()
                    .ToListAsync();
                var categorySeries = categories.Select(x => new {
                    x.Id,
                    x.CName,
                    x.CDesc,
                    x.Code,
                    x.Image,
                    Series = series
                        .Where(z => z.CategoryId == x.Id)
                        .Select(z => z.SeriesCode)
                        .ToList()
                });
                serializedCategories = JsonConvert.SerializeObject(categorySeries, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedCategories, TimeSpan.FromHours(120));
            }
            return serializedCategories;
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<string> GetSeries(string code, int langId)
        {
            var cacheKey = "Series_" + code + "_" + langId;
            var cached = _cache.Get<string>(cacheKey);
            string serializedSeries;
            if (cached != null) {
                serializedSeries = cached;
            } else {
                var brandIds = await _dataContext.Product
                    .Where(t => t.Active && t.Category.Code == code)
                    .Select(t => t.Brand.Id)
                    .Distinct()
                    .ToListAsync();
                var productInSeries = await _dataContext.Product
                    .Where(z => z.Active && z.Series != null)
                    .Select(z => new {
                        z.Id,
                        SeriesId = z.Series.Id
                    }).GroupBy(z => z.SeriesId)
                    .Select(x => new {
                        SeriesId = x.Key,
                        Count = x.Count()
                    }).ToListAsync();
                var series = await _dataContext.Series
                    .Where(t => t.Active && brandIds.Contains(t.Brand.Id))
                        .Select(t => new {
                            t.Id,
                            CName = langId == 0 ? t.Name.Text1 : (langId == 1 ? t.Name.Text2 : t.Name.Text3),
                            CDesc = langId == 0 ? t.Description.Text1 : (langId == 1 ? t.Description.Text2 : t.Description.Text3),
                            t.Code,
                            BrandId = t.Brand.Id,
                            Image = new {
                                Id = t.Image == null ? Guid.Empty : t.Image.Id
                            }
                        }).OrderBy(t => t.Code).ToListAsync();
                var seriesCount = series
                    .Select(z => new {
                           z.Id,
                           z.CName,
                           z.CDesc,
                           z.BrandId,
                           z.Code,
                           z.Image,
                           Count = productInSeries
                                .Where(x => x.SeriesId == z.Id)
                                .Select(x => x.Count)
                    })
                .ToList();
                serializedSeries = JsonConvert.SerializeObject(seriesCount, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedSeries, TimeSpan.FromHours(120));
            }
            return serializedSeries;
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<string> GetFaqs(int langId)
        {
            var cacheKey = "CatalogFaqs_" + langId;
            var cached = _cache.Get<string>(cacheKey);
            string serializedFaqs;
            if (cached != null) {
                serializedFaqs = cached;
            } else {
                var faqs = await _dataContext.Faq
                    .Select(t => new {
                        t.Id,
                        CName = langId == 0 ? t.Name.Text1 : (langId == 1 ? t.Name.Text2 : t.Name.Text3),
                        CDesc = langId == 0 ? t.Description.Text1 : (langId == 1 ? t.Description.Text2 : t.Description.Text3),
                    }).ToListAsync();
                serializedFaqs = JsonConvert.SerializeObject(faqs, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedFaqs, TimeSpan.FromHours(120));
            }
            return serializedFaqs;
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<string> GetBrands(string search)
        {
            search = string.IsNullOrEmpty(search) ? string.Empty : search.ToLower();
            var cacheKey = string.IsNullOrEmpty(search) ? "Brands" : "Brands_" + search;
            var cached = _cache.Get<string>(cacheKey);
            string serializedBrands;
            if (cached != null && !IsAdmin) {
                serializedBrands = cached;
            } else {
                Expression<Func<Brand, bool>> expr;
                if (string.IsNullOrEmpty(search)) {
                    expr = t => true;
                } else {
                    expr = t => t.Name.ToLower().Contains(search);
                }
                var brands = await _dataContext.Brand
                    .Where(expr)
                    .OrderBy(t => t.Name).ToListAsync();
                var list = new List<BrandData>();
                foreach (Brand c in brands) {
                    var b = new BrandData {
                        Id = c.Id,
                        Name = c.Name,
                        ImageId = new ImageId {
                            Id = c.Image?.Id ?? Guid.Empty
                        }
                    };
                    list.Add(b);
                }
                serializedBrands = JsonConvert.SerializeObject(list, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedBrands, TimeSpan.FromHours(24));
            }
            return serializedBrands;
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<string> GetSearchCategories(string code, int langId)
        {
            var cacheKey = string.IsNullOrEmpty(code) ? "GetSearchCategories_" + langId : "GetSearchCategories_" + code + "_" + langId;
            var cached = _cache.Get<string>(cacheKey);
            var serializedCategories = string.Empty;
            if (cached != null) {
                serializedCategories = cached;
            } else {
                Expression<Func<Category, bool>> expr;
                if (string.IsNullOrEmpty(code)) {
                    expr = t => true;
                } else {
                    expr = t => t.Code == code;
                }
                var categories = await _dataContext.Category
                    .Where(expr)
                    .Select(t => new {
                        t.Id,
                        t.Code,
                        Name = langId == 0 ? t.Name.Text1 : (langId == 1 ? t.Name.Text2 : t.Name.Text3)
                    }).ToListAsync();
                if (categories != null) {
                    serializedCategories = JsonConvert.SerializeObject(categories, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                    _cache.Set(cacheKey, serializedCategories, TimeSpan.FromHours(24));
                }
            }
            return serializedCategories;
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<string> GetCategoriesBrands(string code)
        {
            var cacheKey = string.IsNullOrEmpty(code) ? "GetCategoriesBrands" : "GetCategoriesBrands" + code;
            var cached = _cache.Get<string>(cacheKey);
            string serializedCategoriesBrands;
            if (cached != null) {
                serializedCategoriesBrands = cached;
            } else {
                Expression<Func<Brand, bool>> expr;
                if (string.IsNullOrEmpty(code)) {
                    expr = t => true;
                } else {
                    var brandIds = await _dataContext.Product
                        .Where(t => t.Category.Code == code && t.Brand != null)
                        .Select(t => t.Brand.Id).ToListAsync();
                    expr = t => brandIds.Contains(t.Id);
                }
                var brands = await _dataContext.Brand
                    .Where(expr)
                    .Select(t => new {
                        t.Id,
                        t.Name,
                        Series = t.Series.Select(x => new {
                            x.Id,
                            CName = _langId == 0 ? x.Name.Text1 : (_langId == 1 ? x.Name.Text2 : x.Name.Text3),
                            x.Code
                        }).ToList()
                    }).ToListAsync();
                serializedCategoriesBrands = JsonConvert.SerializeObject(brands, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedCategoriesBrands, TimeSpan.FromHours(24));
            }
            return serializedCategoriesBrands;
        }

        [HttpGet]
        [ResponseCache(CacheProfileName = "Default")]
        public async Task<string> GetProduct(string id, int langId)
        {
            string cacheKey = "Product_" + id + "_" + langId;
            var cached = _cache.Get<string>(cacheKey);
            string serializedProduct;
            if (cached != null) {
                serializedProduct = cached;
            } else {
                var guid = Guid.Parse(id);
                var now = DateTime.UtcNow;

                var product = _dataContext.Product
                    .Where(t => t.Id == guid);

                var logonUser = HttpContext.User.Identity?.Name;
                var info = await _userManager.Users.Where(t => t.UserName == logonUser || t.Email == logonUser).OrderBy(t => t.UserName).FirstOrDefaultAsync();
                var user = info == null ? null : await _dataContext.User.Where(t => t.Email == info.Email).FirstAsync();

                var s = new Stopwatch();
                s.Start();

                var rating = user == null ? 0 :
                    await _dataContext.ProductRate
                        .Where(x => x.Product.Id == guid && x.User.Id == user.Id)
                        .Select(x => x.Rate)
                        .FirstOrDefaultAsync();

                s.Stop();
                Log("rating", s, true);
                s.Reset();

                s.Start();

                var item = await product.Select(t => new ProductItem {
                    Id = t.Id,
                    // Name = langId == 0 ? t.Name.Text1 : (langId == 1 ? t.Name.Text2 : t.Name.Text3),
                    // Description = langId == 0 ? t.Description.Text1 : (langId == 1 ? t.Description.Text2 : t.Description.Text3),
                    // Category = langId == 0 ? t.Category.Name.Text1 : (langId == 1 ? t.Category.Name.Text2 : t.Category.Name.Text3),
                    // t.Category.Code,
                    // Images = t.Images.OrderByDescending(z => z.IsPreview).Select(z => new {
                    //    z.Id,
                    //    z.IsPreview
                    // }),
                    // t.Brand,
                    // t.Series,
                    ProductCode = t.ProductCode,
                    Active = t.Active,
                    IsExists = t.IsExists,
                    Age = t.Age,
                    InternalProductCode = t.InternalProductCode,
                    // Material = t.Material != null ? (langId == 0 ? t.Material.Name.Text1 : (langId == 1 ? t.Material.Name.Text2 : t.Material.Name.Text3)) : string.Empty,
                    NumberOfDetails = t.NumberOfDetails,
                    Pol = t.Pol,
                    WarrantyNumberOfMonth = t.WarrantyNumberOfMonth,
                    Size = t.Size,
                    Rating = rating,
                    Rate = t.Rate,
                    RateCount = t.RateCount,
                    // ProductPrice = t.ProductPrice.Where(z => z.StartDate < now).OrderByDescending(m => m.StartDate).FirstOrDefault()
                }).FirstAsync();

                item.Name = langId == 0
                        ? product.Select(t => t.Name.Text1).FirstOrDefault()
                        : (langId == 1 ? product.Select(t => t.Name.Text2).FirstOrDefault() : product.Select(t => t.Name.Text3).FirstOrDefault());
                item.Description = langId == 0
                    ? product.Select(t => t.Description.Text1).FirstOrDefault()
                    : (langId == 1 ? product.Select(t => t.Description.Text2).FirstOrDefault() : product.Select(t => t.Description.Text3).FirstOrDefault());
                item.CategoryId = product.Where(t => t.Category != null).Select(t => t.Category.Id).FirstOrDefault();
                item.MaterialId = product.Where(t => t.Material != null).Select(t => t.Material.Id).FirstOrDefault();
                item.Category = langId == 0 ? _dataContext.Category.Where(t => t.Id == item.CategoryId).Select(t => t.Name.Text1).FirstOrDefault()
                    : (langId == 1 
                        ? _dataContext.Category.Where(t => t.Id == item.CategoryId).Select(t => t.Name.Text2).FirstOrDefault()
                        : _dataContext.Category.Where(t => t.Id == item.CategoryId).Select(t => t.Name.Text3).FirstOrDefault());
                item.Material = langId == 0
                    ? _dataContext.Material.Where(t => t.Id == item.MaterialId).Select(t => t.Name.Text1).FirstOrDefault()
                    : (langId == 1
                        ? _dataContext.Category.Where(t => t.Id == item.CategoryId).Select(t => t.Name.Text2).FirstOrDefault()
                        : _dataContext.Category.Where(t => t.Id == item.CategoryId).Select(t => t.Name.Text3).FirstOrDefault());
                item.BrandId = product.Select(t => t.Brand.Id).FirstOrDefault();
                item.SeriesId = product.Select(t => t.Series.Id).FirstOrDefault();
                item.ImageIds = product.SelectMany(x => x.Images.Select(t => t.Id)).ToList();
                item.ProductPriceIds = product.SelectMany(x => x.ProductPrice.Select(t => t.Id)).ToList();
                item.Brand = _dataContext.Brand.FirstOrDefault(t => t.Id == item.BrandId);
                item.Series = _dataContext.Series.FirstOrDefault(t => t.Id == item.SeriesId);
                item.Images = _dataContext.ProductImage.Where(t => item.ImageIds.Contains(t.Id)).ToList();
                item.ProductPrice = _dataContext.ProductPrice.Where(t => item.ProductPriceIds.Contains(t.Id) && t.StartDate < now).OrderByDescending(m => m.StartDate).FirstOrDefault();

                s.Stop();
                Log("item", s);
                s.Reset();

                s.Start();

                var recents = await _dataContext.ProductRecent
                    .Where(t => t.Product.Id == item.Id)
                    .Select(t => new {
                        t.Id,
                        ProductId = t.Recent.Id,
                        ProductName = langId == 0 ? t.Recent.Name.Text1 : (langId == 1 ? t.Recent.Name.Text2 : t.Recent.Name.Text3),
                        ProductPrice = t.Recent.ProductPrice.Where(z => z.StartDate < now).OrderByDescending(m => m.StartDate).FirstOrDefault(),
                        t.Recent.Rate,
                        t.Recent.RateCount,
                        t.IsReal,
                        t.Index
                    })
                    .OrderByDescending(t => t.IsReal)
                    .ThenBy(t => t.Index)
                    .ToListAsync();

                s.Stop();
                Log("recent", s);
                s.Reset();

                var productItem = new {
                    item.Id,
                    item.Name,
                    item.Description,
                    item.Category,
                    item.Code,
                    item.Images,
                    item.Brand,
                    item.Series,
                    item.ProductCode,
                    item.Active,
                    item.IsExists,
                    item.Age,
                    item.InternalProductCode,
                    item.Material,
                    item.NumberOfDetails,
                    item.Pol,
                    item.WarrantyNumberOfMonth,
                    item.Size,
                    item.Rating,
                    item.Rate,
                    item.RateCount,
                    item.ProductPrice,
                    Recents = recents
                };
                serializedProduct = JsonConvert.SerializeObject(productItem, Formatting.None, new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedProduct, TimeSpan.FromHours(4)); 
            }
            return serializedProduct;
        }

        private async Task<int> ReceiveTotalPages(SearchModel model)
        {
            var hash = JsonConvert.SerializeObject(model).GetHashCode();
            var cacheKey = "Count_" + hash;
            var cacheSearchKey = "Search_" + hash;
            var cached = _cache.Get<string>(cacheSearchKey);
            int count;
            var pageSize = model.PageSize;
            pageSize = pageSize != -1 ? pageSize : _configuration.GetValue<int>("CatalogPageSize");

            if (cached != null) {
                count = _cache.Get<int>(cacheKey);
            } else {
                var now = DateTime.UtcNow;
                var langId = model.LangId;
                var priceRange = string.IsNullOrEmpty(model.PriceRange) ? "0,1000000000" : model.PriceRange;
                var ageBit = model.Age;
                var page = model.Page;
                var text = model.Text?.Trim().ToLower();
                var polBit = model.Pol;
                var productCode = model.ProductCode;
                var onlyWithComments = model.OnlyWithComments;
                var sortBy = model.SortBy;
                var seriesIds = model.SeriesIds.ToList();
                var categoryIds = model.CategoryIds.Select(Guid.Parse).ToList();
                var brandIds = model.BrandIds.Select(Guid.Parse).ToList();
                var min = decimal.Parse(priceRange.Split(',')[0]);
                var max = decimal.Parse(priceRange.Split(',')[1]);

                Expression<Func<Product, bool>> exprPrice = t =>
                        t.ProductPrice.Where(z => z.StartDate < now).OrderByDescending(m => m.StartDate).FirstOrDefault().Price >= min &&
                        t.ProductPrice.Where(z => z.StartDate < now).OrderByDescending(m => m.StartDate).FirstOrDefault().Price <= max;

                Expression<Func<Product, bool>> exprCategory = null;
                if (categoryIds.Count > 0) {
                    exprCategory = t => categoryIds.Contains(t.Category.Id);
                } else {
                    exprCategory = t => true;
                }

                Expression<Func<Product, bool>> exprBrand = null;
                if (brandIds.Count > 0) {
                    exprBrand = t => brandIds.Contains(t.Brand.Id);
                } else {
                    exprBrand = t => true;
                }

                Expression<Func<Product, bool>> exprSeries = null;
                if (seriesIds.Count > 0) {
                    exprSeries = t => seriesIds.Contains(t.Series.Code);
                } else {
                    exprSeries = t => true;
                }

                Expression<Func<Product, bool>> exprProductCode = null;
                if (!string.IsNullOrEmpty(productCode)) {
                    exprProductCode = t => t.InternalProductCode.Contains(productCode) || t.ProductCode.Contains(productCode);
                } else {
                    exprProductCode = t => true;
                }

                Expression<Func<Product, bool>> exprPol = null;
                if (polBit != 0) {
                    exprPol = t => (polBit & t.Pol) != 0;
                } else {
                    exprPol = t => true;
                }

                Expression<Func<Product, bool>> exprAge = null;
                if (ageBit != 0) {
                    exprAge = t => (ageBit & t.Age) != 0;
                } else {
                    exprAge = t => true;
                }

                Expression<Func<Product, bool>> exprComments = null;
                if (onlyWithComments) {
                    exprComments = t => t.Comments.Any();
                } else {
                    exprComments = t => true;
                }

                Expression<Func<Product, bool>> exprText = null;
                if (!string.IsNullOrEmpty(text)) {
                    exprText = t => 
                        (langId == 0 ? t.Name.Text1 : (langId == 1 ? t.Name.Text2 : t.Name.Text3)).ToLower().Contains(text) || 
                        (langId == 0 ? t.Description.Text1 : (langId == 1 ? t.Description.Text2 : t.Description.Text3)).ToLower().Contains(text);
                } else {
                    exprText = t => true;
                }

                var products = _dataContext.Product
                    .Where(exprPrice)
                    .Where(exprCategory)
                    .Where(exprBrand)
                    .Where(exprSeries)
                    .Where(exprProductCode)
                    .Where(exprPol)
                    .Where(exprAge)
                    .Where(exprText)
                    .Where(exprComments)
                    .Select(t => new {
                        CategoryCode = t.Category.Code,
                        t.CreatedAt,
                        t.Id,
                        Price = t.ProductPrice.Where(z => z.StartDate < now).OrderByDescending(m => m.StartDate).Select(x => x.Price).FirstOrDefault(),
                        SeriesId = t.Series.Id,
                        t.Rate
                    });

                switch (sortBy) {
                    case "ByDateAsc":
                        products = products.OrderBy(t => t.CreatedAt).ThenBy(t => t.Price);
                        break;
                    case "Category":
                        products = products.OrderBy(t => t.CategoryCode).ThenBy(t => t.SeriesId).ThenBy(t => t.Price);
                        break;
                    case "ByDateDesc":
                        products = products.OrderByDescending(t => t.CreatedAt).ThenBy(t => t.Price);
                        break;
                    case "FromChipestToExpensive":
                        products = products.OrderBy(t => t.Price).ThenByDescending(t => t.Rate);
                        break;
                    case "FromExpensiveToChipest":
                        products = products.OrderByDescending(t => t.Price).ThenByDescending(t => t.Rate);
                        break;
                    case "ByPopularity":
                        products = products.OrderByDescending(t => t.Rate).ThenBy(t => t.Price);
                        break;
                    case "ByRating":
                        products = products.OrderByDescending(t => t.Rate).ThenBy(t => t.Price);
                        break;
                }

                var ids = products.Select(t => t.Id);

                var pagedIds = await ids.Skip((page - 1) * pageSize).Take(pageSize).ToListAsync();

                var indexIds = pagedIds.Select((t, i) => new IndexGuid {
                    Index = i,
                    Id = t
                });

                var serializedIndexedIds = JsonConvert.SerializeObject(indexIds, Formatting.None);
                count = ids.Count();

                _cache.Set(cacheKey, count, TimeSpan.FromHours(4));
                _cache.Set(cacheSearchKey, serializedIndexedIds, TimeSpan.FromHours(4));
            }

            var total = Math.Ceiling((decimal)count / pageSize);

            if (total == 0) {
                total = 1;
            }

            return (int)total;
        }

        private static void Log(string l, Stopwatch w, bool init = false) {
                using (var s = new StreamWriter("d:\\a.txt", !init)) {
                s.WriteLine($"line = {l}, time = {w.Elapsed.TotalSeconds}");
            }
        }

        private async Task<string> _SearchNotAll(SearchModel model)
        {
            SearchModel.Normalize(ref model);
            var logonUser = HttpContext.User.Identity?.Name;
            var info = await _userManager.Users.Where(t => t.UserName == logonUser || t.Email == logonUser).OrderBy(t => t.UserName).FirstOrDefaultAsync();
            var user = info == null ? null : await _dataContext.User.Where(t => t.Email == info.Email).FirstAsync();
            var now = DateTime.UtcNow;
            var langId = model.LangId;
            var total = await ReceiveTotalPages(model);
            var hash = JsonConvert.SerializeObject(model).GetHashCode();

            // cacheKey may exists in cache
            var cacheKey = "Products_" + hash;
            var cached = _cache.Get<string>(cacheKey);

            // cacheSearchKey must exists in cache
            var cacheSearchKey = "Search_" + hash;
            var serializedIndexedIds = _cache.Get<string>(cacheSearchKey);
            var indexedIds = JsonConvert.DeserializeObject<List<IndexGuid>>(serializedIndexedIds);
            var ids = indexedIds.Select(t => t.Id).ToList();

            List<ProductItem> list;
            string serializedList;

            if (cached != null) {
                serializedList = cached;
                list = JsonConvert.DeserializeObject<List<ProductItem>>(serializedList);
            } else {
                var products = _dataContext.Product.Where(t => ids.Contains(t.Id));
                var items = products
                    .Select(t => new {
                        t.Id,
                        t.Age,
                        t.Active,
                        t.IsExists,
                        t.CreatedAt,
                        t.NumberOfDetails,
                        t.InternalProductCode,
                        t.Pol,
                        Price = t.ProductPrice.Where(z => z.StartDate < now).OrderByDescending(m => m.StartDate).Select(x => x.Price).FirstOrDefault(),
                        t.ProductCode,
                        t.Size,
                        t.WarrantyNumberOfMonth
                    });

                var localizedItems = await items.Select(t => new ProductItem {
                    Id = t.Id,
                    Age = t.Age,
                    Active = t.Active,
                    IsExists = t.IsExists,
                    ProductCode = t.ProductCode,
                    InternalProductCode = t.InternalProductCode,
                    NumberOfDetails = t.NumberOfDetails,
                    Pol = t.Pol,
                    WarrantyNumberOfMonth = t.WarrantyNumberOfMonth,
                    Size = t.Size,
                    CreatedAt = t.CreatedAt,
                    Price = t.Price,
                }).ToListAsync();

                foreach (var li in localizedItems) {
                    var product = _dataContext.Product.Where(t => t.Id == li.Id);
                    li.Name = langId == 0
                        ? product.Select(t => t.Name.Text1).FirstOrDefault()
                        : (langId == 1
                            ? product.Select(t => t.Name.Text2).FirstOrDefault()
                            : product.Select(t => t.Name.Text3).FirstOrDefault());
                    li.Description = langId == 0
                        ? product.Select(t => t.Description.Text1).FirstOrDefault()
                        : (langId == 1
                            ? product.Select(t => t.Description.Text2).FirstOrDefault()
                            : product.Select(t => t.Description.Text3).FirstOrDefault());
                    li.CategoryId = product.Where(t => t.Category != null).Select(t => t.Category.Id).FirstOrDefault();
                    li.MaterialId = product.Where(t => t.Material != null).Select(t => t.Material.Id).FirstOrDefault();
                    li.Category = langId == 0
                        ? _dataContext.Category.Where(t => t.Id == li.CategoryId).Select(t => t.Name.Text1)
                            .FirstOrDefault()
                        : (langId == 1
                            ? _dataContext.Category.Where(t => t.Id == li.CategoryId).Select(t => t.Name.Text2).FirstOrDefault()
                            : _dataContext.Category.Where(t => t.Id == li.CategoryId).Select(t => t.Name.Text3).FirstOrDefault());
                    li.Material = langId == 0
                        ? _dataContext.Material.Where(t => t.Id == li.MaterialId).Select(t => t.Name.Text1)
                            .FirstOrDefault()
                        : (langId == 1
                            ? _dataContext.Category.Where(t => t.Id == li.CategoryId).Select(t => t.Name.Text2).FirstOrDefault()
                            : _dataContext.Category.Where(t => t.Id == li.CategoryId).Select(t => t.Name.Text3).FirstOrDefault());
                    li.BrandId = product.Select(t => t.Brand.Id).FirstOrDefault();
                    li.SeriesId = product.Select(t => t.Series.Id).FirstOrDefault();
                    li.ImageIds = product.SelectMany(x => x.Images.Select(t => t.Id)).ToList();
                    li.ProductPriceIds = product.SelectMany(x => x.ProductPrice.Select(t => t.Id)).ToList();
                    li.Brand = _dataContext.Brand.FirstOrDefault(t => t.Id == li.BrandId);
                    li.Series = _dataContext.Series.FirstOrDefault(t => t.Id == li.SeriesId);
                    li.Images = _dataContext.ProductImage.Where(t => li.ImageIds.Contains(t.Id)).ToList();
                    li.ProductPrice = _dataContext.ProductPrice.Where(t => li.ProductPriceIds.Contains(t.Id) && t.StartDate < now).OrderByDescending(m => m.StartDate).FirstOrDefault();
                }

                list = localizedItems;

                serializedList = JsonConvert.SerializeObject(list, Formatting.None, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
                _cache.Set(cacheKey, serializedList, TimeSpan.FromHours(4));
            }

            var dataList = new List<dynamic>();

            foreach (var t in list) {
                Guid pId = t.Id;
                double rating = 0;
                if (user == null) {
                    rating = 0;
                } else {
                    rating = await _dataContext.ProductRate
                        .Where(x => x.Product.Id == pId && x.User.Id == user.Id)
                        .Select(x => x.Rate)
                        .FirstOrDefaultAsync();
                }

                var cacheRateKey = "ProductRate_" + pId;
                dynamic rate;
                var serializedRate = _cache.Get<string>(cacheRateKey);

                if (serializedRate == null) {
                    rate = await _dataContext.Product.Where(z => z.Id == pId).Select(z => new {
                        z.Rate,
                        z.RateCount
                    }).FirstOrDefaultAsync();
                    serializedRate = JsonConvert.SerializeObject(rate);
                    _cache.Set(cacheRateKey, serializedRate, TimeSpan.FromHours(120));
                } else {
                    rate = JsonConvert.DeserializeObject<dynamic>(serializedRate);
                }

                dataList.Add(new {
                    i = indexedIds.Where(z => z.Id == pId).Select(z => z.Index).FirstOrDefault(),
                    t.Id,
                    t.Name,
                    t.Description,
                    t.Category,
                    t.Images,
                    t.Brand,
                    t.Series,
                    t.ProductCode,
                    t.Active,
                    t.IsExists,
                    t.Age,
                    t.InternalProductCode,
                    t.Material,
                    t.NumberOfDetails,
                    t.Pol,
                    t.WarrantyNumberOfMonth,
                    t.Size,
                    t.CreatedAt,
                    t.Price,
                    Rating = rating,
                    Rate = rate == null ? 0 : rate.Rate,
                    RateCount = rate == null ? 0 : rate.RateCount,
                    t.ProductPrice
                });
            }

            var o = new {
                total,
                data = dataList.OrderBy(t => t.i).ToList()
            };

            var data = JsonConvert.SerializeObject(o, Formatting.None, new JsonSerializerSettings {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            });

            return data;
        }

        private async Task<string> _SearchAll(SearchModel model)
        {
            SearchModel.Normalize(ref model);
            var now = DateTime.UtcNow;
            var langId = model.LangId;
            var priceRange = string.IsNullOrEmpty(model.PriceRange) ? "0,1000000000" : model.PriceRange;
            var ageBit = model.Age;
            var pageSize = model.PageSize;
            var page = model.Page;
            var text = model.Text?.Trim().ToLower();
            var polBit = model.Pol;
            var productCode = model.ProductCode;
            var onlyWithComments = model.OnlyWithComments;
            var sortBy = model.SortBy;
            var seriesIds = model.SeriesIds.ToList();
            var categoryIds = model.CategoryIds.Select(Guid.Parse).ToList();
            var brandIds = model.BrandIds.Select(Guid.Parse).ToList();
            var min = decimal.Parse(priceRange.Split(',')[0]);
            var max = decimal.Parse(priceRange.Split(',')[1]);
            var logonUser = HttpContext.User.Identity?.Name;
            var info = await _userManager.Users.Where(t => t.UserName == logonUser || t.Email == logonUser).OrderBy(t => t.UserName).FirstOrDefaultAsync();
            var user = info == null ? null : await _dataContext.User.Where(t => t.Email == info.Email).FirstAsync();

            pageSize = pageSize != -1 ? pageSize : _configuration.GetValue<int>("CatalogPageSize");

            Expression<Func<Product, bool>> exprPrice = t =>
                    t.ProductPrice.Where(z => z.StartDate < now).OrderByDescending(m => m.StartDate).FirstOrDefault().Price >= min &&
                    t.ProductPrice.Where(z => z.StartDate < now).OrderByDescending(m => m.StartDate).FirstOrDefault().Price <= max;

            Expression<Func<Product, bool>> exprCategory = null;
            if (categoryIds.Count > 0) {
                exprCategory = t => categoryIds.Contains(t.Category.Id);
            } else {
                exprCategory = t => true;
            }

            Expression<Func<Product, bool>> exprBrand = null;
            if (brandIds.Count > 0) {
                exprBrand = t => brandIds.Contains(t.Brand.Id);
            } else {
                exprBrand = t => true;
            }

            Expression<Func<Product, bool>> exprSeries = null;
            if (seriesIds.Count > 0) {
                exprSeries = t => seriesIds.Contains(t.Series.Code);
            } else {
                exprSeries = t => true;
            }

            Expression<Func<Product, bool>> exprProductCode = null;
            if (!string.IsNullOrEmpty(productCode)) {
                exprProductCode = t => t.InternalProductCode.Contains(productCode) || t.ProductCode.Contains(productCode);
            } else {
                exprProductCode = t => true;
            }

            Expression<Func<Product, bool>> exprPol = null;
            if (polBit != 0) {
                exprPol = t => (polBit & t.Pol) != 0;
            } else {
                exprPol = t => true;
            }

            Expression<Func<Product, bool>> exprAge = null;
            if (ageBit != 0) {
                exprAge = t => (ageBit & t.Age) != 0;
            } else {
                exprAge = t => true;
            }

            Expression<Func<Product, bool>> exprComments = null;
            if (onlyWithComments) {
                exprComments = t => t.Comments.Any();
            } else {
                exprComments = t => true;
            }

            Expression<Func<Product, bool>> exprText = null;
            if (!string.IsNullOrEmpty(text)) {
                exprText = t => 
                    (langId == 0 ? t.Name.Text1 : (langId == 1 ? t.Name.Text2 : t.Name.Text3)).ToLower().Contains(text) || 
                    (langId == 0 ? t.Name.Text1 : (langId == 1 ? t.Name.Text2 : t.Name.Text3)).ToLower().Contains(text);
            } else {
                exprText = t => true;
            }

            var total = await _dataContext.Product
                .Where(exprPrice)
                .Where(exprCategory)
                .Where(exprBrand)
                .Where(exprSeries)
                .Where(exprProductCode)
                .Where(exprPol)
                .Where(exprAge)
                .Where(exprText)
                .Where(exprComments)
                .CountAsync();

            if (page < 1) {
                page = 1;
            }

            var products = _dataContext.Product;

            var items = products
                .Where(exprPrice)
                .Where(exprCategory)
                .Where(exprBrand)
                .Where(exprSeries)
                .Where(exprProductCode)
                .Where(exprPol)
                .Where(exprAge)
                .Where(exprText)
                .Where(exprComments)
                .Select(t => new {
                    t.Active,
                    t.Age,
                    t.Brand,
                    t.Category,
                    t.Comments,
                    t.CreatedAt,
                    t.Description,
                    t.Id,
                    t.Images,
                    t.InternalProductCode,
                    t.IsExists,
                    t.Material,
                    t.Name,
                    t.NumberOfDetails,
                    t.Pol,
                    t.ProductPrice,
                    Price = t.ProductPrice.Where(z => z.StartDate < now).OrderByDescending(m => m.StartDate).Select(x => x.Price).FirstOrDefault(),
                    t.ProductCode,
                    t.Series,
                    t.Size,
                    t.UpdatedAt,
                    t.Videos,
                    t.Rate,
                    t.RateCount,
                    t.WarrantyNumberOfMonth
                });

            if (sortBy == "ByDateAsc") {
                items = items.OrderBy(t => t.CreatedAt).ThenBy(t => t.Price);
            } else if (sortBy == "Category") {
                items = items.OrderBy(t => t.Category.Code).ThenBy(t => t.Series == null ? t.Series.Id : Guid.Empty).ThenBy(t => t.Price);
            } else if (sortBy == "ByDateDesc") {
                items = items.OrderByDescending(t => t.CreatedAt).ThenBy(t => t.Price);
            } else if (sortBy == "FromChipestToExpensive") {
                items = items.OrderBy(t => t.Price).ThenByDescending(t => t.Rate);
            } else if (sortBy == "FromExpensiveToChipest") {
                items = items.OrderByDescending(t => t.Price).ThenByDescending(t => t.Rate);
            } else if (sortBy == "ByPopularity") {
                items = items.OrderByDescending(t => t.Rate).ThenBy(t => t.Price);
            } else if (sortBy == "ByRating") {
                items = items.OrderByDescending(t => t.Rate).ThenBy(t => t.Price);
            }

            items = items.Skip((page - 1) * pageSize).Take(pageSize);

            var allItems = items.Select(t => new {
                t.Id,
                Name = langId == 0 ? t.Name.Text1 : (langId == 1 ? t.Name.Text2 : t.Name.Text3),
                Names = new [] { t.Name.Text1, t.Name.Text2, t.Name.Text3 },
                Descriptions = new [] { t.Description.Text1, t.Description.Text1, t.Description.Text2, t.Description.Text3 },
                Category = langId == 0 ? t.Category.Name.Text1 : (langId == 1 ? t.Category.Name.Text2 : t.Category.Name.Text3),
                Images = t.Images.OrderByDescending(z => z.IsPreview).Select(z => new {
                    z.Id,
                    z.IsPreview
                }),
                t.Brand,
                t.Series,
                t.ProductCode,
                t.Active,
                t.IsExists,
                t.Age,
                t.InternalProductCode,
                Material = t.Material != null ? (langId == 0 ? t.Material.Name.Text1 : (langId == 1 ? t.Material.Name.Text2 : t.Material.Name.Text3)) : string.Empty,
                t.NumberOfDetails,
                t.Pol,
                t.WarrantyNumberOfMonth,
                t.Size,
                ProductPrices = t.ProductPrice.OrderByDescending(z => z.StartDate).ToList(),
                t.Price,
                t.CreatedAt,
                t.Rate,
                t.RateCount,
                ProductPrice = t.ProductPrice.Where(z => z.StartDate < now).OrderByDescending(m => m.StartDate).FirstOrDefault()
            });

            switch (sortBy) {
                case "ByDateAsc":
                    allItems = allItems.OrderBy(t => t.CreatedAt).ThenBy(t => t.Price);
                    break;
                case "Category":
                    allItems = allItems.OrderBy(t => t.Category).ThenBy(t => t.Series == null ? t.Series.Id : Guid.Empty).ThenBy(t => t.Price);
                    break;
                case "ByDateDesc":
                    allItems = allItems.OrderByDescending(t => t.CreatedAt).ThenBy(t => t.Price);
                    break;
                case "FromChipestToExpensive":
                    allItems = allItems.OrderBy(t => t.Price).ThenByDescending(t => t.Rate);
                    break;
                case "FromExpensiveToChipest":
                    allItems = allItems.OrderByDescending(t => t.Price).ThenByDescending(t => t.Rate);
                    break;
                case "ByPopularity":
                    allItems = allItems.OrderByDescending(t => t.Rate).ThenBy(t => t.Price);
                    break;
                case "ByRating":
                    allItems = allItems.OrderByDescending(t => t.Rate).ThenBy(t => t.Price);
                    break;
            }

            var list = await allItems.ToListAsync();
            var dataList = new List<dynamic>();
            foreach (var t in list) {
                var rating = user == null ? 0 : await _dataContext.ProductRate
                    .Where(x => x.Product.Id == t.Id && x.User.Id == user.Id)
                    .Select(x => x.Rate)
                    .FirstOrDefaultAsync();

                dataList.Add(new {
                    t.Id,
                    t.Name,
                    t.Names,
                    t.Descriptions,
                    t.Category,
                    t.Images,
                    t.Brand,
                    t.Series,
                    t.ProductCode,
                    t.Active,
                    t.IsExists,
                    t.Age,
                    t.InternalProductCode,
                    t.Material,
                    t.NumberOfDetails,
                    t.Pol,
                    t.WarrantyNumberOfMonth,
                    t.Size,
                    t.ProductPrices,
                    t.Price,
                    t.CreatedAt,
                    Rating = rating,
                    t.Rate,
                    t.RateCount,
                    t.ProductPrice
                });
            }

            var data = dataList.Count > 0 ? JsonConvert.SerializeObject(dataList, Formatting.None, new JsonSerializerSettings {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            }) : "[]";

            var serializedData = JsonConvert.SerializeObject(new {
                page,
                data,
                total
            }, Formatting.None);

            return serializedData;
        }

        [HttpGet]
        public async Task<string> Search(string model)
        {
            var m = SearchModel.Parse(model);
            var all = m.All;
            if (all) {
                return await _SearchAll(m);
            }
            return await _SearchNotAll(m);
        }
    }
}