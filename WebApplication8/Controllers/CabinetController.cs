﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using WebApplicationMVCFacebook.Data;
using WebApplicationMVCFacebook.Models;

namespace WebApp.Controllers
{
    [Authorize]
    // [StoreExceptionFilter]
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class CabinetController : BaseController
    {
        private readonly IHostingEnvironment _env;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public CabinetController(ApplicationDbContext dataContext,
            IMemoryCache cache,
            IConfiguration configuration,
            IHttpContextAccessor httpContextAccessor,
            IHostingEnvironment env,
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
            : base(dataContext, cache, configuration, httpContextAccessor)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _env = env;
        }

        [HttpGet]
        public async Task<IActionResult> GetUserInfo()
        {
            // var logonUser = HttpContext.Current.Request.Params["LOGON_USER"];
            // var userInfo = await UserManager.Users.Where(t => t.UserName == logonUser || t.Email == logonUser).OrderBy(t => t.UserName).FirstAsync();
            var logonUser = HttpContext.User.Identity?.Name;
            var count = _userManager.Users.Count();
            var info = await _userManager.Users.Where(t => t.UserName == logonUser || t.Email == logonUser).OrderBy(t => t.UserName).FirstOrDefaultAsync();
            var email = info.Email;
            var user = await _dataContext.User.Where(t => t.Email == email).FirstAsync();
            return Ok(new {
                user.Id,
                user.Email,
                Phone = user.PhoneNumber,
                Address = user.LastShipAddress,
                user.EmailConfirmedFlag
            });
        }

        [HttpGet]
        public async Task<IActionResult> GetOrders(int page, string number, string calendar, string userName) {
            return Ok(new List<int>());
        }


        /*
        [HttpPost]
        [Route("RemoveOrder")]
        public async Task<IHttpActionResult> RemoveOrder(RemoveModel model)
        {
            var order = await _dataContext.Order
                .Include("OrderAddress")
                .Include("ProductSets")
                .Where(t => t.Id == model.Id).FirstAsync();

            _dataContext.Order.Remove(order);

            await _dataContext.SaveChangesAsync();

            return Content(HttpStatusCode.OK, string.Empty);
        }

        [HttpPost]
        [Route("RemoveProfile")]
        public async Task<IHttpActionResult> RemoveProfile(RemoveModel model)
        {
            var userId = model.Id.ToString();
            var user = await UserManager.Users.Where(t => t.Id == userId).FirstOrDefaultAsync();

            if (user != null) {
                var dataUser = await _dataContext.User.Where(t => t.Email == user.Email).FirstOrDefaultAsync();
                if (dataUser != null) {
                    _dataContext.User.Remove(dataUser);
                }
                UserManager.Delete(user);
            }

            return Content(HttpStatusCode.OK, string.Empty);
        }

        [HttpPost]
        [Route("SaveUserInfo")]
        public async Task<IHttpActionResult> SaveUserInfo(UserInfo data)
        {
            if (data.Address == null && data.Phone == null && string.IsNullOrEmpty(data.Email)) {
                data.Address = string.Empty;
                data.Phone = string.Empty;
                data.Email = string.Empty;
                return Content(HttpStatusCode.OK, string.Empty);
            }

            var logonUser = HttpContext.Current.Request.Params["LOGON_USER"];
            var userInfo = await UserManager.Users.Where(t => t.UserName == logonUser || t.Email == logonUser).OrderBy(t => t.UserName).FirstAsync();
            var email = data.Email;
            var userId = data.Id;
            var user = await _dataContext.User.Where(t => t.Id == userId).FirstAsync();
            var changed = false;

            if (user.LastShipAddress != data.Address) {
                user.LastShipAddress = data.Address;
                changed = true;
            }

            if (user.PhoneNumber != data.Phone) {
                user.PhoneNumber = data.Phone;
                changed = true;
            }

            if ((userInfo.Email != data.Email || user.Email != data.Email) && !string.IsNullOrEmpty(data.Email)) {
                var confirmed = Cache[user.Id + "_EmailChangeConfirm"];
                if (confirmed != null && user.EmailConfirmedFlag == 1) {
                    user.Email = data.Email.Trim();
                    var result = await UserManager.SetEmailAsync(userInfo.Id, email);
                    if (!result.Succeeded) {
                        throw new Exception("Email change error: " + result.ToString());
                    }
                    changed = true;
                } else {
                    var uid = Guid.NewGuid();
                    user.TempGuid = uid.ToString().Replace("-", string.Empty);
                    var guid = user.TempGuid;
                    var action = "confirmchangeemail";
#if DEBUG
                    var websiteUrl = ConfigurationManager.AppSettings["WebsiteUrlLocal"];
#else
                    var websiteUrl = ConfigurationManager.AppSettings["WebsiteUrlRemote"];
#endif
                    Cache[user.Id + "_EmailChangeConfirm"] = data.Email;
                    var confirmUrl = websiteUrl + "/#/" + action + "/" + guid;
                    var values = new Dictionary<string, string>();
                    values.Add("[ACTION_LINK]", confirmUrl);
                    General.SendEmail(Request.RequestUri.Scheme, Request.RequestUri.Host, Request.RequestUri.Port, user.FullName, data.Email, "confirmemailchange", ", email confirmation", values);
                    user.EmailConfirmedFlag = 0;
                    changed = true;
                }
            }

            if (changed) {
                await _dataContext.SaveChangesAsync();
            }

            return Content(HttpStatusCode.OK, user.EmailConfirmedFlag);
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("OrderAction")]
        public async Task<IHttpActionResult> OrderAction(OrderActionModel model)
        {
            var order = await _dataContext.Order
                .Include("ProductSets.Product.Category")
                .Include("ProductSets.Product.Name")
                .Include("ProductSets.Product.Description")
                .Include("ProductSets.Product.Images")
                .Include("ProductSets.Product.Videos")
                .Include("ProductSets.Product.ProductPrice")
                .Include("OrderState.Name")
                .Include("DeliveryMethod.Name.Language")
                .Include("NPCity")
                .Include("NPWarehouse")
                .Include("OrderAddress")
                .Include("User")
                .Include("UserShadow")
                .Where(t => t.Id == model.Id)
            .FirstAsync();

            var orderStateId = Guid.Empty;
            if (model.Action == "New") {
                orderStateId = Guid.Parse("9003b4b6-089f-4bb6-a659-85677a9e98cd");
            } else if (model.Action == "Placed") {
                orderStateId = Guid.Parse("2f232b72-82c8-40f7-bca6-64217a5cb916");
            } else if (model.Action == "Processed") {
                orderStateId = Guid.Parse("a37f286e-a6cc-43fd-b8a2-30475b412e53");
            } else if (model.Action == "Succeeded") {
                orderStateId = Guid.Parse("bc84afe9-c679-4298-bcb4-66c3485ba645");
            } else if (model.Action == "Canceled") {
                orderStateId = Guid.Parse("c13f74d7-be87-41c7-9de9-589944bd8621");
            } else if (model.Action == "Failed") {
                orderStateId = Guid.Parse("9dd4c231-ffbc-483a-a1b6-4adbaf612c5f");
            }

            var orderState = await _dataContext.OrderState.Include("Name.Language").Where(t => t.Id == orderStateId).FirstAsync();
            order.OrderState = orderState;
            await _dataContext.SaveChangesAsync();

            var mainContent = string.Empty;
            mainContent += "<table>";
            mainContent += "<thead>";
            mainContent += "<th>";
            mainContent += "#".Translate();
            mainContent += "</th>";
            mainContent += "<th>";
            mainContent += "ProductName".Translate();
            mainContent += "</th>";
            mainContent += "<th>";
            mainContent += "Count".Translate();
            mainContent += "</th>";
            mainContent += "<th>";
            mainContent += "Price".Translate() + ", &nbsp;&#8372;";
            mainContent += "</th>";
            mainContent += "<th>";
            mainContent += "Cost".Translate() + ", &nbsp;&#8372;";
            mainContent += "</th>";
            mainContent += "</thead>";
            mainContent += "<tbody>";

            var i = 1;
            decimal total = 0m;

            foreach (var ps in order.ProductSets) {
                var price = ps.Product.ProductPrice.Where(y => y.StartDate < order.PopulationDate).OrderByDescending(w => w.StartDate).Select(h => h.Price).FirstOrDefault();
                var cost = ps.Quantity * price;
                total += cost;

                mainContent += "<tr>";
                mainContent += "<td>";
                mainContent += i++;
                mainContent += "</td>";
                mainContent += "<td class=\"text-left\">";
                mainContent += ps.Product.Name.Where(t => t.Language.Id == langId).Select(t => t.Text).FirstOrDefault();
                mainContent += "</td>";
                mainContent += "<td>";
                mainContent += ps.Quantity;
                mainContent += "</td>";
                mainContent += "<td>";
                mainContent += price;
                mainContent += "</td>";
                mainContent += "<td>";
                mainContent += cost;
                mainContent += "</td>";
                mainContent += "</tr>";
            }
            mainContent += "</tbody>";
            mainContent += "</table>";

            var values = new Dictionary<string, string>
            {
                {"[MAIN_CONTENT]", mainContent},
                {"[TOTAL_SUM]", "Total".Translate() + ":&nbsp;" + total + "&nbsp;&#8372;"}
            };

            var deliveryInfo = "<div style=\"margin-bottom:15px;\"><b>" + "DeliveryMethod".Translate() + "</b>: " + order.DeliveryMethod.Name.Where(t => t.Language.Id == langId).Select(t => t.Text).FirstOrDefault() + "</div>";
            if (order.DeliveryMethod.Id == Guid.Parse("F9329628-6E0C-4132-8BD9-95404A602331")) {
                // pickup
                deliveryInfo += "<div>" + "PickupAddressText".Translate() + "</div>";
            } else if (order.DeliveryMethod.Id == Guid.Parse("B683A766-4239-4E45-B392-AD7C23E91EB0")) {
                // np
                var area = await _dataContext.NPArea.Where(t => t.Ref == order.NPCity.Area).FirstOrDefaultAsync();
                if (area != null) {
                    deliveryInfo += "<b>" + "Area".Translate() + "</b>";
                    deliveryInfo += "<div style=\"margin-bottom:15px;\">" + area.Description + "</div>";
                }
                deliveryInfo += "<b>" + "City".Translate() + "</b>";
                deliveryInfo += "<div style=\"margin-bottom:15px;\">" + order.NPCity.DescriptionRu + "</div>";
                deliveryInfo += "<b>" + "Warehouse".Translate() + "</b>";
                deliveryInfo += "<div>" + order.NPWarehouse.DescriptionRu + "</div>";
            }

            var orderStateInfo = "<div><b> " + "OrderState".Translate() + ":</b> " + order.OrderState.Name.Where(t => t.Language.Id == langId).Select(t => t.Text).FirstOrDefault() + "</div>";

            var phoneInfo = "<div><b> " + "Phone".Translate() + ":</b> " + order.Phone + "</div>";

            values.Add("[ORDER_STATE]", orderStateInfo);

            values.Add("[DELIVERY_INFO]", deliveryInfo);

            values.Add("[ORDER_NUMBER]", order.Number);

            values.Add("[PHONE_INFO]", phoneInfo);

            var fullName = order.User != null ? order.User.FullName : order.UserShadow.FullName;
            var email = order.User != null ? order.User.Email : order.UserShadow.Email;

            if (model.Action != "New") {
                General.SendEmail(
                    Request.RequestUri.Scheme,
                    Request.RequestUri.Host,
                    Request.RequestUri.Port,
                    fullName,
                    email,
                    "orderstate_" + model.Action.ToLower(),
                    ", " + "OrderNumber".Translate() + ": " + order.Number + ", " + "OrderState".Translate() + ": " + ("OrderState_" + model.Action).Translate(),
                    values);
            }

            General.SendEmail(
                Request.RequestUri.Scheme,
                Request.RequestUri.Host,
                Request.RequestUri.Port,
                fullName,
                email,
                "orderstate_" + model.Action.ToLower(),
                ", " + "OrderNumber".Translate() + ": " + order.Number + ", " + "OrderState".Translate() + ": " + ("OrderState_" + model.Action).Translate(),
                values,
                true);

            return Content(HttpStatusCode.OK, new {
                orderState.Id,
                Code = orderState.Name.Where(x => x.Language.Id == langId).Select(x => x.Code).FirstOrDefault(),
                Name = orderState.Name.Where(x => x.Language.Id == langId).Select(x => x.Text).FirstOrDefault()
            });
        }

        public string GetExternal(User user, string key)
        {
            var userInfo = UserManager.Users.Where(t => t.Email == user.Email).OrderBy(t => t.UserName).First();
            string externalId;
            if (userInfo.Logins.Count > 0) {
                if (key == "ExternalId") {
                    externalId = userInfo.Logins.First().ProviderKey;
                    return externalId;
                }
                if (key == "Type") {
                    var externalType = userInfo.Logins.First().LoginProvider;
                    return externalType;
                }
                if (key == "Name") {
                    var externalName = userInfo.UserName;
                    return externalName;
                }
                if (key == "PictureUrl") {
                    externalId = userInfo.Logins.First().ProviderKey;
                    var externalPictureUrl = _dataContext.UserExtraData.Where(t => t.ProviderKey == externalId).Select(z => z.PictureUrl).FirstOrDefault();
                    return externalPictureUrl;
                }
            }
            return null;
        }

        [HttpGet]
        [Route("GetOrders")]
        public async Task<IHttpActionResult> GetOrders(int page, string number, string calendar, string userName)
        {
            const int pageSize = 10;
            var logonUser = HttpContext.Current.Request.Params["LOGON_USER"];
            var userInfo = await UserManager.Users.Where(t => t.UserName == logonUser || t.Email == logonUser).OrderBy(t => t.UserName).FirstAsync();
            var user = await _dataContext.User.Where(t => t.Email == userInfo.Email).FirstAsync();
            var now = DateTime.UtcNow;
            var date = string.IsNullOrEmpty(calendar) ? now : DateTime.ParseExact(calendar, "dd.MM.yyyy", CultureInfo.InvariantCulture).Date.ToUniversalTime();
            var dayMinusOne = date.AddDays(-1);
            var isAdmin = UserManager.IsInRole(userInfo.Id, "Admin");

            Expression<Func<Order, bool>> adminFilter = (t => t.Id != null);
            Expression<Func<Order, bool>> userFilter = (t => t.User.Email == user.Email);
            Expression<Func<Order, bool>> filter = isAdmin ? adminFilter : userFilter;
            Expression<Func<Order, bool>> filterNumber = string.IsNullOrEmpty(number) ? adminFilter : (t => t.Number.Contains(number));
            Expression<Func<Order, bool>> filterCalendar = string.IsNullOrEmpty(calendar) ? adminFilter : (t => t.PopulationDate >= dayMinusOne && t.PopulationDate <= date);
            Expression<Func<Order, bool>> filterUserName = string.IsNullOrEmpty(userName) ? adminFilter : (t => t.User.FullName.Contains(userName));

            var orders = await _dataContext.Order
                .Include("ProductSets.Product.Category")
                .Include("ProductSets.Product.Name")
                .Include("ProductSets.Product.Description")
                .Include("ProductSets.Product.Images")
                .Include("ProductSets.Product.Videos")
                .Include("ProductSets.Product.ProductPrice")
                .Include("OrderState.Name")
                .Include("DeliveryMethod.Name")
                .Include("NPCity")
                .Include("NPWarehouse")
                .Include("OrderAddress")
                .Include("User")
                .Include("UserShadow")
            .Where(filter)
            .Where(filterNumber)
            .Where(filterUserName)
            .Where(filterCalendar)
            .OrderByDescending(t => t.PopulationDate)
            .Skip((page - 1) * pageSize)
            .Take(pageSize)
            .Select(z => new {
                z.AdditionalInfo,
                DeliveryMethod = new {
                    z.DeliveryMethod.Id,
                    Name = z.DeliveryMethod.Name.Where(x => x.Language.Id == langId).Select(x => x.Text).FirstOrDefault()
                },
                z.Id,
                NPCity = z.NPCity != null ? new {
                    z.NPCity.CityId,
                    z.NPCity.Ref,
                    z.NPCity.Description,
                    z.NPCity.DescriptionRu
                } : null,
                NPWarehouse = z.NPWarehouse != null ? new {
                    z.NPWarehouse.Ref,
                    z.NPWarehouse.Description,
                    z.NPWarehouse.DescriptionRu,
                    z.NPWarehouse.Phone,
                    z.NPWarehouse.TypeOfWarehouse
                } : null,
                OrderState = new {
                    z.OrderState.Id,
                    Code = z.OrderState.Name.Where(x => x.Language.Id == langId).Select(x => x.Code).FirstOrDefault(),
                    Name = z.OrderState.Name.Where(x => x.Language.Id == langId).Select(x => x.Text).FirstOrDefault()
                },
                z.OrderAddress,
                z.Number,
                z.Phone,
                z.PopulationDate,
                z.User,
                z.UserShadow,
                z.TotalPrice,
                ProductSets = z.ProductSets.Select(x => new {
                    x.Id,
                    x.Quantity,
                    Product = new {
                        x.Product.Id,
                        Name = x.Product.Name.Where(y => y.Language.Id == langId).Select(w => w.Text).FirstOrDefault(),
                        Description = x.Product.Description.Where(y => y.Language.Id == langId).Select(w => w.Text).FirstOrDefault(),
                        Category = x.Product.Category.Name.Where(y => y.Language.Id == langId).Select(w => w.Text).FirstOrDefault(),
                        Images = x.Product.Images.OrderByDescending(y => y.IsPreview).Select(w => new {
                            w.Id,
                            w.IsPreview
                        }),
                        x.Product.Active,
                        x.Product.IsExists,
                        ProductPrice = x.Product.ProductPrice.Where(y => y.StartDate < z.PopulationDate).OrderByDescending(w => w.StartDate).FirstOrDefault()
                    },
                    x.FixPrice,
                    x.Discount
                })
            })
            .ToListAsync();

            var eo = new List<dynamic>();

            foreach (var o in orders) {
                var e = new {
                    o.AdditionalInfo,
                    o.DeliveryMethod,
                    o.Id,
                    o.NPCity,
                    o.NPWarehouse,
                    o.OrderState,
                    o.OrderAddress,
                    o.Number,
                    o.Phone,
                    o.PopulationDate,
                    o.ProductSets,
                    o.UserShadow,
                    o.TotalPrice,
                    User = o.User == null ? null : new {
                        ExternalId = GetExternal(o.User, "ExternalId"),
                        Type = GetExternal(o.User, "Type"),
                        Name = GetExternal(o.User, "Name"),
                        PictureUrl = GetExternal(o.User, "PictureUrl"),
                        Data = new {
                            o.User.Id,
                            o.User.CreatedAt,
                            o.User.FullName,
                            o.User.PhoneNumber,
                            o.User.LastShipAddress
                        }
                    }
                };

                eo.Add(e);
            }

            return Content(HttpStatusCode.OK, eo);
        }
        */
        }
}
