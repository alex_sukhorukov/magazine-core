﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using DbModel;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using WebApplicationMVCFacebook.Models;

namespace WebApplicationMVCFacebook.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        protected IConfiguration _configuration;
        public DbSet<Brand> Brand { get; set; }
        public DbSet<BrandImage> BrandImage { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<ProductRate> ProductRate { get; set; }
        public DbSet<ProductRecent> ProductRecent { get; set; }
        public DbSet<UserExtraData> UserExtraData { get; set; }
        public DbSet<Cart> Cart { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<Material> Material { get; set; }
        public DbSet<Faq> Faq { get; set; }
        public DbSet<CategoryImage> CategoryImage { get; set; }
        public DbSet<Slide> Slide { get; set; }
        public DbSet<SlideImage> SlideImage { get; set; }
        public DbSet<Language> Language { get; set; }
        public DbSet<Comment> Comment { get; set; }
        public DbSet<LocalizableString> LocalizableString { get; set; }
        public DbSet<Order> Order { get; set; }
        public DbSet<Series> Series { get; set; }
        public DbSet<SeriesImage> SeriesImage { get; set; }
        public DbSet<OrderSequence> OrderSequence { get; set; }
        public DbSet<OrderAddress> OrderAddress { get; set; }
        public DbSet<OrderState> OrderState { get; set; }
        public DbSet<Product> Product { get; set; }
        public DbSet<ProductImage> ProductImage { get; set; }
        public DbSet<ProductPrice> ProductPrice { get; set; }
        public DbSet<ProductSet> ProductSet { get; set; }
        public DbSet<DeliveryMethod> DeliveryMethod { get; set; }
        public DbSet<NPCity> NPCity { get; set; }
        public DbSet<NPWarehouse> NPWarehouse { get; set; }
        public DbSet<NPWarehouseType> NPWarehouseType { get; set; }
        public DbSet<NPArea> NPArea { get; set; }

        public ApplicationDbContext(IConfiguration configuration, DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            _configuration = configuration;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
            builder.Entity<Brand>().ToTable("Brand");
            builder.Entity<BrandImage>().ToTable("BrandImage");
            builder.Entity<ProductRate>().ToTable("ProductRate");
            builder.Entity<ProductRecent>().ToTable("ProductRecent");
            builder.Entity<NPArea>().ToTable("NPArea");
            builder.Entity<NPCity>().ToTable("NPCity");
            builder.Entity<Series>().ToTable("Series");
            builder.Entity<SeriesImage>().ToTable("SeriesImage");
            builder.Entity<NPWarehouse>().ToTable("NPWarehouse");
            builder.Entity<NPWarehouseType>().ToTable("NPWarehouseType");
            builder.Entity<User>().ToTable("User");
            builder.Entity<UserExtraData>().ToTable("UserExtraData");
            builder.Entity<Cart>().ToTable("Cart");
            builder.Entity<Language>().ToTable("Language");
            builder.Entity<Category>().ToTable("Category");
            builder.Entity<Material>().ToTable("Material");
            builder.Entity<Faq>().ToTable("Faq");
            builder.Entity<CategoryImage>().ToTable("CategoryImage");
            builder.Entity<Slide>().ToTable("Slide");
            builder.Entity<SlideImage>().ToTable("SlideImage");
            builder.Entity<Comment>().ToTable("Comment");
            builder.Entity<LocalizableString>().ToTable("LocalizableString");
            builder.Entity<Order>().ToTable("Order");
            builder.Entity<OrderAddress>().ToTable("OrderAddress");
            builder.Entity<OrderState>().ToTable("OrderState");
            builder.Entity<Product>().ToTable("Product");
            builder.Entity<ProductImage>().ToTable("ProductImage");
            builder.Entity<ProductPrice>().ToTable("ProductPrice");
            builder.Entity<ProductSet>().ToTable("ProductSet");
            builder.Entity<DeliveryMethod>().ToTable("DeliveryMethod");

            builder.Entity<Brand>()
                .HasOne(t => t.Image)
                .WithOne(t => t.Brand)
                .HasForeignKey<BrandImage>(x => x.BrandForeignKey);

            builder.Entity<Category>()
                .HasOne(t => t.Image)
                .WithOne(t => t.Category)
                .HasForeignKey<CategoryImage>(x => x.CategoryForeignKey);

            builder.Entity<Order>()
                .HasOne(t => t.OrderAddress)
                .WithOne(t => t.Order)
                .HasForeignKey<OrderAddress>(x => x.OrderForeignKey);

            builder.Entity<Slide>()
                .HasOne(t => t.Image)
                .WithOne(t => t.Slide)
                .HasForeignKey<SlideImage>(x => x.SlideForeignKey);

            builder.Entity<Series>()
                .HasOne(t => t.Image)
                .WithOne(t => t.Series)
                .HasForeignKey<SeriesImage>(x => x.SeriesForeignKey);

            //builder.Entity<Category>()
            //    .HasMany(t => t.Description);

            //builder.Entity<DeliveryMethod>()
            //    .HasMany(t => t.Name);

            //builder.Entity<LocalizableString>()
            //    .HasMany(t => t.ProductsNames);

            //builder.Entity<LocalizableString>()
            //    .HasMany(t => t.ProductsDescriptions);

            //builder.Entity<LocalizableString>()
            //    .HasMany(t => t.CategoriesNames);

            //builder.Entity<LocalizableString>()
            //    .HasMany(t => t.CategoriesDescriptions);

            //builder.Entity<LocalizableString>()
            //    .HasMany(t => t.OrderStates);

            //builder.Entity<LocalizableString>()
            //    .HasMany(t => t.DeliveryMethods);

            //builder.Entity<OrderState>()
            //    .HasMany(t => t.Name);

            //builder.Entity<Product>()
            //    .HasMany(t => t.Name);

            //builder.Entity<Product>()
            //    .HasMany(t => t.Description);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("DefaultConnection"));
            optionsBuilder.EnableSensitiveDataLogging(false);
            optionsBuilder.UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking);
        }
    }
}
