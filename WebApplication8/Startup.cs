﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebApplicationMVCFacebook.Data;
using WebApplicationMVCFacebook.Models;

namespace WebApplication8
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public IMemoryCache _cache { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMemoryCache();

            //services.AddCors(o => o.AddPolicy("MyPolicy", builder =>
            //{
            //    builder.AllowAnyOrigin()
            //           .AllowAnyMethod()
            //           .AllowAnyHeader();
            //}));

            services.AddResponseCaching();
            services.AddResponseCompression(options => {
                options.Providers.Add<GzipCompressionProvider>();
            });

            services.Configure<GzipCompressionProviderOptions>(options => {
                options.Level = CompressionLevel.Fastest;
            });

            services.AddMvc(options => {
                // options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
                // options.Filters.Add(new CorsAuthorizationFilterFactory("MyPolicy"));

                options.CacheProfiles.Add("Default",
                    new CacheProfile() {
                        Duration = 60
                    });

                options.CacheProfiles.Add("Never",
                    new CacheProfile() {
                        Location = ResponseCacheLocation.None,
                        NoStore = true
                    });

            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.Configure<ApiBehaviorOptions>(options =>
            {
                options.SuppressConsumesConstraintForFormFileParameters = true;
                options.SuppressInferBindingSourcesForParameters = true;
                options.SuppressModelStateInvalidFilter = true;
            });

            //services.AddHttpsRedirection(options => {
            //    options.RedirectStatusCode = StatusCodes.Status307TemporaryRedirect;
            //    options.HttpsPort = 44382;
            //});

            services.AddRouting();

            HibernatingRhinos.Profiler.Appender.EntityFramework.EntityFrameworkProfiler.Initialize();

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();
            // /*
            services.AddAuthentication().AddFacebook(options => {
                options.AppId = Configuration["Authentication:Facebook:AppId"];
                options.AppSecret = Configuration["Authentication:Facebook:AppSecret"];
            });

            services.AddAuthentication().AddGoogle(options => {
                options.ClientId = Configuration["Authentication:Google:ClientId"];
                options.ClientSecret = Configuration["Authentication:Google:ClientSecret"];
            });

            services.AddAuthentication().AddTwitter(options => {
                options.ConsumerKey = Configuration["Authentication:Twitter:ConsumerKey"];
                options.ConsumerSecret = Configuration["Authentication:Twitter:ConsumerSecret"];
                options.RetrieveUserDetails = true;
            });

            services.AddAuthentication().AddInstagram(options => {
                options.ClientId = Configuration["Authentication:Instagram:ClientId"];
                options.ClientSecret = Configuration["Authentication:Instagram:ClientSecret"];
                options.BackchannelHttpHandler = new InstagramBackChannelHandler(_cache);
                //options.Scope.Add("basic");
            });
            // */
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, IMemoryCache cache)
        {
            _cache = cache;

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment()) {
                app.UseDeveloperExceptionPage();
            } else {
                app.UseHsts();
            }

            // app.UseHttpsRedirection();

            app.UseStaticFiles();

            app.UseResponseCaching();
            app.UseResponseCompression();

            app.UseAuthentication();

            app.UseDefaultFiles(new DefaultFilesOptions {
                DefaultFileNames = new[] { "index.html" }
            });

            app.UseFileServer(new FileServerOptions {
                EnableDefaultFiles = true,
                RequestPath = ""
            });

            // Enable Cors
            // app.UseCors("MyPolicy");

            app.UseMvc(routes => {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }

    public class InstagramBackChannelHandler : HttpClientHandler
    {
        private IMemoryCache _cache = null;

        public InstagramBackChannelHandler(IMemoryCache cache) : base()
        {
            _cache = cache;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var result = await base.SendAsync(request, cancellationToken);
            var text = await result.Content.ReadAsStringAsync();
            if (request.RequestUri.AbsoluteUri.Contains("https://api.instagram.com/oauth/access_token")) {
                var data = JsonConvert.DeserializeObject(text) as JObject;
                var id = data["user"]?["id"]?.Value<string>();
                var pictureUrl = data["user"]?["profile_picture"]?.Value<string>();
                var locale = data["language"]?.Value<string>();
                var username = data["user"]?["username"]?.Value<string>();
                if (!string.IsNullOrEmpty(id)) {
                    if (_cache.Get<Dictionary<string, string>>(id) == null) {
                        _cache.Set(id, new Dictionary<string, string>());
                    }
                    var values = _cache.Get<Dictionary<string, string>>(id);
                    values["username"] = username;
                    values["Locale"] = locale;
                    values["PictureUrl"] = pictureUrl;
                }
            }
            return result;
        }
    }
}
