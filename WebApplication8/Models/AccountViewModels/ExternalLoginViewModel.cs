using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplicationMVCFacebook.Models.AccountViewModels
{
    public class ExternalLoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string ExternalId { get; set; }
        public string UserName { get; set; }
        public string PictureUrl { get; set; }
    }

    public class ExternalLoginInfoModel
    {
        public string Provider { get; set; }
        public string ReturnUrl { get; set; }
    }
}
