﻿using System;
using DbModel;
using System.Collections.Generic;

namespace WebApp.Models
{
    internal class ProductItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string ProductCode { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public string Code { get; set; }
        public Guid CategoryId { get; set; }
        public Guid MaterialId { get; set; }
        public ICollection<ProductImage> Images { get; set; }
        public string Material { get; set; }
        public ProductPrice ProductPrice { get; set; }
        public ICollection<Guid> ProductPriceIds { get; set; }
        public Guid BrandId { get;set; }
        public Guid SeriesId { get; set; }
        public ICollection<Guid> ImageIds { get; set; }
        public Brand Brand { get; set; }
        public Series Series { get; set; }
        public bool Active { get; set; }
        public double Rating { get; set; }
        public double Rate { get; set; }
        public int RateCount { get; set; }
        public bool IsExists { get; set; }
        public int Age { get; set; }
        public string InternalProductCode { get; set; }
        public int Pol { get; set; }
        public int NumberOfDetails { get; set; }
        public int WarrantyNumberOfMonth { get; set; }
        public string Size { get; set; }
        public DateTime CreatedAt { get; set; }
        public decimal Price { get; set; }
    }
}