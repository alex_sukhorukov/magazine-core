'use strict';
module.exports = function (grunt) {

    var debug = true;

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    // Show grunt task time
    require('time-grunt')(grunt);

    // Configurable paths for the app
    var appConfig = {
        app: 'app',
        dist: 'wwwroot'
    };

    // Grunt configuration
    grunt.initConfig({

        // Project settings
        homer: appConfig,

        // The grunt server settings
        connect: {
            connect: {
                uses_defaults: {}
            }
        },
        // Compile less to css
        less: {
            development: {
                options: {
                    compress: !debug,
                    optimization: debug ? 0 : 3
                },
                files: {
                    "app/styles/style.css": "app/less/style.less"
                }
            }
        },
        // Watch for changes in live edit
        watch: {
            configFiles: {
                files: ['app/**/*.js', 'app/**/*.less', 'app/**/*.html', 'app/**/*.css'],
                tasks: ['clean:dist', 'less', 'useminPrepare', 'concat', 'copy:dist', 'cssmin', 'uglify', 'filerev', 'usemin', 'htmlmin'],
                options: {
                    reload: true,
                    livereload: {
                        host: 'localhost',
                        port: 9001
                    }
                }
            }
        },
        uglify: {
            options: {
                compress: !debug,
                comments: !debug,
                screwIE8: false,
                sourceMap: false,
                beautify: debug,
                timings: true,
                verbose: true,
                warn: true,
                mangle: false
            }
        },
        clean: {
            dist: {
                options: {
                    force: true
                },
                files: [{
                    dot: true,
                    src: [
                        '.tmp',
                        '<%= homer.dist %>/scripts/*.*',
                        '<%= homer.dist %>/styles/img/*.*',
                        '<%= homer.dist %>/styles/*.*',
                        '<%= homer.dist %>/{,*/}*',
                        '!<%= homer.dist %>/.git*'
                    ]
                }]
            },
            server: '.tmp'
        },
        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= homer.app %>',
                        dest: '<%= homer.dist %>',
                        src: [
                            '*.{ico,png,jpg,txt}',
                            '.htaccess',
                            '*.html',
                            'scripts/directives/**/*.html',
                            'views/**/{,*/}*.html',
                            'styles/img/*.*',
                            'styles/*.{woff,ttf,svg,eot}',
                            'data/**/{,*/}*.*',
                            'images/**/{,*/}*.*',
                            'img/**/{,*/}*.*'
                        ]
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'bower_components/fontawesome',
                        src: ['fonts/*.*'],
                        dest: '<%= homer.dist %>'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'bower_components/bootstrap',
                        src: ['fonts/*.*'],
                        dest: '<%= homer.dist %>'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'bower_components/fancybox/source',
                        src: ['*.{png,jpg,jpeg,gif,webp,svg}'],
                        dest: '<%= homer.dist %>/styles'
                    },
                    {
                        expand: true,
                        dot: true,
                        cwd: 'app/fonts/pe-icon-7-stroke/',
                        src: ['fonts/*.*'],
                        dest: '<%= homer.dist %>'
                    }
                ]
            },
            styles: {
                expand: true,
                cwd: '<%= homer.app %>/styles',
                dest: '.tmp/styles/',
                src: '{,*/}*.css'
            }
        },
        // Renames files for browser caching purposes
        filerev: {
            dist: {
                src: [
                    '<%= homer.dist %>/scripts/{,*/}*.js',
                    '<%= homer.dist %>/styles/{,*/}*.css',
                    '<%= homer.dist %>/styles/fonts/*'
                ]
            }
        },
        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: !debug,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: false,
                    removeCommentsFromCDATA: true,
                    removeOptionalTags: false
                },
                files: [{
                    expand: true,
                    cwd: '<%= homer.dist %>',
                    src: ['*.html', 'views/{,*/}*.html'],
                    dest: '<%= homer.dist %>'
                }]
            }
        },
        useminPrepare: {
            html: 'app/index.html',
            options: {
                dest: '<%= homer.dist %>'
            }
        },
        usemin: {
            html: ['<%= homer.dist %>/index.html']
        }
    });

    grunt.registerTask('live', [
        'clean:server',
        'copy:styles',
        'connect:livereload',
        'watch'
    ]);

    grunt.registerTask('server', [
        'build',
        'connect:dist:keepalive'
    ]);

    grunt.registerTask('build', [
        'clean:dist',
        'less',
        'useminPrepare',
        'concat',
        'copy:dist',
        'cssmin',
        'uglify',
        'filerev',
        'usemin',
        'htmlmin',
        'watch:configFiles'
    ]);
};
