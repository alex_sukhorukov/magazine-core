﻿function loginService($http, $injector, $q) {
    var urlBase = '/api/Account/';

    return {
        externalLogin: externalLogin
    };

    function externalLogin(model) {
        var request = $http.get(urlBase + 'ExternalLogin', {
            params: {
                provider: model.provider,
                returnUrl: model.returnUrl
            }
        });
        return request.then(handleSuccess, handleError);
    }

    function handleError(response) {
        if (angular.isObject(response.data) && response.data.message) {
            return $q.reject(response.data.message);
        }
        return $q.reject('An unknown error occurred.');
    }

    function handleSuccess(response) {
        return response.data;
    }
}

angular
    .module('homer')
    .service('loginService', loginService);
