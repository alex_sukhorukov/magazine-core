﻿function adminService($http, $injector, $q) {
    var urlBase = '/api/Admin/';

    return ({
        product: {
            save: save,
            remove: remove,
            removeProductPrice: removeProductPrice
        },
        string: {
            get: getStrings,
            save: saveString,
            remove: removeString
        },
        category: {
            get: getCategories,
            save: saveCategory,
            remove: removeCategory
        },
        faq: {
            get: getFaqs,
            save: saveFaq,
            remove: removeFaq
        },
        brand: {
            get: getBrands,
            save: saveBrand,
            remove: removeBrand
        },
        slide: {
            get: getSlides,
            save: saveSlide,
            remove: removeSlide
        },
        series: {
            get: getSeries,
            save: saveSeries,
            remove: removeSeries
        },
        formatDate: formatDate,
        ping: ping,
        removeImage: removeImage,
        resetCache: resetCache,
        removeSlideImage: removeSlideImage,
        removeBrandImage: removeBrandImage,
        removeCategoryImage: removeCategoryImage,
        removeSeriesImage: removeSeriesImage,
        getNPAreas: getNPAreas,
        getNPCities: getNPCities,
        getNPWarehouses: getNPWarehouses,
        updateNovaPoshta: updateNovaPoshta
    });

    function convertUTCDateToLocalDate(date) {
        var newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();
        newDate.setHours(hours - offset);
        return newDate;
    }

    function formatDate(dt, utc) {
        if (!dt) {
            return '';
        }
        if (!dt['getDate']) {
            dt = new Date(Date.parse(dt));
        }
        if (utc) {
            dt = convertUTCDateToLocalDate(dt);
        }
        var d = dt.getDate();
        var m = dt.getMonth() + 1;
        var y = dt.getFullYear();
        var hh = dt.getHours();
        var mm = dt.getMinutes();
        if (d < 10) {
            d = '0' + d;
        }
        if (m < 10) {
            m = '0' + m;
        }
        if (hh < 10) {
            hh = '0' + hh;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        return d + '.' + m + '.' + y + ' ' + hh + ':' + mm;
    }

    function ping() {
        var request = $http.get(urlBase + 'Ping');
        return (request.then(handleSuccess, handleError));
    }

    function getNPCities(areaRef) {
        var request = $http.get(urlBase + 'GetNPCities', {
            params: {
                area: areaRef
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getSalesAmount(year) {
        var request = $http.get(urlBase + 'GetSalesAmount', {
            params: {
                year: year
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getSalesCount(year) {
        var request = $http.get(urlBase + 'GetSalesCount', {
            params: {
                year: year
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getSalesByRegion(year) {
        var request = $http.get(urlBase + 'GetSalesByRegion', {
            params: {
                year: year
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getSalesByCategory(year) {
        var request = $http.get(urlBase + 'GetSalesByCategory', {
            params: {
                year: year
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getSalesYears() {
        var request = $http.get(urlBase + 'GetSalesYears');
        return (request.then(handleSuccess, handleError));
    }

    function getNPAreas() {
        var request = $http.get(urlBase + 'GetNPAreas');
        return (request.then(handleSuccess, handleError));
    }

    function getNPWarehouses(cityRef) {
        var request = $http.get(urlBase + 'GetNPWarehouses', {
            params: {
                city: cityRef
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getStrings(search) {
        var request = $http.get(urlBase + 'GetStrings', {
            params: {
                search: search
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getCategories(search) {
        var request = $http.get(urlBase + 'GetCategories', {
            params: {
                search: search
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getFaqs(search) {
        var request = $http.get(urlBase + 'GetFaqs', {
            params: {
                search: search
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getBrands(search) {
        var request = $http.get(urlBase + 'GetBrands', {
            params: {
                search: search
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getSlides(search) {
        var request = $http.get(urlBase + 'GetSlides', {
            params: {
                search: search
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getSeries(search) {
        var request = $http.get(urlBase + 'GetSeries', {
            params: {
                search: search
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function save(model) {
        var request = $http.post(urlBase + 'Save', model);
        return (request.then(handleSuccess, handleError));
    }

    function updateNovaPoshta() {
        var request = $http.post(urlBase + 'UpdateNovaPoshta');
        return (request.then(handleSuccess, handleError));
    }

    function saveString(model) {
        var request = $http.post(urlBase + 'SaveString', model);
        return (request.then(handleSuccess, handleError));
    }

    function saveCategory(model) {
        var request = $http.post(urlBase + 'SaveCategory', model);
        return (request.then(handleSuccess, handleError));
    }

    function saveFaq(model) {
        var request = $http.post(urlBase + 'SaveFaq', model);
        return (request.then(handleSuccess, handleError));
    }

    function saveBrand(model) {
        var request = $http.post(urlBase + 'SaveBrand', model);
        return (request.then(handleSuccess, handleError));
    }

    function saveSlide(model) {
        var request = $http.post(urlBase + 'SaveSlide', model);
        return (request.then(handleSuccess, handleError));
    }

    function saveSeries(model) {
        var request = $http.post(urlBase + 'SaveSeries', model);
        return (request.then(handleSuccess, handleError));
    }

    function removeImage(id, imageId) {
        var request = $http.post(urlBase + 'RemoveImage', {
            id: id,
            imageId: imageId
        });
        return (request.then(handleSuccess, handleError));
    }

    function resetCache() {
        var request = $http.get(urlBase + 'ResetCache');
        return (request.then(handleSuccess, handleError));
    }

    function removeCategoryImage(id, imageId) {
        var request = $http.post(urlBase + 'RemoveCategoryImage', {
            id: id,
            imageId: imageId
        });
        return (request.then(handleSuccess, handleError));
    }

    function removeSeriesImage(id, imageId) {
        var request = $http.post(urlBase + 'RemoveSeriesImage', {
            id: id,
            imageId: imageId
        });
        return (request.then(handleSuccess, handleError));
    }

    function removeBrandImage(id, imageId) {
        var request = $http.post(urlBase + 'RemoveBrandImage', {
            id: id,
            imageId: imageId
        });
        return (request.then(handleSuccess, handleError));
    }

    function removeSlideImage(id, imageId) {
        var request = $http.post(urlBase + 'RemoveSlideImage', {
            id: id,
            imageId: imageId
        });
        return (request.then(handleSuccess, handleError));
    }

    function removeProductPrice(id) {
        var request = $http.post(urlBase + 'RemoveProductPrice', {
            id: id
        });
        return (request.then(handleSuccess, handleError));
    }

    function remove(id) {
        var request = $http.post(urlBase + 'Remove', {
            id: id
        });
        return (request.then(handleSuccess, handleError));
    }

    function removeString(id) {
        var request = $http.post(urlBase + 'RemoveString', {
            id: id
        });
        return (request.then(handleSuccess, handleError));
    }

    function removeCategory(id) {
        var request = $http.post(urlBase + 'RemoveCategory', {
            id: id
        });
        return (request.then(handleSuccess, handleError));
    }

    function removeFaq(id) {
        var request = $http.post(urlBase + 'RemoveFaq', {
            id: id
        });
        return (request.then(handleSuccess, handleError));
    }

    function removeBrand(id) {
        var request = $http.post(urlBase + 'RemoveBrand', {
            id: id
        });
        return (request.then(handleSuccess, handleError));
    }

    function removeSlide(id) {
        var request = $http.post(urlBase + 'RemoveSlide', {
            id: id
        });
        return (request.then(handleSuccess, handleError));
    }

    function removeSeries(id) {
        var request = $http.post(urlBase + 'RemoveSeries', {
            id: id
        });
        return (request.then(handleSuccess, handleError));
    }

    function handleError(response) {
        if (angular.isObject(response.data) && response.data.message) {
            return ($q.reject(response.data.message));
        }
        return ($q.reject('An unknown error occurred.'));
    }

    function handleSuccess(response) {
        return (response.data);
    }
};

/**
 * Pass function into module
 */
angular
    .module('homer')
    .service('adminService', adminService);