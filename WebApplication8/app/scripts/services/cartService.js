﻿function cartService($http, $injector, $q) {
    var urlBase = '/api/Cart/';

    return ({
        preOrder: preOrder,
        placeOrder: placeOrder,
        getDeliveryMethods: getDeliveryMethods
    });

    function getDeliveryMethods() {
        var request = $http.get(urlBase + 'GetDeliveryMethods');
        return (request.then(handleSuccess, handleError));
    }

    function preOrder(model) {
        var request = $http.post(urlBase + 'PreOrder', model);
        return (request.then(handleSuccess, handleError));
    }

    function placeOrder(model) {
        var request = $http.post(urlBase + 'PlaceOrder', model);
        return (request.then(handleSuccess, handleError));
    }

    function handleError(response) {
        if (angular.isObject(response.data) && response.data.message) {
            return ($q.reject(response.data.message));
        }
        return ($q.reject('An unknown error occurred.'));
    }

    function handleSuccess(response) {
        return (response.data);
    }
};

/**
 * Pass function into module
 */
angular
    .module('homer')
    .service('cartService', cartService);
