﻿function translationService($http, $injector, $q, localStorageService) {
    var urlBase = '/api/Values/';

    return {
        get: get,
        changeLocale: changeLocale,
        getComments: getComments,
        addComment: addComment,
        addSubComment: addSubComment,
        productRate: productRate,
        removeComment: removeComment,
        translate: translate
    };

    function addSubComment(model) {
        var request = $http.post(urlBase + 'AddSubComment', model);
        return request.then(handleSuccess, handleError);
    }

    function productRate(model) {
        var request = $http.post(urlBase + 'ProductRate', model);
        return request.then(handleSuccess, handleError);
    }

    function addComment(model) {
        var request = $http.post(urlBase + 'AddComment', {
            ProductId: model.productId,
            Text: model.text,
            Title: model.title
        });
        return request.then(handleSuccess, handleError);
    }

    function removeComment(id) {
        var request = $http.post(urlBase + 'RemoveComment', {
            Id: id
        });
        return request.then(handleSuccess, handleError);
    }

    function getComments(productId, page) {
        var request = $http.get(urlBase + 'GetComments', {
            params: {
                productId: productId,
                page: page
            }
        });
        return request.then(handleSuccess, handleError);
    }

    function translate(code) {
        var langId = localStorageService.get('langId') || 0;
        var _v = window['_translate'] || {};
        return _v[code] !== undefined ? _v[code] : '#' + code;
    }

    function changeLocale(langId) {
        var request = $http.get(urlBase + 'ChangeLocale', {
            params: {
                langId: langId
            }
        });
        return request.then(handleSuccess, handleError);
    }

    function get(langId, callback, error) {
        jQuery(function () {
            jQuery.ajax({
                url: urlBase + 'GetLocalizableStrings',
                data: {
                    langId: langId
                },
                success: function (result) {
                    result = JSON.parse(result);
                    callback(result);
                },
                failure: function (result) {
                    error(result);
                },
                async: false
            });
        });
    }

    function handleError(response) {
        if (angular.isObject(response.data) && response.data.message) {
            return ($q.reject(response.data.message));
        }
        return ($q.reject('An unknown error occurred.'));
    }

    function handleSuccess(response) {
        return (response.data);
    }
};

/**
 * Pass function into module
 */
angular
    .module('homer')
    .service('translationService', translationService);