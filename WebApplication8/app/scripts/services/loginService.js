﻿function loginService($http, $injector, $q) {
    var urlBase = '/api/Account/';

    return ({
        getKeys: getKeys
    });

    function getKeys() {
        var request = $http.get(urlBase + 'GetLoginKeys');
        return (request.then(handleSuccess, handleError));
    }

    function handleError(response) {
        if (angular.isObject(response.data) && response.data.message) {
            return ($q.reject(response.data.message));
        }
        return ($q.reject('An unknown error occurred.'));
    }

    function handleSuccess(response) {
        return (response.data);
    }
};

/**
 * Pass function into module
 */
angular
    .module('homer')
    .service('loginService', loginService);
