﻿function cabinetService($http, $injector, $q) {
    var urlBase = '/api/Cabinet/';

    return ({
        saveUserInfo: saveUserInfo,
        getOrders: getOrders,
        getUserInfo: getUserInfo,
        orderAction: orderAction,
        removeOrder: removeOrder,
        removeProfile: removeProfile
    });

    function orderAction(id, action) {
        var request = $http.post(urlBase + 'OrderAction', {
            Id: id,
            Action: action
        });
        return (request.then(handleSuccess, handleError));
    }

    function getOrders(page, n, c, u) {
        var request = $http.get(urlBase + 'GetOrders', {
            params: {
                page: page,
                number: n,
                calendar: c,
                userName: u
            }
        });
        return (request.then(handleSuccess, handleError));
    }

    function getUserInfo() {
        var request = $http.get(urlBase + 'GetUserInfo');
        return (request.then(handleSuccess, handleError));
    }

    function removeOrder(id) {
        var request = $http.post(urlBase + 'RemoveOrder', {
            Id: id
        });
        return (request.then(handleSuccess, handleError));
    }

    function removeProfile(id) {
        var request = $http.post(urlBase + 'RemoveProfile', {
            Id: id
        });
        return (request.then(handleSuccess, handleError));
    }

    function saveUserInfo(model) {
        var request = $http.post(urlBase + 'SaveUserInfo', model);
        return (request.then(handleSuccess, handleError));
    }

    function handleError(response) {
        if (angular.isObject(response.data) && response.data.message) {
            return ($q.reject(response.data.message));
        }
        return ($q.reject('An unknown error occurred.'));
    }

    function handleSuccess(response) {
        return (response.data);
    }
};

/**
 * Pass function into module
 */
angular
    .module('homer')
    .service('cabinetService', cabinetService);
