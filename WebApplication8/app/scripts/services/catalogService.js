﻿function catalogService($http, $injector, $q, localStorageService, $rootScope) {
    var urlBase = '/api/Catalog/';

    return {
        product: {
            search: search
        },
        getProduct: getProduct,
        receiveTotalPages: receiveTotalPages,
        getLanguages: getLanguages,
        getProductImage: getProductImage,
        getFaqs: getFaqs,
        getBrands: getBrands,
        getSlides: getSlides,
        getCategories: getCategories,
        getSeries: getSeries,
        getSearchCategories: getSearchCategories,
        getCategoriesBrands: getCategoriesBrands
    };

    function getSlides(search) {
        var request = $http.get(urlBase + 'GetSlides', {
            params: {
                search: search
            }
        });
        return request.then(handleSuccess, handleError);
    }

    function getBrands(search) {
        var request = $http.get(urlBase + 'GetBrands', {
            params: {
                search: search
            }
        });
        return request.then(handleSuccess, handleError);
    }

    function getSearchCategories(code) {
        var langId = localStorageService.get('langId') || 0;
        var request = $http.get(urlBase + 'GetSearchCategories', {
            params: {
                code: code,
                langId: langId
            }
        });
        return request.then(handleSuccess, handleError);
    }

    function getCategoriesBrands(code) {
        var request = $http.get(urlBase + 'GetCategoriesBrands', {
            params: {
                code: code
            }
        });
        return request.then(handleSuccess, handleError);
    }

    function getSeries(code) {
        var langId = localStorageService.get('langId') || 0;
        var request = $http.get(urlBase + 'GetSeries', {
            params: {
                code: code,
                langId: langId
            }
        });
        return request.then(handleSuccess, handleError);
    }

    function getCategories() {
        var langId = localStorageService.get('langId') || 0;
        var request = $http.get(urlBase + 'GetCategories', {
            params: {
                langId: langId
            }
        });
        return request.then(handleSuccess, handleError);
    }

    function getFaqs() {
        var langId = localStorageService.get('langId') || 0;
        var request = $http.get(urlBase + 'GetFaqs', {
            params: {
                langId: langId
            }
        });
        return request.then(handleSuccess, handleError);
    }

    function getLanguages() {
        var request = $http.get(urlBase + 'GetLanguages');
        return request.then(handleSuccess, handleError);
    }

    function receiveTotalPages(model) {
        var langId = localStorageService.get('langId') || 0;
        var sortBy = $rootScope.sortBy;
        model.langId = langId;
        model.sortBy = sortBy;
        var request = $http.post(urlBase + 'ReceiveTotalPages', model);
        return request.then(handleSuccess, handleError);
    }

    function getProductImage(o) {
        var request = $http.get(urlBase + 'GetProductImage', {
            params: {
                id: o.id,
                imageId: o.imageId
            }
        });
        return request.then(handleSuccess, handleError);
    }

    function getProduct(id) {
        var langId = localStorageService.get('langId') || 0;
        var request = $http.get(urlBase + 'GetProduct', {
            params: {
                id: id, 
                langId: langId
            }
        });
        return request.then(handleSuccess, handleError);
    }

    function search(model) {
        var sortBy = $rootScope.sortBy;
        var langId = localStorageService.get('langId') || 0;
        model.langId = langId;
        model.sortBy = sortBy;
        model = JSON.stringify(model);
        var request = $http.get(urlBase + 'Search', {
            params: {
                model: model
            }
        });
        return request.then(handleSuccess, handleError);
    }

    function handleError(response) {
        if (angular.isObject(response.data) && response.data.message) {
            return ($q.reject(response.data.message));
        }
        return ($q.reject('An unknown error occurred.'));
    }

    function handleSuccess(response) {
        return (response.data);
    }
}

angular
    .module('homer')
    .service('catalogService', catalogService);