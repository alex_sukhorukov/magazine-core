﻿/**
 * HOMER - Responsive Admin Theme
 * version 1.8
 *
 */


angular
    .module('homer')
    .config(configState)
    .run(function ($rootScope, $state, editableOptions, $templateCache, localStorageService, translationService, $http) {
        $rootScope.$state = $state;
        $rootScope.appTitle = appTitle;
        editableOptions.theme = 'bs3';

        var langId = localStorageService.get('langId') || 0;

        if (typeof langId !== 'number') {
            langId = 0;
            localStorageService.set('langId', langId);
        }

        $rootScope.langId = langId;

        $rootScope.translate = function (code) {
            return translationService.translate(code);
        }

        window['_translate'] = localStorageService.get('_translate') || {};

        translationService.get(langId,
            function (values) {
                for (var i = 0; i < values.length; i++) {
                    _translate[values[i].Code] = values[i].Text;
                }
                localStorageService.set('_translate', _translate);
                var templates = [
                    "views/store/index.html",
                    "views/store/catalog.html",
                    "views/store/cabinet.html",
                    "views/store/about.html",
                    "views/store/delivery.html",
                    "views/store/product.html",
                    "views/store/contacts.html",
                    "views/store/faq.html",
                    "views/store/feedback.html",
                    "views/store/login.html",
                    "views/store/register.html",
                    "views/store/error_one.html",
                    "views/store/error_two.html",
                    "views/store/password_recovery.html",
                    "views/store/recovery.html",
                    "views/store/cart.html",
                    "views/store/order.html",
                    "views/store/search.html"
                ]; 
                $http.defaults.headers.common = { 'Cache-Control': 'public, max-age=3600' };
                var url;
                for (i in templates) {
                    url = templates[i];
                    $http.get(url, {
                        cache: $templateCache
                    });
                }
            },
            function (response) {
                debugger;
            });

        var w = getBodyWidth();
        $rootScope.small = getBodySmall(w);
        $rootScope.medium = getBodyMedium(w);
    });

angular.module('homer')
    .factory('timeoutHttpIntercept', function ($rootScope, $q) {
        return {
            'request': function (config) {
                config.timeout = 300000;
                return config;
            }
        };
    });

function configState($stateProvider, $urlRouterProvider, $compileProvider, $httpProvider, $locationProvider) {
    // Optimize load start with remove binding information inside the DOM element
    $compileProvider.debugInfoEnabled(false);
    $locationProvider.hashPrefix('');
    $locationProvider.html5Mode({
        enabled: false,
        requireBase: false
    });

    $httpProvider.interceptors.push('timeoutHttpIntercept');

    $stateProvider
        .state('index', {
            url: "/index",
            templateUrl: "views/store/index.html",
            controller: 'indexCtrl',
            data: {
                code: 'Index'
            }
        })
        .state('catalog', {
            url: "/catalog/:code",
            templateUrl: "views/store/catalog.html",
            controller: 'catalogCtrl',
            data: {
                code: 'Catalog'
            }
        })
        .state('catalog_series', {
            url: "/catalog/:code/series/:series",
            templateUrl: "views/store/catalog.html",
            controller: 'catalogCtrl',
            data: {
                code: 'Catalog'
            }
        })
        .state('series', {
            url: "/catalog/:code/series",
            templateUrl: "views/store/series.html",
            controller: 'seriesCtrl',
            data: {
                code: 'Seriess'
            }
        })
        .state('cabinet', {
            url: "/cabinet/:tab?",
            templateUrl: "views/store/cabinet.html",
            controller: 'cabinetCtrl',
            data: {
                code: 'Cabinet'
            }
        })
        .state('about', {
            url: "/about",
            templateUrl: "views/store/about.html",
            controller: 'emptyCtrl',
            data: {
                code: 'About'
            }
        })
        .state('delivery', {
            url: "/delivery",
            templateUrl: "views/store/delivery.html",
            controller: 'emptyCtrl',
            data: {
                code: 'DeliveryAndPayments'
            }
        })
        .state('terms', {
            url: "/terms",
            templateUrl: "views/store/terms.html",
            controller: 'emptyCtrl',
            data: {
                code: 'Terms'
            }
        })
        .state('product', {
            url: "/product/:id",
            templateUrl: "views/store/product.html",
            controller: 'productCtrl',
            data: {}
        })
        .state('contacts', {
            url: "/contacts",
            templateUrl: "views/store/contacts.html",
            controller: 'contactsCtrl',
            data: {
                code: 'Contacts'
            }
        })
        .state('faq', {
            url: "/faq",
            templateUrl: "views/store/faq.html",
            controller: 'faqCtrl',
            data: {
                code: 'FAQ'
            }
        })
        .state('feedback', {
            url: "/feedback",
            templateUrl: "views/store/feedback.html",
            controller: 'feedbackCtrl',
            data: {
                code: 'Feedback'
            }
        })
        .state('confirmemail', {
            url: "/confirmemail/:id",
            controller: 'loginCtrl',
            templateUrl: "views/store/login.html",
            data: {
                code: 'Login',
                specialClass: 'blank'
            }
        })
        .state('confirmchangeemail', {
            url: "/confirmchangeemail/:id",
            controller: 'cabinetCtrl',
            templateUrl: "views/store/cabinet.html",
            data: {
                code: 'Cabinet'
            }
        })
        .state('login', {
            url: "/login",
            controller: 'loginCtrl',
            templateUrl: "views/store/login.html",
            data: {
                code: 'Login',
                specialClass: 'blank'
            }
        })
        .state('register', {
            url: "/register",
            controller: 'loginCtrl',
            templateUrl: "views/store/register.html",
            data: {
                code: 'Register',
                specialClass: 'blank'
            }
        })
        // TODO: implement error pages (404, 500)
        // TODO: implement 401 error page
        .state('error404', {
            url: "/error404",
            templateUrl: "views/store/error_one.html",
            controller: 'emptyCtrl',
            data: {
                code: 'Error 404',
                specialClass: 'blank'
            }
        })
        .state('error500', {
            url: "/error500",
            templateUrl: "views/store/error_two.html",
            controller: 'emptyCtrl',
            data: {
                code: 'Error 500',
                specialClass: 'blank'
            }
        })
        .state('password_recovery', {
            url: "/password_recovery",
            controller: 'loginCtrl',
            templateUrl: "views/store/password_recovery.html",
            data: {
                code: 'Recovery',
                specialClass: 'blank'
            }
        })
        .state('recovery', {
            url: "/recovery/:id",
            controller: 'loginCtrl',
            templateUrl: "views/store/recovery.html",
            data: {
                code: 'Recovery',
                specialClass: 'blank'
            }
        })
        .state('order', {
            url: "/order",
            controller: 'cartCtrl',
            templateUrl: "views/store/order.html",
            data: {
                code: 'OrderPlacement'
            }
        })
        .state('cart', {
            url: "/cart",
            templateUrl: "views/store/cart.html",
            controller: 'cartCtrl',
            data: {
                code: 'Cart'
            }
        })
        .state('search', {
            url: "/search",
            templateUrl: "views/store/search.html",
            controller: 'searchCtrl',
            data: {
                code: 'Search'
            }
        });

    $urlRouterProvider.otherwise(function ($injector, $location) {
        var state = $injector.get('$state');
        var $rootScope = $injector.get("$rootScope");
        if (location.hash.indexOf("access_token=") !== -1 || location.hash === "#/error=access_denied") {
            state.go("login", $location.search());
        } else if (!state.current.name) {
            debugger;
            state.go('index');
        }
        return $location.path();
    });
}