/**
 * HOMER - Responsive Admin Theme
 * version 1.8
 *
 */

window['apiUrl'] = location.origin;
window['appTitle'] = 'Igruhi';
window.debugMode = false;

function updateTitle($state, v) {
    if (typeof v === "string") {
        var title = v;
    } else {
        title = v.translate($state.current.data.code);
    }
    $state.current.data.pageTitle = title;
    document.title = window.appTitle + ' | ' + title;
}

(function () {
    angular.module('homer', [
        'ui.router',                // Angular flexible routing
        'ngSanitize',               // Angular-sanitize
        'ui.bootstrap',             // AngularJS native directives for Bootstrap
        'cgNotify',                 // Angular notify
        'ngAnimate',                // Angular animations
        'ui.map',                   // Ui Map for Google maps
        // 'summernote',               // Summernote plugin
        'xeditable',                // Angular-xeditable
        'ui.select',                // AngularJS ui-select
        'LocalStorageModule',
        'vcRecaptcha',
        'angularFileUpload',
        //'ui.grid',
        //'ui.grid.selection',
        //'ui.grid.cellNav',
        //'ui.grid.pinning',
        //'ui.grid.autoResize',
        //'ui.grid.resizeColumns'
    ])
})();