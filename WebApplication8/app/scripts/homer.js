/**
 * HOMER - Responsive Admin Theme
 * version 1.8
 *
 */

function onscrollx(force) {
    var fn = function () {
        if ($('body').hasClass('page-small') && !$('body').hasClass('show-sidebar')) {
            var top = $(window).scrollTop();
            if (top <= 62) {
                top = 62;
            }
            var navigationH = $('#navigation').outerHeight(true);
            var wrapperH = $('#wrapper').outerHeight(true);
            if (top-62 > (wrapperH - navigationH)) {
                top = (wrapperH - navigationH) + 62;
            }
        } else {
            top = 62;
        }
        $('#menu').css({ top: top });
    };
    var prevTimer = $('body').data('menutimer');
    if (prevTimer) {
        clearTimeout(prevTimer);
        prevTimer = 0;
    }
    var timer = setTimeout(fn, 50);
    $('body').data('menutimer', timer);
};

$(document).ready(function () {

    var m = (new Date()).getMonth() + 1;
    $('body').css({
        background: "url('../images/calendar/" + m.toString() + ".jpg')"
    });

    // Set minimal height of #wrapper to fit the window
    $(window).on('resize', function () {
        fixWrapperHeight();
    });

    // Add special class to minimalize page elements when screen is less than 768px
    setTimeout(function () {
        $(window).trigger('resize');
    }, 10);

    if ($(window).width() < 769) {
        $('body').addClass('page-small');
    } else {
        $('body').removeClass('page-small');
    }
    $('.splash').css('display', 'none');
    window.addEventListener("touchstart", onscrollx, false);
    window.addEventListener("touchend", onscrollx, false);
    window.addEventListener("touchcancel", onscrollx, false);
    window.addEventListener("touchmove", onscrollx, false);
    if (isMobile.any()) {
        $(window).on('scroll', onscrollx);
    }

    $(window).on("orientationchange", function () {
        $('body').data('timer_count', 0);
        clearInterval($('body').data('timer'));
        var timerId = setInterval(function () {
            setTimeout(function () {
                var tc = $('body').data('timer_count');
                $('body').data('timer_count', tc + 1);
                $(window).trigger('resize');
                onscrollx();
                if (tc > 5) {
                    $('body').data('timer_count', 0);
                    clearInterval($('body').data('timer'));
                }
            }, 100);
        }, 150);
        $('body').data('timer', timerId);
    });
});

function fixWrapperHeight() {
    var headerH = 62;
    var footerHeight = Math.floor($('.footer').height());
    var windowH = Math.floor($(window).height()) - headerH - footerHeight - 2;
    var navHeight = Math.floor($("#navigation").height());
    var h = (Math.max(windowH, navHeight)) + 'px';
    if (h < 480) {
        h = 480;
    }
    if ($("#wrapper").css("min-height") !== h) {
        $("#wrapper").css("min-height", h);
    };
}

function getBodySmall(w) {
    return w < 769;
}

function getBodyMedium(w) {
    return w < 992;
}

function getBodyWidth() {
    return $(window).width();
}

function swipedetect(el, events, callback, handleclick) {

    var touchsurface = el,
        swipedir,
        startX,
        startY,
        distX,
        distY,
        threshold = 5, //required min distance traveled to be considered swipe
        restraint = 150, // maximum distance allowed at the same time in perpendicular direction
        allowedTime = 1500, // maximum time allowed to travel that distance
        elapsedTime,
        startTime,
        handleswipe = callback || function (swipedir) { };

    var fnDown = function (touchobj, e) {
        swipedir = 'none';
        dist = 0;
        startX = touchobj.pageX;
        startY = touchobj.pageY;
        startTime = new Date().getTime(); // record time when finger first makes contact with surface
        e.preventDefault();
    }

    var fnUp = function (touchobj, e) {
        distX = touchobj.pageX - startX; // get horizontal dist traveled by finger while in contact with surface
        distY = touchobj.pageY - startY; // get vertical dist traveled by finger while in contact with surface
        elapsedTime = new Date().getTime() - startTime; // get time elapsed
        if (elapsedTime <= allowedTime) { // first condition for awipe met
            if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint) { // 2nd condition for horizontal swipe met
                swipedir = (distX < 0) ? 'left' : 'right'; // if dist traveled is negative, it indicates left swipe
            }
            else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint) { // 2nd condition for vertical swipe met
                swipedir = (distY < 0) ? 'up' : 'down'; // if dist traveled is negative, it indicates up swipe
            }
        }
        if (events.indexOf(swipedir) !== -1) {
            handleswipe(swipedir, e);
            e.preventDefault();
        } else if (swipedir === 'none') {
            handleclick(e);
        }
    }

    if (!isMobile.any()) {
        touchsurface.addEventListener('mousedown', function (e) {
            var touchobj = e;
            fnDown(touchobj, e);
        }, false);
    } else {
        touchsurface.addEventListener('touchstart', function (e) {
            var touchobj = e.changedTouches[0];
            fnDown(touchobj, e);
        }, false);
    }

    touchsurface.addEventListener('touchmove', function (e) {
        e.preventDefault(); // prevent scrolling when inside DIV
    }, false);

    if (!isMobile.any()) {
        touchsurface.addEventListener('mouseup', function (e) {
            var touchobj = e;
            fnUp(touchobj, e);
        }, false);
        touchsurface.addEventListener('click', function (e) {
            if (Math.abs(distX) > threshold || Math.abs(distY) > threshold) {
                e.stopPropagation();
                e.preventDefault();
            }
        }, false);
    } else {
        touchsurface.addEventListener('touchend', function (e) {
            var touchobj = e.changedTouches[0];
            fnUp(touchobj, e);
        }, false);
    }
}