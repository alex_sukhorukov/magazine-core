/**
 * HOMER - Responsive Admin Theme
 * Copyright 2015 Webapplayers.com
 *
 */

var rainbow = new Rainbow();
rainbow.setSpectrum('lightgreen', 'turquoise', 'skyblue', 'paleturquoise', 'khaki', 'navajowhite', 'gold', 'lightsalmon', 'lightpink');
rainbow.setNumberRange(1, 100);

angular
    .module('homer')
    .directive('sideNavigation', sideNavigation)
    .directive('minimalizaMenu', minimalizaMenu)
    .directive('sparkline', sparkline)
    .directive('icheck', icheck)
    .directive('inputMask', inputMask)
    .directive('autofocus', autofocus)
    .directive('panelTools', panelTools)
    .directive('panelCloseTools', panelCloseTools)
    .directive('panelToolsFullscreen', panelToolsFullscreen)
    .directive('smallHeader', smallHeader)
    .directive('animatePanel', animatePanel)
    .directive('landingScrollspy', landingScrollspy)
    .directive('clockPicker', clockPicker)
    .directive('onLoadImage', onLoadImage)
    .directive('dateTimePicker', dateTimePicker)
    .directive('datePicker', datePicker)
    .directive('summernoteair', summernoteair)
    .directive('laddabtn', laddabtn)
    .directive('backgroundColorFun', backgroundColorFun)
    .directive('onRenderAnimate', onRenderAnimate)
    .directive('sliderRange', sliderRange)
    .directive('wordsCloud', wordsCloud);

function wordsCloud() {
    return {
        restrict: 'A',
        scope: {
            words: '='
        },
        link: function (scope, element) {
            var el = $(element[0]);

            var words = [
              { text: "Lorem", weight: 13, link: 'http://a.com' },
              { text: "Ipsum", weight: 10.5, link: 'http://a.com' },
              { text: "Dolor", weight: 9.4, link: 'http://a.com' },
              { text: "Sit", weight: 8, link: 'http://a.com' },
              { text: "Amet", weight: 6.2, link: 'http://a.com' },
              { text: "Consectetur", weight: 5, link: 'http://a.com' },
              { text: "Adipiscing", weight: 5, link: 'http://a.com' },
              { text: "zdipiscing", weight: 3, link: 'http://a.com' }
            ];

            el.jQCloud(words, {
                autoResize: true, fontSize: {
                    to: 0.025,
                    from: 0.22
                }
            });
        }
    };

    
}

function sliderRange() {
    return {
        restrict: 'A',
        replace: true,
        transclude: true,
        scope: {
            tooltip: '@',
            val: '=',
            min: '=',
            max: '=',
            step: '=',
            changeFn: '='
        },
        link: function (scope, element) {
            var el = $(element[0]);
            var tooltip = scope.tooltip || 'hide';
            var sliderEl = el.bootstrapSlider({
                min: scope.min,
                step: scope.step,
                max: scope.max,
                value: scope.val,
                tooltip: tooltip,
                scale: 'logarithmic'
            });
            sliderEl.on('change', function (event) {
                if (scope.changeFn) {
                    scope.changeFn(event);
                }
            });
            el.data('sliderEl', sliderEl);
        }
    }
}

function onRenderAnimate() {
    return {
        restrict: 'A',
        scope: {
            animateIndex: '=',
            animateEffect: '@'
        },
        link: function (scope, element) {
            var el = $(element[0]);
            el.hide();
            setTimeout(function () {
                el.addClass(scope.animateEffect);
                el.show();
            }, scope.animateIndex * 50);
        }
    }
}

function inputMask() {
    return {
        restrict: 'A',
        scope: {
            eMask: '='
        },
        link: function (scope, element) {
            var el = $(element[0]);
            el.inputmask({ "mask": scope.eMask });
        }
    }
}

function backgroundColorFun() {
    return {
        restrict: 'A',
        scope: {
            backgroundColorFunIndex: '=',
        },
        link: function (scope, element) {
            var el = $(element[0]);
            var index = Math.floor(Math.random() * 100);
            var styles = {
                // backgroundColor: '#' + rainbow.colourAt(index)
            };
            el.css(styles);
            el.parent().css({
                // border: '2px solid ' + '#' + rainbow.colourAt(index),
                border: '2px solid rgb(250, 225, 163)',
                borderRadius: '4px'
            });
        }
    }
}

function autofocus($timeout) {
    return {
        restrict: 'A',
        scope: {
        },
        link: function (scope, element, attr) {
            var el = $(element[0]);
            $timeout(function () {
                el.focus();
            });
        }
    };
};

function onLoadImage() {
    return {
        restrict: 'A',
        scope: {
            onLoadFn: '=',
            onLoadItem: '='
        },
        link: function (scope, element, attr) {
            var el = $(element[0]);
            el.on('load', function () {
                var args = arguments;
                var el = $(this);
                scope.onLoadFn(scope.onLoadItem, 'p');
            });
        }
    };
};

// sideNavigation - Directive for run metsiMenu on sidebar navigation
function sideNavigation($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element) {
            // Call the metsiMenu plugin and plug it to sidebar navigation
            element.metisMenu();

            // Colapse menu in mobile mode after click on element
            var menuElement = $('#side-menu a:not([href$="\\#"])');
            menuElement.click(function () {
                if ($(window).width() < 769) {
                    $("body").toggleClass("show-sidebar");
                }
            });


        }
    };
};

// minimalizaSidebar - Directive for minimalize sidebar
function minimalizaMenu($rootScope, $timeout, $state) {
    return {
        restrict: 'EA',
        template: '<div class="header-link hide-menu" ng-click="minimalize($event)"><i class="fa fa-bars"></i></div>',
        controller: function ($scope, $element) {
            if ($(window).width() < 769) {
                $rootScope.bodysmall = true;
            } else {
                $rootScope.bodysmall = false;
            }

            var rz = function () {
                var w = getBodyWidth();
                $rootScope.small = getBodySmall(w);
                $rootScope.medium = getBodyMedium(w);
                if ($rootScope.small) {
                    if ($rootScope.showSidebar) {
                        $rootScope.bodysmall = false;
                    } else {
                        $rootScope.bodysmall = true;
                    }
                    $rootScope.hideSidebar = false;
                } else {
                    $rootScope.bodysmall = false;
                    $rootScope.showSidebar = false;
                }
            }

            $(window).on('resize', function () {
                $timeout(function () {
                    rz();
                }, 10);
            });

            $scope.minimalize = function (e) {
                if ($(window).width() < 769) {
                    $rootScope.showSidebar = !$rootScope.showSidebar;
                } else {
                    $rootScope.hideSidebar = !$rootScope.hideSidebar;
                }
                rz();
                $timeout(function () {
                    var ct = 5;
                    clearInterval($scope.wrapperInterval);
                    $scope.wrapperInterval = setInterval(function () {
                        if (--ct > 0) {
                            $(window).trigger('resize');
                        } else {
                            clearInterval($scope.wrapperInterval);
                            $scope.wrapperInterval = 0;
                        }
                    }, 10);
                }, 50);
                e.stopPropagation();
                e.preventDefault();
            }

            $('body').on('click', '#side-menu', function (e) {
                if ($rootScope.showSidebar) {
                    $timeout(function () {
                        $scope.minimalize(e);
                    });
                }
            });
        }
    };
};

// sparkline - Directive for Sparkline chart
function sparkline() {
    return {
        restrict: 'A',
        scope: {
            sparkData: '=',
            sparkOptions: '=',
        },
        link: function (scope, element, attrs) {
            scope.$watch(scope.sparkData, function () {
                render();
            });
            scope.$watch(scope.sparkOptions, function(){
                render();
            });
            var render = function () {
                $(element).sparkline(scope.sparkData, scope.sparkOptions);
            };
        }
    }
};

// icheck - Directive for custom checkbox icheck
function icheck($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function($scope, element, $attrs, ngModel) {
            return $timeout(function() {
                var value;
                value = $attrs['value'];

                $scope.$watch($attrs['ngModel'], function(newValue){
                    $(element).iCheck('update');
                })

                return $(element).iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green'

                }).on('ifChanged', function(event) {
                        if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                            $scope.$apply(function() {
                                return ngModel.$setViewValue(event.target.checked);
                            });
                        }
                        if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                            return $scope.$apply(function() {
                                return ngModel.$setViewValue(value);
                            });
                        }
                    });
            });
        }
    };
}


// panelTools - Directive for panel tools elements in right corner of panel
function panelTools($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/panel_tools.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            var hpanel = $element.closest('div.hpanel');
            $scope.showhide = function (e) {
                e.stopPropagation();
                e.preventDefault();
                var icon = $element.find('i:first');
                var body = hpanel.find('div.panel-body');
                var footer = hpanel.find('div.panel-footer');
                body.slideToggle(300);
                footer.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                hpanel.toggleClass('').toggleClass('panel-collapse');
                $timeout(function () {
                    hpanel.resize();
                    hpanel.find('[id^=map-]').resize();
                }, 50);
            }
            var heading = hpanel.find('div.panel-heading');
            heading.on('click', $scope.showhide);
        }
    };
};

// panelTools - Directive for panel tools elements in right corner of panel
function panelCloseTools($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/panel_tools.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            // Function for close ibox
            $scope.closebox = function () {
                var hpanel = $element.closest('div.hpanel');
                hpanel.remove();
            }
        }
    };
};

// panelToolsFullscreen - Directive for panel tools elements in right corner of panel with fullscreen option
function panelToolsFullscreen($timeout) {
    return {
        restrict: 'A',
        scope: true,
        templateUrl: 'views/common/panel_tools_fullscreen.html',
        controller: function ($scope, $element) {
            // Function for collapse ibox
            $scope.showhide = function () {
                var hpanel = $element.closest('div.hpanel');
                var icon = $element.find('i:first');
                var body = hpanel.find('div.panel-body');
                var footer = hpanel.find('div.panel-footer');
                body.slideToggle(300);
                footer.slideToggle(200);
                // Toggle icon from up to down
                icon.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
                hpanel.toggleClass('').toggleClass('panel-collapse');
                $timeout(function () {
                    hpanel.resize();
                    hpanel.find('[id^=map-]').resize();
                }, 50);
            };

            // Function for close ibox
            $scope.closebox = function () {
                var hpanel = $element.closest('div.hpanel');
                hpanel.remove();
                if($('body').hasClass('fullscreen-panel-mode')) { $('body').removeClass('fullscreen-panel-mode');}
            };

            // Function for fullscreen
            $scope.fullscreen = function () {
                var hpanel = $element.closest('div.hpanel');
                var icon = $element.find('i:first');
                $('body').toggleClass('fullscreen-panel-mode');
                icon.toggleClass('fa-expand').toggleClass('fa-compress');
                hpanel.toggleClass('fullscreen');
                setTimeout(function() {
                    $(window).trigger('resize');
                }, 100);
            }

        }
    };
};

// smallHeader - Directive for page title panel
function smallHeader() {
    return {
        restrict: 'A',
        scope:true,
        controller: function ($scope, $element) {
            $scope.small = function() {
                var icon = $element.find('i:first');
                var breadcrumb  = $element.find('#hbreadcrumb');
                $element.toggleClass('small-header');
                breadcrumb.toggleClass('m-t-lg');
                icon.toggleClass('fa-arrow-up').toggleClass('fa-arrow-down');
            }
        }
    }
}

function animatePanel($timeout,$state) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {

            //Set defaul values for start animation and delay
            var startAnimation = 0;
            var delay = 0.06;   // secunds
            var start = Math.abs(delay) + startAnimation;

            // Store current state where directive was start
            var currentState = $state.current.name;

            // Set default values for attrs
            if(!attrs.effect) { attrs.effect = 'zoomIn'};
            if(attrs.delay) { delay = attrs.delay / 10 } else { delay = 0.06 };
            if(!attrs.child) { attrs.child = '.row > div'} else {attrs.child = "." + attrs.child};

            // Get all visible element and set opactiy to 0
            var panel = element.find(attrs.child);
            panel.addClass('opacity-0');

            // Count render time
            var renderTime = panel.length * delay * 1000 + 700;

            // Wrap to $timeout to execute after ng-repeat
            $timeout(function(){

                // Get all elements and add effect class
                panel = element.find(attrs.child);
                panel.addClass('stagger').addClass('animated-panel').addClass(attrs.effect);

                var panelsCount = panel.length + 10;
                var animateTime = (panelsCount * delay * 10000) / 10;

                // Add delay for each child elements
                panel.each(function (i, elm) {
                    start += delay;
                    var rounded = Math.round(start * 10) / 10;
                    $(elm).css('animation-delay', rounded + 's');
                    // Remove opacity 0 after finish
                    $(elm).removeClass('opacity-0');
                });

                // Clear animation after finish
                $timeout(function(){
                    $('.stagger').css('animation', '');
                    $('.stagger').removeClass(attrs.effect).removeClass('animated-panel').removeClass('stagger');
                    panel.resize();
                }, animateTime)

            });



        }
    }
}

function landingScrollspy(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            element.scrollspy({
                target: '.navbar-fixed-top',
                offset: 80
            });
        }
    }
}

// clockPicker - Directive for clock picker plugin
function clockPicker() {
    return {
        restrict: 'A',
        link: function(scope, element) {
            element.clockpicker();
        }
    };
};

function summernoteair() {
    return {
        restrict: 'A',
        scope: {
            value: '='
        },
        link: function (scope, element, attr) {
            $(element).summernote({
                onChange: function () {
                    scope.$apply();
                },
                airMode: JSON.parse(attr.mode)
            });
            $(element).summernote().code(scope.value);
        }
    };
};

function laddabtn() {
    return {
        restrict: 'C',
        link: function (scope, element, attr) {
            $(element[0]).ladda();
        }
    };
};

function dateTimePicker(){
    return {
        require: '?ngModel',
        restrict: 'AE',
        scope: {
            pick12HourFormat: '@',
            language: '@',
            useCurrent: '@',
            location: '@'
        },
        link: function (scope, elem, attrs) {
            elem.datetimepicker({
                pick12HourFormat: scope.pick12HourFormat,
                locale: scope.language,
                useCurrent: scope.useCurrent
            });
        }
    };
}

function datePicker($rootScope) {
    return {
        require: '?ngModel',
        restrict: 'AE',
        scope: {
            language: '@',
            location: '@'
        },
        link: function (scope, elem, attrs) {
            var el = $(elem[0]);
            el.datetimepicker({
                format: 'DD.MM.YYYY',
                viewMode: 'days',
                inline: attrs.inline ? true : false,
                locale: scope.language
            });
            el.on("dp.change", function (e) {
                var d = el.data("DateTimePicker").date();
                if (d !== null) {
                    scope.dateTime = new Date(d.format());
                } else {
                    scope.dateTime = null;
                }
                $rootScope.$broadcast("emit:dateTimePicker", {
                    action: 'changed',
                    dateTime: scope.dateTime,
                    element: el.attr('id')
                });
                scope.$apply();
            });
        }
    };
}