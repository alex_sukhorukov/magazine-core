﻿(function () {
    'use strict';
    angular.module('homer')
        .directive('xdatatable', function() {
            return {
                restrict: 'A',
                link: function (scope, element) {
                    var el = $(element[0]);
                    el.ready(function () {
                        el.dataTable({
                            "language": {
                                "url": "../../data/datatable/" + scope.langId + ".json"
                            },
                            "ordering": false,
                            "info": false
                        });
                    });
                }
            };
        });
})();