(function() {
    'use strict';
    angular.module('homer')
        .directive("pol", ['$timeout', 'translationService',
            function ($timeout, translationService) {
                return {
                    restrict: 'AE',
                    templateUrl: 'scripts/directives/pol/pol.html',
                    replace: true,
                    transclude: true,
                    scope: {
                        v: '=',
                        onChange: '='
                    },
                    link: function(scope, elements, attr) {
                        var el = $(elements[0]);
                        scope.c1 = el.find('input[value=1]');
                        scope.c2 = el.find('input[value=2]');
                        scope.click = function () {
                            $timeout(function () {
                                var r = 0;
                                var items = el.find('input[name=pol]:checked');
                                $.each(items, function (i, it) {
                                    if (i == 0) {
                                        r = parseInt(it.value);
                                    } else {
                                        r = (r | parseInt(it.value));
                                    }
                                });
                                scope.onChange(r);
                            });
                        }
                        scope.translate = function (code) {
                            return translationService.translate(code);
                        }
                        scope.setValue = function (v) {
                            if (v === undefined || v === null) {
                                v = '';
                            }
                            var c1v = scope.c1v = (parseInt(v) & 1) == 1;
                            scope.c1.prop('checked', c1v);
                            var c2v = scope.c2v = (parseInt(v) & 2) == 2;
                            scope.c2.prop('checked', c2v);
                        }
                        scope.$watch('v', function (v) {
                            scope.setValue(v);
                        });
                    }
                }
            }
        ]);
})();