﻿(function () {
    'use strict';
    angular.module('homer')
        .directive('calcLeftMargin', function() {
            return {
                restrict: 'A',
                link: function (scope, element) {
                    var el = $(element[0]);
                    el.ready(function () {
                        var w = el.width();
                        if (w !== 0) {
                            el.css({
                                'margin-left': -Math.round(w / 2) + 'px'
                            });
                        } else {
                            el.data('interval', setInterval(function () {
                                w = el.width();
                                if (w !== 0) {
                                    clearInterval(el.data('interval'));
                                    el.css({
                                        'margin-left': -Math.round(w / 2) + 'px'
                                    });
                                }
                                scope.$apply();
                            }), 250);
                        }
                    });
                }
            };
        });
})();