(function() {
    'use strict';
    angular.module('homer')
        .directive("searchPanel", ['$timeout', '$rootScope', 'translationService', '$compile', 'catalogService',
            function ($timeout, $rootScope, translationService, $compile, catalogService) {
                return {
                    restrict: 'AE',
                    templateUrl: 'scripts/directives/search/search.html',
                    replace: true,
                    transclude: true,
                    scope: {
                        isSmall: '=',
                        isCollapsed: '=',
                        searchCfg: '=',
                        applySearch: '=',
                        clearSearch: '=',
                        onRefresh: '=',
                        code: '='
                    },
                    link: function(scope, elements, attr) {
                        var el = $(elements[0]);
                        var h = 300;
                        var filtersEl = el.find('.filters');
                        var min = 0, max = 0, categoryIds = [], brandIds = [], seriesIds = [];
                        scope.small = scope.isSmall === true;

                        if (($rootScope.small || scope.bodysmall) && scope.isSmall !== true) {
                            scope.filterVisible = false;
                        } else {
                            scope.filterVisible = scope.isCollapsed === undefined ? true : (scope.isCollapsed ? false : true);
                        }

                        scope.categoryCheck = function (c) {
                            scope.filtersChanged();
                        }

                        scope.clearText = function (cb) {
                            scope.text = '';
                            scope.textChanged(function () {
                                if ($rootScope.currentState != 'index') {
                                    if (!scope.small) {
                                        $timeout(function () {
                                            scope.applySearch();
                                        });
                                    }
                                }
                            });
                        }

                        scope.brandCheck = function (b) {
                            if (!b.checked) {
                                $.each(b.Series, function (i, t) {
                                    t.checked = false;
                                });
                            }
                            scope.filtersChanged();
                        };

                        scope.brandSeriesCheck = function (bs) {
                            scope.filtersChanged();
                        };

                        scope.ifDifferent = function (a, b) {
                            if (!a || !b) {
                                return true;
                            }
                            if (a.length !== b.length) {
                                return true;
                            }
                            for (var i = 0; i < a.length; i++) {
                                if (a[i] !== b[i]) {
                                    return true;
                                }
                            }
                            return false;
                        };

                        scope.onChangeCode = function (code) {
                            catalogService.getSearchCategories(code || '')
                                .then(function (categories) {
                                    if (scope.ifDifferent(scope.searchCategories, categories)) {
                                        scope.searchCategories = categories;
                                        $.each(scope.searchCategories, function (i, r) {
                                            $.each(categoryIds, function (j, t) {
                                                if (r.Id === t) {
                                                    r.ichecked = true;
                                                }
                                            });
                                        });
                                    }
                                    catalogService.getCategoriesBrands(code || '')
                                        .then(function (brands) {
                                            if (scope.ifDifferent(scope.searchBrands, brands)) {
                                                if (seriesIds && $rootScope.cseries && seriesIds.indexOf($rootScope.cseries) == -1) {
                                                    seriesIds = [];
                                                    seriesIds.push($rootScope.cseries);
                                                }
                                                scope.searchBrands = brands;
                                                $.each(scope.searchBrands, function (i, q) {
                                                    q.ichecked = false;
                                                    $.each(brandIds, function (j, w) {
                                                        if (q.Id == w) {
                                                            q.ichecked = true;
                                                            return false;
                                                        }
                                                    });
                                                    $.each(q.Series, function (z, s) {
                                                        s.ichecked = false;
                                                        $.each(seriesIds, function (x, m) {
                                                            if (s.Code == m) {
                                                                s.ichecked = true;
                                                                return false;
                                                            }
                                                        });
                                                    });
                                                });
                                            }
                                            $timeout(function () {
                                                scope.filtersChanged();
                                            });
                                        }, function (err) {
                                            debugger;
                                        });
                                }, function (err) {
                                    debugger;
                                });
                        }

                        scope.applyCustom = function () {
                            $rootScope.$broadcast('applyCustom');
                        }

                        scope.keyPress = function (e) {
                            if (e.keyCode == 13) {
                                scope.textChanged(function () {
                                    if (!scope.small) {
                                        $timeout(function () {
                                            scope.applySearch();
                                        });
                                    }
                                });
                            }
                        }

                        scope.$watch('code', function (n, o) {
                            scope.onChangeCode(n);
                        });

                        $rootScope.$watch('cseries', function (n, o) {
                            if (!n) {
                                var searchCfg = scope.searchCfg;
                                searchCfg = { text: '' };
                                $rootScope.latestSearch = $rootScope.latestSearch || undefined;
                                $rootScope._searchCfg = $rootScope._searchCfg || undefined;
                                if ($rootScope._searchCfg) {
                                    if ($rootScope.latestSearch) {
                                        $rootScope.latestSearch.seriesIds = [];
                                    }
                                    $rootScope._searchCfg.seriesIds = [];
                                }
                                seriesIds = [];
                                brandIds = [];
                                categoryIds = [];
                            }
                            scope.onChangeCode($rootScope.code);
                        });

                        scope.onChangePol = function (p) {
                            scope.pol = p;
                            scope.filtersChanged();
                        }

                        scope.onChangeAge = function (v) {
                            scope.age = v;
                            scope.filtersChanged();
                        }

                        scope.advancedSearch = function () {
                            scope.filterVisible = !scope.filterVisible;
                            $timeout(function () {
                                $(window).trigger('resize');
                            }, 50);
                        }

                        scope.getUrl = function (url) {
                            $rootScope._t = $rootScope._t || 1;
                            return url + '&_t=' + $rootScope._t;
                        }

                        scope.translate = function (code) {
                            return translationService.translate(code);
                        }

                        scope.id = function (id) {
                            return (scope.small ? '_' : '') + id.replace(/-/g, '_');
                        }

                        scope.textChanged = function (cb) {
                            clearTimeout(scope.textTimer);
                            scope.textTimer = setTimeout(function () {
                                scope.filtersChanged();
                                scope.$apply();
                                if (cb) {
                                    cb();
                                }
                            }, 500);
                        }

                        scope.clearFilters = function () {
                            scope.text = '';
                            scope.age = 0;
                            scope.pol = 0;
                            scope.number = '';
                            scope.exists = false;
                            scope.onlyWithComments = false;
                            scope.min = 0;
                            scope.max = 100000;
                            scope.priceRangeArray = [scope.min, scope.max];
                            scope.priceRange = scope.min + "," + scope.max;
                            var priceMinEl = el.find('.price-range-min');
                            var priceMaxEl = el.find('.price-range-max');
                            priceMinEl.val(scope.min);
                            priceMaxEl.val(scope.max);
                            var slider = el.find('.price-range');
                            var sliderEl = slider.data('sliderEl');
                            sliderEl.bootstrapSlider('setValue', [scope.min, scope.max]);
                            if (!scope.small) {
                                $.each(scope.searchCategories, function (i, c) {
                                    c.checked = false;
                                });
                            }
                            $.each(scope.searchBrands, function (i, b) {
                                if (!scope.small) {
                                    b.checked = false;
                                }
                                $.each(b.Series, function (j, s) {
                                    s.checked = false;
                                });
                            });
                            scope.filtersChanged();
                            $timeout(function () {
                                if (scope.clearSearch) {
                                    scope.clearSearch();
                                }
                            });
                        }

                        scope.setValues = function (cfg, skipApply) {
                            scope.text = cfg.text;
                            scope.age = cfg.age;
                            scope.pol = cfg.pol;
                            scope.number = cfg.productCode;
                            scope.exists = cfg.exists;
                            scope.onlyWithComments = cfg.onlyWithComments;
                            if (cfg.priceRange) {
                                min = parseInt(cfg.priceRange.split(',')[0]);
                                max = parseInt(cfg.priceRange.split(',')[1]);
                            }
                            scope.min = 0;
                            scope.max = 100000;
                            scope.priceRangeArray = [scope.min, scope.max];

                            if (!scope.isSmall) {
                                categoryIds = cfg.categoryIds;
                                brandIds = cfg.brandIds;
                                seriesIds = cfg.seriesIds;
                            }

                            if (skipApply !== true) {
                                scope.filtersChanged();
                            }
                        }

                        scope.filtersChanged = function () {
                            var searchCfg = scope.searchCfg;
                            searchCfg.text = scope.text;
                            searchCfg.pol = scope.pol;
                            searchCfg._page = 1;
                            searchCfg.priceRange = scope.priceRange;
                            searchCfg.age = scope.age;
                            searchCfg.exists = scope.exists;
                            searchCfg.productCode = scope.number;
                            searchCfg.onlyWithComments = scope.onlyWithComments;
                            searchCfg.categoryIds = $.map($.grep(scope.searchCategories, function (c, i) {
                                return (c.checked);
                            }), function (cg, j) {
                                return cg.Id;
                            });
                            var brands = $.grep(scope.searchBrands, function (b, i) {
                                return b.checked;
                            });
                            searchCfg.brandIds = $.map(brands, function (bg, j) {
                                return bg.Id;
                            });
                            searchCfg.seriesIds = [];
                            scope.allUnchecked = {};
                            $.each(brands, function (i, b) {
                                scope.allUnchecked[b.Id] = 0;
                                $.each(b.Series, function (j, c) {
                                    if (c.checked) {
                                        scope.allUnchecked[b.Id]++;
                                        searchCfg.seriesIds.push(c.Code);
                                    }
                                });
                                scope.allUnchecked[b.Id] = (scope.allUnchecked[b.Id] === 0 || scope.allUnchecked[b.Id] === b.Series.length);
                            });
                            if (scope.isSmall) {
                                $rootScope.$broadcast('search', searchCfg);
                            } else {
                                if ($rootScope.searchGlobalShadow) {
                                    searchCfg = $rootScope.searchGlobalShadow;
                                    delete $rootScope.searchGlobalShadow;
                                    scope.setValues(searchCfg);
                                    return;
                                }
                                $rootScope.$broadcast('searchGlobal', searchCfg);
                            }
                        }

                        scope.rangeChanged = function () {
                            clearTimeout(scope.rangeTimer);
                            scope.rangeTimer = setTimeout(function () {
                                scope.filtersChanged();
                                scope.$apply();
                            }, 500);
                        }

                        var someFieldsFilled = false;
                        for (var p in $rootScope.searchGlobalShadow) {
                            if ((p == 'text' && $rootScope.searchGlobalShadow[p]) || p != 'text') {
                                someFieldsFilled = true;
                                break;
                            }
                        }

                        if (!scope.isSmall && $rootScope.searchGlobalShadow && someFieldsFilled) {
                            var searchCfg = $rootScope.searchGlobalShadow;
                            delete $rootScope.searchGlobalShadow;
                            scope.setValues(searchCfg, true);
                        } else {
                            scope.text = scope.text || '';
                            scope.age = scope.age || 0;
                            scope.pol = scope.pol || 0;
                            scope.number = scope.number || '';
                            scope.small = scope.isSmall === true;
                            scope.min = scope.min || 0;
                            scope.max = scope.max || 100000;
                            scope.priceRangeArray = [scope.min, scope.max];
                            scope.priceRange = scope.min + "," + scope.max;
                        }

                        var priceEl = el.find('.range-wrap');
                        var html =
                            '<div style="margin: 0px -8px 5px -10px;">' +
                            '<input type="text" style="width:90px;display:inline-block;" class="form-control price-range-min">' +
                            '<span style="text-align:center;display:inline-block;width:28px;">-</span>' +
                            '<input type="text" style="width:90px;display:inline-block;float:right;" class="form-control price-range-max">' +
                            '</div>' +
                            '<input class="price-range" ng-model="priceRange" slider-range val="priceRangeArray" min="min" max="max" step="1" type="text" change-fn="rangeChanged">';

                        var x = angular.element(html);
                        priceEl.append(x);
                        $compile(x)(scope);

                        var priceMinEl = el.find('.price-range-min');
                        var priceMaxEl = el.find('.price-range-max');
                        var slider = el.find('.price-range');
                        var sliderEl = slider.data('sliderEl');

                        if (max) {
                            sliderEl.bootstrapSlider('setValue', [min, max]);
                            scope.priceRange = min + "," + max;
                            var priceMinEl = el.find('.price-range-min');
                            var priceMaxEl = el.find('.price-range-max');
                            priceMinEl.val(min);
                            priceMaxEl.val(max);
                        }

                        var fn = function () {
                            var _max = 100000;
                            var minvalue = parseInt(priceMinEl.val()) || 0;
                            var maxvalue = parseInt(priceMaxEl.val()) || _max;
                            priceMinEl.val(minvalue);
                            priceMaxEl.val(maxvalue);
                            if (minvalue < 0) { priceMinEl.val(0); minvalue = 0; }
                            if (minvalue > _max) { priceMinEl.val(0); minvalue = 0; }
                            if (maxvalue < 0) { priceMaxEl.val(_max); maxvalue = _max; }
                            if (maxvalue > _max) { priceMaxEl.val(_max); maxvalue = _max; }
                            scope.priceRange = minvalue + ',' + maxvalue;
                            sliderEl.bootstrapSlider('setValue', [minvalue, maxvalue]);
                            scope.filtersChanged();
                            scope.$apply();
                        };

                        priceMinEl.on('change', fn);
                        priceMaxEl.on('change', fn);

                        scope.$watch('priceRange', function (n, o) {
                            // TODO: need to fix undefined value
                            if (n) {
                                var v = n.split(',');
                                priceMinEl.val(v[0]);
                                priceMaxEl.val(v[1]);
                            }
                        });

                        $timeout(function () {
                            $(window).trigger('resize');
                        });
                    }
                }
            }
        ]);
})();