(function() {
    'use strict';
    angular.module('homer')
        .directive("age", ['$timeout',
            function ($timeout) {
                return {
                    restrict: 'AE',
                    templateUrl: 'scripts/directives/age/age.html',
                    replace: true,
                    transclude: true,
                    scope: {
                        v: '=',
                        onChange: '='
                    },
                    link: function(scope, elements, attr) {
                        var el = $(elements[0]);
                        scope.c1 = el.find('input[value=1]');
                        scope.c2 = el.find('input[value=2]');
                        scope.c3 = el.find('input[value=4]');
                        scope.c4 = el.find('input[value=8]');
                        scope.c5 = el.find('input[value=16]');
                        scope.c6 = el.find('input[value=32]');
                        scope.click = function () {
                            $timeout(function () {
                                var r = 0;
                                var items = el.find('input[name=age]:checked');
                                $.each(items, function (i, it) {
                                    if (i == 0) {
                                        r = parseInt(it.value);
                                    } else {
                                        r = (r | parseInt(it.value));
                                    }
                                });
                                scope.onChange(r);
                            });
                        }
                        scope.setValue = function (v) {
                            if (v === undefined || v === null) {
                                v = '';
                            }
                            var c1v = scope.c1v = (parseInt(v) & 1) == 1;
                            scope.c1.prop('checked', c1v);
                            var c2v = scope.c2v = (parseInt(v) & 2) == 2;
                            scope.c2.prop('checked', c2v);
                            var c3v = scope.c3v = (parseInt(v) & 4) == 4;
                            scope.c3.prop('checked', c3v);
                            var c4v = scope.c4v = (parseInt(v) & 8) == 8;
                            scope.c4.prop('checked', c4v);
                            var c5v = scope.c5v = (parseInt(v) & 16) == 16;
                            scope.c5.prop('checked', c5v);
                            var c6v = scope.c6v = (parseInt(v) & 32) == 32;
                            scope.c6.prop('checked', c6v);
                        }
                        scope.$watch('v', function (v) {
                            scope.setValue(v);
                        });
                    }
                }
            }
        ]);
})();