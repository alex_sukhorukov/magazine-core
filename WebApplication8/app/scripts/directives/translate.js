﻿(function () {
    'use strict';
    angular.module('homer')
        .directive('translate', function () {
            return {
                restrict: 'A',
                link: function (scope, element, attr) {
                    var el = $(element[0]);
                    el.ready(function () {
                        el.text(scope.translate(attr.translate));
                    });
                }
            };
        });
})();