﻿(function () {
    (function ($) {
        $.each(['show', 'hide'], function (i, ev) {
            var el = $.fn[ev];
            $.fn[ev] = function () {
                this.trigger(ev);
                return el.apply(this, arguments);
            };
        });
    })(jQuery);

    (function ($) {
          var methods = ['addClass', 'toggleClass', 'removeClass'];
          $.each(methods, function (index, method) {
            var originalMethod = $.fn[method];
            $.fn[method] = function () {
                var result = originalMethod.apply(this, arguments);
                if (!this || this.length < 1) {
                    return result;
                }
                var oldClass = this[0].className;
                var newClass = this[0].className;
                this.trigger(method, [oldClass, newClass]);
                return result;
            };
          });
    }(jQuery));

    'use strict';
    angular.module('homer')
        .directive('autofocus', function() {
            return {
                restrict: 'A',
                link: function (scope, element) {
                    var el = element[0];
                    var focusFn = function () {
                        setTimeout(function () {
                            el.focus();
                        }, 250);
                    }
                    $(el).ready(focusFn);
                    $(el).on('show', focusFn);
                }
            };
        });
})();