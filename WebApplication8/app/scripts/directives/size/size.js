(function() {
    'use strict';
    angular.module('homer')
        .directive("size", ['$timeout', '$rootScope', 'translationService', '$compile',
            function ($timeout, $rootScope, translationService, $compile) {
                return {
                    restrict: 'AE',
                    templateUrl: 'scripts/directives/size/size.html',
                    replace: true,
                    transclude: true,
                    scope: {
                        v: '=',
                        onChange: '='
                    },
                    link: function(scope, elements, attr) {
                        var el = $(elements[0]);
                        scope.width = 0;
                        scope.height = 0;
                        scope.length = 0;

                        var trackFn = function (n, o) {
                            if (!scope.height) {
                                scope.v = parseInt(scope.length) + ' x ' + parseInt(scope.width);
                            } else {
                                scope.v = parseInt(scope.length) + ' x ' + parseInt(scope.width) + ' x ' + parseInt(scope.height);
                            }
                            if (scope.onChange) {
                                scope.onChange(scope.v);
                            }
                        };

                        scope.setValue = function (v) {
                            if (v === undefined || v === null) {
                                v = '';
                            }
                            var parts = v.split('x');
                            if (parts.length > 1) {
                                scope.length = parts[0].trim();
                                scope.width = parts[1].trim();
                            } else {
                                scope.length = 0;
                                scope.width = 0;
                            }
                            if (parts.length > 2) {
                                scope.height = parts[2].trim();
                            } else {
                                scope.height = 0;
                            }
                        }

                        scope.spinOption = {
                            verticalbuttons: true,
                            min: 0,
                            max: 10000,
                            step: 0.1,
                            decimals: 1
                        };

                        scope.translate = function (code) {
                            return translationService.translate(code);
                        }
                        scope.$watch('v', function (v) {
                            scope.setValue(v);
                        });
                        scope.$watch('width', trackFn);
                        scope.$watch('height', trackFn);
                        scope.$watch('length', trackFn);
                    }
                }
            }
        ]);
})();