(function() {
    'use strict';
    angular.module('homer')
        .directive("paging", [
            function() {
                return {
                    restrict: 'AE',
                    templateUrl: 'scripts/directives/paging/paging.html',
                    replace: true,
                    transclude: true,
                    scope: {
                        pages: '=',
                        onpageclick: '='
                    },
                    link: function(scope, elements, attr) {
                        var el = $(elements[0]);
                        scope.pages = scope.pages || {};
                        var trackFn = function (n, o) {
                            scope.pages.items = [];
                            var count = scope.pages.visibleCount;
                            var total = scope.pages.totalCount;
                            var current = scope.pages.currentPage;
                            var start = scope.pages.currentPage - Math.round(count / 2) + 1;
                            scope.pages.firstReached = false;
                            scope.pages.lastReached = false;
                            if (start < 1) {
                                start = 1;
                            }
                            var counted = 0;
                            for (var i = start; i < start + count; i++) {
                                if (i > total) {
                                    i = start - 1;
                                    while (counted < count) {
                                        if (i == 0) {
                                            break;
                                        }
                                        if (i == 1) {
                                            scope.pages.firstReached = true;
                                        }
                                        if (i == total) {
                                            scope.pages.lastReached = true;
                                        }
                                        scope.pages.items.splice(0, 0, {
                                            index: i,
                                            active: i == current
                                        });
                                        counted++;
                                        i--;
                                    }
                                    break;
                                }
                                if (i == 1) {
                                    scope.pages.firstReached = true;
                                }
                                scope.pages.items.push({
                                    index: i,
                                    active: i == current
                                });
                                if (i == total) {
                                    scope.pages.lastReached = true;
                                }
                                counted++;
                            }
                        };
                        scope.$watch('pages.visibleCount', trackFn);
                        scope.$watch('pages.currentPage', trackFn);
                        scope.$watch('pages.totalCount', trackFn);
                        scope.$watch('pages', trackFn);
                    }
                }
            }
        ]);
})();