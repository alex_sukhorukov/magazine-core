(function() {
    'use strict';
    angular.module('homer')
        .directive("recent", ['$timeout', '$rootScope', 'translationService', '$compile',
            function ($timeout, $rootScope, translationService, $compile) {
                return {
                    restrict: 'AE',
                    templateUrl: 'scripts/directives/recent/recent.html',
                    replace: true,
                    transclude: true,
                    scope: {
                        items: '='
                    },
                    link: function(scope, elements, attr) {
                        var el = $(elements[0]);
                        scope.hideLeft = true;
                        scope.hideRight = false;
                        scope.pos = 0;
                        var wrapper = el.find('.recent-wrapper');

                        scope.getUrl = function (url) {
                            return url + '&_t=' + scope._t;
                        }

                        scope.translate = function (code) {
                            return translationService.translate(code);
                        }

                        var resizeFn = function () {
                            var items = wrapper.find('.recent-item');
                            if (items.length == 0) {
                                return;
                            }
                            var el = $(elements[0]);
                            var w = el.width() - 15 * 2;
                            scope.pw = w;
                            var iw = (w - 90) / 4;
                            scope.n = 4;
                            if (iw < 180) {
                                iw = (w - 60) / 3;
                                scope.n = 3;
                                if (iw < 180) {
                                    scope.n = 2;
                                    iw = (w - 30) / 2;
                                    if (iw < 180) {
                                        scope.n = 1;
                                        iw = w;
                                    }
                                }
                            }
                            iw = parseInt(iw);
                            $.each(items, function (i, t) {
                                $(t).css({
                                    width: iw + 'px'
                                });
                            });
                            var itemWidth = $(items[0]).outerWidth();
                            if (scope.pos > 0) {
                                var shift = scope.pos * (30 + itemWidth);
                                el.animate({
                                    scrollLeft: shift + 'px'
                                }, 0, 'linear', function () {
                                });
                            }
                        }

                        scope.$watch('items', function () {
                            resizeFn();
                            scope.show = true;
                        });

                        $timeout(function () {
                            $(window).on('resize', resizeFn);
                        });

                        scope.go = function (dir, e) {
                            if (e && e.preventDefault) {
                                e.preventDefault();
                                e.stopPropagation();
                            }
                            if (scope.scrolling) {
                                return false;
                            }
                            var item = wrapper.find('.recent-item')[0];
                            var itemWidth = $(item).outerWidth() + 30;
                            var skip = false;
                            if (dir == 'left') {
                                if (scope.pos - 1 >= 0) {
                                    scope.pos--;
                                    var l = el[0].scrollLeft - itemWidth;
                                    if (scope.pos - 1 < 0) {
                                        scope.hideLeft = true;
                                    } else {
                                        scope.hideLeft = false;
                                    }
                                    scope.hideRight = false;
                                } else {
                                    skip = true;
                                    scope.hideLeft = true;
                                }
                            } else {
                                if (scope.pos + 1 + scope.n <= scope.items.length) {
                                    scope.pos++;
                                    var l = el[0].scrollLeft + itemWidth;
                                    if (scope.pos + 1 + scope.n > scope.items.length) {
                                        scope.hideRight = true;
                                    } else {
                                        scope.hideRight = false;
                                    }
                                    scope.hideLeft = false;
                                } else {
                                    skip = true;
                                    scope.hideRight = true;
                                }
                            }
                            if (!skip) {
                                scope.scrolling = true;
                                el.animate({
                                    scrollLeft: l + 'px'
                                }, 200, 'swing', function () {
                                    resizeFn();
                                    scope.scrolling = false;
                                });
                            }
                        }

                        swipedetect(wrapper[0], 'left right', function (swipedir, e) {
                            $timeout(function () {
                                if (swipedir == 'left') {
                                    scope.go('right', e);
                                } else if (swipedir == 'right') {
                                    scope.go('left', e);
                                }
                            });
                        }, function (e) {
                            if (e.button !== 2) {
                                $(e.target).parents('a').click();
                            }
                        }); 
                    }
                }
            }
        ]);
})();