﻿/**
 *
 * catalogCtrl
 *
 */

angular
    .module('homer')
    .controller('seriesCtrl', seriesCtrl);

function seriesCtrl($http, $scope, $timeout, $state, $rootScope, catalogService, translationService, localStorageService, adminService) {

    var scope = $scope;
    $rootScope.header = true;
    updateTitle($state, translationService);
    $rootScope.$broadcast('optionalLoggedIn');
    $scope.cseries = undefined;
    $rootScope.cseries = undefined;

    $scope.startLoading = true;

    // if user clicked back
    // if ($scope.code === $state.params.code)

    $scope.code = $state.params.code;
    $rootScope.code = $state.params.code;
    updateTitle($state, $scope.translate($state.current.data.code) + ' '  + $scope.code);

    if (!$scope.code) {
        $state.go("index");
    }

    $scope.getSeriesUrl = function (code, s) {
        if (s.isCategory) {
            return '#/catalog/' + code;
        } else {
            return '#/catalog/' + code + '/series/' + s.Code;
        }
    }

    $scope.getSeriesImageUrl = function (s) {
        if (s.isCategory) {
            return '/api/Catalog/GetPreviewCategoryImage?id=';
        } else {
            return '/api/Catalog/GetPreviewSeriesImage?id=';
        }
    }

    catalogService.getSeries($scope.code)
        .then(function (series) {
            for (var c in $scope.categories) {
                if ($scope.code === $scope.categories[c].Code) {
                    $scope.categories[c].isCategory = true;
                    series.splice(0, 0, $scope.categories[c]);
                }
            }
            $scope.series = series;
            $scope.startLoading = false;
            $timeout(function () {
                window.prerenderReady = true;
            }, 3000);
        }, function (err) {
            debugger;
            $scope.startLoading = false;
            $timeout(function () {
                window.prerenderReady = true;
            }, 3000);
        });
}
