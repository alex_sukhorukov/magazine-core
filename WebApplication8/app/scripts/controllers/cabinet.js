﻿/**
 *
 * cabinetCtrl
 *
 */

angular
    .module('homer')
    .controller('cabinetCtrl', cabinetCtrl);

function cabinetCtrl($http, $scope, $timeout, $state, $rootScope,
    translationService, cabinetService, localStorageService) {

    debugger;

    updateTitle($state, translationService);
    $rootScope.code = '';
    $rootScope.header = true;
    $scope.oneAtATime = true;
    $scope.showMoreOrders = true;
    $scope.pageOrders = $scope.pageOrders || 1;
    $scope.loading = false;

    var isConfirmChangeEmail = location.hash.indexOf('#/confirmchangeemail/') !== -1;

    if (isConfirmChangeEmail) {
        var guid = location.hash.replace('#/confirmchangeemail/', '')
        var request = {
            method: "post",
            url: "/api/Account/ConfirmChangeEmail",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                guid: guid
            }
        };
        $http(request).then(function (response) {
            if (response.data) {
                $scope.loginMsg = 'Смена Email подтверждена.';
                $scope.errorMsg = '';
            } else {
                $scope.errorMsg = 'Смена Email не подтверждена или ссылка устарела.';
                $scope.loginMsg = '';
            }
            $scope.getUserInfo();
        }, function (err) {
            $scope.handleError(err);
        });
    }

    $scope.orderStateColor = function (orderStateId) {
        if (orderStateId.toLowerCase() === 'bc84afe9-c679-4298-bcb4-66c3485ba645') {
            return '#2c2';
        } else if (orderStateId.toLowerCase() === 'c13f74d7-be87-41c7-9de9-589944bd8621') {
            return '#e7e';
        } else if (orderStateId.toLowerCase() === '9dd4c231-ffbc-483a-a1b6-4adbaf612c5f') {
            return '#f77';
        } else {
            return '#000';
        }
    }

    $scope.rainbow = new Rainbow();
    $scope.rainbow.setSpectrum('#34495e', '#9b59b6', '#3498db', '#62cb31', '#ffb606', '#e67e22', '#e74c3c', '#c0392b', 'LightSteelBlue', 'Maroon', 'MediumOrchid', 'MediumSeaGreen', 'OliveDrab', 'Orange', 'Orchid', 'PaleGoldenRod', 'Pink', 'RosyBrown', 'Turquoise', 'Tomato', 'YellowGreen', 'DarkSeaGreen', 'Crimson', 'Coral', 'MediumPurple', 'PeachPuff');
    $scope.rainbow.setNumberRange(1, 26);

    $scope.model = {
        orders: [],
        email: '',
        phone: '',
        address: '',
        calendar: '',
        number: '',
        userName: ''
    };

    $scope.removeProfile = function (p, e) {
        e.preventDefault();
        e.stopPropagation();
        swal({
            title: $scope.translate('RemoveProfileQuestion'),
            text: $scope.translate('Question'),
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: $scope.translate('Yes'),
            cancelButtonText: $scope.translate('No'),
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                $scope.loading = true;
                cabinetService.removeProfile(p.Id)
                    .then(function () {
                        var index = $scope.auth.Logins.indexOf(p);
                        if (index !== -1) {
                            $scope.auth.Logins.splice(index, 1);
                        }
                        $scope.loading = false;
                        localStorageService.set('auth', $scope.auth);
                    }, function (err) {
                        debugger;
                        $scope.loading = false;
                    });
            }
        });
    }

    $scope.removeOrder = function(o) {
        swal({
            title: $scope.translate('RemoveOrderQuestion'),
            text: $scope.translate('Question'),
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: $scope.translate('Yes'),
            cancelButtonText: $scope.translate('No'),
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                $scope.loading = true;
                cabinetService.removeOrder(o.Id)
                    .then(function () {
                        var index = $scope.model.orders.indexOf(o);
                        if (index !== -1) {
                            $scope.model.orders.splice(index, 1);
                        }
                        $scope.loading = false;
                    }, function (err) {
                        debugger;
                        $scope.loading = false;
                    });
            }
        });
        
    }

    $scope.getUserInfo = function (cb) {
        $scope.loading = true;
        $scope.loginMsg = '';
        $scope.errorMsg = '';
        cabinetService.getUserInfo()
            .then(function (userInfo) {
                $scope.model.id = userInfo.Id;
                $scope.model.email = userInfo.Email;
                $scope.model.phone = userInfo.Phone;
                $scope.model.address = userInfo.Address;
                $scope.auth.Email = $scope.model.email;
                $scope.model.emailConfirmedFlag = userInfo.EmailConfirmedFlag;
                localStorageService.set('auth', $scope.auth);
                $scope.loading = false;
                if (cb) {
                    cb();
                }
            }, function (err) {
                debugger;
                $scope.loading = false;
                if (cb) {
                    cb();
                }
            });
    }

    $scope.getUserInfo(function () {
        $scope.$watch('model.email', $scope.saveInfo);
        $scope.$watch('model.phone', $scope.saveInfo);
        $scope.$watch('model.address', $scope.saveInfo);
        $rootScope.$broadcast('requiredLoggedIn');
        $timeout(function () {
            window.prerenderReady = true;
        }, 3000);
    });

    $scope.orderTotal = function (o) {
        var s = o.TotalPrice;
        return s;
    }

    $rootScope.$on("emit:dateTimePicker", function (scope, opt) {
        var dt = opt.dateTime;
        if (opt.element.indexOf('calendar') === 0) {
            $scope.model.calendar = $scope.formatShortDate(dt);
        }
    });

    $scope.orderAction = function (order, action) {
        swal({
            title: $scope.translate('ChangeOrderStateTo') + ' \"' + $scope.translate('OrderState_' + action) + '\"?',
            text: $scope.translate('Question'),
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: $scope.translate('Yes'),
            cancelButtonText: $scope.translate('No'),
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                $scope.loading = true;
                cabinetService.orderAction(order.Id, action)
                    .then(function (orderState) {
                        order.OrderState = orderState;
                        $scope.loading = false;
                    }, function (err) {
                        debugger;
                        $scope.loading = false;
                    });
            }
        });
    }

    $rootScope.$broadcast('requiredLoggedIn');

    if ($scope.active === undefined) {
        $scope.active = 0;
    }

    if ($state.params && $state.params.tab === "orders") {
        $scope.active = 1;
    }

    $scope.applyFilter = function () {
        $scope.pageOrders = 1;
        $scope.model.orders = [];
        $scope.model.calendar = $('#calendar').val();
        $scope.loading = true;
        cabinetService.getOrders($scope.pageOrders, $scope.model.number, $scope.model.calendar, $scope.model.userName)
            .then(function (response) {
                $scope.model.orders = $scope.model.orders.concat(response);
                $scope.showMoreOrders = response.length === 10;
                if ($scope.showMoreOrders) {
                    $scope.pageOrders = $scope.pageOrders + 1;
                }
                $scope.loading = false;
            }, function (err) {
                debugger;
                $scope.loading = false;
            });
    }

    $scope.clearFilter = function () {
        $scope.model.number = '';
        $scope.model.calendar = '';
        $('#calendar').val('');
        $scope.model.userName = '';
        $scope.applyFilter();
    }

    $scope.loadMore = function () {
        $scope.loading = true;
        cabinetService.getOrders($scope.pageOrders, $scope.model.number, $scope.model.calendar, $scope.model.userName)
            .then(function (response) {
                $scope.model.orders = $scope.model.orders.concat(response);
                $scope.showMoreOrders = response.length === 10;
                if ($scope.showMoreOrders) {
                    $scope.pageOrders = $scope.pageOrders + 1;
                }
                $scope.loading = false;
            }, function (err) {
                $scope.loading = false;
                debugger;
            });
    }

    $scope.loading = true;
    cabinetService.getOrders(1, '', '', '')
        .then(function (response) {
            $scope.model.orders = response;
            $scope.showMoreOrders = response.length === 10;
            if ($scope.showMoreOrders) {
                $scope.pageOrders = $scope.pageOrders + 1;
            }
            $scope.loading = false;
        }, function (err) {
            $scope.loading = false;
            debugger;
        });

    $scope.saveInfo = function (n, o) {
        if (n === o) {
            return;
        }
        if ($scope.saveTimer) {
            $timeout.cancel($scope.saveTimer);
        }
        $scope.saveTimer = $timeout(function () {
            var email = $scope.model.email;
            cabinetService.saveUserInfo($scope.model)
                .then(function (response) {
                    $scope.loginMsg = '';
                    if (parseInt(response) === 0) {
                        $scope.loginMsg = 'На Email ' + email +' отправлено письмо для подтверждения.';
                    }
                }, function (err) {
                    debugger;
                });
            $scope.saveTimer = undefined;
        }, 500);
    }

    if (isConfirmChangeEmail) {
        setTimeout(function () {
            location.replace('#/cabinet/');
        });
    }
}
