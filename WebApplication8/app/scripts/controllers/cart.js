﻿/**
 *
 * cartCtrl
 *
 */

angular
    .module('homer')
    .controller('cartCtrl', cartCtrl);

function cartCtrl($http, $scope, $timeout, $state, $rootScope, translationService, localStorageService, cartService, cabinetService) {
    updateTitle($state, translationService);
    $rootScope.header = true;
    $rootScope.code = '';
    $scope.model = $scope.model || {};
    $scope.mask = '+38 (999) 999-99-99';
    $scope.orderNumber = $scope.orderNumber || {};

    $scope.$watch('loggedIn', function (n, o) {
        if (n) {
            cabinetService.getUserInfo()
                .then(function (userInfo) {
                    $scope.model.phone = userInfo.Phone;
                    $scope.model.id = userInfo.Id;
                    $scope.model.email = userInfo.Email;
                    $scope.model.address = userInfo.Address;
                }, function (err) {
                    debugger;
                });
        }
    });

    $scope.plusCountCart = function (p) {
        p.Count++;
        localStorageService.set('cart', $scope.cart);
    }

    $scope.loadCities();

    $scope.minusCountCart = function (p) {
        if (p.Count > 0) {
            p.Count--;
        }
        localStorageService.set('cart', $scope.cart);
    }

    $scope.removeFromCart = function (id) {
        swal({
            title: $scope.translate('RemoveFromCartQuestion'),
            text: $scope.translate('Question'),
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#2af",
            confirmButtonText: $scope.translate('Yes'),
            cancelButtonText: $scope.translate('No'),
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                if ($scope.cart[id]) {
                    delete $scope.cart[id];
                    localStorageService.set('cart', $scope.cart);
                    $scope.$apply();
                }
            }
        });
    }

    $scope.proceedOrder = function () {
        if ($scope.totalCart() > 0 && !$scope.isCartEmpty()) {
            $state.go('order');
        }
    }

    $scope.totalCart = function () {
        var t = 0;
        for (var c in $scope.cart) {
            t += $scope.cart[c].ProductPrice.Price * $scope.cart[c].Count;
        }
        return t;
    }

    $scope.beautyAmount = function (value) {
        var v = value.toString();
        var s = '', k = 0;
        for (var i = v.length - 1; i >= 0; i--) {
            if (k === 3) {
                s = ' ' + s;
            }
            s = v[i] + s;
            k++;
        }
        return s;
    }

    $scope.phoneValid = function() {
        return $scope.model.phone && $scope.model.phone.length >= 10 && $scope.model.phone.indexOf('_') === -1;
    }

    $scope.emailValid = function () {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test($scope.model.email);
    }

    $scope.canOrder = function () {
        if (!$scope.deliveryMethod.selected) {
            return false;
        }
        if ($scope.isNP($scope.deliveryMethod)) {
            return $scope.phoneValid() && $scope.city && $scope.city.selected && $scope.warehouse && $scope.warehouse.selected && ($scope.loggedIn || ($scope.emailValid() && $scope.model.fullName));
        } else {
            return $scope.phoneValid() && ($scope.loggedIn || ($scope.emailValid() && $scope.model.fullName));
        }
    }

    $scope.order = function () {
        var model = {
            DeliveryMethodId: $scope.deliveryMethod.selected.Id,
            NPCityRef: $scope.city.selected ? $scope.city.selected.Ref : null,
            NPWarehouseRef: $scope.warehouse.selected ? $scope.warehouse.selected.Ref : null,
            Phone: $scope.model.phone,
            Email: $scope.model.email,
            FullName: $scope.model.fullName,
            AdditionalInfo: $scope.model.addinfo,
            ProductSet: []
        };

        for (var k in $scope.cart) {
            var t = $scope.cart[k];
            model.ProductSet.push({
                Id: t.Id,
                Count: t.Count
            });
        }

        var wfpMesssage = function(event) {
            if (event.data && event.data.substr(0, 1) === '{') {
                var jdata = JSON.parse(event.data);

                if ($scope.orderNumber[jdata.orderReference] === 'received' || (jdata.transactionStatus !== 'Approved' && jdata.transactionStatus !== 'Pending')) {
                    return;
                }

                $scope.orderNumber[jdata.orderReference] = 'received';

                $timeout(function () {
                    swal({
                        title: $scope.translate('OrderDone'),
                        text: $scope.loggedIn ? $scope.trustAsHtml($scope.translate('YouMaySeeOrdersInCabinetPage')) : $scope.trustAsHtml($scope.translate('YouMaySeeOrdersInCabinetPageAnonymous')),
                        html: true,
                        type: "success"
                    });

                    for (var k in $scope.cart) {
                        delete $scope.cart[k];
                    }

                    localStorageService.set('cart', $scope.cart);

                    $timeout(function () {
                        if ($scope.loggedIn) {
                            $state.go("cabinet", { "tab": 'orders' });
                        }
                    }, 250);
                });
            }
        }

        if (!$scope.wfpSubscribed) {
            $scope.wfpSubscribed = true;
            window.addEventListener("message", wfpMesssage, false);
        }

        var pay = function (m, model) {
            var langId = localStorageService.get('langId') || 0;

            cartService.placeOrder(model)
                .then(function (orderId) {
                    cabinetService.orderAction(orderId, 'New')
                        .then(function (response) {
                        }, function (err) {
                            debugger;
                        });
                }, function (err) {
                    debugger;
                });

            w4p.run({
                language: langId === 0 ? 'RU' : 'UA',
                merchantAccount: m.merchantAccount,
                merchantDomainName: m.merchantDomainName,
                authorizationType: "SimpleSignature",
                merchantSignature: m.hash,
                orderReference: m.orderNumber,
                orderDate: m.orderDate,
                amount: m.amount,
                serviceUrl: 'https://www.igruhi.com/api/cart/serviceurl',
                currency: m.currency,
                productName: m.productName,
                productPrice: m.productPrice,
                productCount: m.productCount,
                clientEmail: model.Email,
                clientPhone: model.Phone,
                straightWidget: true
            },
            function (response) {
                // on approved
                // debugger;
            },
            function (response) {
                // on declined
                debugger;
            },
            function (response) {
                // on pending or in processing
                // debugger;
            });
        }

        cartService.preOrder(model)
            .then(function (data) {
                model.preOrderNumber = data.orderNumber;
                pay(data, model);
            }, function (err) {
                debugger;
            });
    }

    $timeout(function () {
        window.prerenderReady = true;
    }, 3000);
}