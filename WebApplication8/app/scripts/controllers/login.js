﻿/**
 *
 * loginCtrl
 *
 */

angular
    .module('homer')
    .controller('loginCtrl', loginCtrl);

function loginCtrl($http, $scope, $timeout, $state, $rootScope, localStorageService, loginAppFactory, vcRecaptchaService, translationService, loginService) {

    $rootScope.header = false;
    $scope.response = null;
    $scope.widgetId = null;

    updateTitle($state, translationService);

    $rootScope.$broadcast('optionalLoggedIn');

    $scope.getKeys = function () {
        loginService.getKeys()
            .then(function (response) {
                $scope.model.key = response.RecaptchaKey;
                $scope.model.redirectUrl = response.RedirectUrl;
                $timeout(function () {
                    window.prerenderReady = true;
                }, 3000);
            }, function (err) {
                debugger;
                $timeout(function () {
                    window.prerenderReady = true;
                }, 3000);
            });
    }
    $scope.model = $scope.model || {};
    if (!$scope.model.key) {
        $scope.getKeys();
    }

    $scope.setResponse = function (response) {
        $scope.response = response;
    };

    $scope.setWidgetId = function (widgetId) {
        $scope.widgetId = widgetId;
    };

    $scope.cbExpiration = function () {
        vcRecaptchaService.reload($scope.widgetId);
        $scope.response = null;
    };

    $scope.authenticateExternalProvider = function (provider) {
        window.location.href = "/api/Account/ExternalLogin?provider=" + provider + "&response_type=token&client_id=self&redirect_uri=" + $scope.model.redirectUrl;
    };

    var isConfirmEmail = location.hash.indexOf('#/confirmemail/') !== -1;

    var isRecoveryEmail = location.hash.indexOf('#/recovery/') !== -1;

    if (isConfirmEmail) {
        var guid = location.hash.replace('#/confirmemail/', '')
        var request = {
            method: "post",
            url: "/api/Account/ConfirmEmail",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                guid: guid
            }
        };
        $http(request).then(function (response) {
            if (response.data) {
                $scope.loginMsg = 'Email подтвержден. Теперь Вы можете войти на сайт.';
                $scope.errorMsg = '';
            } else {
                $scope.errorMsg = 'Email не подтвержден.';
                $scope.loginMsg = '';
            }
            $state.transitionTo('login');
        }, function (err) {
            $scope.handleError(err);
        });
    }

    $scope.checkRecovery = function () {
        console.log($scope.recoveryUid);
        console.log($scope.recoveryUid.length);
        return (!$scope.recoveryUid || $scope.recoveryUid.length !== 32);
    }

    $scope.resetPassword = function () {
        var recoveryUid = $scope.recoveryUid;
        var pwd = $scope.pwd;
        var pwd2 = $scope.pwd2;

        var request = {
            method: "post",
            url: "/api/Account/ResetPassword",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                RecoveryUid: recoveryUid,
                Password: pwd,
                ConfirmPassword: pwd2
            }
        };
        return $http(request).then(function (response) {
            $state.transitionTo("login");
            $rootScope.$broadcast('updateErrorMsg', '');
            $rootScope.$broadcast('updateLoginMsg', 'Пароль успешено сменен.');
        }, function (err) {
            $scope.handleError(err);
        });
    }

    if (isRecoveryEmail) {
        var recoveryUid = location.hash.replace('#/recovery/', '');
        $scope.recoveryUid = recoveryUid;
    }

    $scope.login = function () {
        var email = $scope.email;
        var pwd = $scope.pwd;
        var response = $scope.response;
        var request = {
            method: "post",
            url: "/api/Account/Login",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                Email: email,
                Password: pwd,
                Response: response
            }
        };
        return $http(request).then(function (response) {
            $rootScope.$broadcast('authItem', response.data);
            $rootScope.$broadcast('checkLogon');
            $state.transitionTo("catalog");
        }, function (err) {
            vcRecaptchaService.reload($scope.widgetId);
            $scope.handleError(err);
        });
    };

    $scope.register = function () {
        var email = $scope.email;
        var pwd = $scope.pwd;
        var pwd2 = $scope.pwd2;

        var request = {
            method: "post",
            url: "/api/Account/Register",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                Email: email,
                Password: pwd,
                ConfirmPassword: pwd2
            }
        };
        return $http(request).then(function (response) {
            $state.transitionTo("login");
            $rootScope.$broadcast('updateErrorMsg', '');
            $rootScope.$broadcast('updateLoginMsg', 'На почтовый ящик ' + $scope.email + ' было отправлено письмо для подтверждения регистрации.');
        }, function (err) {
            $scope.handleError(err);
        });
    };

    $scope.recovery = function () {
        var email = $scope.email;
        var request = {
            method: "post",
            url: "/api/Account/Recovery",
            headers: {
                "Content-Type": "application/json"
            },
            data: {
                Email: email
            }
        };
        return $http(request).then(function (response) {
            $rootScope.$broadcast('updateErrorMsg', '');
            $rootScope.$broadcast('updateLoginMsg', 'На почтовый ящик ' + $scope.email + ' было отправлено письмо для подтверждения сброса пароля.');
        }, function (err) {
            $scope.handleError(err);
        });
    };

    if (location.hash === "#/error=access_denied") {
        debugger;
        $scope.errorMsg = location.hash;
        // $rootScope.$broadcast('updateLoginMsg', location.hash);
        // TODO: implement
    }
}