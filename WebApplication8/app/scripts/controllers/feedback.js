﻿/**
 *
 * feedbackCtrl
 *
 */

angular
    .module('homer')
    .controller('feedbackCtrl', feedbackCtrl);

function feedbackCtrl($http, $scope, $timeout, $state, $rootScope, translationService, cabinetService) {
    updateTitle($state, translationService);
    $rootScope.header = true;
    $rootScope.code = '';
    $scope.getComments('', 1, function () {
        $timeout(function () {
            window.prerenderReady = true;
        }, 3000);
    });
    $scope.clearComment();
}
