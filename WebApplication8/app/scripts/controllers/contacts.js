﻿/**
 *
 * contactsCtrl
 *
 */

angular
    .module('homer')
    .controller('contactsCtrl', emptyCtrl);

function contactsCtrl($http, $scope, $timeout, $state, $rootScope, translationService) {
    updateTitle($state, translationService);
    $rootScope.header = true;
    $rootScope.code = '';
    $rootScope.$broadcast('optionalLoggedIn');
    $timeout(function () {
        window.prerenderReady = true;
    }, 3000);
}