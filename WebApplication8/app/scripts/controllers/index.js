﻿/**
 *
 * emptyCtrl
 *
 */

angular
    .module('homer')
    .controller('indexCtrl', indexCtrl);

function indexCtrl($http, $scope, $timeout, $state, $rootScope, translationService) {
    updateTitle($state, translationService);

    $rootScope.$broadcast('optionalLoggedIn');
    $rootScope.header = true;
    $rootScope.code = '';

    $scope.showItem = function (p, prefix) {
        var el = $('#' + prefix + p.Id);
        el.fadeIn(250, function () {
            el.css({
                opacity: 1,
                display: 'inline-block'
            });
        });
        $scope.tabSelected();
    };

    $scope.$watch('categories', function (n, o) {
        if (n && n.length > 0) {
            $timeout(function () {
                window.prerenderReady = true;
            }, 500);
        }
    });

    $timeout(function () {
        window.prerenderReady = true;
    }, 3000);
}
