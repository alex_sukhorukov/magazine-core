﻿/**
 *
 * appCtrl
 *
 */

angular
    .module('homer')
    .controller('appCtrl', appCtrl);

window.w4p = new Wayforpay();

function appCtrl(
            $http,
            $scope,
            $timeout,
            $rootScope,
            loginAppFactory,
            localStorageService,
            catalogService,
            translationService,
            adminService,
            cartService,
            $state,
            $sce, vcRecaptchaService) {

    $rootScope.sortBy = localStorageService.get('sortBy') || 'ByDateDesc';
    $rootScope.viewType = localStorageService.get('viewType') || 0;
    $rootScope.header = true;
    $rootScope.code = '';
    $scope.search = {
        text: ''
    };
    $rootScope.searchGlobal = {
        text: ''
    };
    $rootScope._t = 0;
    $scope.category = {};
    $scope.brand = {};
    $scope.categories = [];
    $scope.deliveryMethod = {};
    $scope.deliveryMethods = [];
    $rootScope.admin = false;
    $scope.areas = [];
    $scope.cities = [];
    $scope.warehouses = [];
    $scope.area = {};
    $scope.city = {};
    $scope.warehouse = {};
    $scope.comments = [];
    $scope.showMoreComments = false;
    $scope.commentsPage = 2;
    $scope.addingComment = false;
    $scope.comment = {
        title: '',
        text: '',
        productId: ''
    };

    $scope.getCategoryUrl = function (c) {
        if (c.Series) {
            var hasSeries = false;
            for (var s in c.Series) {
                if (c.Series[s]) {
                    hasSeries = true;
                    break;
                }
            }
        }
        if (!hasSeries) {
            return '#/catalog/' + c.Code;
        } else {
            return '#/catalog/' + c.Code + '/series';
        }
    }

    $scope.customClearFilter = function () {
        $rootScope.$broadcast('applyCustom');
    }

    $scope.fillProductParams = function (m) {
        var fn = function (i, p) {
            p.params = [];
            if (p.Brand) {
                p.params.push({
                    name: $scope.translate('Brand'),
                    value: p.Brand.Name,
                    image: $scope.getUrl('/api/Catalog/GetPreviewBrandImage?id=' + p.Brand.Id)
                });
            }
            if (p.Series) {
                p.params.push({
                    name: $scope.translate('Series'),
                    value: p.Series.Name
                });
            }
            if (p.Pol) {
                var boyFlag = (p.Pol & 1) !== 0;
                var girlFlag = (p.Pol & 2) !== 0;
                var v = '';
                if (boyFlag && !girlFlag) {
                    v = $scope.translate('ForBoys');
                }
                if (!boyFlag && girlFlag) {
                    v = $scope.translate('ForGirls');
                }
                if (boyFlag && girlFlag) {
                    v = $scope.translate('ForBothBoysAndGirls');
                }
                p.params.push({
                    name: $scope.translate('Pol'),
                    value: v
                });
            }
            if (p.Age) {
                v = '';
                var ages = 0;
                if ((p.Age & 1) !== 0) {
                    v += "0-3";
                    ages++;
                }
                if ((p.Age & 2) !== 0) {
                    if (v) { v += ", " };
                    v += "3-6";
                    ages++;
                }
                if ((p.Age & 4) !== 0) {
                    if (v) { v += ", " };
                    v += "4-7";
                    ages++;
                }
                if ((p.Age & 8) !== 0) {
                    if (v) { v += ", " };
                    v += "7-9";
                    ages++;
                } if ((p.Age & 16) !== 0) {
                    if (v) { v += ", " };
                    v += "9-16";
                    ages++;
                }
                if ((p.Age & 32) !== 0) {
                    if (v) { v += ", " };
                    v += "16+";
                    ages++;
                }
                if (ages === 6) {
                    v = $scope.translate('AllAges');
                }
                p.params.push({
                    name: $scope.translate('Age'),
                    value: v
                });
            }
            p.params.push({
                name: $scope.translate('Number'),
                value: p.ProductCode
            });
            if (p.InternalProductCode) {
                p.params.push({
                    name: $scope.translate('Article'),
                    value: p.InternalProductCode
                });
            }
            if (p.WarrantyNumberOfMonth) {
                p.params.push({
                    name: $scope.translate('WarrantyNumberOfMonth'),
                    value: p.WarrantyNumberOfMonth
                });
            }
            if (p.Size && p.Size.indexOf('0 x 0') === -1) {
                p.params.push({
                    name: $scope.translate('Size'),
                    value: p.Size
                });
            }
        };
        if (Array.isArray(m)) {
            $.each(m, fn);
        } else {
            fn(0, m);
        }
    }

    $scope.changeView = function (t) {
        $scope.viewType = t;
        localStorageService.set('viewType', t);
    }

    $scope.keyPress = function (e) {
        if (e.keyCode === 13) {
            $scope.applySearchFilter();
        }
    }

    $scope.applySearchFilter = function () {
        $rootScope.searchGlobalShadow = $.extend({}, $rootScope.searchGlobal);
        $rootScope.currentPage = 1;
        $state.go('search');
    }

    $scope.$on('searchGlobal', function (scope, searchCfg) {
        $rootScope.searchGlobal = searchCfg;
    });

    $scope.getProfileUrl = function (s) {
        if (!s) {
            return;
        }
        if (s.Type === 'Twitter') {
            return 'https://twitter.com/' + s.Name;
        } else if (s.Type === "Facebook") {
            return 'https://facebook.com/' + s.ExternalId;
        } else if (s.Type === "Google") {
            return 'https://plus.google.com/' + s.ExternalId;
        } else if (s.Type === "Instagram") {
            return 'https://instagram.com/' + s.Name;
        }
    }

    $scope.getProfileImage = function (s) {
        if (!s) {
            return;
        }
        if (s.Type === 'Twitter') {
            return 'https://avatars.io/twitter/' + s.Name+ '/original';
        } else if (s.Type === 'Facebook') {
            return 'https://graph.facebook.com/' + s.ExternalId + '/picture?type=square&width=200&height=200';
        } else {
            if (s.PictureUrl) {
                return s.PictureUrl.indexOf('?') !== -1 ? s.PictureUrl + '&sz=180' : s.PictureUrl + '?sz=200';
            } else {
                return '/images/user.png';
            }
        }
    }

    $scope.$on('requiredLoggedIn', function () {
        $scope.requireLoggedIn = true;
        if (!$scope.loggedIn) {
            $scope.checkLogon(function () {
                if (!$scope.loggedIn) {
                    swal({
                        title: $scope.translate('Information'),
                        text: $scope.translate('NeedLoggonToProceed'),
                        type: "success"
                    });
                    $state.go('login');
                }
            });
        }
    });

    $scope.$on('optionalLoggedIn', function () {
        $scope.requireLoggedIn = false;
    });

    $scope.loadMoreComments = function(productId) {
        $scope.getComments(productId, $scope.commentsPage);
    }

    $scope.addSubComment = function (comment, e) {
        if (e.keyCode === 27) {
            comment.subComment = '';
            comment.readyToComment = false;
            return;
        }
        if (e.keyCode !== 13) {
            return;
        }
        if (!comment.subComment) {
            return;
        }
        $scope.addingComment = true;
        var model = {
            CommentId: comment.Id,
            UserId: comment.User.Id,
            ExternalId: comment.User.ExternalId,
            ProductId: comment.Product ? comment.Product.Id : '',
            Text: comment.subComment
        };
        translationService.addSubComment(model)
            .then(function (subComment) {
                comment.showSubComments = true;
                $scope.addingComment = false;
                comment.SubComments = comment.SubComments || [];
                comment.SubComments.push(subComment);
                comment.subComment = '';
                comment.readyToComment = false;
            }, function (err) {
                debugger;
                $scope.addingComment = false;
            });
    }

    $scope.addNewComment = function (model, cb) {
        $scope.addingComment = true;
        translationService.addComment(model)
            .then(function (comment) {
                $scope.addingComment = false;
                $scope.comments.splice(0, 0, comment);
                if (cb) {
                    cb();
                }
            }, function (err) {
                debugger;
                $scope.addingComment = false;
            });
    }

    $scope.addComment = function () {
        $scope.addNewComment($scope.comment, function () {
            $scope.comment.text = '';
            $scope.comment.title = '';
        });
    }

    $scope.repeatGetComments = function () {
        $scope.commentsPage = 1;
        $scope.getComments($scope.lastProductId, 1);
    }

    $scope.clearComment = function() {
        $scope.comment = {
            title: '',
            text: '',
            productId: ''
        };
    }

    $scope.getComments = function (productId, page, cb) {
        $scope.lastProductId = productId;
        if (page === 1) {
            $scope.showMoreComments = false;
            $scope.commentsPage = 1;
        }
        $scope.loadingMore = true;
        if (page === 1) {
            $scope.comments = [];
        }
        $timeout(function () {
            translationService.getComments(productId, page)
                .then(function (comments) {
                    if ($scope.commentsPage === 1) {
                        $scope.comments = comments;
                    } else {
                        $scope.comments = $scope.comments.concat(comments);
                    }
                    $scope.showMoreComments = comments.length === 4;
                    if ($scope.showMoreComments) {
                        $scope.commentsPage++;
                    }
                    $scope.loadingMore = false;
                    $scope.tabSelected();
                    if (cb) {
                        cb();
                    }
                }, function (err) {
                    debugger;
                    $scope.loadingMore = false;
                });
        });
    }

    $scope.removeComment = function (c, sc, isSubComment) {
        var comment = isSubComment ? sc : c;
        swal({
            title: $scope.translate('RemoveCommentQuestion'),
            text: $scope.translate('Question'),
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: $scope.translate('Yes'),
            cancelButtonText: $scope.translate('No'),
            closeOnConfirm: true,
            closeOnCancel: true
        },
        function (isConfirm) {
            if (isConfirm) {
                translationService.removeComment(comment.Id)
                    .then(function () {
                        if (isSubComment) {
                            var index = c.SubComments.indexOf(sc);
                            if (index >= 0) {
                                c.SubComments.splice(index, 1);
                            }
                        } else {
                            index = $scope.comments.indexOf(c);
                            if (index >= 0) {
                                $scope.comments.splice(index, 1);
                            }
                            $scope.repeatGetComments();
                        }

                    }, function (err) {
                        debugger;
                    });
            }
        });
    }

    $rootScope.$broadcast('optionalLoggedIn');

    $scope.trustAsHtml = function (text) {
        return $sce.trustAsHtml(text);
    };

    cartService.getDeliveryMethods()
        .then(function (methods) {
            $scope.deliveryMethods = methods;
            if ($scope.deliveryMethods.length > 0) {
                $scope.deliveryMethod.selected = $scope.deliveryMethods[0];
                $rootScope.$broadcast('deliveryMethodsLoaded');
            }
        }, function (err) {
            debugger;
        });

    catalogService.getCategories()
        .then(function (categories) {
            $scope.categories = categories;
            if ($scope.categories.length > 0) {
                $scope.category.selected = $scope.categories[0];
            }
        }, function (err) {
            debugger;
        });

    catalogService.getBrands('')
        .then(function (brands) {
            $scope.brands = brands;
            if ($scope.brands.length > 0) {
                $scope.brand.selected = $scope.brands[0];
            }
        }, function (err) {
            debugger;
        });

    $scope.cart = localStorageService.get('cart') || {};
    $scope.products = $scope['products'] || [];

    $scope.preventDefault = function (e) {
        e.preventDefault();
        e.stopPropagation();
    }

    $scope.clickStar = function (p, s, e) {
        p.Rating = parseFloat(s).toFixed(1).replace('.0', '');
        var model = {
            Id: p.Id,
            Count: s
        };
        translationService.productRate(model)
            .then(function (result) {
                p.Rate = (result.avgRate).toFixed(1).replace('.0', '');
                p.RateCount = result.count;
            }, function (err) {
                debugger;
            });
        e.stopPropagation();
        e.preventDefault();
    }

    $scope.clickedStar = function (p) {
        if (p) {
            return parseFloat(p.Rating).toFixed(1).replace('.0', '') || 0;
        }
        return 0;
    }

    $scope.isCartEmpty = function () {
        return $scope.cartCount() === 0;
    }

    $scope.cartCount = function () {
        var r = 0;
        for (var i in $scope.cart) {
            r++;
        }
        return r;
    }

    $scope.inCart = function (p) {
        if (!p) {
            return false;
        }
        return $scope.cart[p.Id];
    }

    $scope.loadCities = function () {
        $scope.areas = [];
        $scope.cities = [];
        $scope.warehouses = [];
        $scope.area.selected = undefined;
        $scope.city.selected = undefined;
        $scope.warehouse.selected = undefined;
        adminService.getNPAreas()
            .then(function (response) {
                $scope.areas = response;
            }, function (err) {
                debugger;
            });
    }

    $scope.isNP = function (dm) {
        var scope = $scope;
        if (!dm || !dm.selected || !dm.selected.Id) {
            return false;
        }
        return dm.selected.Id.toUpperCase() === 'B683A766-4239-4E45-B392-AD7C23E91EB0';
    }

    $scope.onSelectNPCity = function (item) {
        $scope.warehouses = [];
        $scope.warehouse.selected = undefined;
        adminService.getNPWarehouses(item.Ref)
            .then(function (response) {
                $scope.warehouses = response;
            }, function (err) {
                debugger;
            });
    }

    $scope.onSelectNPArea = function (item) {
        $scope.cities = [];
        $scope.city.selected = undefined;
        $scope.warehouses = [];
        $scope.warehouse.selected = undefined;
        adminService.getNPCities(item.Ref)
            .then(function (response) {
                $scope.cities = response;
            }, function (err) {
                debugger;
            });
    }

    $scope.goToCart = function (p, e) {
        if ($scope.cart[p.Id]) {
            delete $scope.cart[p.Id];
        } else {
            $scope.cart[p.Id] = p;
            $scope.cart[p.Id].Count = 1;
        }
        localStorageService.set('cart', $scope.cart);
        e.preventDefault();
        e.stopPropagation();
    }

    $scope.convertUTCDateToLocalDate = function(date) {
        var newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);
        var offset = date.getTimezoneOffset() / 60;
        var hours = date.getHours();
        newDate.setHours(hours + offset);
        return newDate;
    }

    $scope.formatDate = function (dt, utc) {
        return adminService.formatDate(dt, utc);
    }

    $scope.formatShortDate = function (dt) {
        if (!dt) {
            return '';
        }
        if (!dt['getDate']) {
            dt = new Date(Date.parse(dt));
        }
        var d = dt.getDate();
        var m = dt.getMonth() + 1;
        var y = dt.getFullYear();
        if (d < 10) {
            d = '0' + d;
        }
        if (m < 10) {
            m = '0' + m;
        }
        return d + '.' + m + '.' + y;
    }

    $scope.changeLanguage = function (langId) {
        $scope.langId = langId;
        localStorageService.set('langId', langId);
        translationService.changeLocale(langId)
            .then(function(response) {
                    var langId = localStorageService.get('langId') || 0;
                    setTimeout(function() {
                        window.location.reload();
                }, 100);
            }, function () {
                debugger;
            });
    }

    $scope.logout = function () {
        var request = {
            method: "post",
            url: "/api/Account/Logout",
            headers: {
                "Content-Type": "application/json",
                'Authorization': 'Bearer ' + $scope.token
            }
        };
        return $http(request).then(function (response) {
            $rootScope.$broadcast('cleanToken');
            $rootScope.$broadcast('checkLogon');
            $timeout(function () {
                $state.go("index");
            }, 50);
        }, function (err) {
            $state.go("index");
        });
    }

    $scope.translate = function (code) {
        return translationService.translate(code);
    }

    $scope.$on('authItem', function (o, data) {
        $scope.auth = data;
        localStorageService.set('auth', $scope.auth);
    });

    $scope.$on('updateLoginMsg', function (o, msg) {
        $scope.loginMsg = msg;
    });

    $scope.$on('updateErrorMsg', function (o, msg) {
        $scope.errorMsg = msg;
    });

    $scope.$on('checkLogon', function () {
        $scope.checkLogon();
    });

    $scope.$on('checkToken', function () {
        var oldToken = $scope.token;
        $scope.token = localStorageService.get('token');
        $scope.auth = localStorageService.get('auth');
        if (!$scope.auth && $scope.loggedIn) {
            $scope.logout();
        }
    });

    $scope.$on('cleanToken', function () {
        $scope.auth = undefined;
        $scope.token = undefined;
        localStorageService.remove('auth');
        localStorageService.remove('token');
        $rootScope.admin = false;
    });

    $scope.registerLocalStorageVar = function (name, cb) {
        if (localStorageService.get(name)) {
            $scope[name] = localStorageService.get(name);
        } else {
            localStorageService.set(name, $scope[name]);
            localStorageService.bind($scope, name);
        }
        if (cb) {
            cb();
        }
    }

    $scope.registerLocalStorageVar('token');
    $scope.registerLocalStorageVar('auth');

    $scope.scrollTopFn = function (event, toState, toParams, fromState, fromParams) {
        if (isMobile.any()) {
            $(window).scrollTop(0);
        } else {
            $(function () {
                $(window).scrollTop(0);
                if (window.globalScrollTopTop) {
                    window.globalScrollReset = true;
                    window.globalScrollTopTop();
                }
                if (!$scope.scrollInitialized) {
                    $('.scrollbar').remove();
                    $scope.scrollInitialized = true;
                    window.dzsscr_init($('body'), {
                        'settings_multiplier': ($.browser.mozilla ? 4 : ($.browser.ua.toLowerCase().indexOf('edge') !== -1 ? 1 : 2)),
                        'type': 'scrollTop',
                        'settings_refresh': 1000,
                        'settings_smoothing': 'on',
                        'settings_autoresizescrollbar': 'on',
                        'settings_skin': 'skin_apple',
                        'enable_easing': 'on',
                        cb: function () {
                            if (!isMobile.any()) {
                                onscrollx();
                            }
                        }
                    });
                }
            });
        }
        onscrollx(true);
    }

    $scope.checkLogon = function (cb) {
        var request = {
            method: "post",
            url: "/api/Account/CheckLogon",
            headers: {
                "Content-Type": "application/json",
                'Authorization': 'Bearer ' + $scope.token
            }
        };
        return $http(request).then(function (response) {
            // debugger;
            $scope.loggedIn = response.data;
            if ($scope.loggedIn) {
                $scope.loginMsg = '';
                $scope.errorMsg = '';
                adminService.ping()
                    .then(function (response) {
                        $rootScope.admin = response;
                    }, function () {
                        $rootScope.admin = false;
                    });
                if (cb) {
                    cb();
                }
            } else {
                $rootScope.admin = false;
                if (cb) {
                    cb();
                }
            }
        }, function (err) {
            debugger;
            $scope.loggedIn = false;
            $rootScope.admin = false;
            if (cb) {
                cb();
            }
        });
    }

    $scope.handleError = function (err) {
        $scope.errorMsg = '';
        $scope.loginMsg = '';
        var msgs = err.data ? err.data.ModelState : [err.statusText || ''];
        $scope.validation = {};
        for (var msg in msgs) {
            $.each(msgs[msg], function (i, m) {
                if (msg === '') {
                    if ($scope.errorMsg) {
                        $scope.errorMsg += '<br />';
                    }
                    $scope.errorMsg += m;
                } else {
                    var nm = msg.replace('model.', '').toLowerCase();
                    $scope.validation[nm] = $scope.validation[nm] || '';
                    if ($scope.validation[nm]) {
                        $scope.validation[nm] += '<br />';
                    }
                    $scope.validation[nm] += $scope.translate(m);
                }
            });
        }
    }

    $scope.crossDomainJsonp = function (event, toState, toParams, fromState, fromParams) {
        /*
        $.ajax({
            crossDomain: true,
            method: "GET",
            dataType: 'jsonp',
            url: apiUrl + "/api/values"
        })
        .error(function (msg) {
            var a = 0;
        })
        .done(function (msg) {
            var b = 1;
            b = 2;
        });
        */
    }

    $scope.clearWindowResizeEvents = function () {
        // $(window).off("resize");
        fixWrapperHeight();
        if (!$scope.onceResize) {
            $scope.onceResize = 1;
            $(window).bind("resize", function () {
                var w = getBodyWidth();
                $rootScope.small = getBodySmall(w);
                $rootScope.medium = getBodyMedium(w);
                if (!$rootScope.$$phase) {
                    $rootScope.$apply();
                }
            });
        }
    }

    $scope.checkRegistration = function (token, redirect) {
        loginAppFactory.checkRegistration(token)
            .then(function (response) {
                if (response.data.HasRegistered) {
                    $rootScope.$broadcast('authItem', response.data);
                    localStorageService.set('token', token);
                    $rootScope.$broadcast('checkLogon');
                    if (redirect) {
                        if ($scope.$$childTail && $scope.$$childTail.widgetId !== undefined) {
                            setTimeout(function () { $('div.g-recaptcha').hide(); }, 0);
                            var rct = 0;
                            var r = setInterval(function () {
                                if (rct >= 60 || $scope.$$childTail.widgetId >= 0) {
                                    clearInterval(r);
                                    $state.go("index");
                                } else {
                                    rct++;
                                }
                                $scope.$apply();
                            }, 500);
                        } else {
                            $state.go("index");
                        }
                    }
                } else {
                    loginAppFactory.signupExternal(token)
                        .then(function (response) {
                            $rootScope.$broadcast('authItem', response.data);
                            localStorageService.set('token', token);
                            $rootScope.$broadcast('checkLogon');
                            if (redirect) {
                                $state.go("index");
                            }
                        }, function (err) {
                            $scope.errorMsg = err.data.Message;
                            $scope.loginMsg = "";
                        });
                }
            }, function (err) {
                $scope.errorMsg = "Check Registration failed";
                $scope.loginMsg = "";
            });
    }

    $scope.checkLocationHash = function () {
        if (location.hash) {
            debugger;
            if (location.hash.indexOf('access_token=') !== -1 && location.hash.split('access_token=')) {
                $scope.token = location.hash.split('access_token=')[1].split('&')[0];
                if ($scope.token) {
                    $scope.checkRegistration($scope.token, true);
                }
            } else {
                $rootScope.$broadcast('checkToken');
            }
        }
    }

    $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        $rootScope.searchGlobalShadow = $.extend({}, $rootScope.searchGlobal);

        $('#ui-view').data('lh', null);
    });

    $scope.tabSelected = function () {
        $(window).trigger('resize');
        setTimeout(function () {
            $(window).trigger('resize');
            setTimeout(function () {
                $(window).trigger('resize');
                setTimeout(function () {
                    $(window).trigger('resize');
                }, 250);
            }, 150);
        }, 50);
    }

    $scope.stop = function(e) {
        e.stopPropagation();
        e.preventDefault();
    }

    $scope.initScroll = function () {
        $scope.scrollTopFn();
        if (isMobile.any()) {
            $('body').css({
                overflow: 'auto'
            });
        } else {
            $('body').css({
                overflow: 'hidden'
            });
            if (!$scope.initScrollOnce) {
                $scope.initScrollOnce = true;
                $(function () {
                    setTimeout(function () {
                        $('body').on('click', function () {
                            $scope.tabSelected();
                        });

                        $('body').on('keyup', function () {
                            $scope.tabSelected();
                        });

                        $('body').on('keydown', function () {
                            $scope.tabSelected();
                        });
                        $scope.tabSelected();
                    }, 0);
                });
            }
        }
    }



    $scope.$on('$viewContentLoaded', function () {
        $scope.initScroll();
    });

    $scope.$on('$stateChangeStart', function () {
        debugger;
        $scope.scrollTopFn();
        if ($('#mobile-collapse').hasClass('in')) {
            $('.mobile-menu button').click();
        }
    });

    $scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {

        $rootScope.currentState = $scope.currentState = toState ? toState.name : '';
        $scope.fromState = fromState ? fromState.name : '';
        $scope.clearWindowResizeEvents();
        $scope.checkLocationHash();
    });

    $scope.getUrl = function (url) {
        return url + '&_t=' + $rootScope._t;
    }

    $scope.$on('forceRandom', function () {
        $rootScope._t++;
    });

    $rootScope.$broadcast('forceRandom');

    $scope.stanimation = 'bounceIn';
    $scope.runIt = true;
    $scope.runAnimation = function() {
        $scope.runIt = false;
        $timeout(function () {
            $scope.runIt = true;
        }, 10);
    };

    $rootScope.$broadcast('checkLogon');

    $(window).trigger('resize');
}

angular.module('homer').factory('loginAppFactory', function (localStorageService, $q, $http) {
    var o = {};
    o.checkRegistration = function (token) {
        var request = {
            method: 'get',
            url: '/api/Account/GetUserInfo',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            }
        };
        return $http(request);
    }
    o.signupExternal = function (token) {
        var request = {
            method: 'post',
            url: '/api/Account/RegisterExternal',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + token
            },
            data: {}
        };
        return $http(request);
    }
    return o;
});

var isMobile = {
    Android: function () {
        return navigator.userAgent.toLowerCase().match(/android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.toLowerCase().match(/blackberry/i);
    },
    iOS: function () {
        return navigator.userAgent.toLowerCase().match(/iphone|ipad|ipod/i);
    },
    Opera: function () {
        return navigator.userAgent.toLowerCase().match(/opera mini/i);
    },
    Windows: function () {
        return navigator.userAgent.toLowerCase().match(/iemobile/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};