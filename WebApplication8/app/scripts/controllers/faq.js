﻿/**
 *
 * faqCtrl
 *
 */

angular
    .module('homer')
    .controller('faqCtrl', faqCtrl);

function faqCtrl($http, $scope, $timeout, $state, $rootScope, translationService, catalogService) {
    updateTitle($state, translationService);
    $rootScope.header = true;
    $rootScope.code = '';
    $rootScope.$broadcast('optionalLoggedIn');

    $scope.model = {
        faqs: []
    };

    $scope.loadingMore = true;

    catalogService.getFaqs()
        .then(function (faqs) {
            $scope.model.faqs = faqs;
            $scope.loadingMore = false;
            $timeout(function () {
                window.prerenderReady = true;
            }, 3000);
        }, function (err) {
            debugger;
            $scope.loadingMore = false;
            $timeout(function () {
                window.prerenderReady = true;
            }, 3000);
        });
}
