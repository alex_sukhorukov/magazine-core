﻿/**
 *
 * searchCtrl
 *
 */

angular
    .module('homer')
    .controller('searchCtrl', searchCtrl);

function searchCtrl($http, $scope, $timeout, $state, $stateParams, $rootScope, translationService, localStorageService, catalogService) {
    updateTitle($state, translationService);
    $rootScope.header = true;
    $rootScope.$broadcast('optionalLoggedIn');
    $rootScope.code = '';
    $scope.startLoading = true;
    $scope.pageSizes = [{
        value: 12,
        active: true
    }, {
        value: 24

    }, {
        value: 48
    }];

    $scope.pages = {
        currentPage: 1,
        totalCount: 1,
        visibleCount: 7
    };

    $scope.getPageSize = function () {
        var value = $scope.pageSizes[0].value;
        $.each($scope.pageSizes, function (i, s) {
            if (s.active) {
                value = s.value;
                return false;
            }
        });
        return value;
    };

    $scope.getOpt = function () {
        var o = $rootScope.searchGlobal || {};
        o.pageSize = $scope.getPageSize();
        if (o._page) {
            $scope.pages.currentPage = $rootScope.currentPage || o._page;
            o._page = 0;
        }
        o.page = $scope.pages.currentPage;
        var langId = localStorageService.get('langId') || 0;
        o.langId = langId;
        o.all = false;
        $.each($scope.categories, function (i, c) {
            if (c.Code === $scope.code) {
                o.categoryIds = [c.Id];
                return false;
            }
        });
        return o;
    };

    $scope.updateTotalPages = function (cb) {
        var opt = $scope.getOpt();
        catalogService.receiveTotalPages(opt)
            .then(function (totalCount) {
                $scope.pages.totalCount = parseInt(totalCount);
                if (cb) {
                    cb();
                }
            }, function (err) {
                debugger;
            });
    };

    $scope.changePageSize = function (size) {
        $.each($scope.pageSizes, function (i, s) {
            s.active = false;
        });
        size.active = true;
        $scope.pages.currentPage = 1;
        if ($scope.pages.totalCount < $scope.pages.currentPage) {
            $scope.pages.currentPage = $scope.pages.totalCount;
        }
        $scope.load($scope.pages.currentPage, true);
    };

    $scope.goToPage = function (page) {
        if (page === 'first') {
            $scope.pages.currentPage = 1;
        } else if (page === 'last') {
            $scope.pages.currentPage = $scope.pages.totalCount;
        } else {
            if (page.active) {
                return;
            }
            $scope.pages.currentPage = page.index;
            $.each($scope.pages.items, function (i, t) {
                if (t.index === page.index) {
                    t.active = true;
                } else if (t.active) {
                    t.active = false;
                }
            });
        }
        var pageSize = $scope.getPageSize();
        $rootScope.currentPage = $scope.pages.currentPage;
        $scope.load($scope.pages.currentPage, true);
    };

    $scope.load = function (page, forceScrollToTop) {
        var opt = $scope.getOpt();
        opt.page = page;
        $scope.products = [];
        $scope.startLoading = true;
        if (forceScrollToTop) {
            $scope.scrollTopFn();
        }
        $timeout(function () {
            catalogService.product.search(opt).then(function (response) {
                $scope.startLoading = false;
                $scope.pages.totalCount = parseInt(response.total);
                var _products = response.data;
                $scope.fillProductParams(_products);
                $scope.products = _products;
                $timeout(function () {
                    window.prerenderReady = true;
                }, 3000);
            }, function (err) {
                $scope.startLoading = false;
                debugger;
                $timeout(function () {
                    window.prerenderReady = true;
                }, 3000);
            });
        });
    };

    $scope.clearFilter = function () {
        $rootScope.currentPage = $scope.pages.currentPage = 1;
        $scope.load(1, true);
    };

    $scope.applySearchFilter = function () {
        $rootScope.currentPage = $scope.pages.currentPage = 1;
        $scope.load(1, true);
    };

    $scope.sort = function (by) {
        $rootScope.sortBy = by;
        localStorageService.set('sortBy', by);
        $scope.applySearchFilter();
    };

    $rootScope.currentPage = $rootScope.currentPage || $scope.pages.currentPage;
    $scope.pages.currentPage = $rootScope.currentPage;

    $timeout(function () {
        $scope.load($rootScope.currentPage, true);
    });
}
