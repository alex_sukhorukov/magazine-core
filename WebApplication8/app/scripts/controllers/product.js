﻿/**
    *
    * productCtrl
    *
    */

angular
    .module('homer')
    .controller('productCtrl', productCtrl);

function productCtrl($http, $scope, $timeout, $state, $rootScope, translationService, catalogService) {
    $rootScope.$broadcast('optionalLoggedIn');
    $rootScope.header = true;
    $state.current.data.pageDesc = '-';

    $scope.onWindowResize = function () {
        var w = $('.tab-content').width();
        w = $('#preview').width();
        var h = w / 1.5;
        $("#gallery").height(h);
        if (w > 800) {
            $("#mirror").height(h);
            $("#mirror").prev().show();
            $("#mirror").show();
            $('#buffer').hide();
        } else {
            $("#mirror").prev().hide();
            $("#mirror").hide();
            $('#buffer').show();
        }
        $(".swipebox-gallery").arrangeImages({
            width: w,
            height: h
        });
    };

    $scope.startGallery = function () {
        $('.swipebox-gallery a:first-child img').click();
        $timeout(function () {
            $(window).trigger('resize');
        }, 150);
    }

    $(function () {
        $(window).off('resize', $scope.onWindowResize);
        $(window).on('resize', $scope.onWindowResize);
        $('.gallery-start').click(function (e) {
            e.preventDefault();
            $('.swipebox-gallery a:first-child img').click();
        });
        $('.ct-img').click(function (e) {
            e.preventDefault();
            $('.swipebox-gallery a:nth-child(6) img').click();
        });
        $(".fancybox-button").fancybox({
            prevEffect: 'none',
            nextEffect: 'none',
            closeBtn: true,
            padding: 0,
            helpers: {
                media: true,
                overlay: {
                    locked: false
                }
            }
        });
    });

    $scope.id = $state.params.id;
    $scope.productId = $scope.id;
    $scope.comment.productId = $scope.id;

    $scope.imageTitle = function (index, product) {
        return index + ' ' + $scope.translate('Of') + ' ' + (product.Images.length - 1);
    }

    catalogService.getProduct($scope.id)
        .then(function (product) {
            $scope.product = product;
            updateTitle($state, $scope.product.Name);
            $rootScope.code = $scope.product.Code;
            $scope.productCode = $scope.product.ProductCode;
            $state.current.data.pageDesc = $scope.product.Description;
            $scope.getComments($scope.id, 1);
            $scope.fillProductParams($scope.product);
            $timeout(function () {
                $(window).trigger('resize');
            }, 10);
            $timeout(function () {
                window.prerenderReady = true;
            }, 3000);
        }, function (err) {
            debugger;
            $timeout(function () {
                window.prerenderReady = true;
            }, 3000);
        });
}

(function ($) {
    $.fn.arrangeImages = function (config) {
        var cfg = config || {};

        cfg.width = cfg.width || 100;
        cfg.height = cfg.height || 100;

        if (cfg.small) {
            cfg.width = cfg.small;
            cfg.height = cfg.small;
        }

        cfg.width2by3 = Math.floor(cfg.width * 2 / 3);
        cfg.width1by2 = Math.floor(cfg.width * 1 / 2);
        cfg.width1by3 = cfg.width - cfg.width2by3;
        cfg.height1by2 = Math.floor(cfg.height * 1 / 2);
        cfg.height2by3 = Math.floor(cfg.height * 2 / 3);
        cfg.height1by3 = cfg.height - cfg.height2by3;

        return this.each(function () {
            var me = $(this);

            if (me.data('pw') === cfg.width && me.data('ph') === cfg.height) {
                return true;
            }

            $('.ct-img').css({
                width: 0,
                height: 0
            });

            var images = me.find('img').not('img.thumbnail');
            images.css({ opacity: 0 });
            var count = images.length;
            if (count === 0) {
                return;
            }

            var rank = count > 6 ? 6 : count;

            var cssProp = {
                width: cfg.width,
                height: cfg.height
            };

            me.css(cssProp);

            me.data('pw', cssProp.width);
            me.data('ph', cssProp.height);

            var smartSizeFn = function (image, iw, ih, forceHeightAlign) {
                var doResize = function (predefinedWidth, predefinedHeight) {
                    var w = parseInt(image.attr('data-w')) || image.width() || predefinedWidth;
                    var h = parseInt(image.attr('data-h')) || image.height() || predefinedHeight;
                    var k1 = w / h;
                    var k2 = iw / ih;
                    var el = image.parent();
                    if (el.hasClass("swipebox-video") && el.find('.play-image').length === 0) {
                        el.append('<div class="play-image"></div>');
                    }
                    var playImage = el.find('.play-image');
                    playImage.css({
                        left: (iw - 62) / 2,
                        top: (ih - 62) / 2
                    });
                    if (false) {
                        image.css({
                            height: 'auto',
                            width: iw
                        });
                        var mt = Math.floor((ih - (Math.floor(image.height()) || Math.floor(predefinedHeight * iw / predefinedWidth))) / 2);
                        image.css({
                            'margin-top': mt,
                            'margin-left': '0.5px'
                        });
                    } else {
                        image.css({
                            width: 'auto',
                            height: ih
                        });
                        var ml = Math.floor((iw - (Math.floor(image.width()) || Math.floor(predefinedWidth * ih / predefinedHeight))) / 2);
                        ml = ml - 2; // because of left-border
                        image.css({
                            'margin-left': ml,
                            'margin-top': '0.5px'
                        });
                    }
                    if (image.width() > 0) {
                        image.attr('data-w', image.width());
                        image.attr('data-h', image.height());
                    }
                };
                predefinedWidth = image.width() || parseInt(image.attr('data-w')) || parseInt(image.attr('width'));
                predefinedHeight = image.height() || parseInt(image.attr('data-h')) || parseInt(image.attr('height'));
                if (image.width() > 40 || predefinedWidth > 40) {
                    doResize(predefinedWidth, predefinedHeight);
                    image.css({ opacity: 1 });
                    $(window).trigger('resize');
                }
                else {
                    image.on('load', function () {
                        image.loaded = true;
                        image.css({ opacity: 1 });
                        $(window).trigger('resize');
                    });
                    if (!image.loaded) {
                        image.load(doResize);
                    }
                }
            }
            me.ready(function () {
                var ct = images.length;
                $.each(images, function (i, img) {
                    var image = $(img);
                    var el = image.parent();
                    switch (rank) {
                        case 1:
                            el.css({
                                width: cfg.width,
                                height: cfg.height,
                                left: 0,
                                top: 0
                            });
                            smartSizeFn(image, cfg.width, cfg.height);
                            break;
                        case 2:
                            if (i === 0) {
                                el.css({
                                    width: cfg.width1by2,
                                    height: cfg.height,
                                    left: 0,
                                    top: 0
                                });
                            } else {
                                el.css({
                                    width: cfg.width1by2,
                                    height: cfg.height,
                                    left: cfg.width1by2,
                                    top: 0
                                });
                            }
                            smartSizeFn(image, cfg.width1by2, cfg.height);
                            break;
                        case 3:
                            if (i === 0) {
                                el.css({
                                    width: cfg.width2by3,
                                    height: cfg.height,
                                    left: 0,
                                    top: 0
                                });
                                smartSizeFn(image, cfg.width2by3, cfg.height);
                            } else if (i === 1) {
                                el.css({
                                    width: cfg.width1by3,
                                    height: cfg.height1by2,
                                    left: cfg.width2by3,
                                    top: 0
                                });
                                smartSizeFn(image, cfg.width1by3, cfg.height1by2);
                            } else {
                                el.css({
                                    width: cfg.width1by3,
                                    height: cfg.height1by2,
                                    left: cfg.width2by3,
                                    top: cfg.height1by2
                                });
                                smartSizeFn(image, cfg.width1by3, cfg.height1by2);
                            }
                            break;
                        case 4:
                            if (i === 0) {
                                el.css({
                                    width: cfg.width2by3,
                                    height: cfg.height,
                                    left: 0,
                                    top: 0
                                });
                                smartSizeFn(image, cfg.width2by3, cfg.height);
                            } else if (i === 1) {
                                el.css({
                                    width: cfg.width1by3,
                                    height: cfg.height1by3,
                                    left: cfg.width2by3,
                                    top: 0
                                });
                                smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                            } else if (i === 2) {
                                el.css({
                                    width: cfg.width1by3,
                                    height: cfg.height1by3,
                                    left: cfg.width2by3,
                                    top: cfg.height1by3
                                });
                                smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                            } else if (i === 3) {
                                el.css({
                                    width: cfg.width1by3,
                                    height: cfg.height1by3,
                                    left: cfg.width2by3,
                                    top: cfg.height2by3
                                });
                                smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                            }
                            break;
                        case 5:
                            if (i === 0) {
                                el.css({
                                    width: cfg.width2by3,
                                    height: cfg.height2by3,
                                    left: 0,
                                    top: 0
                                });
                                smartSizeFn(image, cfg.width2by3, cfg.height2by3);
                            } else if (i === 1) {
                                el.css({
                                    width: cfg.width1by3,
                                    height: cfg.height1by3,
                                    left: cfg.width2by3,
                                    top: 0
                                });
                                smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                            } else if (i === 2) {
                                el.css({
                                    width: cfg.width1by3,
                                    height: cfg.height1by3,
                                    left: cfg.width2by3,
                                    top: cfg.height1by3
                                });
                                smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                            } else if (i === 3) {
                                el.css({
                                    width: cfg.width1by2,
                                    height: cfg.height1by3,
                                    left: 0,
                                    top: cfg.height2by3
                                });
                                smartSizeFn(image, cfg.width1by2, cfg.height1by3);
                            } else if (i === 4) {
                                el.css({
                                    width: cfg.width1by2,
                                    height: cfg.height1by3,
                                    left: cfg.width1by2,
                                    top: cfg.height2by3
                                });
                                smartSizeFn(image, cfg.width1by2, cfg.height1by3);
                            }
                            break;
                        case 6:
                            if (i === 0) {
                                el.css({
                                    width: cfg.width2by3,
                                    height: cfg.height2by3,
                                    left: 0,
                                    top: 0
                                });
                                smartSizeFn(image, cfg.width2by3, cfg.height2by3);
                            } else if (i === 1) {
                                el.css({
                                    width: cfg.width1by3,
                                    height: cfg.height1by3,
                                    left: cfg.width2by3,
                                    top: 0
                                });
                                smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                            } else if (i === 2) {
                                el.css({
                                    width: cfg.width1by3,
                                    height: cfg.height1by3,
                                    left: cfg.width2by3,
                                    top: cfg.height1by3
                                });
                                smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                            } else if (i === 3) {
                                el.css({
                                    width: cfg.width1by3,
                                    height: cfg.height1by3,
                                    left: 0,
                                    top: cfg.height2by3
                                });
                                smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                            } else if (i === 4) {
                                el.css({
                                    width: cfg.width1by3,
                                    height: cfg.height1by3,
                                    left: cfg.width1by3,
                                    top: cfg.height2by3
                                });
                                smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                            } else if (i >= 5) {
                                el.css({
                                    width: cfg.width1by3,
                                    height: cfg.height1by3,
                                    left: cfg.width2by3,
                                    top: cfg.height2by3
                                });
                                if (i >= 6) {
                                    el.css({
                                        'z-index': -2,
                                        'opacity': 0
                                    });
                                }
                                smartSizeFn(image, cfg.width1by3, cfg.height1by3);
                                if (ct >= 6) {
                                    if (config.opaque !== 0) {
                                        var p = el.position();
                                        var s = {
                                            width: el.width(),
                                            height: el.height()
                                        };
                                        $('.ct-img').css({
                                            left: p.left,
                                            top: p.top,
                                            width: s.width,
                                            height: s.height
                                        });
                                        $('.ct-img-val').text('+' + (ct - 5));
                                    }
                                }
                            }
                            break;
                    }
                    var cls = 'place-' + rank + '-' + i;
                    if (!el.hasClass(cls)) {
                        el.addClass(cls);
                    }
                });
            });
        });
    };
})(jQuery);