﻿/**
 *
 * catalogCtrl
 *
 */

angular
    .module('homer')
    .controller('catalogCtrl', catalogCtrl);

function catalogCtrl($http, $scope, $timeout, $state, $rootScope, catalogService, translationService, localStorageService) {

    var scope = $scope;
    $rootScope.header = true;
    updateTitle($state, translationService);
    $rootScope.$broadcast('optionalLoggedIn');
    $scope.slides = [];
    $scope.startLoading = true;

    // if user clicked back
    // if ($scope.code === $state.params.code)

    if ($rootScope.prevCode && $rootScope.prevCode !== $state.params.code) {
        // came from different category code
    }

    $scope.code = $state.params.code;
    $rootScope.code = $state.params.code;
    $scope.cseries = $state.params.series;
    $rootScope.cseries = $state.params.series;

    if ($scope.cseries) {
        $rootScope.latestSearch = $rootScope.latestSearch || { text: '' };
        var o = $rootScope.latestSearch;
        o.seriesIds = [];
        $rootScope._searchCfg = $rootScope._searchCfg || { categoryIds: [], brandIds: [] };
        $rootScope._searchCfg.seriesIds = [];
    } else {
        $rootScope.latestSearch = $rootScope.latestSearch || undefined;
        $rootScope._searchCfg = $rootScope._searchCfg || undefined;
    }

    if ($rootScope.latestSearch && $rootScope.latestSearch.seriesIds) {
        var seriesIds = $rootScope.latestSearch.seriesIds;
        var _seriesIds = $rootScope._searchCfg.seriesIds;
        var brandIds = $rootScope.latestSearch.brandIds;
        var _brandIds = $rootScope._searchCfg.brandIds;
        var categoryIds = $rootScope.latestSearch.categoryIds;
        var _categoryIds = $rootScope._searchCfg.categoryIds;
        var removeSeriesIds = [], removeCategoryIds = [], removeBrandIds = [];

        $.each(seriesIds, function (i, s) {
            if (!($rootScope.prevCode && $rootScope.prevCode !== $state.params.code)) {
                var index = _seriesIds.indexOf(s);
                if (index === -1) {
                    _seriesIds.push(s);
                }
            } else {
                removeSeriesIds.push(i);
            }
        });
        $.each(removeSeriesIds, function (i, r) {
            seriesIds.splice(r, 1);
        });

        $.each(categoryIds, function (i, s) {
            if (!($rootScope.prevCode && $rootScope.prevCode !== $state.params.code)) {
                var index = _categoryIds.indexOf(s);
                if (index === -1) {
                    _categoryIds.push(s);
                }
            } else {
                removeCategoryIds.push(i);
            }
        });
        $.each(removeCategoryIds, function (i, r) {
            categoryIds.splice(r, 1);
        });

        $.each(brandIds, function (i, s) {
            if (!($rootScope.prevCode && $rootScope.prevCode !== $state.params.code)) {
                var index = _brandIds.indexOf(s);
                if (index === -1) {
                    _brandIds.push(s);
                }
            } else {
                removeBrandIds.push(i);
            }
        });
        $.each(removeBrandIds, function (i, r) {
            brandIds.splice(r, 1);
        });

        if (seriesIds && $scope.cseries && seriesIds.indexOf($scope.cseries) === -1) {
            seriesIds.push($scope.cseries);
        }
    }

    $rootScope.prevCode = $state.params.code;

    if ($rootScope.latestSearch) {
        $rootScope.search = $rootScope.searchCfg = jQuery.extend(true, {}, $rootScope.latestSearch);
    } else {
        $rootScope.search = $rootScope.searchCfg = { text: '' };
    }


    $timeout(function () {
        $scope.load(1, true);
    }, 100);

    $scope.showfilters = false;

    if (!$scope.code) {
        $state.go("index");
    }

    $scope.pageSizes = [{
        value: 12,
        active: true
    }, {
        value: 24

    }, {
        value: 48
    }];

    $scope.pages = {
        currentPage:  1,
        totalCount:   1,
        visibleCount: 7
    };

    $scope.getPageSize = function () {
        var value = $scope.pageSizes[0].value;
        $.each($scope.pageSizes, function (i, s) {
            if (s.active) {
                value = s.value;
                return false;
            }
        });
        return value;
    }

    var categoryLoaded = $scope.$on('categoryLoaded', function () {
        categoryLoaded();
        if (scope.slides.length === 0) {
            return;
        }
        $timeout(function () {
            scope.initSlider();
        }, 10);
    });

    catalogService.getSlides($scope.code).then(function (response) {
        scope.slides = Array.isArray(response) ? response : JSON.parse(response.data);
        $scope.$broadcast('categoryLoaded');
    }, function (err) { });

    $scope.getOpt = function () {
        var o = $rootScope.latestSearch || {};
        o.pageSize = $scope.getPageSize();
        if (o._page) {
            $scope.pages.currentPage = o._page;
            o._page = 0;
        }
        o.page = $scope.pages.currentPage;
        var langId = localStorageService.get('langId') || 0;
        o.langId = langId;
        o.all = false;
        $.each($scope.categories, function (i, c) {
            if (c.Code === $scope.code) {
                o.categoryIds = [c.Id];
                return false;
            }
        });
        return o;
    }

    $scope.updateTotalPages = function (cb) {
        var opt = $scope.getOpt();
        catalogService.receiveTotalPages(opt)
            .then(function (totalCount) {
                $scope.pages.totalCount = parseInt(totalCount);
                if (cb) {
                    cb();
                }
            }, function (err) {
                debugger;
            });
    }

    $scope.load = function (page, forceScrollToTop) {
        var opt = $scope.getOpt();
        $scope.products = [];
        $scope.startLoading = true;
        if (forceScrollToTop) {
            $scope.scrollTopFn();
        }
        $timeout(function () {
            catalogService.product.search(opt).then(function (response) {
                $scope.pages.totalCount = parseInt(response.total);
                $scope.startLoading = false;
                var _products = response.data;
                $scope.fillProductParams(_products);
                $scope.products = _products;
                $timeout(function () {
                    scope.tabSelected();
                });
                $timeout(function () {
                    window.prerenderReady = true;
                }, 3000);
            }, function (err) {
                debugger;
                $scope.startLoading = false;
                $timeout(function () {
                    window.prerenderReady = true;
                }, 3000);
            });
        });
    }

    $scope.$on('applyCustom', function () {
        $rootScope.search = jQuery.extend(true, {}, $rootScope._searchCfg);
        $rootScope.latestSearch = jQuery.extend(true, {}, $rootScope.search);
        $scope.applyFilter();
    });

    $scope.applyFilter = function () {
        $scope.load(1, true);
    }

    $scope.sort = function (by) {
        $rootScope.sortBy = by;
        localStorageService.set('sortBy', by);
        $scope.applyFilter();
    }

    $scope.$on('search', function (scope, searchCfg) {
        $rootScope._searchCfg = searchCfg;
        var opt = $scope.getOpt();
        var ss = JSON.stringify(opt);
        if ($scope.pss !== ss) {
            $scope.pss = ss;
            $scope.popt = opt;
        }

    });

    $scope.clearFilter = function () {
        $scope.load(1, true);
    }

    $scope.changePageSize = function (size) {
        $.each($scope.pageSizes, function (i, s) {
            s.active = false;
        });
        size.active = true;
        $scope.pages.currentPage = 1;
        if ($scope.pages.totalCount < $scope.pages.currentPage) {
            $scope.pages.currentPage = $scope.pages.totalCount;
        }
        $scope.load($scope.pages.currentPage, true);
    }

    $scope.showItem = function (p, prefix, cb) {
        var el = $('#' + prefix + p.Id);
        el.css({
            display: 'inline-block'
        });
        if (cb) {
            cb();
        }
        $scope.tabSelected();
    }

    $scope.goToPage = function (page) {
        if (page === 'first') {
            $scope.pages.currentPage = 1;
        } else if (page === 'last') {
            $scope.pages.currentPage = $scope.pages.totalCount;
        } else {
            if (page.active) {
                return;
            }
            $scope.pages.currentPage = page.index;
            $.each($scope.pages.items, function (i, t) {
                if (t.index === page.index) {
                    t.active = true;
                } else if (t.active) {
                    t.active = false;
                }
            });
        }
        var pageSize = $scope.getPageSize();
        $scope.load($scope.pages.currentPage, true);
    }

    $scope.initSlider = function () {
        jQuery(document).ready(function ($) {

            var jssor_1_SlideshowTransitions = [
                { $Duration: 1200, x: -0.3, $During: { $Left: [0.3, 0.7] }, $Easing: { $Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 },
                { $Duration: 1200, x: 0.3, $SlideOut: true, $Easing: { $Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear }, $Opacity: 2 }
            ];

            var jssor_1_options = {
                $AutoPlay: true,
                $SlideshowOptions: {
                    $Class: $JssorSlideshowRunner$,
                    $Transitions: jssor_1_SlideshowTransitions,
                    $TransitionsOrder: 1
                },
                $ArrowNavigatorOptions: {
                    $Class: $JssorArrowNavigator$
                },
                $BulletNavigatorOptions: {
                    $Class: $JssorBulletNavigator$
                },
                $ThumbnailNavigatorOptions: {
                    $Class: $JssorThumbnailNavigator$,
                    $Cols: 1,
                    $Align: 0,
                    $NoDrag: true
                }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            function ScaleSlider() {
                var refSize = $('.content').outerWidth();
                var w = $('#wrapper .normalheader .panel-body').outerWidth();
                if (w !== jssor_1_slider.prevWidth) {
                    jssor_1_slider.$ScaleWidth(w);
                    jssor_1_slider.prevWidth = w;
                }
            }
            $(function () {
                ScaleSlider();
            });
            $(window).on("resize", ScaleSlider);
            $(window).on("load", ScaleSlider);
            $(window).on("orientationchange", ScaleSlider);
        });
    }
}
