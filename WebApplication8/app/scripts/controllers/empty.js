﻿/**
 *
 * emptyCtrl
 *
 */

angular
    .module('homer')
    .controller('emptyCtrl', emptyCtrl);

function emptyCtrl($http, $scope, $timeout, $state, $rootScope, translationService) {
    updateTitle($state, translationService);
    $rootScope.header = true;
    $rootScope.code = '';
    $rootScope.$broadcast('optionalLoggedIn');
    $timeout(function () {
        window.prerenderReady = true;
    }, 3000);
}
