﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class ProductPrice
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        [Required]
        public virtual Product Product { get; set; }

        [Required]
        public DateTime PopulationDate { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        [DefaultValue(0)]
        public decimal Price { get; set; }
    }
}