﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class SlideImage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public byte[] Data { get; set; }

        public DateTime PopulationDate { get; set; }

        [Required]
        public virtual Slide Slide { get; set; }

        [Required]
        public Guid SlideForeignKey { get; set; }
    }

    public class Slide
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public virtual Category Category { get; set; }

        [DefaultValue(0)]
        public int Position { get; set; }

        public string Url { get; set; }

        public virtual SlideImage Image { get; set; }
    }
}