﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class CategoryImage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public byte[] Data { get; set; }

        public DateTime PopulationDate { get; set; }

        [Required]
        public virtual Category Category { get; set; }

        [Required]
        public Guid CategoryForeignKey { get; set; }
    }

    public class Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public virtual LocalizableString Name { get; set; }

        public virtual LocalizableString Description { get; set; }

        public string Code { get; set; }

        [Required]
        [DefaultValue(true)]
        public bool Active { get; set; }

        public virtual CategoryImage Image { get; set; }
    }
}