﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class ProductImage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public byte[] Data { get; set; }

        [DefaultValue(false)]
        public bool IsPreview { get; set; }

        public DateTime PopulationDate { get; set; }

        [Required]
        public virtual Product Product { get; set; }
    }

    public class ProductVideo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public string VideoUrl { get; set; }

        [Required]
        public virtual Product Product { get; set; }
    }

    public class ProductRate
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public virtual User User { get; set; }

        [Required]
        public virtual Product Product { get; set; }

        public double Rate { get; set; }
    }

    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public virtual LocalizableString Name { get; set; }

        public virtual LocalizableString Description { get; set; }

        public virtual ICollection<Comment> Comments { get; set; } = new List<Comment>();

        [Required]
        public virtual Category Category { get; set; }

        public virtual Series Series { get; set; }

        public virtual Brand Brand { get; set; }

        public virtual ICollection<ProductPrice> ProductPrice { get; set; } = new List<ProductPrice>();

        public virtual ICollection<ProductImage> Images { get; set; } = new List<ProductImage>();

        public virtual ICollection<ProductVideo> Videos { get; set; } = new List<ProductVideo>();

        [Required]
        [DefaultValue(true)]
        public bool IsExists { get; set; }

        [Required]
        [DefaultValue(true)]
        public bool Active { get; set; }

        [Required]
        [DefaultValue(0)]
        public int Age { get; set; }

        [Required]
        [DefaultValue(0)]
        public int Pol { get; set; }

        [Required]
        [DefaultValue(0)]
        public int NumberOfDetails { get; set; }

        public string InternalProductCode { get; set; }

        public string Size { get; set; }

        public int WarrantyNumberOfMonth { get; set; }

        public Material Material { get; set; } = new Material();

        [Required]
        public string ProductCode { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime UpdatedAt { get; set; }

        public double Rate { get; set; } = 0;

        public int RateCount { get; set; } = 0;
    }
}