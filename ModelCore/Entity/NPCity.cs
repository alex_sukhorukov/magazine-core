﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class NPCity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Ref { get; set; }

        [Required]
        public string Description { get; set; }

        public string DescriptionRu { get; set; }

        public int Delivery1 { get; set; }

        public int Delivery2 { get; set; }

        public int Delivery3 { get; set; }

        public int Delivery4 { get; set; }

        public int Delivery5 { get; set; }

        public int Delivery6 { get; set; }

        public int Delivery7 { get; set; }

        public Guid Area { get; set; }

        public string CityId { get; set; }
    }
}