﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class Cart
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Number { get; set; }

        public virtual User User { get; set; }

        [Required]
        public DateTime PopulationDate { get; set; }

        public virtual ICollection<ProductSet> ProductSets { get; set; }

        public Cart()
        {
            ProductSets = new List<ProductSet>();
        }
    }
}