﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class User
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        [Required]
        [MaxLength(25)]
        public string Login { get; set; }

        [NotMapped]
        public string Password { get; set; }

        [Required]
        public string Email { get; set; }

        public int? EmailConfirmedFlag { get; set; }

        public string TempGuid { get; set; }

        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }

        public DateTime LastLoginAt { get; set; }

        public DateTime CreatedAt { get; set; }

        public string UserHostAddress { get; set; }

        public string UserAgent { get; set; }

        public string UserLanguages { get; set; }

        public string LastShipAddress { get; set; }

        public string FullName { get; set; }
    }
}