﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class ProductSet
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public virtual Product Product { get; set; }

        [Required]
        public virtual int Quantity { get; set; }

        public virtual Cart Cart { get; set; }

        public virtual Order Order { get; set; }

        [Required]
        [DefaultValue(0)]
        public decimal FixPrice { get; set; }

        [Required]
        [DefaultValue(0)]
        public decimal Discount { get; set; }
    }
}