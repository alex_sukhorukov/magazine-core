﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public string Number { get; set; }

        [Required]
        public virtual OrderState OrderState { get; set; }

        [Required]
        public virtual DeliveryMethod DeliveryMethod { get; set; }

        public virtual OrderAddress OrderAddress { get; set; }

        public virtual NPCity NPCity { get; set; }

        public virtual NPWarehouse NPWarehouse { get; set; }

        public virtual User User { get; set; }

        public virtual UserShadow UserShadow { get; set; }

        public string Phone { get; set; }

        public string AdditionalInfo { get; set; }

        [Required]
        public DateTime PopulationDate { get; set; }

        public virtual ICollection<ProductSet> ProductSets { get; set; }

        [Required]
        [DefaultValue(0)]
        public decimal TotalPrice { get; set; }

        public Order()
        {
            ProductSets = new List<ProductSet>();
        }
    }
}