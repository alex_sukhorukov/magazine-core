﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class UserExtraData
    {
        [Key]
        [MaxLength(128)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string ProviderKey { get; set; }

        public string PictureUrl { get; set; }

        public string Locale { get; set; }
    }
}