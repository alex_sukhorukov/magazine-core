﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class SeriesImage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public byte[] Data { get; set; }

        public DateTime PopulationDate { get; set; }

        [Required]
        public virtual Series Series { get; set; }

        [Required]
        public Guid SeriesForeignKey { get; set; }
    }

    public class Series
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        [Required]
        public string Code { get; set; }

        [Required]
        public virtual Brand Brand { get; set; }

        public virtual LocalizableString Name { get; set; }

        public virtual LocalizableString Description { get; set; }

        [Required]
        [DefaultValue(true)]
        public bool Active { get; set; }

        public virtual SeriesImage Image { get; set; }
    }
}