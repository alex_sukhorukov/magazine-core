﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class NPArea
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Ref { get; set; }

        [Required]
        public Guid AreasCenter { get; set; }

        [Required]
        public string Description { get; set; }
    }
}