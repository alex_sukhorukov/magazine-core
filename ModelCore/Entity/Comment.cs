﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class Comment
    {
        public Comment() {
            SubComments = new List<Comment>();
            IsSubComment = false;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public string Title { get; set; }

        public string Text { get; set; }

        public int Rate { get; set; }

        public int Likes { get; set; }

        public int Dislikes { get; set; }

        [Required]
        public virtual DateTime PopulationDate { get; set; }

        [Required]
        public virtual User User { get; set; }

        public virtual ICollection<Comment> SubComments { get; set; }

        public virtual Product Product { get; set; }

        public bool IsSubComment { get; set; }
    }
}