﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class NPWarehouse
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Ref { get; set; }

        [Required]
        public string Description { get; set; }

        public string DescriptionRu { get; set; }

        public string SiteKey { get; set; }

        public string Phone { get; set; }

        public Guid TypeOfWarehouse { get; set; }

        public int Number { get; set; }

        public Guid CityRef { get; set; }

        public string CityDescription { get; set; }

        public string CityDescriptionRu { get; set; }

        public int PostFinance { get; set; }

        public int BicycleParking { get; set; }

        public int POSTerminal { get; set; }

        public int InternationalShipping { get; set; }

        public int TotalMaxWeightAllowed { get; set; }

        public int PlaceMaxWeightAllowed { get; set; }
    }
}