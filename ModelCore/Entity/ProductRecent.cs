﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class ProductRecent
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        [Required]
        public virtual Product Product { get; set; }

        public virtual Product Recent { get; set; }

        [Required]
        public DateTime PopulationDate { get; set; }

        public int Index { get; set; }

        public bool IsReal { get; set; }
    }
}