﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DbModel
{
    public class LocalizableString
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public string Text1 { get; set; }

        public string Text2 { get; set; }

        public string Text3 { get; set; }

        public string Code { get; set; }

        [Required]
        [DefaultValue(false)]
        public bool IsSerializable { get; set; }

        [DefaultValue(true)]
        public bool IsSystem { get; set; }

        public LocalizableString()
        {
            IsSystem = true;
            IsSerializable = false;
        }
    }
}