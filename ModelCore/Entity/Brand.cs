﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.InteropServices;

namespace DbModel
{
    public class BrandImage
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        public byte[] Data { get; set; }

        public DateTime PopulationDate { get; set; }

        [Required]
        public virtual Brand Brand { get; set; }

        [Required]
        public Guid BrandForeignKey { get; set; }
    }

    public class Brand
    {
        public Brand()
        {
            Series = new List<Series>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual BrandImage Image { get; set; }

        public virtual ICollection<Series> Series { get; set; }
    }
}