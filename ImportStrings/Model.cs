﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using DbModel;
using Microsoft.EntityFrameworkCore;

namespace ImportStrings
{
    public class DataContext : DbContext
    {
        public DbSet<LocalizableString> LocalizableString { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<LocalizableString>().ToTable("LocalizableString");
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            var configuration = builder.Build();
            optionsBuilder.UseSqlServer(configuration.GetConnectionString("DefaultConnection"));
        }
    }
}
